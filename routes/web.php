<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', [
	'uses' => 'FrontController@index',
	'as' => 'index',
]);

Route::get('/nosotros', [
	'uses' => 'FrontController@nosotros',
	'as' => 'nosotros',
]);

Route::get('/cursos', [
	'uses' => 'FrontController@cursos',
	'as' => 'cursos',
]);

Route::get('/cursos/{slug}', [
	'uses' => 'FrontController@cursosDetalle',
	'as' => 'curso.detalle',
])->where('slug', '[\w\d\-\_]+');


Route::get('/blog', [
	'uses' => 'FrontController@blog',
	'as' => 'blog',
]);

Route::get('/blog/{slug}', [
	'uses' => 'FrontController@blogDetalle',
	'as' => 'blog.detalle',
])->where('slug', '[\w\d\-\_]+');


Route::get('/blog/archivo/{autor}', [
	'uses' => 'FrontController@autorBlogList',
	'as' => 'autor.blog.list',
])->where('slug', '[\w\d\-\_]+');


Route::get('/mensaje_del_director', [
	'uses' => 'FrontController@mensaje_director',
	'as' => 'mensaje_director',
]);

Route::get('/contacto', [
	'uses' => 'FrontController@contacto',
	'as' => 'contacto',
]);

Route::get('/eventos', [
	'uses' => 'FrontController@eventos',
	'as' => 'eventos',
]);

Route::get('/galerias', [
	'uses' => 'FrontController@galerias',
	'as' => 'galerias',
]);

Route::get('/galerias/{slug}', [
	'uses' => 'FrontController@galeriaDetalle',
	'as' => 'galeria.detalle',
])->where('slug', '[\w\d\-\_]+');


Route::get('/info/{slug}', [
	'uses' => 'FrontController@landingDetalle',
	'as' => 'landing.detalle',
])->where('slug', '[\w\d\-\_]+');

Route::get('/info/{slug}/gracias', [
	'uses' => 'FrontController@landingSuccessDetalle',
	'as' => 'landing.success',
])->where('slug', '[\w\d\-\_]+');


Route::get('/testimonios', [
	'uses' => 'FrontController@testimonios',
	'as' => 'testimonios',
]);

Route::get('/aviso_de_privacidad', [
	'uses' => 'FrontController@privacidad',
	'as' => 'privacidad',
]);

Route::get('/terminos_y_condiciones', [
	'uses' => 'FrontController@terminos',
	'as' => 'terminos',
]);

Route::get('/busqueda-general', [
    'uses' => 'FrontSearchController@query',
    'as' => 'search.query',
]);

Route::get('/info/{slug}/guardar_contacto', [
    'uses' => 'FrontController@storeContact',
    'as' => 'contact.store.landing',
]);

Route::get('/contact_form/guardar_contacto', [
    'uses' => 'FrontController@storeContactForm',
    'as' => 'contact.store.form',
]);
 
Route::get('/gracias', [
	'uses' => 'FrontController@formSuccess',
	'as' => 'form.success',
]);

Auth::routes();

Route::group(['middleware' => 'auth'], function(){
	Route::resource('/admin/usuarios', 'UserController');

	Route::get('/admin', [
		'uses' => 'BackController@index',
		'as' => 'home',
	]);

	Route::get('/admin/usuarios', [
		'uses' => 'AuthController@adminList',
		'as' => 'admin.index',
	]);

	Route::get('/admin/register-admin', [
		'uses' => 'AuthController@register',
		'as' => 'admin.register',
	]);

	Route::post('/admin/register-admin', [
		'uses' => 'AuthController@postRegister',
		'as' => 'admin.register',
	]);

	Route::delete('/admin/delete-admin/{id}', [
		'uses' => 'AuthController@destroy',
		'as' => 'admin.destroy',
	]);

	Route::get('/admin/perfil', [
		'uses' => 'BackController@userProfile',
		'as' => 'admin.perfil',
	]);

	Route::put('/admin/perfil/imagen/{id}', [
		'uses' => 'BackController@updateImage',
		'as' => 'admin.image.update',
	]);

	Route::put('/admin/perfil/{id}', [
		'uses' => 'BackController@updateProfile',
		'as' => 'admin.update',
	]);

	Route::resource('/admin/roles', 'RoleController');

	/*
	Route::post('/admin/registro-admin', [
		'uses' => 'UserController@postRegistro',
		'as' => 'admin.registro',
	]);
	*/

	/* -------------------------------------- */
	/* ------ BLOG, GALERIAS Y CURSOS ------- */
	/* --------------------------------.----- */

	Route::resource('/admin/blog', 'BlogController');
	Route::resource('/admin/categorias', 'CategoryController');
	Route::resource('/admin/etiquetas', 'TagController');
	Route::resource('/admin/cursos', 'CourseController');
	Route::resource('/admin/galerias', 'GalleryController');

	Route::delete('/admin/galerias/extra-destroy/{id}', [
		'uses' => 'GalleryController@softDestroy',
		'as' => 'extra.destroy',
	]);

	Route::get('/admin/galerias/dar-baja/{id}', [
		'uses' => 'GalleryController@darDeBaja',
		'as' => 'galerias.baja',
	]);

	Route::get('/admin/galerias/dar-alta/{id}', [
		'uses' => 'GalleryController@darDeAlta',
		'as' => 'galerias.alta',
	]);

	Route::post('/admin/galerias/nueva-imagen', [
		'uses' => 'GalleryController@storeImage',
		'as' => 'imagen.store',
	]);

	Route::post('/admin/galerias/nueva-imagen/{id}', [
		'uses' => 'GalleryController@editImage',
		'as' => 'imagen.edit',
	]);

	Route::resource('/admin/eventos', 'EventController');

	Route::get('/admin/eventos/destroy/{id}', [
		'uses' => 'EventController@softDestroy',
		'as' => 'eventos.hard.destroy',
	]);

	Route::resource('/admin/campanas', 'CampaignController');
	Route::resource('/admin/landing_pages', 'LandingPageController');
	Route::resource('/admin/contactos', 'ContactController');


	Route::get('/contactos_landing_exportacion/{landing}', [
		'uses' => '\App\Http\Controllers\CampaignController@export',
		'as' => 'contactos.export',
	]);

	Route::get('/contactos_exportacion', [
		'uses' => '\App\Http\Controllers\CampaignController@fullExport',
		'as' => 'contactos.full.export',
	]);

	//Route::resource('/admin/ventas/listados', 'CRMController');
	Route::resource('/admin/ventas/listados', 'SalesController');

	Route::get('/ventas_exportacion', [
		'uses' => 'SalesController@export',
		'as' => 'ventas.export',
	]);

	Route::post('/ventas_importacion', [
		'uses' => 'SalesController@import',
		'as' => 'ventas.import',
	]);

	/* VISTAS DE FILTRO CRM */

	Route::get('/admin/ventas/filtros/nuevos-registros', [
		'uses' => 'SalesController@filterNewRegistry',
		'as' => 'crm.filter_1'
	]);

	Route::get('/admin/ventas/filtros/prospectos', [
		'uses' => 'SalesController@filterProspects',
		'as' => 'crm.filter_2'
	]);

	Route::get('/admin/ventas/filtros/interesados', [
		'uses' => 'SalesController@filterInterested',
		'as' => 'crm.filter_3'
	]);

	Route::get('/admin/ventas/filtros/promesa-de-venta', [
		'uses' => 'SalesController@filterSalesPromise',
		'as' => 'crm.filter_4'
	]);

	Route::get('/admin/ventas/filtros/cierres-exitosos', [
		'uses' => 'SalesController@filterSalesComplete',
		'as' => 'crm.filter_5'
	]);

	Route::get('/admin/ventas/filtros/prospectos-detenidos', [
		'uses' => 'SalesController@filterActionRequired',
		'as' => 'crm.filter_6'
	]);

	Route::put('/admin/ventas/update-estado/{id}', [
		'uses' => 'SalesController@updateEstado',
		'as' => 'crm.updateEstado'
	]);
	
	Route::put('/admin/contactos/update-estado/{id}', [
		'uses' => 'SalesController@updateStatus',
		'as' => 'crm.updateStatus'
	]);

	/* Actualizaciones de Estado para Prospectos */

	Route::put('/admin/contactos/update-estado-global/{id}', [
		'uses' => 'SalesController@updateGlobalStatus',
		'as' => 'crm.updateGlobalStatus'
	]);

	Route::get('/admin/completar-registro/{id}', [
		'uses' => 'SalesController@completarRegistro',
		'as' => 'crm.completarRegistro'
	]);

	Route::get('/admin/completar-prospecto/{id}', [
		'uses' => 'SalesController@completarProspecto',
		'as' => 'crm.completarProspecto'
	]);

	Route::get('/admin/completar-interesado/{id}', [
		'uses' => 'SalesController@completarInteresado',
		'as' => 'crm.completarInteresado'
	]);

	Route::get('/admin/completar-promesa-de-venta/{id}', [
		'uses' => 'SalesController@completarPromesaDeVenta',
		'as' => 'crm.completarPromesaDeVenta'
	]);

	Route::get('/admin/prospecto-perdido/{id}', [
		'uses' => 'SalesController@prospectoPerdido',
		'as' => 'crm.prospectoPerdido'
	]);

	/* KPIS de Venta */

	Route::get('/admin/indicadores-ventas', [
		'uses' => 'SalesKpiController@indexKPI',
		'as' => 'ventas.kpi.index',
	]);

	Route::get('/admin/indicadores-ventas/{id}', [
		'uses' => 'SalesKpiController@showKPI',
		'as' => 'ventas.kpi.show',
	]);

	/* ------------------------- */
	/* ------------------------- */
	/* ------------------------- */
	
	Route::resource('/admin/actividades', 'ActivityController');

	Route::post('/admin/actividades/completar/{id}', [
		'uses' => 'ActivityController@completeActivity',
		'as' => 'actividades.completar'
	]);

	Route::get('/admin/actividades/prioridad/{id}', [
		'uses' => 'ActivityController@priorityActivity',
		'as' => 'actividades.prioridad'
	]);

	Route::post('/admin/actividades/{activity_id}/guardar_intento', [
		'uses' => 'ActivityController@storeTry',
		'as' => 'actividad.intento'
	]);

	Route::get('/admin/actividades/{activity_id}/guardar_intento/detalle', [
		'uses' => 'ActivityController@showTry',
		'as' => 'actividad.intento.mostrar'
	]);

	Route::resource('/admin/programas', 'ProgramController');
	Route::resource('/admin/metas-objetivos', 'GoalsController');
	Route::resource('/admin/pagos', 'PaymentController');

	/* ----------------------------- */
	/* ------ SEARCH QUERIES ------- */
	/* ----------------------------- */

	Route::get('/admin/busqueda/resultados', [
		'uses' => 'SearchController@contactsSearch',
		'as' => 'crm.search',
	]);

	/* ----------------------------- */
	/* ------ RELOJ CHECADOR ------- */
	/* ----------------------------- */

	Route::resource('/admin/reloj-checador', 'TimeClockController');

	Route::get('/admin/reloj-checador-historico', [
		'uses' => 'TimeClockController@history',
		'as' => 'reloj-checador.history'
	]);

});
