<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ActivityTry extends Model
{
	protected $table = 'sales_activities_tries';
    protected $primaryKey = 'id';

    public function activity()
    {
        return $this->belongsTo(\App\Activity::class, 'activity_id', 'id');
    }
}
