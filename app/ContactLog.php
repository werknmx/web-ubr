<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ContactLog extends Model
{
    protected $table = 'contacts_logs';
    protected $primaryKey = 'id';

    protected $fillable = [
        'contact_id', 'user_id', 'log', 'created_at', 'updated_at'
    ];

    public function contact()
    {
        return $this->belongsTo(\App\Contact::class, 'contacts_logs', 'contact_id', 'id');
    }

    public function user()
    {
        return $this->belongsTo(\App\User::class, 'contacts_logs', 'user_id', 'id');
    }
}
