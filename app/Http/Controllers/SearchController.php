<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use DB;
use Auth;
use Session;

/*Modelos Disponibles */
use App\Contact;

class SearchController extends Controller
{
    public function contactsSearch(Request $request)
    {	
    	$search_query = $request->input('search_query');
    	$search_order = $request->input('search_order');
    	$search_date = $request->input('search_date');

    	if ($search_query == NULL) {

    		if ($search_order == NULL || $search_order == 0) {

    			if ($search_date) {
	    			$resultados = Contact::where('is_active', '=', true)
                    ->where(DB::raw('created_at'), 'LIKE', "%{$search_date}%")->get();

		    		return view('back.crm.search.query')->with('contacts', $resultados);
	    		}

    		}else{
    			/* Opciones de Filtro */
    			if ($search_order == 1) {
    				$resultados = Contact::where('is_active', '=', true)
                    ->orderBy('created_at', 'asc')->get();

    			}elseif ($search_order == 2) {
    				$resultados = Contact::where('is_active', '=', true)
                    ->orderBy('created_at', 'desc')->get();
    			}

    			return view('back.crm.search.query')->with('contacts', $resultados);
    		}

    	}else{
            $resultados = Contact::where('is_active', '=', true)
            ->whereHas('origin', function ($query) use ($search_query) {
                $query->where('title', 'LIKE', '%' . strtolower($search_query) . '%');
            })->orWhere('company', 'LIKE', '%' . strtolower($search_query) . '%')
            ->orWhere('name', 'LIKE', '%' . strtolower($search_query) . '%')
            ->orWhere('sur_name', 'LIKE', '%' . strtolower($search_query) . '%')
            ->orWhere('email', 'LIKE', '%' . strtolower($search_query) . '%')
            ->get();

    		return view('back.crm.search.query')->with('contacts', $resultados);

    	}
          
        if ($search_query == NULL && $search_order == NULL && search_date == NULL) {
        	$resultados = NULL;

        	return view('back.crm.search.query')->with('contacts', $resultados);
        }
    }
}
