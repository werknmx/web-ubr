<?php

namespace App\Http\Controllers;

use Session;
use App\Program;

use Illuminate\Http\Request;

class ProgramController extends Controller
{
    public function index()
    {
        $programas = Program::orderBy('created_at', 'desc')->paginate(10);

        return view('back.programs.index')->with('programas', $programas);
    }

    public function create()
    {
        //
    }

    public function store(Request $request)
    {
        //Validar
        $this -> validate($request, array(
            'name' => 'required|max:255',
        ));

        // Guardar datos en la base de datos
        $programa = new Program;

        $programa->name = $request->name;
        $programa->slug = str_slug($request->name);
        $programa->description = $request->description;

        $programa -> save();

        // Mensaje de session
        Session::flash('exito', 'Tu programa se guardó correctamente en la base de datos.');

        // Enviar a vista
        return redirect()->back();
    }

    public function show(Program $program)
    {
        //
    }

    public function edit(Program $program)
    {
        //
    }

    public function update(Request $request, Program $program)
    {
        //
    }

    public function destroy(Program $program)
    {
        //
    }
}
