<?php

namespace App\Http\Controllers;

use Auth;
use Session;

use Image;
use Purifier;
use Carbon\Carbon;

use App\Course;
use App\Campaign;
use App\LandingPage;

use Illuminate\Http\Request;

class LandingPageController extends Controller
{
    public function index()
    {
        //
    }

    public function create()
    {
        //
    }

    public function store(Request $request)
    {
        //Validar
        $this -> validate($request, array(
            'title' => 'required|max:255',
        ));

        // Guardar datos en la base de datos
        $landing = new LandingPage;

        $landing->campaign_id = $request->campaign_id;
        $landing->title = $request->title;
        $landing->description = $request->description;
        $landing->slug = str_slug($request->title);

        $landing->place = $request->place;

        $landing->template_id = $request->template_id;
        $landing->courses_id = $request->courses_id;
        $landing->origin_name = $request->origin_name;

        $landing->btn_link = $request->btn_link;

        $landing->start_date =Carbon::parse($request->start_date)->format('Y-m-d');
        $landing->end_date = Carbon::parse($request->end_date)->format('Y-m-d');

        $landing->seo_title = $request->title;
        $landing->seo_description = $request->description;
        $landing->seo_keywords = $request->seo_keywords;
        $landing->seo_robots = $request->seo_robots;
        $landing->google_analytics_code = $request->google_analytics_code;
        $landing->google_ads_conversion_code = $request->google_ads_conversion_code;
        $landing->facebook_pixel_code = $request->facebook_pixel_code;

        $landing->save();

        // Mensaje de session
        Session::flash('exito', 'Tu Landing Page se guardó correctamente en la base de datos. Se creó la ruta directa y puede ser compartida ahora.');

        // Enviar a vista
        return redirect()->back();
    }

    public function show($id)
    {
        //
    }

    public function edit($id)
    {
        $landing = LandingPage::find($id);
        $courses = Course::all();

        return view('back.landing_pages.edit')->with('landing', $landing)->with('courses', $courses);
    }

    public function update(Request $request, $id)
    {
        //Validar
        $this -> validate($request, array(
            'title' => 'required|max:255',
        ));

        // Guardar datos en la base de datos
        $landing = LandingPage::find($id);

        $landing->campaign_id = $request->campaign_id;
        $landing->title = $request->title;
        $landing->description = $request->description;
        $landing->slug = str_slug($request->title);

        $landing->place = $request->place;

        $landing->template_id = $request->template_id;
        $landing->courses_id = $request->courses_id;
        $landing->origin_name = $request->origin_name;

        $landing->btn_link = $request->btn_link;

        $landing->start_date =Carbon::parse($request->start_date)->format('Y-m-d');
        $landing->end_date = Carbon::parse($request->end_date)->format('Y-m-d');

        $landing->seo_title = $request->title;
        $landing->seo_description = $request->description;
        $landing->seo_keywords = $request->seo_keywords;
        $landing->seo_robots = $request->seo_robots;
        $landing->google_analytics_code = $request->google_analytics_code;
        $landing->google_ads_conversion_code = $request->google_ads_conversion_code;
        $landing->facebook_pixel_code = $request->facebook_pixel_code;

        $landing->save();

        // Mensaje de session
        Session::flash('exito', 'Tu Landing Page se editó correctamente en la base de datos.');

        // Enviar a vista
        return redirect()->back();
    }

    public function destroy($id)
    {
        //
    }
}
