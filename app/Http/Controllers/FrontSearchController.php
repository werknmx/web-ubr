<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use DB;
use Auth;
use Session;

use App\Event;
use App\Blog;
use App\Course;
use App\Gallery;

class FrontSearchController extends Controller
{
    public function query(Request $request)
    {   
        $query = $request->input('query');

        $events = Event::where(DB::raw('name'), 'LIKE', "%{$query}%")
            ->orWhere('description', 'LIKE', "%{$query}%")
            ->orWhere('start_date', 'LIKE', "%{$query}%")
            ->orWhere('end_date', 'LIKE', "%{$query}%")
            ->get();

        $blogs = Blog::where(DB::raw('title'), 'LIKE', "%{$query}%")
            ->orWhere('summary', 'LIKE', "%{$query}%")
            ->get();

        $courses = Course::where(DB::raw('title'), 'LIKE', "%{$query}%")
            ->orWhere('description', 'LIKE', "%{$query}%")
            ->get();

        $galleries = Gallery::where(DB::raw('title'), 'LIKE', "%{$query}%")
            ->orWhere('body', 'LIKE', "%{$query}%")
            ->get();

        $results = $events->count() + $blogs->count() + $courses->count() + $galleries->count();

        return view('query')->with('events', $events)->with('blogs', $blogs)->with('courses', $courses)->with('galleries', $galleries)->with('results', $results);
    }
}
