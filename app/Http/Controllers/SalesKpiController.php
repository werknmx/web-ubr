<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Imports\ContactsImport;
use Maatwebsite\Excel\Facades\Excel;

use Carbon\Carbon;
use App\Contact;
use App\ContactInterest;
use App\Program;
use App\Course;
use App\Event;
use App\User;
use App\Payment;
use App\Goals;
use App\LandingPage;

use App\Activity;
use App\ActivityHandler;
use App\ActivityType;

use Auth;
use Session;

use App\Charts\InscritosChart;
use App\Charts\ActividadesChart;

class SalesKpiController extends Controller
{
    /* ------------------------- */
    /* ------------------------- */
    /* ----- VISTAS DE KPIS ---- */
    /* ------------------------- */
    /* ------------------------- */

    public function indexKPI()
    {
        $programs = Program::all();

        $users = User::role('sales_agent')->get();
        
        /* ------------------ */
        /* ------------------ */
        /* RANGOS Y OBJETIVOS */
        /* ------------------ */
        /* ------------------ */

        $venta_anual = Goals::where('name', 'Venta Total Anual')->firstOrFail();
        $cierres_mensuales = Goals::where('name', 'Cierres Mensuales')->firstOrFail();
        $promesa_venta = Goals::where('name', 'Promesa de Venta')->firstOrFail();

        $year_start = Carbon::now()->startOfYear();
        $year_end = Carbon::now()->endOfYear();

        $month_start = Carbon::now()->startOfMonth();
        $month_end = Carbon::now()->endOfMonth();

        $week_start = Carbon::now()->startOfWeek();
        $week_end = Carbon::now()->endOfWeek();

        $today = Carbon::now()->format('Y-m-d');

        $ventas_total = Contact::where('closing_date', '<=', $year_end)
        ->where('closing_date', '>=', $year_start)
        ->where('status', 'Cierre Exitoso')
        ->get();

        $ventas_mes = Contact::where('closing_date', '<=', $month_end)
        ->where('closing_date', '>=', $month_start)
        ->where('status', 'Cierre Exitoso')
        ->get();

        $ventas_semana = Contact::where('closing_date', '<=', $week_end)
        ->where('closing_date', '>=', $week_start)
        ->where('status', 'Cierre Exitoso')
        ->get();

        $ventas_hoy = Contact::where('closing_date', '<=', $today)
        ->where('closing_date', '>=', $today)
        ->where('status', 'Cierre Exitoso')
        ->get();

        $ven_total = 0;
        $ven_mes = 0;
        $ven_semana = 0;

        foreach ($ventas_total as $v_total) {
            foreach($v_total->interests as $v)
            {
               $ven_total += $v->normal_price;
            };
        };

        foreach ($ventas_mes as $v_month) {
            foreach($v_month->interests as $v)
            {
               $ven_mes += $v->normal_price;
            };
        };

        foreach ($ventas_semana as $v_week) {
            foreach($v_week->interests as $v)
            {
               $ven_semana += $v->normal_price;
            };
        };

        $ven_total;
        $ven_mes;
        $ven_semana;
        // TERMINA VENTAS

        // GRÁFICOS
        $ins_1 = Contact::where('status', 'Cierre Exitoso')->where('assigned_to', '5')->where('closing_date', '<=', $week_end)->where('closing_date', '>=', $week_start)->count();
        $ins_2 = Contact::where('status', 'Cierre Exitoso')->where('assigned_to', '6')->where('closing_date', '<=', $week_end)->where('closing_date', '>=', $week_start)->count();
        $ins_3 = Contact::where('status', 'Cierre Exitoso')->where('assigned_to', '7')->where('closing_date', '<=', $week_end)->where('closing_date', '>=', $week_start)->count();

        $chart = new InscritosChart;
        $chart->labels(['Ventas Juancho', 'Ventas Ramirez', 'Ventas Martinez']);
        $chart->dataset('Inscritos', 'bar', [$ins_1, $ins_2, $ins_3]);

        $act_1 = Activity::where('completed', true)->where('issuer_id', '5')->where('completed_at', '<=', $week_end)->where('completed_at', '>=', $week_start)->count();
        $act_2 = Activity::where('completed', true)->where('issuer_id', '6')->where('completed_at', '<=', $week_end)->where('completed_at', '>=', $week_start)->count();
        $act_3 = Activity::where('completed', true)->where('issuer_id', '7')->where('completed_at', '<=', $week_end)->where('completed_at', '>=', $week_start)->count();

        $chart2 = new ActividadesChart;
        $chart2->labels(['Ventas Juancho', 'Ventas Ramirez', 'Ventas Martinez']);
        $chart2->dataset('Actividades', 'bar', [$act_1, $act_2, $act_3]);

        return view('back.crm.kpi.index')->with('chart', $chart)
        ->with('chart2', $chart2)
        ->with('programs', $programs)
        ->with('users', $users)
        ->with('ven_total', $ven_total)
        ->with('ven_mes', $ven_mes)
        ->with('ven_semana', $ven_semana)
        ->with('ventas_semana', $ventas_semana)
        ->with('ventas_mes', $ventas_mes)
        ->with('ventas_hoy', $ventas_hoy)
        ->with('venta_anual', $venta_anual)
        ->with('cierres_mensuales', $cierres_mensuales)
        ->with('promesa_venta', $promesa_venta);
    }

    public function showKPI($id)
    {
        $program = Program::find($id);

        return view('back.crm.kpi.show')->with('program', $program);
    }
}
