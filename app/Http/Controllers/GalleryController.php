<?php

namespace App\Http\Controllers;

use Auth;
use Session;

use Image;
use Purifier;

use App\User;
use App\Gallery;
use App\GalleryImages;

use Illuminate\Http\Request;

class GalleryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $galerias_activas = Gallery::where('is_active', true)->orderBy('created_at', 'desc')->paginate(10);
        $galerias_inactivas = Gallery::where('is_active', false)->orderBy('created_at', 'desc')->paginate(10);

        return view('back.galleries.index')->with('galerias_activas', $galerias_activas)->with('galerias_inactivas', $galerias_inactivas);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('back.galleries.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // Guardar datos en la base de datos
        $galeria = new Gallery;

        $galeria->title = $request->title;
        $galeria->slug = str_slug($request->title);
        $galeria->body = $request->body;
        $galeria->is_active = true;

        if ($request->hasFile('cover_image')) {
            $image = $request->file('cover_image');
            $nombre_archivo = time() . '.' . $image->getClientOriginalExtension();
            $ubicacion = public_path('img/galerias/covers/' . $nombre_archivo);

            Image::make($image)->resize(1280,null, function($constraint){ $constraint->aspectRatio(); })->save($ubicacion);

            $galeria->cover_image = $nombre_archivo;
        }

        $galeria->save();

        // Mensaje de session
        Session::flash('exito', 'Tu galeria se guardó correctamente en la base de datos.');

        return redirect()->route('galerias.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Gallery  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $galeria = Gallery::find($id);

        return view('back.galleries.show')->with('galeria', $galeria);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Gallery  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $galeria = Gallery::find($id);

        return view('back.galleries.edit')->with('galeria', $galeria);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Gallery  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        // Guardar datos en la base de datos
        $galeria = Gallery::find($id);

        $galeria->title = $request->title;
        $galeria->slug = str_slug($request->title);
        $galeria->body = $request->body;
        $galeria->is_active = true;

        if ($request->hasFile('cover_image')) {
            $image = $request->file('cover_image');
            $nombre_archivo = time() . '.' . $image->getClientOriginalExtension();
            $ubicacion = public_path('img/galerias/covers/' . $nombre_archivo);

            Image::make($image)->resize(1280,null, function($constraint){ $constraint->aspectRatio(); })->save($ubicacion);

            $galeria->cover_image = $nombre_archivo;
        }

        $galeria->save();

        // Mensaje de session
        Session::flash('exito', 'Tu galeria se editó correctamente en la base de datos.');

        return redirect()->route('galerias.index');
    }

    public function destroy($id)
    {
        $galeria = Gallery::findOrFail($id);

        foreach($galeria->imagenes as $image){
            $extra = GalleryImages::find($image->id);
            unlink(public_path() .  '/img/galerias/extras/' . $extra->image);
            $extra->delete();
        }

        if($galeria->cover_image == NULL){

        }else{
            unlink(public_path() .  '/img/galerias/covers/' . $galeria->cover_image);
        }
       
        $galeria->delete();

        Session::flash('exito', 'La galería y sus imágenes internas ha sido borrado exitosamente.');

        return redirect()->route('galerias.index');
    }

    public function softDestroy($id)
    {
        $extra = GalleryImages::findOrFail($id);
        unlink(public_path() .  '/img/galerias/extras/' . $extra->image);
        $extra->delete();

        Session::flash('exito', 'Imágen dentro de galería eliminada correctamente.');

        return redirect()->back();
    }   

    public function storeImage(Request $request)
    {
        //Validar
        $this -> validate($request, array(
            'description' => 'nullable',
        ));

        // Guardar datos en la base de datos
        $var_imagen = new GalleryImages;

        $var_imagen->description = $request->description;
        $var_imagen->gallery_id = $request->gallery_id;

        // Esto se logra gracias a la libreria de imagen Intervention de Laravel
        if ($request->hasFile('image')) {
            $imagen = $request->file('image');
            $nombre_archivo = str_random(3) . '_galeriaitem' . '.' . $imagen->getClientOriginalExtension();
            $ubicacion = public_path('img/galerias/extras/' . $nombre_archivo);

            Image::make($imagen)->resize(1280,null, function($constraint){ $constraint->aspectRatio(); })->save($ubicacion);

            $var_imagen->image = $nombre_archivo;
        }

        $var_imagen->save();

        // Mensaje de session
        Session::flash('exito', 'Tu imagen se guardó y relacionó con su galería correctamente en la base de datos.');

        // Enviar a vista
        return redirect()->back();
    }

    public function darDeBaja($id)
    {
        $galeria = Gallery::find($id);
        $galeria->is_active = false;

        $galeria->save();

        // Mensaje de session
        Session::flash('exito', 'El registro de galería se dió de baja de la lista.');

        // Enviar a vista
        return redirect()->back();
    }

    public function darDeAlta($id)
    {
        $galeria = Gallery::find($id);
        $galeria->is_active = true;

        $galeria->save();

        // Mensaje de session
        Session::flash('exito', 'El registro de galería se dió nuevamente de alta en la lista.');

        // Enviar a vista
        return redirect()->back();
    }
}
