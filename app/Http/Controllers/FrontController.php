<?php

namespace App\Http\Controllers;

use App\Blog;
use App\Course;

use App\Gallery;
use App\User;
use App\Event;
use App\Contact;
use App\Activity;
use App\LandingPage;
use Carbon\Carbon;

use Illuminate\Http\Request;

class FrontController extends Controller
{
    public function index(){
        $publicaciones = Blog::orderBy('created_at', 'desc')->take(2)->get();
        $galerias = Gallery::where('is_active', true)->orderBy('created_at', 'desc')->take(12)->get();

    	return view('index')->with('publicaciones', $publicaciones)->with('galerias', $galerias);
    }

    public function nosotros(){
        $galerias = Gallery::where('is_active', true)->orderBy('created_at', 'desc')->take(12)->get();

    	return view('nosotros')->with('galerias', $galerias);
    }

    public function cursos(){
        $cursos = Course::orderBy('created_at', 'desc')->get();
    	return view('cursos')->with('cursos', $cursos);
    }

    public function cursosDetalle($slug){
        $curso = Course::where('slug', '=', $slug)->first();
        return view('cursos_detalle')->with('curso', $curso);
    }

    public function blog(){
        $publicaciones = Blog::orderBy('created_at', 'desc')->paginate(25);

    	return view('blog')->with('publicaciones', $publicaciones);
    }

    public function blogDetalle($slug){
        $publicacion = Blog::where('slug', '=', $slug)->first();
        $publicaciones = Blog::where('slug', '!=', $slug)->take(3);

        return view('blog_detalle')->with('publicacion', $publicacion)->with('publicaciones', $publicaciones);
    }

    public function autorBlogList($autor){
        $autor = User::find($autor)->get();

        /*
        $publicacion = Blog::where('slug', '=', $slug)->first();
        $publicaciones = Blog::where('slug', '!=', $slug)->get();
        */

        return view('blog_autor_list')->with('autor', $autor);
    }

    public function mensaje_director(){
    	return view('mensaje_director');
    }

    public function contacto(){
    	return view('contacto');
    }

    public function eventos(){
        $galerias = Gallery::where('is_active', true)->orderBy('created_at', 'desc')->take(12)->get();

        // EVENTOS POR MES
        $eventos_enero = Event::where('start_date', '<=', Carbon::now()->month(1)->endOfMonth())
        ->where('start_date', '>=', Carbon::now()->month(1)->startOfMonth())
        ->get();

        $eventos_febrero = Event::where('start_date', '<=', Carbon::now()->month(2)->endOfMonth())
        ->where('start_date', '>=', Carbon::now()->month(2)->startOfMonth())
        ->get();

        $eventos_marzo = Event::where('start_date', '<=', Carbon::now()->month(3)->endOfMonth())
        ->where('start_date', '>=', Carbon::now()->month(3)->startOfMonth())
        ->get();

        $eventos_abril = Event::where('start_date', '<=', Carbon::now()->month(4)->endOfMonth())
        ->where('start_date', '>=', Carbon::now()->month(4)->startOfMonth())
        ->get();

        $eventos_mayo = Event::where('start_date', '<=', Carbon::now()->month(5)->endOfMonth())
        ->where('start_date', '>=', Carbon::now()->month(5)->startOfMonth())
        ->get();

        $eventos_junio = Event::where('start_date', '<=', Carbon::now()->month(6)->endOfMonth())
        ->where('start_date', '>=', Carbon::now()->month(6)->startOfMonth())
        ->get();

        $eventos_julio = Event::where('start_date', '<=', Carbon::now()->month(7)->endOfMonth())
        ->where('start_date', '>=', Carbon::now()->month(7)->startOfMonth())
        ->get();

        $eventos_agosto = Event::where('start_date', '<=', Carbon::now()->month(8)->endOfMonth())
        ->where('start_date', '>=', Carbon::now()->month(8)->startOfMonth())
        ->get();

        $eventos_septiembre = Event::where('start_date', '<=', Carbon::now()->month(9)->endOfMonth())
        ->where('start_date', '>=', Carbon::now()->month(9)->startOfMonth())
        ->get();

        $eventos_octubre = Event::where('start_date', '<=', Carbon::now()->month(10)->endOfMonth())
        ->where('start_date', '>=', Carbon::now()->month(10)->startOfMonth())
        ->get();

        $eventos_noviembre = Event::where('start_date', '<=', Carbon::now()->month(11)->endOfMonth())
        ->where('start_date', '>=', Carbon::now()->month(11)->startOfMonth())
        ->get();

        $eventos_diciembre = Event::where('start_date', '<=', Carbon::now()->month(12)->endOfMonth())
        ->where('start_date', '>=', Carbon::now()->month(12)->startOfMonth())
        ->get();

        $all_events = Event::where('start_date', '<=', Carbon::now()->month(12)->endOfMonth())
        ->where('start_date', '>=', Carbon::now()->month(1)->startOfMonth())
        ->get();

    	return view('eventos')->with('galerias', $galerias)->with('eventos_enero', $eventos_enero)->with('eventos_febrero', $eventos_febrero)->with('eventos_marzo', $eventos_marzo)->with('eventos_abril', $eventos_abril)->with('eventos_mayo', $eventos_mayo)->with('eventos_junio', $eventos_junio)->with('eventos_julio', $eventos_julio)->with('eventos_agosto', $eventos_agosto)->with('eventos_septiembre', $eventos_septiembre)->with('eventos_octubre', $eventos_octubre)->with('eventos_noviembre', $eventos_noviembre)->with('eventos_diciembre', $eventos_diciembre)->with('all_events', $all_events);
    }

    public function galerias(){
        $galerias = Gallery::where('is_active', true)->orderBy('created_at', 'desc')->get();

    	return view('galerias')->with('galerias', $galerias);
    }

    public function galeriaDetalle($slug){
        $galeria = Gallery::where('slug', '=', $slug)->first();
        $galerias = Gallery::where('is_active', true)->where('slug', '!=', $slug)->take(3);

        return view('galeria_detalle')->with('galeria', $galeria)->with('galerias', $galerias);
    }

    public function landingDetalle($slug){
        $landing = LandingPage::where('slug', '=', $slug)->first();
        $cursos = Course::orderBy('created_at', 'desc')->get();

        $watched = $landing->view_count;
        $landing->view_count = $watched + 1;
        $landing->save();

        return view('landing_detalle')->with('landing', $landing)->with('cursos', $cursos);
    }

    public function landingSuccessDetalle($slug){
        $landing = LandingPage::where('slug', '=', $slug)->first();
        $cursos = Course::orderBy('created_at', 'desc')->get();

        return view('form_success')->with('landing', $landing)->with('cursos', $cursos);
    }

    public function storeContact(Request $request, $slug)
    {
        $landing = LandingPage::where('slug', $slug)->first();

        $contact = new Contact;

        $contact->name = $request->name;
        $contact->email = $request->email;
        $contact->phone_cel = $request->phone_cel;
        $contact->status = 'Registro';
        $contact->assigned_to = 1;

        $contact->landing_origin_id = $landing->id;
        $contact->save();

        /* Crear Primera actividad automáticamente */

        // Guardar datos en la base de datos
        $activity = new Activity;

        $activity->module_name = 'Creación Automática';
        $activity->issuer_id = $contact->assigned_to;
        $activity->activity_type_id = '1';
        $activity->activity_description = 'Tarea creada automáticamente.';
        $activity->activity_status = 'Pendiente';
        $activity->date_start = Carbon::now();
        $activity->date_end = Carbon::tomorrow();
        $activity->delivery_time = '15:00:00';
        $activity->contact_id = $contact->id;
        $activity->completed = false;
        $activity->is_urgent = false;

        $activity->save();

        $activity->handler()->sync($request->assigned_to, false);
        $activity->interests()->sync($request->interest_id, false);

        $watched = $landing->register_count;
        $landing->register_count = $watched + 1;
        $landing->save();


        return redirect()->route('landing.success', $landing->slug);
    }

    public function storeContactForm(Request $request)
    {
        $contact = new Contact;

        $contact->name = $request->name;
        $contact->sur_name = $request->sur_name;
        $contact->email = $request->email;
        $contact->phone_cel = $request->phone_cel;
        $contact->status = 'Registro';
        $contact->assigned_to = 1;

        $contact->landing_origin_id = NULL;
        $contact->global_origin = $request->global_origin;

        $contact->save();

        $contact->interests()->sync($request->interest_id, false);

        /* Crear Primera actividad automáticamente */

        // Guardar datos en la base de datos
        $activity = new Activity;

        $activity->module_name = 'Creación Automática';
        $activity->issuer_id = $contact->assigned_to;
        $activity->activity_type_id = '1';
        $activity->activity_description = 'Tarea creada automáticamente.';
        $activity->activity_status = 'Pendiente';
        $activity->date_start = Carbon::now();
        $activity->date_end = Carbon::tomorrow();
        $activity->delivery_time = '15:00:00';
        $activity->contact_id = $contact->id;
        $activity->completed = false;
        $activity->is_urgent = false;

        $activity->save();

        $activity->handler()->sync($request->assigned_to, false);
        $activity->interests()->sync($request->interest_id, false);

        return redirect()->route('form.success');
    }

    public function formSuccess(){
        return view('form_success');
    }

    public function testimonios(){
    	return view('testimonios');
    }

    public function privacidad(){
        return view('legales.privacidad');
    }

    public function terminos(){
        return view('legales.terminos');
    }
}
