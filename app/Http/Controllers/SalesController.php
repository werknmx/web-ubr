<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Exports\AllContactExport;
use App\Imports\ContactsImport;
use Maatwebsite\Excel\Facades\Excel;

use Carbon\Carbon;
use App\Contact;
use App\ContactLog;
use App\ContactInterest;
use App\Program;
use App\Course;
use App\User;
use App\Payment;
use App\Goals;
use App\LandingPage;

use App\Activity;
use App\ActivityHandler;
use App\ActivityType;

use Auth;
use Session;

use App\Charts\InscritosChart;
use App\Charts\ActividadesChart;

class SalesController extends Controller
{
    public function index()
    {
        $contacts = Contact::where('assigned_to', Auth::user()->id)->orderBy('created_at', 'desc')->get();

        $courses = Course::all();
        $programs = Program::all();
        $landing_pages = LandingPage::all();

        $sales_agents = User::all();

        $new_contact_count = Contact::where('status', 'Registro')->where('assigned_to', Auth::user()->id)->count();
        $prospect_count = Contact::where('status', 'Prospecto')->where('assigned_to', Auth::user()->id)->count();
        $interested_count = Contact::where('status', 'Interesado')->where('assigned_to', Auth::user()->id)->count();
        $sale_promise_count = Contact::where('status', 'Promesa de Venta')->where('assigned_to', Auth::user()->id)->count();
        $sale_complete = Contact::where('status', 'Cierre Exitoso')->where('assigned_to', Auth::user()->id)->count();
        
        $promesa_venta = Goals::where('name', 'Promesa de Venta')->first();

        $today = Carbon::now();
        $last_days = Carbon::now()->subDays(3);

        $alert_prospect = Contact::where('updated_at', '<=', $last_days)->where('status', '!=', 'Cierre Exitoso')->count();

        $activities = Activity::where('issuer_id', Auth::user()->id)->where('completed', false)->where('activity_status', '!=', 'Incompleto por Cierre')->get();

        $activity_types = ActivityType::all();
        $users = User::all();

        return view('back.crm.index')
        ->with('contacts', $contacts)
        ->with('new_contact_count', $new_contact_count)
        ->with('prospect_count', $prospect_count)
        ->with('interested_count', $interested_count)
        ->with('sale_promise_count', $sale_promise_count)
        ->with('courses', $courses)
        ->with('sales_agents', $sales_agents)
        ->with('activities', $activities)
        ->with('activity_types', $activity_types)
        ->with('users', $users)
        ->with('programs', $programs)
        ->with('landing_pages', $landing_pages)
        ->with('promesa_venta', $promesa_venta)
        ->with('sale_complete', $sale_complete)
        ->with('alert_prospect', $alert_prospect);
    }

    /* ------------------------ */
    /* ------------------------ */
    /* FUNCIONALIDAD DE FILTROS */
    /* ------------------------ */
    /* ------------------------ */

    public function filterNewRegistry()
    {
        $contacts = Contact::where('status', 'Registro')->get();
        $activities = Activity::where('issuer_id', Auth::user()->id)->where('completed', false)->get();
        $activity_types = ActivityType::all();
        $users = User::where('name', Auth::user()->name)->get();

        $filter_name = 'Nuevos Registros';

        return view('back.crm.filtered')->with('contacts', $contacts)->with('activities', $activities)->with('activity_types', $activity_types)->with('filter_name', $filter_name)->with('users', $users);
    }

    public function filterProspects()
    {
        $contacts = Contact::where('status', 'Prospecto')->where('assigned_to', Auth::user()->id)->get();

        $activities = Activity::where('issuer_id', Auth::user()->id)->where('completed', false)->get();
        $activity_types = ActivityType::all();
        $users = User::where('name', Auth::user()->name)->get();

        $filter_name = 'Prospectos';

        return view('back.crm.filtered')->with('contacts', $contacts)->with('activities', $activities)->with('activity_types', $activity_types)->with('filter_name', $filter_name)->with('users', $users);
    }

    public function filterInterested()
    {
        $contacts = Contact::where('status', 'Interesado')->where('assigned_to', Auth::user()->id)->get();

        $activities = Activity::where('issuer_id', Auth::user()->id)->where('completed', false)->get();
        $activity_types = ActivityType::all();
        $users = User::where('name', Auth::user()->name)->get();

        $filter_name = 'Interesados';

        return view('back.crm.filtered')->with('contacts', $contacts)->with('activities', $activities)->with('activity_types', $activity_types)->with('filter_name', $filter_name)->with('users', $users);
    }

    public function filterSalesPromise()
    {
        $contacts = Contact::where('status', 'Promesa de Venta')->where('assigned_to', Auth::user()->id)->get();

        $activities = Activity::where('issuer_id', Auth::user()->id)->where('completed', false)->get();
        $activity_types = ActivityType::all();
        $users = User::where('name', Auth::user()->name)->get();

        $filter_name = 'Promesa de Venta';

        return view('back.crm.filtered')->with('contacts', $contacts)->with('activities', $activities)->with('activity_types', $activity_types)->with('filter_name', $filter_name)->with('users', $users);
    }

    public function filterSalesComplete()
    {
        $today = Carbon::now();
        $last_days = Carbon::now()->subDays(15);

        $contacts = Contact::where('status', 'Cierre Exitoso')
        ->where('assigned_to', Auth::user()->id)
        ->where('closing_date', '<=', $today)
        ->where('closing_date', '>=', $last_days)
        ->get();

        $activities = Activity::where('issuer_id', Auth::user()->id)->where('completed', false)->get();
        $activity_types = ActivityType::all();
        $users = User::where('name', Auth::user()->name)->get();

        $filter_name = 'Cierre Exitoso';

        return view('back.crm.filtered')->with('contacts', $contacts)->with('activities', $activities)->with('activity_types', $activity_types)->with('filter_name', $filter_name)->with('users', $users);
    }

    public function filterActionRequired()
    {
        $today = Carbon::now();
        $last_days = Carbon::now()->subDays(3);

        $contacts = Contact::where('updated_at', '<=', $last_days)
        ->where('status', '!=', 'Cierre Exitoso')
        ->get();

        $activities = Activity::where('issuer_id', Auth::user()->id)->where('completed', false)->get();
        $activity_types = ActivityType::all();
        $users = User::where('name', Auth::user()->name)->get();

        $filter_name = 'Prospectos Detenidos';

        return view('back.crm.filtered')->with('contacts', $contacts)->with('activities', $activities)->with('activity_types', $activity_types)->with('filter_name', $filter_name)->with('users', $users);
    }

    /* ------------------------- */
    /* ------------------------- */
    /* ------ CLAB CONTACTO ---- */
    /* ------------------------- */
    /* ------------------------- */

    public function create()
    {
        return view('back.crm.create');
    }

    public function store(Request $request)
    {
        //Validar
        $this -> validate($request, array(
            'name' => 'required|max:255',
        ));

        // Guardar datos en la base de datos
        $contact = new Contact;

        $contact->name = $request->name;
        $contact->sur_name = $request->sur_name;
        $contact->image = $request->image;
        $contact->email = $request->email;
        $contact->country = $request->country;
        $contact->state = $request->state;
        $contact->city = $request->city;

        $contact->phone_main = $request->phone_main;
        $contact->phone_cel = $request->phone_cel;
        $contact->status = $request->status;
        $contact->created_by = $request->created_by;
        $contact->assigned_to = $request->assigned_to;
        $contact->landing_origin_id = $request->landing_origin_id;
        $contact->birthday = $request->birthday;
        
        $contact->company = $request->company;
        $contact->job_title = $request->job_title;
        $contact->company_type = $request->company_type;
        $contact->contact_id = $request->contact_id;

        $contact->global_origin = $request->global_origin;
        $contact->is_active = true;
        $contact->save();

        $contact->interests()->sync($request->interest_id, false);

        // Guardar datos en la base de datos
        $contact_log = new ContactLog;
        $contact_log->contact_id = $contact->id;
        $contact_log->user_id = $request->created_by;
        $contact_log->log = 'Prospecto creado';
        $contact_log->save();

        /* Crear Primera actividad automáticamente */
        $activity = new Activity;

        $activity->module_name = 'Creación Automática';
        $activity->issuer_id = $request->assigned_to;
        $activity->activity_type_id = '1';
        $activity->activity_description = 'Tarea creada automáticamente.';
        $activity->activity_status = 'Pendiente';
        $activity->date_start = Carbon::now();
        $activity->date_end = Carbon::tomorrow();
        $activity->delivery_time = '15:00:00';
        $activity->contact_id = $contact->id;
        $activity->completed = false;
        $activity->is_urgent = false;

        $activity->save();

        $activity->handler()->sync($request->assigned_to, false);
        $activity->interests()->sync($request->interest_id, false);

        // Mensaje de session
        Session::flash('exito', 'El contacto se guardó correctamente en la base de datos.');

        // Enviar a vista
        return redirect()->back();
    }

    public function show($id)
    {
        /* Obtener Prospecto */
        $contact = Contact::find($id);

        /* Funciones de Actividades */
        $activities = Activity::where('contact_id', $contact->id)->orderBy('completed_at', 'asc')->get();
        $activities_completed = Activity::where('contact_id', $contact->id)->where('completed', true)->count();


        $activity_types = ActivityType::all();

        /* Opciones de Vinculación */
        $courses = Course::all();
        $programs = Program::all();
        $landing_pages = LandingPage::all();

        /* Agendes de Venta */
        $sales_agents = User::all();

        /* CALCULANDO POTENCIAL DE VENTA */
        $total_potencia = 0;

        foreach($contact->interests as $ct) {
           $total_potencia += $ct->normal_price;
        };

        $total_potencia;
        /*-----------------------------------*/
        $users = User::where('id', $contact->assigned_to)->get();

        /* Contactos Vinculados al Principal */
        $contact_sons = Contact::where('contact_id', $contact->id)->get();

        return view('back.crm.show')
        ->with('contact', $contact)
        ->with('activities', $activities)
        ->with('activities_completed', $activities_completed)
        ->with('activity_types', $activity_types)
        ->with('users', $users)
        ->with('total_potencia', $total_potencia)
        ->with('contact_sons', $contact_sons)
        ->with('courses', $courses)
        ->with('programs', $programs)
        ->with('landing_pages', $landing_pages)
        ->with('sales_agents', $sales_agents);
    }

    public function edit($id)
    {
        $contact = Contact::find($id);

        $courses = Course::all();
        $programs = Program::all();

        $sales_agents = User::all();

        return view('back.crm.edit')->with('contact', $contact)->with('course', $courses)->with('programs', $programs)->with('sales_agents', $sales_agents);
    }

    public function update(Request $request, $id)
    {
        //Validar
        $this -> validate($request, array(
            'name' => 'required|max:255',
        ));

        // Guardar datos en la base de datos
        $contact = Contact::find($id);

        $contact->name = $request->name;
        $contact->sur_name = $request->sur_name;
        $contact->image = $request->image;
        $contact->email = $request->email;
        $contact->country = $request->country;
        $contact->state = $request->state;
        $contact->city = $request->city;
        $contact->phone_main = $request->phone_main;
        $contact->phone_cel = $request->phone_cel;

        $contact->assigned_to = $request->assigned_to;
        $contact->birthday = $request->birthday;
        $contact->company = $request->company;
        $contact->job_title = $request->job_title;
        $contact->company_type = $request->company_type;
        $contact->global_origin = $request->global_origin;

        $contact->is_active = true;

        $contact->save();

        $contact->interests()->sync($request->interest_id, false);

        // Mensaje de session
        Session::flash('exito', 'El contacto se guardó correctamente en la base de datos.');

        // Enviar a vista
        return redirect()->route('listados.show', $contact->id);
    }

    public function destroy($id)
    {
        // Guardar datos en la base de datos
        $contact = Contact::find($id);

        foreach ($contact->activities as $act) {
            $activity = Activity::find($act->id);

            $activity->delete();
        }
        $contact->delete();

        Session::flash('exito', 'El contacto y todas sus actividades han sido eliminadas. Sus datos no se verán reflejados en KPI');
        return redirect()->route('listados.index');
    }

    /* FUNCIONALIDADES DE ACTUALIZACIÓN DE ESTADO */

    public function updateStatus(Request $request, $id)
    {
        $contact = Contact::find($id);

        if ($request->status == 'Cierre Exitoso') {
            $contact->status = $request->status;

            $contact->closing_date = Carbon::now();
        }else{
            $contact->status = $request->status;
        }
        
        $contact->save();

        Session::flash('exito', 'Estado actual:' . ' ' . $contact->status );

        return redirect()->back();
    }

    public function updateGlobalStatus(Request $request, $id)
    {
        $contact = Contact::find($id);
        $contact->global_status = $request->global_status;
        $contact->save();

        Session::flash('exito', 'Estado Global actual:' . ' ' . $contact->global_status );

        return redirect()->back();
    }

    public function completarRegistro($id)
    {
        $contact = Contact::find($id);
        $contact->status = 'Prospecto';
        $contact->save();

        Session::flash('exito', 'Estado actual:' . ' ' . $contact->status );

        return redirect()->back();
    }

    public function completarProspecto($id)
    {
        $contact = Contact::find($id);
        $contact->status = 'Interesado';
        $contact->save();

        Session::flash('exito', 'Estado actual:' . ' ' . $contact->status );

        return redirect()->back();
    }

    public function completarInteresado($id)
    {
        $contact = Contact::find($id);

        $contact->status = 'Promesa de Venta';
        $contact->save();

        Session::flash('exito', 'Estado actual:' . ' ' . $contact->status );
        
        return redirect()->back();
    }

    public function completarPromesaDeVenta($id)
    {
        $contact = Contact::find($id);
        if ($request->status == 'Cierre Exitoso') {
            $contact->status = 'Cierre Exitoso';

            $payment = new Payment;

            $payment->contact_id = $contact->id;
            $payment->reference = $request->reference;
            $payment->attachment = $request->attachment;

            $payment->save();

        }else{
            $contact->status = 'Cierre Perdido';
        }
        
        $contact->save();

        Session::flash('exito', 'Estado actual:' . ' ' . $contact->status );

        return redirect()->back();
    }

    public function prospectoPerdido($id)
    {
        $contact = Contact::find($id);

        $activities_pendientes = Activity::where('contact_id', $contact->id)->where('activity_status', 'Pendiente')->get();

        foreach ($activities_pendientes as $act) {
            $act->activity_status = 'Incompleto por Cierre';
            $act->save();
        }

        $activities_importantes = Activity::where('contact_id', $contact->id)->where('activity_status', 'Importante')->get();

        foreach ($activities_importantes as $act) {
            $act->activity_status = 'Incompleto por Cierre';
            $act->save();
        }

        $contact->status = 'Cierre Perdido';
        $contact->save();

        Session::flash('exito', 'Estado actual:' . ' ' . $contact->status );
        
        return redirect()->back();
    }

    /* FUNCIONALIDADES DE EXPORTACIÓN */
    public function export() 
    {
        return Excel::download(new AllContactExport, 'prospectos_total.xlsx');
    }
    
    public function import(Request $request) 
    {
        $filename = $request->import_file;

        try {
            Excel::import(new ContactsImport, $filename);
             // Mensaje de session
            Session::flash('exito', 'La información se importó a tu base de datos sin errores. Los registros repetidos fueron ignorados automáticamente. Todas las actividades referente a los nuevos prospectos se crearon exitosamente.');

            return redirect()->back();

        } catch (\Maatwebsite\Excel\Validators\ValidationException $e) {
             $failures = $e->failures();
             
             foreach ($failures as $failure) {
                 $failure->row(); // row that went wrong
                 $failure->attribute(); // either heading key (if using heading row concern) or column index
                 $failure->errors(); // Actual error messages from Laravel validator

                 return redirect()->back()->with('errors', $errors);
             }
        }
        
    }
}
