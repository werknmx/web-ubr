<?php

namespace App\Http\Controllers;

use Auth;
use Session;

use Carbon\Carbon;
use App\User;

use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;

use Illuminate\Http\Request;

class AuthController extends Controller
{
    public function adminList()
    {
        $usuarios = User::all();
        $roles = Role::all();

        return view('back.users.index')->with('usuarios', $usuarios)->with('roles', $roles);
    }

	public function register()
    {
    	return view('back.users.register');
    }

    public function postRegister(Request $request)
    {
    	$this->validate($request, [
    		'name' => 'required',
    		'email' => 'email|required|unique:users',
    		'password' => 'required|min:4',
    	]);

    	$admin = new User([
    		'name' => $request->input('name'),
    		'email' => $request->input('email'),
    		'password' => bcrypt($request->input('password')),
    	]);

        $rol = Role::findByName($request->rol);

        // Guardar primero el admin
        $admin->save();

        // Asignar el Rol
        $admin->assignRole($rol->name);

    	return redirect()->route('admin.index');
    }

    public function destroy($id)
    {
        $user = User::find($id);

        if($user->id == Auth::user()->id){
            alert()->warning('No puedes borrar el usuario que está actualmente conectado.', '¡Cuidado!')->persistent("Entendido");
			Session::flash('exito', 'No puedes borrar el usuario que está actualmente conectado.');
        	return redirect()->back();
        }else{
        	$user->delete();

            alert()->success('Usuario fue borrado exitosamente de la base de datos.', '¡Éxito!')->autoclose(1000);
        	Session::flash('exito', 'El Usuario ha sido borrado exitosamente.');

        	return redirect()->back();
        }
        

        
    }
}
