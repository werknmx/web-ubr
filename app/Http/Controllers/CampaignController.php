<?php

namespace App\Http\Controllers;

use Auth;
use Session;

use Image;
use Purifier;

use App\Campaign;
use App\Course;
use App\LandingPage;

use App\Exports\ContactExport;
use App\Exports\AllContactExport;
use Maatwebsite\Excel\Facades\Excel;

use Illuminate\Http\Request;

class CampaignController extends Controller
{
    public function index()
    {
        $campañas = Campaign::orderBy('created_at', 'desc')->paginate(10);

        return view('back.campaigns.index')->with('campañas', $campañas);
    }

    public function create()
    {
        //
    }

    public function store(Request $request)
    {
        //Validar
        $this -> validate($request, array(
            'title' => 'required|max:255',
        ));

        // Guardar datos en la base de datos
        $campaign = new Campaign;

        $campaign->title = $request->title;
        $campaign->description = $request->description;
        $campaign->slug = str_slug($request->title);

        $campaign->save();

        // Mensaje de session
        Session::flash('exito', 'Tu campaña se guardó correctamente en la base de datos.');

        // Enviar a vista
        return redirect()->back();
    }

    public function show($id)
    {
        $campaign = Campaign::find($id);

        $landings = LandingPage::where('campaign_id', $campaign->id)->get();
        $courses = Course::all();

        return view('back.campaigns.show')->with('campaign', $campaign)->with('landings', $landings)->with('courses', $courses);
    }

    public function edit($id)
    {
        $campaign = Campaign::find($id);

        return view('back.campaigns.edit')->with('campaign', $campaign);
    }

    public function update(Request $request, $id)
    {
        //
    }

    public function destroy($id)
    {
        //
    }

    public function export($landing) 
    {
        return (new ContactExport)->forLanding($landing)->download('prospectos_landings.xlsx');
    }

    public function fullExport() 
    {
        return Excel::download(new AllContactExport, 'prospectos_todas_campañas.xlsx');
    }
}
