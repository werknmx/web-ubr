<?php

namespace App\Http\Controllers;

use Session;
use Artisan;

use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;

use Illuminate\Http\Request;

class RoleController extends Controller
{
    public function index()
    {
        $roles = Role::all();
        $permisos = Permission::all();

        return view('back.roles.index')->with('roles', $roles)->with('permisos', $permisos);
    }

    public function create()
    {
        //
    }

    public function store(Request $request)
    {
        $rol = new Role;

        $rol->name = $request->name;
        $rol->guard_name = 'web';

        $rol->save();

        Artisan::call('cache:clear');

        $permissions = $request->permiso_id;
        $rol->givePermissionTo($permissions);

        Artisan::call('cache:clear');

        // Mensaje de session
        Session::flash('success', 'El rol se guardó exitosamente en la base de datos.');

        // Enviar a vista
        return redirect()->back();
    }

    public function show($id)
    {
        //
    }

    public function edit($id)
    {
        //
    }

    public function update(Request $request, $id)
    {
        //
    }

    public function destroy($id)
    {
        $rol = Role::find($id);

        $rol->delete();

        Session::flash('exito', 'El rol ha sido borrada exitosamente.');

        return redirect()->back();
    }
}
