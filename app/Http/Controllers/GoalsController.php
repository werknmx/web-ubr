<?php

namespace App\Http\Controllers;

use Session;
use App\Goals;

use Illuminate\Http\Request;

class GoalsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Goals  $goals
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Goals  $goals
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Goals  $goals
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $meta = Meta::find($id);

        $meta->meta_objetivo = $request->meta_objetivo;
        $meta->fecha_inicio = $request->fecha_inicio;
        $meta->fecha_final = $request->fecha_final;

        $meta->save();

        // Mensaje de session
        Session::flash('exito', 'Se actualizó el objetivo de venta Anual');

        // Enviar a vista

        return redirect()->back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Goals  $goals
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
