<?php

namespace App\Http\Controllers;

use Auth;
use Session;

use Image;
use Purifier;

use App\User;
use App\Course;
use App\Program;

use Illuminate\Http\Request;

class CourseController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $cursos = Course::orderBy('created_at', 'desc')->paginate(10);

        return view('back.courses.index')->with('cursos', $cursos);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $programs = Program::all();

        return view('back.courses.create')->with('programs', $programs);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //Validar
        $this -> validate($request, array(

        ));

        // Guardar datos en la base de datos
        $curso = new Course;

        $curso->title = $request->title;
        $curso->slug = str_slug($request->title);
        $curso->description = $request->description;
        $curso->objective = $request->objective;
        $curso->addressed_to = $request->addressed_to;
        $curso->slogans = $request->slogans;
        $curso->accreditation_type = $request->accreditation_type;
        $curso->admission_profile = Purifier::clean($request->admission_profile);
        $curso->egress_profile = Purifier::clean($request->egress_profile);
        $curso->duration = $request->duration;
        $curso->modality = $request->modality;
        $curso->timetable = $request->timetable;
        $curso->is_active = $request->is_active;
        $curso->capacity = $request->capacity;
        $curso->timetable = $request->timetable;

        $curso->program_id = $request->program_id;

        $curso->normal_price = $request->normal_price;
        $curso->special_price = $request->special_price;
        $curso->has_discount = $request->has_discount;

        $curso->is_active = true;

        if ($request->hasFile('image')) {
            $image = $request->file('image');
            $nombre_archivo = time() . '.' . $image->getClientOriginalExtension();
            $ubicacion = public_path('img/cursos/covers/' . $nombre_archivo);

            Image::make($image)->resize(1280,null, function($constraint){ $constraint->aspectRatio(); })->save($ubicacion);

            $curso->image = $nombre_archivo;
        }

        $curso->save();

        // Mensaje de session
        Session::flash('exito', 'Tu curso se guardó correctamente en la base de datos.');

        return redirect()->route('cursos.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Course  $course
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $curso = Course::find($id);

        return view('back.courses.show')->with('curso', $curso);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Course  $course
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $curso = Course::find($id);
        $programs = Program::all();

        return view('back.courses.edit')->with('curso', $curso)->with('programs', $programs);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Course  $course
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        // Guardar datos en la base de datos
        $curso = Course::find($id);

        $curso->title = $request->title;
        $curso->slug = str_slug($request->title);
        $curso->description = $request->description;
        $curso->objective = $request->objective;
        $curso->addressed_to = $request->addressed_to;
        $curso->slogans = $request->slogans;
        $curso->accreditation_type = $request->accreditation_type;
        $curso->admission_profile = Purifier::clean($request->admission_profile);
        $curso->egress_profile = Purifier::clean($request->egress_profile);
        $curso->duration = $request->duration;
        $curso->modality = $request->modality;
        $curso->timetable = $request->timetable;
        $curso->is_active = $request->is_active;
        $curso->capacity = $request->capacity;
        $curso->timetable = $request->timetable;

        $curso->program_id = $request->program_id;

        $curso->normal_price = $request->normal_price;
        $curso->special_price = $request->special_price;
        $curso->has_discount = $request->has_discount;

        $curso->is_active = true;

        if ($request->hasFile('image')) {
            $image = $request->file('image');
            $nombre_archivo = time() . '.' . $image->getClientOriginalExtension();
            $ubicacion = public_path('img/cursos/covers/' . $nombre_archivo);

            Image::make($image)->resize(1280,null, function($constraint){ $constraint->aspectRatio(); })->save($ubicacion);

            $curso->image = $nombre_archivo;
        }

        $curso->save();

        // Mensaje de session
        Session::flash('warning', 'Tu curso se editó correctamente en la base de datos.');

        return redirect()->route('cursos.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Course  $course
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $curso = Course::find($id);
        unlink(public_path() .  '/img/cursos/covers/' . $curso->image);
        $curso->delete();

        Session::flash('warning', 'La publicación y sus imágenes fueron eliminadas correctamente.');

        return redirect()->back();
    }
}
