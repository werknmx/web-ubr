<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Session;
use Auth;

use App\Event;
use Carbon\Carbon;

class EventController extends Controller
{
    public function index()
    {
        $calendar_events = Event::all();

        return view('back.events.index')->with('calendar_events', $calendar_events);
    }

    public function create()
    {
        //
    }

    public function store(Request $request)
    {
        $event = new Event;

        $event->name = $request->name;
        $event->description = str_replace(array("\r", "\n"), '', $request->description);
        $event->type = $request->type;
        $event->location = $request->location;
        $event->contact_info_phone = $request->contact_info_phone;
        $event->contact_info_mail = $request->contact_info_mail;

        $event->start_date =Carbon::parse($request->start_date)->format('Y-m-d');
        $event->end_date = Carbon::parse($request->end_date)->format('Y-m-d');

        $event->save();

        return redirect()->back();
    }

    public function show($id)
    {
        //
    }

    public function edit($id)
    {
        $evento = Event::find($id);
        return view('back.events.edit')->with('evento', $evento);
    }

    public function update(Request $request, $id)
    {
        $event = Event::find($id);

        $event->name = $request->name;
        $event->description = $request->description;
        $event->type = $request->type;
        $event->location = $request->location;
        $event->contact_info_phone = $request->contact_info_phone;
        $event->contact_info_mail = $request->contact_info_mail;

        $event->start_date =Carbon::parse($request->start_date)->format('Y-m-d');
        $event->end_date = Carbon::parse($request->end_date)->format('Y-m-d');

        $event->save();

        return redirect()->route('eventos.index');
    }

    public function softDestroy($id)
    {
        $event = Event::findOrFail($id);
        $event->delete();

        Session::flash('exito', 'El evento ha sido borrado exitosamente.');

        return redirect()->route('eventos.index');
    }
}
