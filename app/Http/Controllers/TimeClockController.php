<?php

namespace App\Http\Controllers;

use Carbon\Carbon;
use App\TimeClock;
use App\User;
use Illuminate\Http\Request;

class TimeClockController extends Controller
{
    public function index()
    {
        $checks = TimeClock::whereDate('check_in', Carbon::now())->get();
        $users = User::all();

        return view('back.time_clock.index')->with('checks', $checks)->with('users', $users);
    }

    public function history()
    {
        $checks = TimeClock::orderBy('created_at', 'desc')->get();

        return view('back.time_clock.history')->with('checks', $checks);
    }


    public function create()
    {
        //
    }

    public function store(Request $request)
    {
        $time = new TimeClock;

        $time->user_id = $request->user_id;
        $time->check_in = Carbon::now();
        $time->comment = $request->comment;

        $time->save();

        return redirect()->back();
    }

    public function show($id)
    {
        //
    }

    public function edit($id)
    {
        //
    }

    public function update(Request $request, $id)
    {
        $time = TimeClock::find($id);
        $time->check_out = Carbon::now();
        $time->save();

        return redirect()->back();
    }

    public function destroy($id)
    {
        //
    }
}
