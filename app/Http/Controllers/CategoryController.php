<?php

namespace App\Http\Controllers;

use Session;
use App\Category;

use Illuminate\Http\Request;

class CategoryController extends Controller
{
    public function index()
    {
        $categorias = Category::orderBy('created_at', 'desc')->paginate(10);

        return view('back.categories.index')->with('categorias', $categorias);
    }

    public function create()
    {
        //
    }

    public function store(Request $request)
    {
        //Validar
        $this -> validate($request, array(
            'name' => 'required|max:255',
        ));

        // Guardar datos en la base de datos
        $categoria = new Category;

        $categoria->name = $request->name;
        $categoria->slug = str_slug($request->name);

        $categoria -> save();

        // Mensaje de session
        Session::flash('exito', 'Tu categoria se guardó correctamente en la base de datos.');

        // Enviar a vista
        return redirect()->back();
    }

    public function show($id)
    {
        //
    }

    public function edit($id)
    {
        //
    }

    public function update(Request $request, $id)
    {
        //
    }

    public function destroy($id)
    {
        //
    }
}
