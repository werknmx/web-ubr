<?php

namespace App\Http\Controllers;

use Auth;
use Session;

use Image;
use Purifier;

use App\Blog;
use App\Category;
use App\Tag;
use App\User;

use Illuminate\Http\Request;

class BlogController extends Controller
{
    public function index()
    {
        $publicaciones = Blog::orderBy('created_at', 'desc')->paginate(10);
        $categorias = Category::all();
        $etiquetas = Tag::all();

        return view('back.blog.index')->with('publicaciones', $publicaciones)->with('categorias', $categorias)->with('etiquetas', $etiquetas);
    }

    public function create()
    {
        $categorias = Category::all();
        $etiquetas = Tag::all();

        $autores = User::all();

        return view('back.blog.create')->with('categorias', $categorias)->with('etiquetas', $etiquetas)->with('autores', $autores);
    }

    public function store(Request $request)
    {
        // Guardar datos en la base de datos
        $publicacion = new Blog;

        $publicacion->title = $request->title;
        $publicacion->slug = str_slug($request->title);
        $publicacion->summary = $request->summary;
        $publicacion->body = Purifier::clean($request->body);
        $publicacion->category_id = $request->category_id;
        $publicacion->autor_id = $request->autor_id;

        if ($request->hasFile('image')) {
            $image = $request->file('image');
            $nombre_archivo = time() . '.' . $image->getClientOriginalExtension();
            $ubicacion = public_path('img/blog/covers/' . $nombre_archivo);

            Image::make($image)->resize(1280,null, function($constraint){ $constraint->aspectRatio(); })->save($ubicacion);

            $publicacion->image = $nombre_archivo;
        }

        $publicacion->save();
        $publicacion->etiquetas()->sync($request->tag_id, true);

        // Mensaje de session
        Session::flash('exito', 'Tu publicación se guardó correctamente en la base de datos.');

        return redirect()->route('blog.index');
    }

    public function show($id)
    {
        $publicacion = Blog::find($id);

        return view('back.blog.show')->with('publicacion', $publicacion);
    }

    public function edit($id)
    {
        $publicacion = Blog::find($id);
        $categorias = Category::all();
        $etiquetas = Tag::all();

        $autores = User::all();

        return view('back.blog.edit')->with('publicacion', $publicacion)->with('categorias', $categorias)->with('etiquetas', $etiquetas)->with('autores', $autores);
    }

    public function update(Request $request, $id)
    {
        // Guardar datos en la base de datos
        $publicacion = Blog::find($id);

        $publicacion->title = $request->title;
        $publicacion->slug = str_slug($request->title);
        $publicacion->summary = $request->summary;
        $publicacion->body = Purifier::clean($request->body);
        $publicacion->category_id = $request->category_id;
        $publicacion->autor_id = $request->autor_id;

        if ($request->hasFile('image')) {
            $image = $request->file('image');
            $nombre_archivo = time() . '.' . $image->getClientOriginalExtension();
            $ubicacion = public_path('img/blog/covers/' . $nombre_archivo);

            Image::make($image)->resize(1280,null, function($constraint){ $constraint->aspectRatio(); })->save($ubicacion);

            $publicacion->image = $nombre_archivo;
        }

        $publicacion->save();
        $publicacion->etiquetas()->sync($request->tag_id, true);

        // Mensaje de session
        Session::flash('warning', 'Tu publicación se editó correctamente en la base de datos.');

        return redirect()->route('blog.index');
    }

    public function destroy($id)
    {
        $publicacion = Blog::find($id);
        unlink(public_path() .  '/img/blog/covers/' . $publicacion->image);
        $publicacion->delete();

        Session::flash('warning', 'La publicación y sus imágenes fueron eliminadas correctamente.');

        return redirect()->back();
    }
}
