<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Auth;
use Image;
use Session;

use Carbon\Carbon;
use App\Contact;
use App\Goals;
use App\Activity;
use App\Blog;
use App\User;

class BackController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        $activities = Activity::where('issuer_id', Auth::user()->id)->where('completed', false)->where('activity_status', '!=', 'Incompleto por Cierre')->get();

        $activities_completed = Activity::where('issuer_id', Auth::user()->id)->where('completed', true)->count();

        $prospect_count = Contact::where('status', 'Prospecto')->where('assigned_to', Auth::user()->id)->count();
        
        $today = Carbon::now();
        $last_days = Carbon::now()->subDays(3);

        $alert_prospect = Contact::where('updated_at', '<=', $last_days)->where('status', '!=', 'Cierre Exitoso')->count();

        $blog = Blog::all()->count();

        /* KPI's Ventas */
        $venta_anual = Goals::where('name', 'Venta Total Anual')->firstOrFail();
        $cierres_mensuales = Goals::where('name', 'Cierres Mensuales')->firstOrFail();
        $promesa_venta = Goals::where('name', 'Promesa de Venta')->firstOrFail();

        $year_start = Carbon::now()->startOfYear();
        $year_end = Carbon::now()->endOfYear();

        $month_start = Carbon::now()->startOfMonth();
        $month_end = Carbon::now()->endOfMonth();

        $week_start = Carbon::now()->startOfWeek();
        $week_end = Carbon::now()->endOfWeek();

        $today = Carbon::now()->format('Y-m-d');

        $ventas_total = Contact::where('closing_date', '<=', $year_end)
        ->where('closing_date', '>=', $year_start)
        ->where('status', 'Cierre Exitoso')
        ->get();

        $ventas_mes = Contact::where('closing_date', '<=', $month_end)
        ->where('closing_date', '>=', $month_start)
        ->where('status', 'Cierre Exitoso')
        ->get();

        $ventas_semana = Contact::where('closing_date', '<=', $week_end)
        ->where('closing_date', '>=', $week_start)
        ->where('status', 'Cierre Exitoso')
        ->get();

        $ventas_hoy = Contact::where('closing_date', '<=', $today)
        ->where('closing_date', '>=', $today)
        ->where('status', 'Cierre Exitoso')
        ->get();

        $ven_total = 0;
        $ven_mes = 0;
        $ven_semana = 0;

        foreach ($ventas_total as $v_total) {
            foreach($v_total->interests as $v)
            {
               $ven_total += $v->normal_price;
            };
        };

        foreach ($ventas_mes as $v_month) {
            foreach($v_month->interests as $v)
            {
               $ven_mes += $v->normal_price;
            };
        };

        foreach ($ventas_semana as $v_week) {
            foreach($v_week->interests as $v)
            {
               $ven_semana += $v->normal_price;
            };
        };

        $ven_total;
        $ven_mes;
        $ven_semana;

        return view('back.index')
        ->with('activities', $activities)
        ->with('activities_completed', $activities_completed)
        ->with('prospect_count', $prospect_count)
        ->with('alert_prospect', $alert_prospect)
        ->with('blog', $blog)
        ->with('ven_total', $ven_total)
        ->with('ven_mes', $ven_mes)
        ->with('ven_semana', $ven_semana)
        ->with('ventas_semana', $ventas_semana)
        ->with('ventas_mes', $ventas_mes)
        ->with('ventas_hoy', $ventas_hoy)
        ->with('venta_anual', $venta_anual)
        ->with('cierres_mensuales', $cierres_mensuales);
    }

    public function userProfile()
    {
        return view('back.users.profile');
    }

    public function updateProfile(Request $request, $id)
    {
        // Guardar datos a la BD, usar el modelo con funcion find() para encontrar el elemento que estamos editabdo por medio del ID
        $user = User::find($id);

        if($request->password == NULL){
            $user->name = $request->input('name');
            $user->email = $request->input('email');
        }else{
            $user->password = bcrypt($request->input('password'));
        }
        
        $user->save();

        // Mensaje de aviso server-side
        Session::flash('success', 'Tu cuenta se actualizó exitosamente.');
        return redirect()->route('admin.perfil');
    }

    public function updateImage(Request $request, $id)
    {
        $user = User::find($id);
        
        $user->image = $request->user_imagen;

        if ($request->hasFile('user_image')) {
            $user_image = $request->file('user_image');
            $filename = 'user_img' . time() . '.' . $user_image->getClientOriginalExtension();
            $location = public_path('img/usuarios/' . $filename);

            Image::make($user_image)->resize(400,null, function($constraint){ $constraint->aspectRatio(); })->save($location);

            $user->image = $filename;
        }

        $user->save();

        Session::flash('success', 'La imagen de tu perfil ha sido editada exitosamente.');
        return redirect()->route('admin.perfil');
    }
}
