<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\User;
use Carbon\Carbon;

use App\Activity;
use App\ActivityHandler;
use App\ActivityType;
use App\ActivityTry;
use App\ActivityLog;

use App\Contact;
use App\ContactLog;

use Auth;
use Session;

class ActivityController extends Controller
{
    public function index()
    {
        //
    }

    public function create()
    {
        //
    }

    public function store(Request $request)
    {
        //Validar
        $this -> validate($request, array(
            'activity_description' => 'required|max:255',
        ));

        // Guardar datos en la base de datos
        $activity = new Activity;

        $activity->module_name = $request->module_name;
        $activity->issuer_id = Auth::user()->id;
        $activity->activity_type_id = $request->activity_type_id;
        $activity->activity_description = $request->activity_description;
        $activity->activity_status = $request->activity_status;
        $activity->date_start = $request->date_start;
        $activity->date_end = $request->date_end;
        $activity->delivery_time = $request->delivery_time;
        $activity->contact_id = $request->contact_id;
        $activity->completed = false;
        $activity->is_urgent = false;

        $activity->save();

        $activity->handler()->sync($request->handler_id, false);
        $activity->interests()->sync($request->interest_id, false);

        // Guardar datos en la base de datos
        $log = new ActivityLog;
        $log->activity_id = $activity->id;
        $log->user_id = Auth::user()->id;
        $log->log = 'Tarea creada por ' . Auth::user()->name . ' exitosamente.';
        $log->save();

        // Mensaje de session
        Session::flash('exito', 'Actividad programada con éxito.');

        // Enviar a vista
        return redirect()->back();
    }

    public function show($id)
    {
        //
    }

    public function edit($id)
    {
        //
    }

    public function update(Request $request, $id)
    {
        //Validar
        $this -> validate($request, array(
            'activity_description' => 'required|max:255',
        ));

        // Guardar datos en la base de datos
        $activity = Activity::find($id);

        $activity->activity_type_id = $request->activity_type_id;
        $activity->activity_description = $request->activity_description;
        $activity->activity_status = $request->activity_status;
        $activity->date_start = $request->date_start;
        $activity->date_end = $request->date_end;
        $activity->delivery_time = $request->delivery_time;

        $activity->save();

        $activity->handler()->sync($request->handler_id, false);
        $activity->interests()->sync($request->interest_id, false);

        // Guardar datos en la base de datos
        $log = new ActivityLog;
        $log->activity_id = $activity->id;
        $log->user_id = Auth::user()->id;
        $log->log = 'Tarea editada por ' . Auth::user()->name . ' exitosamente.';
        $log->save();

        // Mensaje de session
        Session::flash('exito', 'Actividad programada con éxito.');

        // Enviar a vista
        return redirect()->back();
    }

    public function destroy($id)
    {
        $activity = Activity::find($id);
        $activity->handler()->detach();
        $activity->interests()->detach();
        $activity->delete();

        // Mensaje de session
        Session::flash('exito', 'Se eliminó la tarea.');

        // Enviar a vista
        return redirect()->back();
    }

    public function completeActivity(Request $request, $id)
    {
        $activity = Activity::find($id);

        if($activity->completed == true){
            // Mensaje de session
            Session::flash('exito', 'Esta tarea ya estaba completada.');

            // Enviar a vista
            return redirect()->back();
        }else{
            $activity->completed = true;
            $activity->completed_by_id = Auth::user()->id;
            $activity->completed_at = Carbon::now();
            $activity->completed_time = Carbon::createFromTimeStamp(strtotime($activity->completed_at));

            $activity->activity_status = 'Completada';
            $activity->save();

            // Guardar datos en la base de datos
            $old_log = new ActivityLog;
            $old_log->activity_id = $activity->id;
            $old_log->user_id = Auth::user()->id;
            $old_log->log = 'Tarea completa exitosamente por medio de proceso de "completar tarea". Comentario: ' . $request->comment;
            $old_log->save();

            // Crear Nueva Actividad 
            $new_activity = new Activity;

            $new_activity->module_name = $request->module_name;
            $new_activity->issuer_id = Auth::user()->id;
            $new_activity->activity_type_id = $request->activity_type_id;
            $new_activity->activity_description = $request->activity_description;
            $new_activity->activity_status = $request->activity_status;
            $new_activity->date_start = $request->date_start;
            $new_activity->date_end = $request->date_end;
            $new_activity->delivery_time = $request->delivery_time;
            $new_activity->contact_id = $request->contact_id;
            $new_activity->completed = false;
            $new_activity->is_urgent = false;

            $new_activity->save();

            $new_activity->handler()->sync($request->handler_id, false);
            $new_activity->interests()->sync($request->interest_id, false);

            // Guardar datos en la base de datos
            $log = new ActivityLog;
            $log->activity_id = $new_activity->id;
            $log->user_id = Auth::user()->id;
            $log->log = 'Tarea creada por ' . Auth::user()->name . ' exitosamente.';
            $log->save();

            // Guardar datos en la base de datos
            $contact_log = new ContactLog;
            $contact_log->contact_id = $request->contact_id;
            $contact_log->user_id = Auth::user()->id;
            $contact_log->log = 'Comentario: ' . $request->comment . '. ID de Tarea completada: ' . $new_activity->id;
            $contact_log->save();

            if ($request->update_status == true) {
                $contact = Contact::find($request->contact_id);

                if ($contact->status == 'Registro'){
                    $contact->status = 'Prospecto';
                }elseif($contact->status == 'Prospecto'){
                    $contact->status = 'Interesado';
                }elseif($contact->status == 'Interesado'){
                    $contact->status = 'Promesa de Venta';
                }elseif($contact->status == 'Promesa de Venta'){
                    // Mensaje de session
                    Session::flash('exito', 'El estado no se actualizó ya que debes seleccionar si es perdido o exitoso, en caso de ser exitoso adjunta el recibo de pago en el proceso normal de cierre usando la barra de estado. Tarea completada.');

                    // Enviar a vista
                    return redirect()->back();
                }

                $contact->save();

                // Mensaje de session
                Session::flash('exito', 'Estatus de prospecto actualizada. Tarea Completada. Todas las demás acciones automáticas, como guardar el historial de acción y el contacto de prospecto, se realizaron exitosamente');
            }else{
                // Mensaje de session
                Session::flash('exito', 'Tarea Completada. Todas las demás acciones automáticas, como guardar el historial de acción y el contacto de prospecto, se realizaron exitosamente');
            }

            // Enviar a vista
            return redirect()->back();
        }
    }

    public function priorityActivity($id)
    {
        $activity = Activity::find($id);

        $activity->activity_status = 'Importante';
        $activity->save();

        // Guardar datos en la base de datos
        $log = new ActivityLog;
        $log->activity_id = $activity->id;
        $log->user_id = Auth::user()->id;
        $log->log = 'Tarea marcada como prioridad por ' . Auth::user()->name;
        $log->save();

        // Mensaje de session
        Session::flash('exito', 'Ahora tu equipo sabrá la importancia de la tarea.');

        // Enviar a vista
        return redirect()->back();
    }

    public function showTry($activity_id)
    {
        $activity = Activity::find($activity_id);

        //Session::flash('error', 'No es posible hacer más de cinco intentos. Contacta con el Gerente Comercial para desboquear.');
        return view('back.crm._show_tries')->with('activity', $activity);
    }

    public function storeTry(Request $request, $activity_id)
    {
        //Validar
        $this -> validate($request, array(
            'comment' => 'required|max:255',
        ));

        // Guardar datos en la base de datos
        $activity = Activity::find($activity_id);

        if ($activity->has_tries < 5) {
            $num = $activity->has_tries;

            $activity->has_tries = $num + 1;

            $activity->save();

            // Guardar datos en la base de datos
            $try = new ActivityTry;
            $try->activity_id = $activity->id;
            $try->comment = $request->comment;
            $try->save();

             // Mensaje de session
            Session::flash('exito', 'Intento de actividad registrado con éxito.');

            // Enviar a vista
            return redirect()->back();
        }else{
            // Mensaje de session
            Session::flash('error', 'No es posible hacer más de cinco intentos. Contacta con el Gerente Comercial para desboquear.');

            return redirect()->back();
        }
    }
}
