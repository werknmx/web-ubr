<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Blog extends Model
{
    public function autor()
    {
        return $this->belongsTo(\App\User::class, 'autor_id', 'id');
    }

    public function categoria()
    {
        return $this->belongsTo(\App\Category::class, 'category_id', 'id');
    }

    public function etiquetas()
    {
    	return $this->belongsToMany('App\Tag', 'tags_blogs', 'blog_id', 'tag_id');
    }
}
