<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class GalleryImages extends Model
{
    protected $table = 'gallery_images';
    protected $primaryKey = 'id';

    public function galeria()
    {
        return $this->belongsTo('App\Gallery');
    }
}
