<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ContactInterest extends Model
{
    protected $table = 'contacts_interests';
    protected $primaryKey = 'id';

    public function activities()
    {
        return $this->belongsToMany(\App\ContactInterest::class, 'contact_interest', 'contact_id', 'activity_id')->withTimestamps();
    }
}
