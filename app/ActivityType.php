<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ActivityType extends Model
{
    protected $table = 'sales_activities_types';
    protected $primaryKey = 'id';
}
