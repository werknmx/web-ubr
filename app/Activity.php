<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Activity extends Model
{
    protected $table = 'sales_activities';
    protected $primaryKey = 'id';

    protected $casts = [
        'completed' => 'boolean',
    ];

    protected $fillable = [
        'module_name', 'issuer_id', 'activity_type_id', 'activity_description', 'activity_status', 'date_start', 'date_end', 'delivery_time', 'contact_id', 'completed', 'is_urgent'
    ];

    public function issuer()
    {
        return $this->belongsTo(\App\User::class, 'issuer_id', 'id');
    }

    public function contact()
    {
        return $this->belongsTo(\App\Contact::class, 'contact_id', 'id');
    }

    public function completedBy()
    {
        return $this->belongsTo(\App\User::class, 'completed_by_id', 'id');
    }

    public function type()
    {
        return $this->belongsTo(\App\ActivityType::class, 'activity_type_id', 'id');
    }

    public function tries()
    {
        return $this->hasMany(\App\ActivityTry::class);
    }

    public function logs()
    {
        return $this->hasMany(\App\ActivityLog::class);
    }

    public function handler()
    {
    	return $this->belongsToMany(\App\User::class, 'sales_activities_handler', 'activity_id', 'handler_id')->withTimestamps();
    }

    public function interests()
    {
        return $this->belongsToMany(\App\Course::class, 'sales_activities_interests', 'activity_id', 'interest_id')->withTimestamps();
    }
}
