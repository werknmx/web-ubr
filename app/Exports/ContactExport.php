<?php

namespace App\Exports;

use App\Contact;
use Maatwebsite\Excel\Concerns\FromQuery;
use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Concerns\WithEvents;
use Maatwebsite\Excel\Events\AfterSheet;

class ContactExport implements FromQuery, WithHeadings, ShouldAutoSize, WithEvents
{
    use Exportable;

    public function headings(): array
    {
        return [
            '#',
            'Nombre',
            'Apellido',
            'Imágen',
            'Email',
            'País',
            'Estado',
            'Teléfono Principal',
            'Teléfono Celular',
            'Estatus',
            'Creado por',
            'Asignado a',
            'Origen',
            'Activo',
            'Creado',
            'Actualizado',
        ];
    }

    public function registerEvents(): array
    {
        return [
            AfterSheet::class    => function(AfterSheet $event) {
                $cellRange = 'A1:W1';
                $event->sheet->getDelegate()->getStyle($cellRange)->getFont()->setSize(20);
            },
        ];
    }

    public function forLanding(int $landing)
    {
        $this->landing = $landing;
        
        return $this;
    }

    public function query()
    {
        return Contact::query()->where('landing_origin_id', $this->landing);
    }
}
