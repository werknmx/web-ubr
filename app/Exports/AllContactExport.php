<?php

namespace App\Exports;

use App\Contact;

use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithEvents;
use Maatwebsite\Excel\Events\AfterSheet;

class AllContactExport implements FromCollection, WithHeadings, ShouldAutoSize, WithEvents
{

    public function headings(): array
    {
        return [
            '# ID',
            'Vinculado con',
            'Nombre',
            'Apellido',
            'Cumpleaños',
            'Archivo de Imagen',
            'Correo Electrónico',
            'País',
            'Estado',
            'Ciudad',
            'Teléfono Principal',
            'Teléfono Celular',
            'Empresa',
            'Puesto',
            'Tipo de Empresa',
            'Estatus Global',
            'Estatus de Venta',
            'Fecha de Cierre',
            'Creado por (ID de Usuario)',
            'Asignado a (ID de Usuario)',
            'Origen Global',
            'Origen de Campaña (ID de Campaña)',
            'Activo en Matriz',
            'Creado',
            'Actualizado',
        ];
    }

    public function registerEvents(): array
    {
        return [
            AfterSheet::class    => function(AfterSheet $event) {
                $cellRange = 'A1:Z1';
                $event->sheet->getDelegate()->getStyle($cellRange)->getFont()->setSize(20);
            },
        ];
    }

    public function collection()
    {
        return Contact::all();
    }
}
