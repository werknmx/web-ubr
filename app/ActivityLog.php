<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ActivityLog extends Model
{
    protected $table = 'sales_activities_logs';
    protected $primaryKey = 'id';

    public function activity()
    {
        return $this->belongsTo(\App\Activity::class, 'activity_id', 'id');
    }

    public function user()
    {
        return $this->belongsTo(\App\User::class, 'user_id', 'id');
    }
}
