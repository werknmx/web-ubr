<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class LandingPage extends Model
{
    public function curso()
    {
        return $this->belongsTo(\App\Course::class, 'courses_id', 'id');
    }

    public function contactos()
    {
        return $this->hasMany('App\Contact', 'landing_origin_id', 'id');
    }
}
