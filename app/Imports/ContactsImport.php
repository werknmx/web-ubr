<?php

namespace App\Imports;

use Carbon\Carbon;
use App\Contact;
use App\ContactLog;
use App\Activity;

//Importación por medio de Colección
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Validator;
use Maatwebsite\Excel\Concerns\ToCollection;
use Maatwebsite\Excel\Concerns\WithHeadingRow;

//Importación por medio de Modelo
//use Maatwebsite\Excel\Concerns\ToModel;
//use Maatwebsite\Excel\Concerns\Importable;
//use Maatwebsite\Excel\Concerns\WithValidation;
//use Maatwebsite\Excel\Concerns\WithHeadingRow;

class ContactsImport implements ToCollection, WithHeadingRow
{
    //use Importable;

    public function collection(Collection $rows)
    {
        Validator::make($rows->toArray(), [
            '*.0' => 'unique:contacts',
            '*.3' => 'unique:contacts',
            '*.8' => 'unique:contacts',
            '*.9' => 'unique:contacts',
         ])->validate();

        foreach ($rows as $row) 
        {
            $client = Contact::create([
                'contact_id' => NULL,
                'name' => $row['name'],
                'sur_name' => $row['sur_name'],
                'birthday' => $row['birthday'],
                'image' => NULL,
                'email' => $row['email'],
                'phone_main' => $row['phone_main'],
                'phone_cel' => $row['phone_cel'],
                'city' => $row['city'],
                'country' => $row['country'],
                'state' => $row['state'],
                'company' => $row['company'],
                'job_title' => $row['job_title'],
                'company_type' => $row['company_type'],
                'global_status' => 'Activo',
                'status' => 'Registro',
                'created_by' => '1',
                'assigned_to' => $row['assigned_to'],
                'global_origin' => $row['global_origin'],
                'landing_origin_id' => NULL,
                'is_active' => true,
            ]);

            $activity = Activity::create([
                'module_name' => 'Creación Automática',
                'issuer_id' => $client->assigned_to,
                'activity_type_id' => "1",
                'activity_description' => 'Tarea creada automáticamente.',
                'activity_status' => "Pendiente",
                'date_start' => Carbon::now(),
                'date_end' => Carbon::tomorrow(),
                'delivery_time' => "15:00:00",
                'contact_id' => $client->id,
                'completed' => false,
                'is_urgent' => false,
            ]);

            $activity->handler()->sync($client->assigned_to, false);
            $activity->interests()->sync($client->interest_id, false);

            $contact_log = ContactLog::create([
                'contact_id' => $client->id,
                'user_id' => $client->assigned_to,
                'log' => 'Prospecto creado desde Importación de Excel',
            ]);
        }
    }        

    /*
    public function model(array $row)
    {
        return new Client([
            'image_file' => NULL,
            'name' => $row['name'],
            'slug' => str_slug($row['name']), 
            'code' => $row['code'],
            'corporate_address' => NULL,
            'street' => $row['street'],
            'street_num' => $row['street_num'],
            'int_num' => $row['int_num'],
            'city' => $row['city'],
            'country' => $row['country'],
            'postal_code' => $row['postal_code'],
            'rfc_num' => $row['rfc_num'],
            'rfc_name' => $row['rfc_name'],
            'is_active' => true,
        ]);
    }
    */

    public function headingRow(): int
    {
        return 1;
    }
}
