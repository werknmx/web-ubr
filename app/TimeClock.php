<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TimeClock extends Model
{
    public function user()
    {
        return $this->belongsTo(\App\User::class, 'user_id', 'id');
    }
}
