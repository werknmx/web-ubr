<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Spatie\Permission\Traits\HasRoles;

use Carbon\Carbon;

class User extends Authenticatable
{
    use Notifiable;
    use HasRoles;

    protected $guard_name = 'web';

    protected $fillable = [
        'name', 'email', 'password',
    ];

    protected $hidden = [
        'password', 'remember_token',
    ];

    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public function publicaciones()
    {
        return $this->hasMany('App\Blog', 'autor_id', 'id');
    }

    public function reloj_checador()
    {
        return $this->hasMany(\App\TimeClock::class, 'user_id', 'id');
    }

    public function conexion()
    {
        return $this->hasMany(\App\TimeClock::class, 'user_id', 'id')->where('check_out', NULL)->whereDate('check_in', Carbon::now());
    }
}
