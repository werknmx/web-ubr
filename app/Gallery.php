<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Gallery extends Model
{
    public function imagenes()
    {
        return $this->hasMany('App\GalleryImages');
    }
}
