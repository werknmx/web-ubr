<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Tag extends Model
{
    public function publicaciones()
    {
    	return $this->belongsToMany('App\Blog', 'tags_blogs', 'tag_id', 'blog_id');
    }
}
