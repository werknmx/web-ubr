<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ActivityInterest extends Model
{
	protected $table = 'sales_activities_interests';
    protected $primaryKey = 'id';

    public function activities()
    {
        return $this->belongsToMany(\App\ActivityInterest::class, 'sales_activities_interest', 'activity_id', 'interest_id')->withTimestamps();
    }
}
