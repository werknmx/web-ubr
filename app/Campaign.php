<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Campaign extends Model
{
    public function course()
    {
        return $this->belongsTo(\App\Program::class, 'program_id', 'id');
    }
}
