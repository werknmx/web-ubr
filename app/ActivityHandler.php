<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ActivityHandler extends Model
{
	protected $table = 'sales_activities_handler';
    protected $primaryKey = 'id';

    public function activities()
    {
        return $this->belongsToMany(\App\ActivityHandler::class, 'sales_activities_handler', 'handler_id', 'activity_id');
    }
}
