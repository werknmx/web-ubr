<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Contact extends Model
{
    protected $fillable = [
        'contact_id',
        'name',
        'sur_name',
        'birthday',
        'image',
        'email',
        'phone_main',
        'phone_cel',
        'city',
        'country',
        'state',
        'company',
        'job_title',
        'company_type',
        'global_status',
        'status',
        'created_by',
        'assigned_to', 
        'global_origin',
        'landing_origin_id',
        'is_active',
    ];

    public function sales_agent()
    {
        return $this->belongsTo('App\User', 'assigned_to', 'id');
    }

    public function origin()
    {
        return $this->belongsTo(\App\LandingPage::class, 'landing_origin_id', 'id');
    }

    public function interests()
    {
    	return $this->belongsToMany(\App\Course::class, 'contacts_interests', 'contact_id', 'interest_id')->withTimestamps();
    }

    public function activities()
    {
        return $this->hasMany(\App\Activity::class, 'contact_id', 'id');
    }

    public function logs()
    {
        return $this->hasMany(\App\ContactLog::class);
    }
}
