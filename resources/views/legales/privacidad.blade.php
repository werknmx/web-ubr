@extends('layouts.front')

@section('stylesheets')

@endsection

@section('content')
<h1 class="legales-title">
	Aviso de Privacidad
</h1>

<section class="legales-content">
	<div class="container">
		<div class="row">
			<div class="col-md-8 offset-md-2">
				<p class="update-time"><span>Ultima Actualización:</span> N/A</p>
				
				<h3>Actualizando</h3>
				<p>Estamos actualizando nuestros términos legales, regresa pronto o llama a +52 55 68330778 o +(52) 5536771594. También puedes informarte mandando un correo acontacto@universidaddebienesraices.com </p>
			</div>
		</div>
	</div>
</section>
@endsection

@section('scripts')
<script type="text/javascript">
	$( document ).ready(function() {
	    $('nav').addClass('darkback_int');
	});
</script>
@endsection