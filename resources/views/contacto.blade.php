@extends('layouts.front')

@section('seo')
<!-- SEO -->
<meta name="description" content="Te enseñaremos más de 15 técnicas comprobadas para generar riqueza y multiplicarla en los bienes raices, incluso sin tener capital inicial o algún tipo de experiencia.">
<meta name="keywords" content="universidad, bienes, raices, inmobiliaria, tratos inmobiliarios, cursos, capacitaciones, tallers, conferencias, aprende a ganar, dinero, ganar dinero">
<meta name="robots" content="INDEX,FOLLOW,NOARCHIVE">

<!-- OG SEO -->
<meta property="og:image:width" content="1293">
<meta property="og:image:height" content="677">
<meta property="og:title" content="UBR - Universidad de Bienes Raíces">
<meta property="og:description" content="Te enseñaremos más de 15 técnicas comprobadas para generar riqueza y multiplicarla en los bienes raices, incluso sin tener capital inicial o algún tipo de experiencia.">
<meta property="og:url" content="http://www.universidaddebienesraices.com/">
<meta property="og:image" content="http://www.universidaddebienesraices.com/img/logo.jpg">
@endsection

@section('stylesheets')
<link href="{{ asset('back/assets/plugins/select2/dist/css/select2.min.css') }}" rel="stylesheet" type="text/css" />

<style type="text/css">
    .select2-container{
        display: block;
        width: 100% !important;
    }

    .select2-container--default .select2-selection--multiple {
        border: none !important;
        outline: 0 !important;
        background-color:rgb(234,234,234) !important;
        padding: 15px 20px !important;
        color:#333 !important;
        border-radius: 15px !important;
        overflow: hidden !important;
    }
</style>
@endsection

@section('content')
<section class="jumbotron jumbotron-intro">
	<div class="overlay"></div>
	<div class="container">
		<div class="row align-items-center">
			<div class="col-md-8">
				<div class="jumbotron-padding">
					<h3 class="wow fadeInLeft">Contacta con nosotros</h3>
					<div class="line-divider wow fadeInLeft"></div>

					<p class="content wow fadeInLeft">Nos interesa lo que tienes que decir, si quieres saber sobre nuestros cursos, facilidades de pago o simplemente decir hola, utiliza el siguiente formulario para estar en contacto.</p>
				</div>
			</div>
		</div>
	</div>
</section>

<section class="contact-boxes">
	<div class="container">
		<div class="row">
			<div class="col-md-8">
				<div class="card">
					<div class="card-body">
						<p>Llena el formulario con la información requerida y contestaremos lo más pronto posible.</p>

						<form action="{{ route('contact.store.form') }}" class="mt-4" role="form">
                            {{ csrf_field() }}
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <input type="text" name="name" class="form-control" placeholder="Nombre" autocomplete="name" required autocomplete="name">
                                    </div>
                                </div>

                                <div class="col-md-6">
                                    <div class="form-group">
                                        <input type="text" name="sur_name" class="form-control" placeholder="Apellido" autocomplete="name" required autocomplete="name">
                                    </div>
                                </div>

                                <div class="col-md-6">
                                    <div class="form-group">
                                        <input type="text" name="email" class="form-control" placeholder="Correo Electrónico" autocomplete="name" required autocomplete="name">
                                    </div>
                                </div>

                                <div class="col-md-6">
                                    <div class="form-group">
                                        <input type="phone" name="phone_cel" class="form-control" placeholder="Teléfono" autocomplete="tel" required autocomplete="tel-national">
                                    </div>
                                </div>

                                <div class="col-md-12">
                                    @php
                                        $courses = App\Course::all();
                                    @endphp
                                    <div class="form-group">
                                        <select class="form-control select2 select2-multiple" name="interest_id[]" required="" data-placeholder="Escoge uno o varios intereses." multiple="multiple">
                                            @foreach($courses as $cr)
                                                <option value="{{ $cr->id }}">{{ $cr->title }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                            </div> 

                            <input type="hidden" name="global_origin" value="Formulario Contacto Sitio Web">
                            <input type="hidden" name="from" value="contact-form">
                       
                            <button name="submit" class="btn btn-contact">
                                <i class="fas fa-envelope"></i> Enviar tu información
                            </button>
                        </form> 
					</div>
				</div>
			</div>

			<div class="col-md-4">
				<div class="card">
					<div class="card-body contacto-direcciones">
						<h4 class="pb-4">Otras formas de contacto</h4>
                        <ul class="list-unstyled">
                            <li class="pb-3">
                                <i class="fa fa-phone"></i> +52 55 68330778 
                            </li>

                            <li class="pb-3">
                                <i class="fa fa-phone"></i> +(52) 5536771594
                            </li>

                            <li class="pb-3">
                                <i class="fas fa-envelope"></i> contacto@universidaddebienesraices.com
                            </li>

                            <li class="pb-3">
                                <i class="fab fa-facebook-square"></i> <a href="https://www.facebook.com/universidaddebienesraices" target="_blank">/universidaddebienesraices</a> 
                            </li>  
                        </ul>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>
@endsection

@section('scripts')
<script src="{{ asset('back/assets/plugins/select2/dist/js/select2.full.min.js') }}" type="text/javascript"></script>

<script type="text/javascript">
    $(".select2").select2();
</script>
@endsection