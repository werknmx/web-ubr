@extends('layouts.front')

@section('stylesheets')

@endsection

@section('content')
<section class="jumbotron jumbotron-intro mb-0">
	<div class="overlay"></div>
	<div class="container">
		<div class="row align-items-center">
			<div class="col-md-8 offset-md-2">
				<div class="text-center">
					<p class="uppercase-title wow fadeInUp">Entérate de lo último en Bienes Raíces y la Universidad.</p>
					<h3 class="wow fadeInUp">Blog y Noticias</h3>
				</div>
			</div>
		</div>
	</div>
</section>

<section class="blog">
	<div class="container">
		<div class="row">
			@foreach($autor->publicaciones as $publicacion)
			<div class="col-md-4">
				<a href="{{ route('blog.detalle', $publicacion->slug) }}" class="card-blog wow fadeInUp">
					<div class="image-box">
						<img src="{{ asset('img/blog/covers/' . $publicacion->image) }}">

						<div class="image-hover">
							<div class="btn btn-primary">Leer Artículo</div>
						</div>
					</div>

					<div class="info-box">
						<h6><span>Publicado en:</span> {{ $publicacion->categoria->name }}</h6>
						<h2>{{ $publicacion->title }}</h2>

						<p>{{ $publicacion->summary }}</p>

						<div class="media autor-blog">
							@if( $publicacion->autor->image == NULL)
	                        <img class="mr-3 rounded-circle" src="{{ 'https://www.gravatar.com/avatar/' . md5(strtolower(trim($publicacion->autor->email))) . '?d=retro&s=50' }}" alt="{{ $publicacion->autor->name }}">
	                        @else
	                        <img width="50px" class="mr-3 rounded-circle" src="{{ asset('img/usuarios/' . $publicacion->autor->image ) }}" alt="{{ $publicacion->autor->name }}">
	                        @endif                        
                                
							<div class="media-body">
								<p class="mt-0">
									<span>Escrito por:</span><br>
									{{ $publicacion->autor->name }}
								</p>
							</div>
						</div>
					</div>

				</a>
			</div>
			@endforeach		
		</div>

	</div>
</section>
@endsection

@section('scripts')

@endsection