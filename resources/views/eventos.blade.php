@extends('layouts.front')

@section('seo')
<!-- SEO -->
<meta name="description" content="Te enseñaremos más de 15 técnicas comprobadas para generar riqueza y multiplicarla en los bienes raices, incluso sin tener capital inicial o algún tipo de experiencia.">
<meta name="keywords" content="universidad, bienes, raices, inmobiliaria, tratos inmobiliarios, cursos, capacitaciones, tallers, conferencias, aprende a ganar, dinero, ganar dinero">
<meta name="robots" content="INDEX,FOLLOW,NOARCHIVE">

<!-- OG SEO -->
<meta property="og:image:width" content="1293">
<meta property="og:image:height" content="677">
<meta property="og:title" content="UBR - Universidad de Bienes Raíces">
<meta property="og:description" content="Te enseñaremos más de 15 técnicas comprobadas para generar riqueza y multiplicarla en los bienes raices, incluso sin tener capital inicial o algún tipo de experiencia.">
<meta property="og:url" content="http://www.universidaddebienesraices.com/">
<meta property="og:image" content="http://www.universidaddebienesraices.com/img/logo.jpg">
@endsection

@section('stylesheets')
<!-- Popup CSS -->
<link href="{{ asset('back/assets/plugins/Magnific-Popup-master/dist/magnific-popup.css') }}" rel="stylesheet">
@endsection

@section('content')
<section class="jumbotron jumbotron-intro mb-0">
	<div class="overlay"></div>
	<div class="container">
		<div class="row align-items-center">
			<div class="col-md-8 offset-md-2">
				<div class="text-center">
					<p class="uppercase-title wow fadeInUp">Participa regularmente en los eventos de la universidad</p>
					<h3 class="wow fadeInUp">Calendario de Eventos</h3>
					<div class="line-divider wow fadeInUp"></div>

					<a href="{{ route('contacto') }}" class="btn btn-primary wow fadeInUp">Contacta con nosotros</a>
				</div>
			</div>
		</div>
	</div>
</section>

<section class="cursos">
	<div class="container">
		<div class="row">
			<div class="col-md-9 col-12">
				<div class="tab-content" id="v-pills-tabContent">
					<div role="tabpanel" class="tab-pane fade show active" id="tab0">
                    	@if(count($all_events) == NULL)
                    		<div class="text-center no-event-text"><h3>No hay eventos en este mes, verifica más adelante o <a href="{{ route('contacto') }}">contácta con nosotros</a></h3></div>
                    	@else
						<div class="card-columns">
							@foreach($all_events as $ev)
							<div class="card card-evento">
								<div class="row no-gutters">
									<div class="date-box col-md-3">
										@php
											$meses = array("Ene","Feb","Mar","Abr","May","Jun","Jul","Ago","Sep","Oct","Nov","Dic");
											$time = strtotime($ev->start_date);

											$day = date('d', $time);
											/*$month = date('M', $time);*/

											$month = $meses[date('n', $time)-1];
										@endphp
										<h4>{{ $day }}</h4>
										<h6>{{ $month }}</h6>
									</div>
									<div class="evento-info col-md-9">
										<h5>{{ $ev->type }}</h5>
										<h2>{{ $ev->name }}</h2>
										<p class="description">{{ $ev->description }}</p>

										@if($ev->location == NULL)
										@else
										<p><small><i class="fas fa-location-arrow"></i> {{ $ev->location }}</small></p>
										@endif

										@if($ev->contact_info_phone == NULL)
										@else
										<p><small><i class="fas fa-phone"></i> {{ $ev->contact_info_phone }}</small></p>
										@endif

										@if($ev->contact_info_mail == NULL)
										@else
										<p><small><i class="fas fa-envelope"></i> {{ $ev->contact_info_mail }}</small></p>
										@endif

										<ul class="list-inline">
											<li class="list-inline-item"><i class="far fa-clock"></i> {{ $ev->start_date }}</li>
											<li class="list-inline-item"><i class="far fa-clock"></i> {{ $ev->end_date }}</li>
										</ul>
									</div>
								</div>
							</div>
							@endforeach
						</div>
						@endif
					</div>

					<div role="tabpanel" class="tab-pane fade show" id="tab1">
                    	@if(count($eventos_enero) == NULL)
                    		<div class="text-center no-event-text"><h3>No hay eventos en este mes, verifica más adelante o <a href="{{ route('contacto') }}">contácta con nosotros</a></h3></div>
                    	@else
						<div class="card-columns">
							@foreach($eventos_enero as $ev)
							<div class="card card-evento">
								<div class="row no-gutters">
									<div class="date-box col-md-3">
										@php
											$meses = array("Ene","Feb","Mar","Abr","May","Jun","Jul","Ago","Sep","Oct","Nov","Dic");

											$time = strtotime($ev->start_date);

											$day = date('d', $time);
											/*$month = date('M', $time);*/

											$month = $meses[date('n', $time)-1];
										@endphp
										<h4>{{ $day }}</h4>
										<h6>{{ $month }}</h6>
									</div>
									<div class="evento-info col-md-9">
										<h5>{{ $ev->type }}</h5>
										<h2>{{ $ev->name }}</h2>
										<p class="description">{{ $ev->description }}</p>

										@if($ev->location == NULL)
										@else
										<p><small><i class="fas fa-location-arrow"></i> {{ $ev->location }}</small></p>
										@endif

										@if($ev->contact_info_phone == NULL)
										@else
										<p><small><i class="fas fa-phone"></i> {{ $ev->contact_info_phone }}</small></p>
										@endif

										@if($ev->contact_info_mail == NULL)
										@else
										<p><small><i class="fas fa-envelope"></i> {{ $ev->contact_info_email }}</small></p>
										@endif

										<ul class="list-inline">
											<li class="list-inline-item"><i class="far fa-clock"></i> {{ $ev->start_date }}</li>
											<li class="list-inline-item"><i class="far fa-clock"></i> {{ $ev->end_date }}</li>
										</ul>
									</div>
								</div>
							</div>
							@endforeach
						</div>
						@endif
					</div>

                    <div role="tabpanel" class="tab-pane fade show" id="tab2">
                    	@if(count($eventos_febrero) == NULL)
                    		<div class="text-center no-event-text"><h3>No hay eventos en este mes, verifica más adelante o <a href="{{ route('contacto') }}">contácta con nosotros</a></h3></div>
                    	@else
						<div class="card-columns">
							@foreach($eventos_febrero as $ev)
							<div class="card card-evento">
								<div class="row no-gutters">
									<div class="date-box col-md-3">
										@php
											$meses = array("Ene","Feb","Mar","Abr","May","Jun","Jul","Ago","Sep","Oct","Nov","Dic");

											$time = strtotime($ev->start_date);

											$day = date('d', $time);
											/*$month = date('M', $time);*/

											$month = $meses[date('n', $time)-1];
										@endphp
										<h4>{{ $day }}</h4>
										<h6>{{ $month }}</h6>
									</div>
									<div class="evento-info col-md-9">
										<h5>{{ $ev->type }}</h5>
										<h2>{{ $ev->name }}</h2>
										<p class="description">{{ $ev->description }}</p>

										@if($ev->location == NULL)
										@else
										<p><small><i class="fas fa-location-arrow"></i> {{ $ev->location }}</small></p>
										@endif

										@if($ev->contact_info_phone == NULL)
										@else
										<p><small><i class="fas fa-phone"></i> {{ $ev->contact_info_phone }}</small></p>
										@endif

										@if($ev->contact_info_mail == NULL)
										@else
										<p><small><i class="fas fa-envelope"></i> {{ $ev->contact_info_email }}</small></p>
										@endif

										<ul class="list-inline">
											<li class="list-inline-item"><i class="far fa-clock"></i> {{ $ev->start_date }}</li>
											<li class="list-inline-item"><i class="far fa-clock"></i> {{ $ev->end_date }}</li>
										</ul>
									</div>
								</div>
							</div>
							@endforeach
						</div>
						@endif
					</div>

					<div role="tabpanel" class="tab-pane fade show" id="tab3">
                    	@if(count($eventos_marzo) == NULL)
                    		<div class="text-center no-event-text"><h3>No hay eventos en este mes, verifica más adelante o <a href="{{ route('contacto') }}">contácta con nosotros</a></h3></div>
                    	@else
						<div class="card-columns">
							@foreach($eventos_marzo as $ev)
							<div class="card card-evento">
								<div class="row no-gutters">
									<div class="date-box col-md-3">
										@php
											$meses = array("Ene","Feb","Mar","Abr","May","Jun","Jul","Ago","Sep","Oct","Nov","Dic");

											$time = strtotime($ev->start_date);

											$day = date('d', $time);
											/*$month = date('M', $time);*/

											$month = $meses[date('n', $time)-1];
										@endphp
										<h4>{{ $day }}</h4>
										<h6>{{ $month }}</h6>
									</div>
									<div class="evento-info col-md-9">
										<h5>{{ $ev->type }}</h5>
										<h2>{{ $ev->name }}</h2>
										<p class="description">{{ $ev->description }}</p>

										@if($ev->location == NULL)
										@else
										<p><small><i class="fas fa-location-arrow"></i> {{ $ev->location }}</small></p>
										@endif

										@if($ev->contact_info_phone == NULL)
										@else
										<p><small><i class="fas fa-phone"></i> {{ $ev->contact_info_phone }}</small></p>
										@endif

										@if($ev->contact_info_mail == NULL)
										@else
										<p><small><i class="fas fa-envelope"></i> {{ $ev->contact_info_email }}</small></p>
										@endif

										<ul class="list-inline">
											<li class="list-inline-item"><i class="far fa-clock"></i> {{ $ev->start_date }}</li>
											<li class="list-inline-item"><i class="far fa-clock"></i> {{ $ev->end_date }}</li>
										</ul>
									</div>
								</div>
							</div>
							@endforeach
						</div>
						@endif
					</div>

					<div role="tabpanel" class="tab-pane fade show" id="tab4">
                    	@if(count($eventos_abril) == NULL)
                    		<div class="text-center no-event-text"><h3>No hay eventos en este mes, verifica más adelante o <a href="{{ route('contacto') }}">contácta con nosotros</a></h3></div>
                    	@else
						<div class="card-columns">
							@foreach($eventos_abril as $ev)
							<div class="card card-evento">
								<div class="row no-gutters">
									<div class="date-box col-md-3">
										@php
											$meses = array("Ene","Feb","Mar","Abr","May","Jun","Jul","Ago","Sep","Oct","Nov","Dic");

											$time = strtotime($ev->start_date);

											$day = date('d', $time);
											/*$month = date('M', $time);*/

											$month = $meses[date('n', $time)-1];
										@endphp
										<h4>{{ $day }}</h4>
										<h6>{{ $month }}</h6>
									</div>
									<div class="evento-info col-md-9">
										<h5>{{ $ev->type }}</h5>
										<h2>{{ $ev->name }}</h2>
										<p class="description">{{ $ev->description }}</p>

										@if($ev->location == NULL)
										@else
										<p><small><i class="fas fa-location-arrow"></i> {{ $ev->location }}</small></p>
										@endif

										@if($ev->contact_info_phone == NULL)
										@else
										<p><small><i class="fas fa-phone"></i> {{ $ev->contact_info_phone }}</small></p>
										@endif

										@if($ev->contact_info_mail == NULL)
										@else
										<p><small><i class="fas fa-envelope"></i> {{ $ev->contact_info_email }}</small></p>
										@endif

										<ul class="list-inline">
											<li class="list-inline-item"><i class="far fa-clock"></i> {{ $ev->start_date }}</li>
											<li class="list-inline-item"><i class="far fa-clock"></i> {{ $ev->end_date }}</li>
										</ul>
									</div>
								</div>
							</div>
							@endforeach
						</div>
						@endif
					</div>

					<div role="tabpanel" class="tab-pane fade show" id="tab5">
                    	@if(count($eventos_mayo) == NULL)
                    		<div class="text-center no-event-text"><h3>No hay eventos en este mes, verifica más adelante o <a href="{{ route('contacto') }}">contácta con nosotros</a></h3></div>
                    	@else
						<div class="card-columns">
							@foreach($eventos_mayo as $ev)
							<div class="card card-evento">
								<div class="row no-gutters">
									<div class="date-box col-md-3">
										@php
											$meses = array("Ene","Feb","Mar","Abr","May","Jun","Jul","Ago","Sep","Oct","Nov","Dic");

											$time = strtotime($ev->start_date);

											$day = date('d', $time);
											/*$month = date('M', $time);*/

											$month = $meses[date('n', $time)-1];
										@endphp
										<h4>{{ $day }}</h4>
										<h6>{{ $month }}</h6>
									</div>
									<div class="evento-info col-md-9">
										<h5>{{ $ev->type }}</h5>
										<h2>{{ $ev->name }}</h2>
										<p class="description">{{ $ev->description }}</p>

										@if($ev->location == NULL)
										@else
										<p><small><i class="fas fa-location-arrow"></i> {{ $ev->location }}</small></p>
										@endif

										@if($ev->contact_info_phone == NULL)
										@else
										<p><small><i class="fas fa-phone"></i> {{ $ev->contact_info_phone }}</small></p>
										@endif

										@if($ev->contact_info_mail == NULL)
										@else
										<p><small><i class="fas fa-envelope"></i> {{ $ev->contact_info_email }}</small></p>
										@endif

										<ul class="list-inline">
											<li class="list-inline-item"><i class="far fa-clock"></i> {{ $ev->start_date }}</li>
											<li class="list-inline-item"><i class="far fa-clock"></i> {{ $ev->end_date }}</li>
										</ul>
									</div>
								</div>
							</div>
							@endforeach
						</div>
						@endif
					</div>

					<div role="tabpanel" class="tab-pane fade show" id="tab6">
                    	@if(count($eventos_junio) == NULL)
                    		<div class="text-center no-event-text"><h3>No hay eventos en este mes, verifica más adelante o <a href="{{ route('contacto') }}">contácta con nosotros</a></h3></div>
                    	@else
						<div class="card-columns">
							@foreach($eventos_junio as $ev)
							<div class="card card-evento">
								<div class="row no-gutters">
									<div class="date-box col-md-3">
										@php
											$meses = array("Ene","Feb","Mar","Abr","May","Jun","Jul","Ago","Sep","Oct","Nov","Dic");

											$time = strtotime($ev->start_date);

											$day = date('d', $time);
											/*$month = date('M', $time);*/

											$month = $meses[date('n', $time)-1];
										@endphp
										<h4>{{ $day }}</h4>
										<h6>{{ $month }}</h6>
									</div>
									<div class="evento-info col-md-9">
										<h5>{{ $ev->type }}</h5>
										<h2>{{ $ev->name }}</h2>
										<p class="description">{{ $ev->description }}</p>

										@if($ev->location == NULL)
										@else
										<p><small><i class="fas fa-location-arrow"></i> {{ $ev->location }}</small></p>
										@endif

										@if($ev->contact_info_phone == NULL)
										@else
										<p><small><i class="fas fa-phone"></i> {{ $ev->contact_info_phone }}</small></p>
										@endif

										@if($ev->contact_info_mail == NULL)
										@else
										<p><small><i class="fas fa-envelope"></i> {{ $ev->contact_info_email }}</small></p>
										@endif

										<ul class="list-inline">
											<li class="list-inline-item"><i class="far fa-clock"></i> {{ $ev->start_date }}</li>
											<li class="list-inline-item"><i class="far fa-clock"></i> {{ $ev->end_date }}</li>
										</ul>
									</div>
								</div>
							</div>
							@endforeach
						</div>
						@endif
					</div>

					<div role="tabpanel" class="tab-pane fade show" id="tab7">
                    	@if(count($eventos_julio) == NULL)
                    		<div class="text-center no-event-text"><h3>No hay eventos en este mes, verifica más adelante o <a href="{{ route('contacto') }}">contácta con nosotros</a></h3></div>
                    	@else
						<div class="card-columns">
							@foreach($eventos_julio as $ev)
							<div class="card card-evento">
								<div class="row no-gutters">
									<div class="date-box col-md-3">
										@php
											$meses = array("Ene","Feb","Mar","Abr","May","Jun","Jul","Ago","Sep","Oct","Nov","Dic");

											$time = strtotime($ev->start_date);

											$day = date('d', $time);
											/*$month = date('M', $time);*/

											$month = $meses[date('n', $time)-1];
										@endphp
										<h4>{{ $day }}</h4>
										<h6>{{ $month }}</h6>
									</div>
									<div class="evento-info col-md-9">
										<h5>{{ $ev->type }}</h5>
										<h2>{{ $ev->name }}</h2>
										<p class="description">{{ $ev->description }}</p>

										@if($ev->location == NULL)
										@else
										<p><small><i class="fas fa-location-arrow"></i> {{ $ev->location }}</small></p>
										@endif

										@if($ev->contact_info_phone == NULL)
										@else
										<p><small><i class="fas fa-phone"></i> {{ $ev->contact_info_phone }}</small></p>
										@endif

										@if($ev->contact_info_mail == NULL)
										@else
										<p><small><i class="fas fa-envelope"></i> {{ $ev->contact_info_email }}</small></p>
										@endif

										<ul class="list-inline">
											<li class="list-inline-item"><i class="far fa-clock"></i> {{ $ev->start_date }}</li>
											<li class="list-inline-item"><i class="far fa-clock"></i> {{ $ev->end_date }}</li>
										</ul>
									</div>
								</div>
							</div>
							@endforeach
						</div>
						@endif
					</div>

					<div role="tabpanel" class="tab-pane fade show" id="tab8">
                    	@if(count($eventos_agosto) == NULL)
                    		<div class="text-center no-event-text"><h3>No hay eventos en este mes, verifica más adelante o <a href="{{ route('contacto') }}">contácta con nosotros</a></h3></div>
                    	@else
						<div class="card-columns">
							@foreach($eventos_agosto as $ev)
							<div class="card card-evento">
								<div class="row no-gutters">
									<div class="date-box col-md-3">
										@php
											$meses = array("Ene","Feb","Mar","Abr","May","Jun","Jul","Ago","Sep","Oct","Nov","Dic");

											$time = strtotime($ev->start_date);

											$day = date('d', $time);
											/*$month = date('M', $time);*/

											$month = $meses[date('n', $time)-1];
										@endphp
										<h4>{{ $day }}</h4>
										<h6>{{ $month }}</h6>
									</div>
									<div class="evento-info col-md-9">
										<h5>{{ $ev->type }}</h5>
										<h2>{{ $ev->name }}</h2>
										<p class="description">{{ $ev->description }}</p>

										@if($ev->location == NULL)
										@else
										<p><small><i class="fas fa-location-arrow"></i> {{ $ev->location }}</small></p>
										@endif

										@if($ev->contact_info_phone == NULL)
										@else
										<p><small><i class="fas fa-phone"></i> {{ $ev->contact_info_phone }}</small></p>
										@endif

										@if($ev->contact_info_mail == NULL)
										@else
										<p><small><i class="fas fa-envelope"></i> {{ $ev->contact_info_email }}</small></p>
										@endif

										<ul class="list-inline">
											<li class="list-inline-item"><i class="far fa-clock"></i> {{ $ev->start_date }}</li>
											<li class="list-inline-item"><i class="far fa-clock"></i> {{ $ev->end_date }}</li>
										</ul>
									</div>
								</div>
							</div>
							@endforeach
						</div>
						@endif
					</div>

					<div role="tabpanel" class="tab-pane fade show" id="tab9">
                    	@if(count($eventos_septiembre) == NULL)
                    		<div class="text-center no-event-text"><h3>No hay eventos en este mes, verifica más adelante o <a href="{{ route('contacto') }}">contácta con nosotros</a></h3></div>
                    	@else
						<div class="card-columns">
							@foreach($eventos_septiembre as $ev)
							<div class="card card-evento">
								<div class="row no-gutters">
									<div class="date-box col-md-3">
										@php
											$meses = array("Ene","Feb","Mar","Abr","May","Jun","Jul","Ago","Sep","Oct","Nov","Dic");

											$time = strtotime($ev->start_date);

											$day = date('d', $time);
											/*$month = date('M', $time);*/

											$month = $meses[date('n', $time)-1];
										@endphp
										<h4>{{ $day }}</h4>
										<h6>{{ $month }}</h6>
									</div>
									<div class="evento-info col-md-9">
										<h5>{{ $ev->type }}</h5>
										<h2>{{ $ev->name }}</h2>
										<p class="description">{{ $ev->description }}</p>

										@if($ev->location == NULL)
										@else
										<p><small><i class="fas fa-location-arrow"></i> {{ $ev->location }}</small></p>
										@endif

										@if($ev->contact_info_phone == NULL)
										@else
										<p><small><i class="fas fa-phone"></i> {{ $ev->contact_info_phone }}</small></p>
										@endif

										@if($ev->contact_info_mail == NULL)
										@else
										<p><small><i class="fas fa-envelope"></i> {{ $ev->contact_info_email }}</small></p>
										@endif

										<ul class="list-inline">
											<li class="list-inline-item"><i class="far fa-clock"></i> {{ $ev->start_date }}</li>
											<li class="list-inline-item"><i class="far fa-clock"></i> {{ $ev->end_date }}</li>
										</ul>
									</div>
								</div>
							</div>
							@endforeach
						</div>
						@endif
					</div>

					<div role="tabpanel" class="tab-pane fade show" id="tab10">
                    	@if(count($eventos_octubre) == NULL)
                    		<div class="text-center no-event-text"><h3>No hay eventos en este mes, verifica más adelante o <a href="{{ route('contacto') }}">contácta con nosotros</a></h3></div>
                    	@else
						<div class="card-columns">
							@foreach($eventos_octubre as $ev)
							<div class="card card-evento">
								<div class="row no-gutters">
									<div class="date-box col-md-3">
										@php
											$meses = array("Ene","Feb","Mar","Abr","May","Jun","Jul","Ago","Sep","Oct","Nov","Dic");

											$time = strtotime($ev->start_date);

											$day = date('d', $time);
											/*$month = date('M', $time);*/

											$month = $meses[date('n', $time)-1];
										@endphp
										<h4>{{ $day }}</h4>
										<h6>{{ $month }}</h6>
									</div>
									<div class="evento-info col-md-9">
										<h5>{{ $ev->type }}</h5>
										<h2>{{ $ev->name }}</h2>
										<p class="description">{{ $ev->description }}</p>

										@if($ev->location == NULL)
										@else
										<p><small><i class="fas fa-location-arrow"></i> {{ $ev->location }}</small></p>
										@endif

										@if($ev->contact_info_phone == NULL)
										@else
										<p><small><i class="fas fa-phone"></i> {{ $ev->contact_info_phone }}</small></p>
										@endif

										@if($ev->contact_info_mail == NULL)
										@else
										<p><small><i class="fas fa-envelope"></i> {{ $ev->contact_info_email }}</small></p>
										@endif

										<ul class="list-inline">
											<li class="list-inline-item"><i class="far fa-clock"></i> {{ $ev->start_date }}</li>
											<li class="list-inline-item"><i class="far fa-clock"></i> {{ $ev->end_date }}</li>
										</ul>
									</div>
								</div>
							</div>
							@endforeach
						</div>
						@endif
					</div>

					<div role="tabpanel" class="tab-pane fade show" id="tab11">
                    	@if(count($eventos_noviembre) == NULL)
                    		<div class="text-center no-event-text"><h3>No hay eventos en este mes, verifica más adelante o <a href="{{ route('contacto') }}">contácta con nosotros</a></h3></div>
                    	@else
						<div class="card-columns">
							@foreach($eventos_noviembre as $ev)
							<div class="card card-evento">
								<div class="row no-gutters">
									<div class="date-box col-md-3">
										@php
											$meses = array("Ene","Feb","Mar","Abr","May","Jun","Jul","Ago","Sep","Oct","Nov","Dic");

											$time = strtotime($ev->start_date);

											$day = date('d', $time);
											/*$month = date('M', $time);*/

											$month = $meses[date('n', $time)-1];
										@endphp
										<h4>{{ $day }}</h4>
										<h6>{{ $month }}</h6>
									</div>
									<div class="evento-info col-md-9">
										<h5>{{ $ev->type }}</h5>
										<h2>{{ $ev->name }}</h2>
										<p class="description">{{ $ev->description }}</p>

										@if($ev->location == NULL)
										@else
										<p><small><i class="fas fa-location-arrow"></i> {{ $ev->location }}</small></p>
										@endif

										@if($ev->contact_info_phone == NULL)
										@else
										<p><small><i class="fas fa-phone"></i> {{ $ev->contact_info_phone }}</small></p>
										@endif

										@if($ev->contact_info_mail == NULL)
										@else
										<p><small><i class="fas fa-envelope"></i> {{ $ev->contact_info_email }}</small></p>
										@endif

										<ul class="list-inline">
											<li class="list-inline-item"><i class="far fa-clock"></i> {{ $ev->start_date }}</li>
											<li class="list-inline-item"><i class="far fa-clock"></i> {{ $ev->end_date }}</li>
										</ul>
									</div>
								</div>
							</div>
							@endforeach
						</div>
						@endif
					</div>

					<div role="tabpanel" class="tab-pane fade show" id="tab12">
                    	@if(count($eventos_diciembre) == NULL)
                    		<div class="text-center no-event-text"><h3>No hay eventos en este mes, verifica más adelante o <a href="{{ route('contacto') }}">contácta con nosotros</a></h3></div>
                    	@else
						<div class="card-columns">
							@foreach($eventos_diciembre as $ev)
							<div class="card card-evento">
								<div class="row no-gutters">
									<div class="date-box col-md-3">
										@php
											$meses = array("Ene","Feb","Mar","Abr","May","Jun","Jul","Ago","Sep","Oct","Nov","Dic");

											$time = strtotime($ev->start_date);

											$day = date('d', $time);
											/*$month = date('M', $time);*/

											$month = $meses[date('n', $time)-1];
										@endphp
										<h4>{{ $day }}</h4>
										<h6>{{ $month }}</h6>
									</div>
									<div class="evento-info col-md-9">
										<h5>{{ $ev->type }}</h5>
										<h2>{{ $ev->name }}</h2>
										<p class="description">{{ $ev->description }}</p>

										@if($ev->location == NULL)
										@else
										<p><small><i class="fas fa-location-arrow"></i> {{ $ev->location }}</small></p>
										@endif

										@if($ev->contact_info_phone == NULL)
										@else
										<p><small><i class="fas fa-phone"></i> {{ $ev->contact_info_phone }}</small></p>
										@endif

										@if($ev->contact_info_mail == NULL)
										@else
										<p><small><i class="fas fa-envelope"></i> {{ $ev->contact_info_email }}</small></p>
										@endif

										<ul class="list-inline">
											<li class="list-inline-item"><i class="far fa-clock"></i> {{ $ev->start_date }}</li>
											<li class="list-inline-item"><i class="far fa-clock"></i> {{ $ev->end_date }}</li>
										</ul>
									</div>
								</div>
							</div>
							@endforeach
						</div>
						@endif
					</div>
				</div>
			</div>

			<div class="col-md-3 col-12">
				<ul class="nav nav-pills nav-stacked" id="v-pills-tab" role="tablist" aria-orientation="vertical">
					<li class="main-btn"><a href="#tab0" aria-controls="tab0" role="tab" data-toggle="tab">Todos los Eventos <i class="fas fa-arrow-right"></i></a></li>
					<li>
						<a href="#tab1" aria-controls="tab1" role="tab" data-toggle="tab">
						<i class="fas fa-caret-right"></i> Enero 
						@if($eventos_enero->count() == NULL)
						@else
						<span class="badge badge-pill badge-success">{{ $eventos_enero->count() }}</span>
						@endif
						</a>
					</li>

					<li>
						<a href="#tab2" aria-controls="tab2" role="tab" data-toggle="tab">
							<i class="fas fa-caret-right"></i> Febrero 
							@if($eventos_febrero->count() == NULL)
							@else
							<span class="badge badge-pill badge-success">{{ $eventos_febrero->count() }}</span>
							@endif
						</a>
					</li>

					<li>
						<a href="#tab3" aria-controls="tab3" role="tab" data-toggle="tab">
							<i class="fas fa-caret-right"></i> Marzo 
							@if($eventos_marzo->count() == NULL)
							@else
							<span class="badge badge-pill badge-success">{{ $eventos_marzo->count() }}</span>
							@endif
						</a>
					</li>

					<li>
						<a href="#tab4" aria-controls="tab4" role="tab" data-toggle="tab">
							<i class="fas fa-caret-right"></i> Abril 
							@if($eventos_abril->count() == NULL)
							@else
							<span class="badge badge-pill badge-success">{{ $eventos_abril->count() }}</span>
							@endif
						</a>
					</li>

					<li>
						<a href="#tab5" aria-controls="tab5" role="tab" data-toggle="tab">
							<i class="fas fa-caret-right"></i> Mayo 
							@if($eventos_mayo->count() == NULL)
							@else
							<span class="badge badge-pill badge-success">{{ $eventos_mayo->count() }}</span>
							@endif
						</a>
					</li>

					<li>
						<a href="#tab6" aria-controls="tab6" role="tab" data-toggle="tab">
							<i class="fas fa-caret-right"></i> Junio 
							@if($eventos_junio->count() == NULL)
							@else
							<span class="badge badge-pill badge-success">{{ $eventos_junio->count() }}</span>
							@endif
						</a>
					</li>

					<li>
						<a href="#tab7" aria-controls="tab7" role="tab" data-toggle="tab">
							<i class="fas fa-caret-right"></i> Julio 
							@if($eventos_julio->count() == NULL)
							@else
							<span class="badge badge-pill badge-success">{{ $eventos_julio->count() }}</span>
							@endif
						</a>
					</li>

					<li>
						<a href="#tab8" aria-controls="tab8" role="tab" data-toggle="tab">
							<i class="fas fa-caret-right"></i> Agosto 
							@if($eventos_agosto->count() == NULL)
							@else
							<span class="badge badge-pill badge-success">{{ $eventos_agosto->count() }}</span>
							@endif
						</a>
					</li>

					<li>
						<a href="#tab9" aria-controls="tab9" role="tab" data-toggle="tab">
							<i class="fas fa-caret-right"></i> Septiembre 
							@if($eventos_septiembre->count() == NULL)
							@else
							<span class="badge badge-pill badge-success">{{ $eventos_septiembre->count() }}</span>
							@endif
						</a>
					</li>

					<li>
						<a href="#tab10" aria-controls="tab10" role="tab" data-toggle="tab">
							<i class="fas fa-caret-right"></i> Octubre 
							@if($eventos_octubre->count() == NULL)
							@else
							<span class="badge badge-pill badge-success">{{ $eventos_octubre->count() }}</span>
							@endif
						</a>
					</li>

					<li>
						<a href="#tab11" aria-controls="tab11" role="tab" data-toggle="tab">
							<i class="fas fa-caret-right"></i> Noviembre 
							@if($eventos_noviembre->count() == NULL)
							@else
							<span class="badge badge-pill badge-success">{{ $eventos_noviembre->count() }}</span>
							@endif
						</a>
					</li>

					<li>
						<a href="#tab12" aria-controls="tab12" role="tab" data-toggle="tab">
							<i class="fas fa-caret-right"></i> Diciembre 
							@if($eventos_diciembre->count() == NULL)
							@else
							<span class="badge badge-pill badge-success">{{ $eventos_diciembre->count() }}</span>
							@endif
						</a>
					</li>
				</ul>
			</div>
			<div class="clearfix visible-lg"></div>
		</div>
	</div>
</section>

<section class="gallery">
	<div class="row no-gutters">
		<div class="col-12 wow fadeInUp">
            <div class="swiper-container swiper-gallery">
                <div class="swiper-wrapper">
                    @foreach($galerias as $galeria)
                    <div class="swiper-slide">
                        <div class="card card-galeria card-galeria-full">
                            <a class="image-popup-vertical-fit" href="{{ asset('img/galerias/covers/' . $galeria->cover_image) }}"> <img src="{{ asset('img/galerias/covers/' . $galeria->cover_image) }}" alt="{{ $galeria->title }}" /> 
                            </a>

                            @if($galeria->imagenes->count())
                                <a href="{{ route('galeria.detalle', $galeria->slug) }}" class="more-images">Ver todas las imágenes</a>
                            @endif

                            <div class="card-galeria-content">
                                <h4>{{ $galeria->title }}</h4>
                                <p>{{ $galeria->body }}</p>
                            </div>
                        </div>
                        
                    </div>
                    @endforeach
                </div>
            </div>
		</div>
	</div>
</section>
@endsection

@section('scripts')
<script src="{{ asset('back/assets/plugins/Magnific-Popup-master/dist/jquery.magnific-popup.min.js') }}"></script>
<script src="{{ asset('back/assets/plugins/Magnific-Popup-master/dist/jquery.magnific-popup-init.js') }}"></script>

<!-- Initialize Swiper -->
<script>
var swiper = new Swiper('.swiper-gallery', {
  slidesPerView: '3',
  spaceBetween: 0,
  loop: true,
  pagination: {
    el: '.swiper-pagination',
    clickable: true,
  },

  breakpoints: {
    // when window width is <= 320px
    320: {
      slidesPerView: 1,
      spaceBetween: 10
    },
    // when window width is <= 480px
    480: {
      slidesPerView: 1,
      spaceBetween: 20
    },
    // when window width is <= 640px
    640: {
      slidesPerView: 2,
      spaceBetween: 30
    }
  }
});
</script>
@endsection