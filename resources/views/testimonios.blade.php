@extends('layouts.front')

@section('seo')
<!-- SEO -->
<meta name="description" content="Te enseñaremos más de 15 técnicas comprobadas para generar riqueza y multiplicarla en los bienes raices, incluso sin tener capital inicial o algún tipo de experiencia.">
<meta name="keywords" content="universidad, bienes, raices, inmobiliaria, tratos inmobiliarios, cursos, capacitaciones, tallers, conferencias, aprende a ganar, dinero, ganar dinero">
<meta name="robots" content="INDEX,FOLLOW,NOARCHIVE">

<!-- OG SEO -->
<meta property="og:image:width" content="1293">
<meta property="og:image:height" content="677">
<meta property="og:title" content="UBR - Universidad de Bienes Raíces">
<meta property="og:description" content="Te enseñaremos más de 15 técnicas comprobadas para generar riqueza y multiplicarla en los bienes raices, incluso sin tener capital inicial o algún tipo de experiencia.">
<meta property="og:url" content="http://www.universidaddebienesraices.com/">
<meta property="og:image" content="http://www.universidaddebienesraices.com/img/logo.jpg">
@endsection

@section('stylesheets')

@endsection

@section('content')
<section class="jumbotron jumbotron-intro mb-0">
	<div class="overlay"></div>
	<div class="container">
		<div class="row align-items-center">
			<div class="col-md-8 offset-md-2">
				<div class="text-center">
					<h3 class="wow fadeInUp">Testimonios</h3>
					<div class="line-divider wow fadeInUp"></div>

					<p class="content wow fadeInUp">Nuestro alumnado está preparado para los nuevos retos que se presentan en el ámbito de los Bienes Raíces. Descubre lo que dicen los Ex-Alumnos.</p>
				</div>
			</div>
		</div>
	</div>
</section>

<section class="testimonios">
    <div class="container">
        <div class="col-md-12">
            <h2 class="big-title wow fadeInUp">Testimonios de nuestro alumnado</h2>
        </div>

        <div class="row">
            <div class="col-md-6">
                <div class="card wow fadeInUp">
                    <iframe width="100%" height="315" src="https://www.youtube.com/embed/yMe7qknB27U" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

                    <div class="card-body">
                        <h3>Alicia de León</h3>
                        <p>Entrenamiento Intensivo CDMX</p>
                    </div>
                </div>
            </div>
            <div class="col-md-6">
                <div class="card wow fadeInUp">
                    <iframe width="100%" height="315" src="https://www.youtube.com/embed/JlVHUcEQkBU" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

                    <div class="card-body">
                        <h3>Ricardo Gómez</h3>
                        <p>Entrenamiento Intensivo CDMX</p>
                    </div>
                </div>
            </div>

            <div class="col-md-6">
                <div class="card wow fadeInUp">
                    <iframe width="100%" height="315" src="https://www.youtube.com/embed/PcVinHTns3M" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

                    <div class="card-body">
                        <h3>Comentario</h3>
                        <p>Entrenamiento Intensivo CDMX</p>
                    </div>
                </div>
            </div>
            <div class="col-md-6">
                <div class="card wow fadeInUp">
                    <iframe width="100%" height="315" src="https://www.youtube.com/embed/9zhmPYNXj9k" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

                    <div class="card-body">
                        <h3>Comentario</h3>
                        <p>Entrenamiento Intensivo CDMX</p>
                    </div>
                </div>
            </div>

            <div class="col-md-6">
                <div class="card wow fadeInUp">
                    <iframe width="100%" height="315" src="https://www.youtube.com/embed/feTm3aJJhws" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

                    <div class="card-body">
                        <h3>Alexander Iriarte</h3>
                        <p>Entrenamiento Intensivo CDMX</p>
                    </div>
                </div>
            </div>
            <div class="col-md-6">
                <div class="card wow fadeInUp">
                    <iframe width="100%" height="315" src="https://www.youtube.com/embed/Ximtl8IOrE4" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

                    <div class="card-body">
                        <h3>Comentario</h3>
                        <p>Entrenamiento Intensivo CDMX</p>
                    </div>
                </div>
            </div>
        </div>

        <!--
        <div class="swiper-container wow fadeInUp">
            <div class="swiper-wrapper">
                <div class="swiper-slide">
                    <div class="testimonial-box">
                        <div class="testimonial-text">
                            <h5>David</h5>
                            <p>Juan nos dio mucha motivación, es perder el miedo a quitarse el no puedo, de ir siempre hacia adelante. Mi negocio lo hice sin capital, apliqué la técnica de compra-venta y hoy tengo un negocio millonario con más de 400% de utilidad.</p>
                        </div>

                        <div class="testimonial-image">
                            <img src="{{ asset('img/testimonial-image-1.png') }}">
                        </div>

                        <span class="icon-custom icon-quote-start"></span>
                        <span class="icon-custom icon-quote-end"></span>
                    </div>
                </div>

                <div class="swiper-slide">
                    <div class="testimonial-box">
                        <div class="testimonial-image">
                            <img src="{{ asset('img/testimonial-image-2.png') }}">
                        </div>

                        <div class="testimonial-text">
                            <h5>Joselyn</h5>
                            <p>Tengo 5 años en Bienes Raíces, pero Juan me ayudó a aprender muchas técnicas que antes no aplicaba. Estoy muy agradecida porque me llevo un gran conocimiento y conocí mucha gente con Universidad de Bienes Raíces</p>
                        </div>

                        <span class="icon-custom icon-quote-start"></span>
                        <span class="icon-custom icon-quote-end"></span>
                    </div>
                </div>

                <div class="swiper-slide">
                    <div class="testimonial-box">
                        <div class="testimonial-image">
                            <img src="{{ asset('img/testimonial-image-3.png') }}">
                        </div>

                        <div class="testimonial-text">
                            <h5>Jose Luís Lumbreas</h5>
                            <p>Poniendo en práctica las técnicas aprendidas logre, mejorar mis ventas sin dinero, una de ellas conseguir una propiedad en obra negra sin dinero.</p>
                        </div>

                        <span class="icon-custom icon-quote-start"></span>
                        <span class="icon-custom icon-quote-end"></span>
                    </div>
                </div>

                <div class="swiper-slide">
                    <div class="testimonial-box">
                        <div class="testimonial-text">
                            <h5>Juan Martín</h5>
                            <p>Aprendí distintas técnicas para optimizar mi dinero y asi generar más ingreso a largo plazo. Recomiendo ampliamente este curso  por las técnicas exclusivas que manejan aquí, saldrán con mucho conocimiento para poder aplicarlo.</p>
                        </div>

                        <div class="testimonial-image">
                            <img src="{{ asset('img/testimonial-image-3.png') }}">
                        </div>

                        <span class="icon-custom icon-quote-start"></span>
                        <span class="icon-custom icon-quote-end"></span>
                    </div>
                </div>

                <div class="swiper-slide">
                    <div class="testimonial-box">
                        <div class="testimonial-image">
                            <img src="{{ asset('img/testimonial-image-3.png') }}">
                        </div>

                        <div class="testimonial-text">
                            <h5>Andrés Zarazuza</h5>
                            <p>Asistí al seminario de inversiones de Bienes Raíces por la inquietud de cómo funcionaban, una vez que tome el curso me di cuenta que hay muchas herramientas para generar dinero. Lo recomiendo ampliamente porque si te cambia mucho tu percepción y tu manera de ver las cosas.</p>
                        </div>

                        <span class="icon-custom icon-quote-start"></span>
                        <span class="icon-custom icon-quote-end"></span>
                    </div>
                </div>

                <div class="swiper-slide">
                    <div class="testimonial-box">
                        <div class="testimonial-text">
                            <h5>Maricarmen Peredo</h5>
                            <p>Juan Carlos es un gran maestro. El entrenamiento te lo recomiendo a ti que eres un padre de familia, para que empieces a preparar a tu hijo ya que el mundo de afuera necesita gente triunfadora, gente con éxito, y Juan Carlos es la persona idónea para enseñarte el conocimiento y poder de cambiar tu vida no solo económicamente, sino en todas las demás áreas de tu vida.</p>
                        </div>

                        <div class="testimonial-image">
                            <img src="{{ asset('img/testimonial-image-1.png') }}">
                        </div>

                        <span class="icon-custom icon-quote-start"></span>
                        <span class="icon-custom icon-quote-end"></span>
                    </div>
                </div>

                <div class="swiper-slide">
                    <div class="testimonial-box">
                        <div class="testimonial-image">
                            <img src="{{ asset('img/testimonial-image-2.png') }}">
                        </div>

                        <div class="testimonial-text">
                            <h5>Mario Bueno</h5>
                            <p>Definitivamente es un curso bastante idóneo para todas aquellas personas que quieran incursionar en Bienes Raíces. Actualmente  la manera más efectiva para hacer negocios es educándonos, y en esta universidad maximizar tus conocimientos.</p>
                        </div>

                        <span class="icon-custom icon-quote-start"></span>
                        <span class="icon-custom icon-quote-end"></span>
                    </div>
                </div>

                <div class="swiper-slide">
                    <div class="testimonial-box">
                        <div class="testimonial-image">
                            <img src="{{ asset('img/testimonial-image-3.png') }}">
                        </div>

                        <div class="testimonial-text">
                            <h5>Nury, Bogotá</h5>
                            <p>Estaba un poco cerrada a venir, porque yo decía que conocía de todo de Bienes Raíces, y no me imagine que me pudiera aportar tanto como me ha aportado en este momento. He tenido muchas sorpresas, he encontrado que aquí se desarrollan muchísimas oportunidades que nunca vemos, porque no tenemos captación de oportunidades y capital racional. No sabía nada de esto, y es sumamente importante para mis negocios. Los felicito por esta Universidad. Los invito a invertir en este entrenamiento, y en ustedes mismo.</p>
                        </div>

                        <span class="icon-custom icon-quote-start"></span>
                        <span class="icon-custom icon-quote-end"></span>
                    </div>
                </div>
            </div>

            <div class="swiper-pagination"></div>
        </div>
        -->
    </div>
</section>
@endsection

@section('scripts')

@endsection