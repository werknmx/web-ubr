@extends('back.layouts.app')

@section('stylesheets')

@endsection

@section('content')

<div class="row page-titles">
    <div class="col-md-5 align-self-center">
        <h3 class="text-themecolor">Categorías</h3>
    </div>
    <div class="col-md-7 align-self-center">
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="javascript:void(0)">Inicio</a></li>
            <li class="breadcrumb-item active">Categorías</li>
        </ol>
    </div>
</div>

<div class="row">
    <div class="col">
        @include('back.layouts.partials._mensajes')
    </div>
</div>

<div class="row">
    <div class="col-md-8">
        <div class="card">
            <div class="card-body">
                <h3>Categorías</h3>
                <hr>

                @if($categorias->count() == NULL || $categorias->count() == 0)
                    <p>No hay categorias. ¡Comienza a crear una!</p>
                @else 
                <div class="table-responsive">
                    <table class="table">
                        <thead>
                            <tr>
                                <th>#</th>
                                <th>Nombre</th>
                                <th>Acciones</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($categorias as $cat)
                            <tr>
                                <td>{{ $cat->id }}</td>
                                <td>{{ $cat->name }}</td>
                                <td class="text-nowrap">
                                    <a href="{{ route('blog.show', $cat->id) }}" data-toggle="tooltip" data-original-title="Ver"><i class="mdi mdi-eye text-inverse m-r-10"></i> </a>
                                    <a href="#" data-toggle="tooltip" data-original-title="Edit"><i class="mdi mdi-pencil text-inverse"></i></a>
                                </td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
                @endif
            </div>
        </div>
    </div>

    <div class="col-md-4">
        <div class="card mb-3">
            <div class="card-body">
                <h3>Categorías</h3>
                <hr>

                <form method="POST" action="{{ route('categorias.store') }}">
                    {{ csrf_field() }}
                    <div class="input-group">
                        <input class="form-control" name="name" placeholder="Nueva Categoria..." type="text">
                        <div class="input-group-addon" style="padding: 0px; background-color: transparent;"><button class="btn btn-success">Guardar</button></div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
@endsection

@section('scripts')

@endsection