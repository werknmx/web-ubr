@extends('back.layouts.app')

@section('stylesheets')

@endsection

@section('content')

<div class="row page-titles">
    <div class="col-md-5 align-self-center">
        <h3 class="text-themecolor">Servicios / Programas</h3>
    </div>
    <div class="col-md-7 align-self-center">
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="javascript:void(0)">Inicio</a></li>
            <li class="breadcrumb-item active">Servicios / Programas</li>
        </ol>
    </div>
</div>

<div class="row">
    <div class="col">
        @include('back.layouts.partials._mensajes')
    </div>
</div>

<div class="row">
    <div class="col-md-8">
        <div class="card">
            <div class="card-body">
                <h3>Servicios / Programas</h3>
                <hr>

                @if($programas->count() == NULL || $programas->count() == 0)
                    <p>No hay programas. ¡Comienza a crear uno!</p>
                @else 
                <div class="table-responsive">
                    <table class="table">
                        <thead>
                            <tr>
                                <th>#</th>
                                <th>Nombre</th>
                                <th>Descripción</th>
                                <th>Acciones</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($programas as $pro)
                            <tr>
                                <td>{{ $pro->id }}</td>
                                <td>{{ $pro->name }}</td>
                                <td>{{ $pro->description }}</td>
                                <td class="text-nowrap">
                                    <a href="{{ route('programas.show', $pro->id) }}" data-toggle="tooltip" data-original-title="Ver"><i class="mdi mdi-eye text-inverse m-r-10"></i> </a>
                                    <a href="#" data-toggle="tooltip" data-original-title="Edit"><i class="mdi mdi-pencil text-inverse m-r-10"></i></a>
                                    <a href="#" data-toggle="tooltip" data-original-title="Borrar"><i class="mdi mdi-delete text-inverse"></i></a>
                                </td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
                @endif
            </div>
        </div>
    </div>

    <div class="col-md-4">
        <div class="card mb-3">
            <div class="card-body">
                <h3>Servicios / Programas</h3>
                <hr>

                <form method="POST" action="{{ route('programas.store') }}">
                    {{ csrf_field() }}

                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group">
                                <label>Nombre</label>
                                <input class="form-control" name="name" type="text" required="">
                            </div>
                        </div>

                        <div class="col-md-12">
                            <div class="form-group">
                                <label>Descripción</label>
                                <textarea class="form-control" name="description" rows="5"></textarea>
                            </div>
                        </div>
                    </div>

                    <button type="submit" class="btn btn-success btn-block">Guardar</button>
                </form>
            </div>
        </div>
    </div>
</div>
@endsection

@section('scripts')

@endsection