@extends('back.layouts.app')

@section('stylesheets')
<!-- Popup CSS -->
<link href="{{ asset('back/assets/plugins/Magnific-Popup-master/dist/magnific-popup.css') }}" rel="stylesheet">

<link rel="stylesheet" href="{{ asset('back/assets/plugins/dropify/dist/css/dropify.min.css') }}">

<link href="{{ asset('back/css/pages/user-card.css') }}" rel="stylesheet">

@endsection

@section('content')
<div class="row page-titles">
    <div class="col-md-5 align-self-center">
        <h3 class="text-themecolor">Editar Galería</h3>
    </div>
    <div class="col-md-7 align-self-center">
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="javascript:void(0)">Inicio</a></li>
            <li class="breadcrumb-item"><a href="javascript:void(0)">Editar Galería</a></li>
            <li class="breadcrumb-item active">{{ $galeria->title }}</li>
        </ol>
    </div>
</div>

<div class="row">
    <div class="col">
        @include('back.layouts.partials._mensajes')
    </div>
</div>

<div class="row">
    <div class="col-md-12">
        <div class="card">
            <div class="card-body">
            	<form method="POST" action="{{ route('galerias.update', $galeria->id) }}" enctype="multipart/form-data">
		            {{ csrf_field() }}
					{{ method_field('PUT') }}

	            	<div class="row">
						<div class="col-md-6">
							<div class="form-group">
								<label>Título</label>
								<input class="form-control" name="title" type="text" placeholder="Título de Galería" required="" value="{{ $galeria->title }}">
							</div>
							
							<div class="form-group">
								<label>Descripción / Detalles</label>
								<textarea class="form-control" rows="8" name="body" placeholder="" required="">{{ $galeria->body }}</textarea>
							</div>
						</div>
						<div class="col-md-6">
							<div class="form-group">
								<label for="cover_image">Imagen de Portada</label>
								<input type="file" name="cover_image" class="dropify" data-height="263" data-default-file="{{ asset('img/galerias/covers/' . $galeria->cover_image) }}" />
							</div>
						</div>
					</div>

					<button type="submit" class="btn btn-primary">Guardar Galería</button>
				</form>
            </div>
        </div>
    </div>
</div>
@endsection

@section('scripts')
<script src="{{ asset('back/assets/plugins/dropify/dist/js/dropify.min.js') }}"></script>
<script>
$(document).ready(function() {
    $('.dropify').dropify();
});
</script>

<script src="{{ asset('back/assets/plugins/Magnific-Popup-master/dist/jquery.magnific-popup.min.js') }}"></script>
<script src="{{ asset('back/assets/plugins/Magnific-Popup-master/dist/jquery.magnific-popup-init.js') }}"></script>

<script>
    $(".delete").on("submit", function(){
        return confirm("¿Estás seguro de querer borrar la galería? Si borras este elemento también se eliminarán las imágenes relacionadas y no podrán ser recuperadas.");
    });
</script>
@endsection