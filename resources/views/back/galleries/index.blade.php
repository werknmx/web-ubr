@extends('back.layouts.app')

@section('stylesheets')
<!-- Popup CSS -->
<link href="{{ asset('back/assets/plugins/Magnific-Popup-master/dist/magnific-popup.css') }}" rel="stylesheet">

<link rel="stylesheet" href="{{ asset('back/assets/plugins/dropify/dist/css/dropify.min.css') }}">

<link href="{{ asset('back/css/pages/user-card.css') }}" rel="stylesheet">

<style type="text/css">
	.action-btns{
		position: absolute;
		bottom: 0px;
		left: 0px;
		width: 100%;
		text-align: center;

		margin-bottom: -20px;
		opacity: 0;

		transition: ease-in-out .2s all;
	}
	.el-card-item:hover .action-btns{
		margin-bottom: 0px;
		opacity: 1;
	}

	.image-indicator{
		position: absolute;
		top: 30px;
		right: 0px;
		background-color: #398bf7;
		color: #fff;
		text-transform: uppercase;
		font-size: .6em;
		padding: 5px 12px;
		font-weight: bolder;
	}

	.galeria-inactiva{
		opacity: .5;
	}

	.galeria-inactiva:hover{
		opacity: 1;
	}
</style>
@endsection

@section('content')
<div class="row page-titles">
    <div class="col-md-5 align-self-center">
        <h3 class="text-themecolor">Galerías</h3>
    </div>
    <div class="col-md-7 align-self-center">
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="javascript:void(0)">Inicio</a></li>
            <li class="breadcrumb-item active">Galerías</li>
        </ol>
    </div>
</div>

<div class="row">
    <div class="col">
        @include('back.layouts.partials._mensajes')
    </div>
</div>

<div class="row mb-4">
    <div class="col col-md-12">
        <a href="#" data-toggle="modal" data-target="#galeriaModal" class="btn btn-rounded btn-success"><i class="fa fa-plus-circle m-r-5"></i> Nueva Galería</a>
    </div>
</div>

<div class="row">
    <div class="col-md-12">
        <div class="card">
            <div class="card-body">
                <h3>Galerías Activas</h3>
                <hr>

                @if($galerias_activas->count() == NULL || $galerias_activas->count() == 0)
                    <p>No hay galerías. ¡Comienza a crear una!</p>
                @else                           
                <div class="card-columns el-element-overlay">
                	@foreach($galerias_activas as $galeria)
                    <div class="card">
                        <div class="el-card-item">

                            <div class="el-card-avatar el-overlay-1">
                                <a class="image-popup-vertical-fit" href="{{ asset('img/galerias/covers/' . $galeria->cover_image) }}"> <img src="{{ asset('img/galerias/covers/' . $galeria->cover_image) }}" alt="user" /> </a>

                                <div class="image-indicator"><i class="fa fa-file-image-o"></i> {{ $galeria->imagenes->count() }} Imágene(s) adicional(es)</div>

                                <div class="action-btns">
	                        		<div class="btn-group" role="group">
	                        			@if($galeria->imagenes->count())
	                                    <a href="{{ route('galerias.show', $galeria->id) }}" class="btn btn-primary" data-toggle="tooltip" title="Ver todas las fotos en esta galería"><i class="fa fa-eye"></i></a>
	                                    @endif
	                                    <a href="{{ route('galerias.baja', $galeria->id) }}" class="btn btn-warning" data-toggle="tooltip" title="Desactivar"><i class="fa fa-angle-double-down"></i></a>
	                                    <a href="{{ route('galerias.edit', $galeria->id) }}" class="btn btn-info" data-toggle="tooltip" title="Editar Galería"><i class="fa fa-pencil"></i></a>

	                                    <form method="POST" class="delete" action="{{ route('galerias.destroy', $galeria->id) }}">
							                <button type="submit" class="btn btn-danger" data-toggle="tooltip" title="Borrar Permanentemente"><i class="fa fa-trash"></i></button>
							                {{ csrf_field() }}
							                {{ method_field('DELETE') }}
						              	</form>
	                                </div>
	                        	</div>

                            </div>

                            

                            <div class="el-card-content">
                                <h3 class="box-title">{{ $galeria->title }}</h3> 
                                <small>{{ $galeria->body }}</small>
                                <br/>

                                <a href="" data-toggle="modal" data-target="#galeriaImagesModal" class="btn btn-rounded btn-success btn-sm mt-4" style="color: #fff;"><i class="fa fa-plus-circle m-r-5"></i> Agregar más fotos</a>

                                <div class="modal fade" id="galeriaImagesModal" tabindex="-1" role="dialog" aria-hidden="true">
									<div class="modal-dialog" role="document">
										<div class="modal-content">
											<form method="POST" action="{{ route('imagen.store') }}" enctype="multipart/form-data">
									            {{ csrf_field() }}
												<div class="modal-header">
													<h5 class="modal-title" id="exampleModalLabel">Agregar Imágenes</h5>
													<button type="button" class="close" data-dismiss="modal" aria-label="Close">
														<span aria-hidden="true">&times;</span>
													</button>
												</div>
												<div class="modal-body">
													<div class="row">
														<div class="col-md-12">		
															<div class="form-group">
																<label>Descripción / Detalles</label>
																<textarea class="form-control" rows="3" name="description" placeholder=""></textarea>
															</div>

															<div class="form-group">
																<label for="image">Imagen</label>
																<input type="file" name="image" class="dropify" data-height="250" required="" />
															</div>

															<input type="hidden" name="gallery_id" value="{{ $galeria->id }}">
														</div>
													</div>
												</div>
												<div class="modal-footer">
													<button type="button" class="btn btn-link" data-dismiss="modal">Cancelar</button>
													<button type="submit" class="btn btn-primary">Guardar</button>
												</div>
											</form>
										</div>
									</div>
								</div>
                            </div>
                        </div>
                    </div>
                    @endforeach
                </div>  
                @endif
            </div>
        </div>

         <div class="card">
            <div class="card-body">
                <h3>Galerías Inactivas</h3>
                <hr>
                @if($galerias_inactivas->count() == NULL || $galerias_inactivas->count() == 0)
               
                @else                           
                <div class="card-columns el-element-overlay">
                	@foreach($galerias_inactivas as $galeria)
                    <div class="card galeria_inactiva">
                        <div class="el-card-item">
                            <div class="el-card-avatar el-overlay-1">
                                <a class="image-popup-vertical-fit" href="{{ asset('img/galerias/covers/' . $galeria->cover_image) }}"> <img src="{{ asset('img/galerias/covers/' . $galeria->cover_image) }}" alt="user" /> </a>

                                <div class="image-indicator"><i class="fa fa-file-image-o"></i> {{ $galeria->imagenes->count() }} Imágene(s) adicional(es)</div>

                                <div class="action-btns">
	                        		<div class="btn-group" role="group">
	                        			@if($galeria->imagenes->count())
	                                    <a href="{{ route('galerias.show', $galeria->id) }}" class="btn btn-primary" data-toggle="tooltip" title="Ver todas las fotos en esta galería"><i class="fa fa-eye"></i></a>
	                                    @endif
	                                    <a href="{{ route('galerias.alta', $galeria->id) }}" class="btn btn-success" data-toggle="tooltip" title="Activar"><i class="fa fa-angle-double-up"></i></a>
	                                    <a href="{{ route('galerias.edit', $galeria->id) }}" class="btn btn-info" data-toggle="tooltip" title="Editar Galería"><i class="fa fa-pencil"></i></a>

	                                    <form method="POST" class="delete" action="{{ route('galerias.destroy', $galeria->id) }}">
							                <button type="submit" class="btn btn-danger" data-toggle="tooltip" title="Borrar Permanentemente"><i class="fa fa-trash"></i></button>
							                {{ csrf_field() }}
							                {{ method_field('DELETE') }}
						              	</form>
	                                </div>
	                        	</div>

                            </div>

                            

                            <div class="el-card-content">
                                <h3 class="box-title">{{ $galeria->title }}</h3> 
                                <small>{{ $galeria->body }}</small>
                                <br/>

                                <a href="" data-toggle="modal" data-target="#galeriaImagesModal" class="btn btn-rounded btn-success btn-sm mt-4" style="color: #fff;"><i class="fa fa-plus-circle m-r-5"></i> Agregar más fotos</a>

                                <div class="modal fade" id="galeriaImagesModal" tabindex="-1" role="dialog" aria-hidden="true">
									<div class="modal-dialog" role="document">
										<div class="modal-content">
											<form method="POST" action="{{ route('imagen.store') }}" enctype="multipart/form-data">
									            {{ csrf_field() }}
												<div class="modal-header">
													<h5 class="modal-title" id="exampleModalLabel">Agregar Imágenes</h5>
													<button type="button" class="close" data-dismiss="modal" aria-label="Close">
														<span aria-hidden="true">&times;</span>
													</button>
												</div>
												<div class="modal-body">
													<div class="row">
														<div class="col-md-12">		
															<div class="form-group">
																<label>Descripción / Detalles</label>
																<textarea class="form-control" rows="3" name="description" placeholder=""></textarea>
															</div>

															<div class="form-group">
																<label for="image">Imagen</label>
																<input type="file" name="image" class="dropify" data-height="250" required="" />
															</div>

															<input type="hidden" name="gallery_id" value="{{ $galeria->id }}">
														</div>
													</div>
												</div>
												<div class="modal-footer">
													<button type="button" class="btn btn-link" data-dismiss="modal">Cancelar</button>
													<button type="submit" class="btn btn-primary">Guardar</button>
												</div>
											</form>
										</div>
									</div>
								</div>
                            </div>
                        </div>
                    </div>
                    @endforeach
                </div>  
                @endif
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="galeriaModal" tabindex="-1" role="dialog" aria-hidden="true">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<form method="POST" action="{{ route('galerias.store') }}" enctype="multipart/form-data">
	            {{ csrf_field() }}
				<div class="modal-header">
					<h5 class="modal-title" id="exampleModalLabel">Crear Nueva Galería</h5>
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
				</div>
				<div class="modal-body">
					<div class="row">
						<div class="col-md-12">
							<div class="form-group">
								<label>Título</label>
								<input class="form-control" name="title" type="text" placeholder="Título de Galería" required="">
							</div>
							
							<div class="form-group">
								<label>Descripción / Detalles</label>
								<textarea class="form-control" rows="3" name="body" placeholder="" required=""></textarea>
							</div>

							<div class="form-group">
								<label for="cover_image">Imagen de Portada</label>
								<input type="file" name="cover_image" class="dropify" data-height="250" required="" />
							</div>
						</div>
					</div>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-link" data-dismiss="modal">Cancelar</button>
					<button type="submit" class="btn btn-primary">Guardar</button>
				</div>
			</form>
		</div>
	</div>
</div>
@endsection

@section('scripts')
<script src="{{ asset('back/assets/plugins/dropify/dist/js/dropify.min.js') }}"></script>
<script>
$(document).ready(function() {
    $('.dropify').dropify();
});
</script>

<script src="{{ asset('back/assets/plugins/Magnific-Popup-master/dist/jquery.magnific-popup.min.js') }}"></script>
<script src="{{ asset('back/assets/plugins/Magnific-Popup-master/dist/jquery.magnific-popup-init.js') }}"></script>

<script>
    $(".delete").on("submit", function(){
        return confirm("¿Estás seguro de querer borrar la galería? Si borras este elemento también se eliminarán las imágenes relacionadas y no podrán ser recuperadas.");
    });
</script>
@endsection