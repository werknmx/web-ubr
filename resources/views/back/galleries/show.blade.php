@extends('back.layouts.app')

@section('stylesheets')
<!-- Popup CSS -->
<link href="{{ asset('back/assets/plugins/Magnific-Popup-master/dist/magnific-popup.css') }}" rel="stylesheet">

<link rel="stylesheet" href="{{ asset('back/assets/plugins/dropify/dist/css/dropify.min.css') }}">

<link href="{{ asset('back/css/pages/user-card.css') }}" rel="stylesheet">

<style type="text/css">
	.action-btns{
		position: absolute;
		bottom: 0px;
		left: 0px;
		width: 100%;
		text-align: center;

		margin-bottom: -20px;
		opacity: 0;

		transition: ease-in-out .2s all;
	}
	.el-card-item:hover .action-btns{
		margin-bottom: 0px;
		opacity: 1;
	}

	.image-indicator{
		position: absolute;
		top: 30px;
		right: 0px;
		background-color: #398bf7;
		color: #fff;
		text-transform: uppercase;
		font-size: .6em;
		padding: 5px 12px;
		font-weight: bolder;
	}

	.galeria-inactiva{
		opacity: .5;
	}

	.galeria-inactiva:hover{
		opacity: 1;
	}
</style>
@endsection

@section('content')
<div class="row page-titles">
    <div class="col-md-5 align-self-center">
        <h3 class="text-themecolor">Galerías</h3>
    </div>
    <div class="col-md-7 align-self-center">
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="javascript:void(0)">Inicio</a></li>
            <li class="breadcrumb-item"><a href="javascript:void(0)">Detalle de Galería</a></li>
            <li class="breadcrumb-item active">{{ $galeria->title }}</li>
        </ol>
    </div>
</div>

<div class="row">
    <div class="col">
        @include('back.layouts.partials._mensajes')
    </div>
</div>

<div class="row mb-4">
    <div class="col col-md-12 text-right">
		<div class="btn-group" role="group">
			@if($galeria->is_active == true)
            <a href="{{ route('galerias.baja', $galeria->id) }}" class="btn btn-warning"><i class="fa fa-angle-double-down"></i> Desactivar Galería</a>
            @else
            <a href="{{ route('galerias.alta', $galeria->id) }}" class="btn btn-success"><i class="fa fa-angle-double-up"></i> Activar Galería</a>
            @endif
            <a href="{{ route('galerias.edit', $galeria->id) }}" class="btn btn-info"><i class="fa fa-pencil"></i> Editar Portada de Galería</a>

            <form method="POST" class="delete" action="{{ route('galerias.destroy', $galeria->id) }}">
                <button type="submit" class="btn btn-danger"><i class="fa fa-trash"></i> Borrar Permanentemente</button>
                {{ csrf_field() }}
                {{ method_field('DELETE') }}
          	</form>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-md-4">
        <div class="card" style="height:550px;">
            <div class="card-body">
                <h3>Portada</h3>
                <hr>
                           
                <div class="el-element-overlay">
                    <div class="card">
                        <div class="el-card-item" style="height: 320px;">
                            <div class="el-card-avatar el-overlay-1">
                                <a class="image-popup-vertical-fit" href="{{ asset('img/galerias/covers/' . $galeria->cover_image) }}"> <img src="{{ asset('img/galerias/covers/' . $galeria->cover_image) }}" alt="user" /> </a>
                            </div>

                            <div class="el-card-content">
                                <h3 class="box-title">{{ $galeria->title }}</h3> 
                                <small>{{ $galeria->body }}</small>
                                <br/>

                                <a href="" data-toggle="modal" data-target="#galeriaImagesModal" class="btn btn-rounded btn-success btn-sm mt-4" style="color: #fff;"><i class="fa fa-plus-circle m-r-5"></i> Agregar más fotos</a>

                                <div class="modal fade" id="galeriaImagesModal" tabindex="-1" role="dialog" aria-hidden="true">
									<div class="modal-dialog" role="document">
										<div class="modal-content">
											<form method="POST" action="{{ route('imagen.store') }}" enctype="multipart/form-data">
									            {{ csrf_field() }}
												<div class="modal-header">
													<h5 class="modal-title" id="exampleModalLabel">Agregar Imágenes</h5>
													<button type="button" class="close" data-dismiss="modal" aria-label="Close">
														<span aria-hidden="true">&times;</span>
													</button>
												</div>
												<div class="modal-body">
													<div class="row">
														<div class="col-md-12">		
															<div class="form-group">
																<label>Descripción / Detalles</label>
																<textarea class="form-control" rows="3" name="description" placeholder=""></textarea>
															</div>

															<div class="form-group">
																<label for="image">Imagen</label>
																<input type="file" name="image" class="dropify" data-height="250" required="" />
															</div>

															<input type="hidden" name="gallery_id" value="{{ $galeria->id }}">
														</div>
													</div>
												</div>
												<div class="modal-footer">
													<button type="button" class="btn btn-link" data-dismiss="modal">Cancelar</button>
													<button type="submit" class="btn btn-primary">Guardar</button>
												</div>
											</form>
										</div>
									</div>
								</div>
                            </div>
                        </div>
                    </div>
                </div>  
            </div>
        </div>
    </div>

    <div class="col-md-8">
    	<div class="card">
            <div class="card-body">
                <h3>Adicionales / Internas</h3>
                <hr>

                <div class="card-columns el-element-overlay">
                	@foreach($galeria->imagenes as $galeria)
                    <div class="card">
                        <div class="el-card-item">
                            <div class="el-card-avatar el-overlay-1">
                                <a class="image-popup-vertical-fit" href="{{ asset('img/galerias/extras/' . $galeria->image) }}"> <img src="{{ asset('img/galerias/extras/' . $galeria->image) }}" alt="user" /> </a>

                                <div class="action-btns">
	                        		<div class="btn-group" role="group">
	                                    <form method="POST" class="soft-delete" action="{{ route('extra.destroy', $galeria->id) }}">
							                <button type="submit" class="btn btn-danger" data-toggle="tooltip" title="Borrar Permanentemente"><i class="fa fa-trash"></i></button>
							                {{ csrf_field() }}
							                {{ method_field('DELETE') }}
						              	</form>
	                                </div>
	                        	</div>

                            </div>

                            <div class="el-card-content">
                                <small>{{ $galeria->description }}</small>
                                <br/>
                            </div>
                        </div>
                    </div>
                    @endforeach
                </div>    
            </div>
        </div>
    </div>
</div>

@endsection

@section('scripts')
<script src="{{ asset('back/assets/plugins/dropify/dist/js/dropify.min.js') }}"></script>
<script>
$(document).ready(function() {
    $('.dropify').dropify();
});
</script>

<script src="{{ asset('back/assets/plugins/Magnific-Popup-master/dist/jquery.magnific-popup.min.js') }}"></script>
<script src="{{ asset('back/assets/plugins/Magnific-Popup-master/dist/jquery.magnific-popup-init.js') }}"></script>

<script>
    $(".delete").on("submit", function(){
        return confirm("¿Estás seguro de querer borrar la galería? Si borras este elemento también se eliminarán las imágenes relacionadas y no podrán ser recuperadas.");
    });

    $(".soft-delete").on("submit", function(){
        return confirm("¿Estás seguro de querer borrar la imágen?");
    });
</script>
@endsection