@extends('back.layouts.app')

@section('stylesheets')

@endsection

@section('content')
<div class="row page-titles">
    <div class="col-md-5 align-self-center">
        <h3 class="text-themecolor">Editar Landing Page</h3>
    </div>
    <div class="col-md-7 align-self-center">
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="javascript:void(0)">Inicio</a></li>
            <li class="breadcrumb-item"><a href="javascript:void(0)">Editar Landing Page</a></li>
            <li class="breadcrumb-item active">{{ $landing->title }}</li>
        </ol>
    </div>
</div>

<div class="row">
    <div class="col">
        @include('back.layouts.partials._mensajes')
    </div>
</div>

<div class="row mb-4">
    <div class="col col-md-12">
        <a href="{{ route('campanas.index') }}" class="btn btn-rounded btn-success">Regresar</a>
    </div>
</div>

<div class="row">
    <div class="col-md-12">
        <div class="card">
            <div class="card-body">
                <form method="POST" action="{{ route('landing_pages.update', $landing->id) }}" enctype="multipart/form-data">
	                {{ csrf_field() }}
                    {{ method_field('PUT') }}
                    <div class="row">
                        <div class="form-group col-md-12">
                            <label>Nombre de Landing Page</label>
                            <input class="form-control" name="title" type="text" placeholder="Título" required="" value="{{ $landing->title }}">
                        </div>
                        
                        <div class="form-group col-md-12">
                            <label>Descripción / Detalles</label>
                            <textarea class="form-control" rows="3" name="description" placeholder="" required="">{{ $landing->description }}</textarea>
                        </div>

                        <div class="col-md-12 form-group">
                            <label class="control-label">Fechas del Evento</label>
                            <div class="input-daterange input-group" id="date-range">
                                <input type="text" class="form-control" name="start_date" value="{{ $landing->start_date }}" />
                                <span class="input-group-addon bg-info b-0 text-white">a</span>
                                <input type="text" class="form-control" name="end_date" value="{{ $landing->end_date }}"/>
                            </div>
                        </div>

                        <div class="col-md-12 form-group">
                            <label class="control-label">Lugar del Evento</label>
                            <input class="form-control" name="place" type="text" placeholder="Ciudad, Estado" required="" value="{{ $landing->place }}">
                        </div> 

                        <div class="form-group col-md-6">
                            <label>Plantilla (Estilo)</label>
                            <select class="form-control" name="template_id">
                                <option value="1">Estilo 1</option>
                                <option value="2">Estilo 2</option>
                                <option value="3">Estilo 2 Variación</option>
                                <option value="4">Estilo 3</option>
                            </select>
                        </div>

                        <div class="form-group col-md-6">
                            <label>Curso Asociado:</label>
                            <select class="form-control" name="courses_id">
                                @foreach($courses as $c)
                                    <option value="{{ $c->id }}">{{ $c->title }}</option>
                                @endforeach
                            </select>
                        </div>

                        <!--
                        <div class="form-group col-md-6">
                            <label>Formulario:</label>
                            <input class="form-control" name="has_form" type="text" required="">
                        </div>
                        -->

                        <div class="form-group col-md-12">
                            <label>Nombre de Origen</label>
                            <input class="form-control" name="origin_name" type="text" required="" value="{{ $landing->origin_name }}">
                            <small>Esta información es importante para el equipo de ventas y análisis.</small>
                        </div>

                        <div class="col-md-12">                
                            <h3>Opciones de Vínculos</h3>
                            <hr>
                        </div>

                        <div class="form-group col-md-12">
                            <label>Link para Botón de "Más Información"</label>
                            <input class="form-control" name="btn_link" type="text" value="{{ $landing->btn_link }}">
                        </div>

                        <div class="col-md-12">                
                            <h3>Opciones SEO</h3>
                            <hr>
                        </div>

                        <div class="form-group col-md-6">
                            <label>Palabras Clave</label>
                            <input class="form-control" name="seo_keywords" type="text" required="" value="{{ $landing->seo_keywords }}">
                            <small>Separa cada palabra con una coma. Opcional</small>
                        </div>

                        <div class="form-group col-md-6">
                            <label>Permisos de Robots</label>
                            <input class="form-control" name="seo_robots" type="text" required="" value="{{ $landing->seo_robots }}">
                            <small>Opcional</small>
                        </div>

                        <div class="form-group col-md-6">
                            <label>Código de Google Analytics</label>
                            <textarea class="form-control" name="google_analytics_code" required="" rows="4">{{ $landing->google_analytics_code }}</textarea>
                            <small>IMPORTANTE: Solo copiar y pegar, no realizar ningún formato adicional</small>
                        </div>

                        <div class="form-group col-md-6">
                            <label>Código de Google Ads</label>
                            <textarea class="form-control" name="google_ads_conversion_code" required="" rows="4">{{ $landing->google_ads_conversion_code }}</textarea>
                            <small>IMPORTANTE: Solo copiar y pegar, no realizar ningún formato adicional</small>
                        </div>

                        <div class="form-group col-md-12">
                            <label>Código de Facebook Pixel</label>
                            <textarea class="form-control" name="facebook_pixel_code" required="" rows="4">{{ $landing->facebook_pixel_code }}</textarea>
                            <small>IMPORTANTE: Solo copiar y pegar, no realizar ningún formato adicional</small>
                        </div>

                        <input type="hidden" name="campaign_id" value="{{ $landing->campaign_id }}">
                    </div>

                    <button type="submit" class="btn btn-primary">Guardar Información</button>
	            </form>
            </div>
        </div>
    </div>
</div>
@endsection

@section('scripts')

@endsection