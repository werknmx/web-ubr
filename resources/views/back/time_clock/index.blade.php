@extends('back.layouts.app')

@section('stylesheets')

@endsection

@section('content')
<div class="row page-titles">
    <div class="col-md-5 align-self-center">
        <h3 class="text-themecolor">Reloj Checador</h3>
    </div>
    <div class="col-md-7 align-self-center">
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="javascript:void(0)">Inicio</a></li>
            <li class="breadcrumb-item active">Reloj Checador</li>
        </ol>
    </div>
</div>

<div class="row">
    <div class="col">
        @include('back.layouts.partials._mensajes')
    </div>
</div>

<div class="row">
    <div class="col-md-4">
        <div class="card">
            <div class="card-body">
                <h3>Estado de Conexión</h3>
                <hr>

                <div class="table-responsive">
                    <table class="table">
                        <thead>
                            <tr>
                                <th>Usuario</th>
                                <th>Estado</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($users as $user)
                            <tr>
                            	<td>
                            		<div>
                                        @if( $user->image == NULL)
                                        <img class="thumb-md rounded-circle mr-2" src="{{ 'https://www.gravatar.com/avatar/' . md5(strtolower(trim($user->email))) . '?d=retro&s=200' }}" alt="{{ $user->name }}">
                                        @else
                                        <img  class="thumb-md rounded-circle mr-2" src="{{ asset('img/usuarios/' . $user->image ) }}" alt="{{ $user->name }}">
                                        @endif
                                        {{ $user->name }}
                                    </div>	
                            	</td>
                                <td>
                                	@if($user->conexion->count() >= 1)
                                		<span class="badge badge-success">CONECTADO</span>
                                	@else
                                		<span class="badge badge-danger">DESCONECTADO</span>
                                	@endif

                                	{{-- 
                                	@foreach($user->conexion as $conect)
							        	<span class="badge badge-success">CONECTADO</span>
									@endforeach
									--}}
                                </td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>

    <div class="col-md-8">
        <div class="card">
            <div class="card-body">
                <h3>Check-In de Hoy</h3>
                <hr>

                @if($checks->count() == NULL || $checks->count() == 0)
                    <p>No hay Check-Ins hoy. ¡Recuerdale a todos marcar su entrada!</p>
                @else 
                <div class="table-responsive">
                    <table class="table">
                        <thead>
                            <tr>
                                <th>Usuario</th>
                                <th>Entrada</th>
                                <th>Ccomentario</th>
                                <th>Salida</th>
                                <th>Acciones</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($checks as $check)
                            <tr>
                                <td>{{ $check->user->name }}</td>
                                <td>{{ $check->check_in }}</td>
                                <td>{{ $check->comment }}</td>
                               	<td>
                               		@if($check->check_out == NULL)
                               			No marcó salida
                               		@else
                               			{{ $check->check_out }}
                               		@endif
                               	</td>
                                <td class="text-nowrap">
                                    <form method="POST" class="delete" action="{{ route('reloj-checador.destroy', $check->id) }}" style="display: inline-block;">
                                        <button type="submit" class="btn btn-sm btn-outline-danger" data-toggle="tooltip" data-original-title="Borrar"><i class="ti-trash"></i></button>
                                        {{ csrf_field() }}
                                        {{ method_field('DELETE') }}
                                    </form>
                                </td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
                @endif
            </div>
        </div>
    </div>
</div>
@endsection

@section('scripts')
<script>
    $(".delete").on("submit", function(){
        return confirm("¿Estás seguro de querer borrar el curso? Si borras este elemento también se eliminarán las imágenes relacionadas y no podrán ser recuperadas.");
    });
</script>
@endsection