@extends('back.layouts.app')

@section('stylesheets')

@endsection

@section('content')
<div class="row page-titles">
    <div class="col-md-5 align-self-center">
        <h3 class="text-themecolor">Historial de Reloj Checador</h3>
    </div>
    <div class="col-md-7 align-self-center">
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="javascript:void(0)">Inicio</a></li>
            <li class="breadcrumb-item active">Historial de Reloj Checador</li>
        </ol>
    </div>
</div>

<div class="row">
    <div class="col">
        @include('back.layouts.partials._mensajes')
    </div>
</div>

<div class="row">
    <div class="col-md-12">
        <div class="card">
            <div class="card-body">
                <h3>Todas las Entradas y Salidas</h3>
                <hr>

                @if($checks->count() == NULL || $checks->count() == 0)
                    <p>No hay Check-Ins hoy. ¡Recuerdale a todos marcar su entrada!</p>
                @else 
                <div class="table-responsive">
                    <table class="table">
                        <thead>
                            <tr>
                                <th>Usuario</th>
                                <th>Entrada</th>
                                <th>Ccomentario</th>
                                <th>Salida</th>
                                <th>Acciones</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($checks as $check)
                            <tr>
                                <td>{{ $check->user->name }}</td>
                                <td>{{ $check->check_in }}</td>
                                <td>{{ $check->comment }}</td>
                               	<td>
                               		@if($check->check_out == NULL)
                               			No marcó salida
                               		@else
                               			{{ $check->check_out }}
                               		@endif
                               	</td>
                                <td class="text-nowrap">
                                    <form method="POST" class="delete" action="{{ route('reloj-checador.destroy', $check->id) }}" style="display: inline-block;">
                                        <button type="submit" class="btn btn-sm btn-outline-danger" data-toggle="tooltip" data-original-title="Borrar"><i class="ti-trash"></i></button>
                                        {{ csrf_field() }}
                                        {{ method_field('DELETE') }}
                                    </form>
                                </td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
                @endif
            </div>
        </div>
    </div>
</div>
@endsection

@section('scripts')
<script>
    $(".delete").on("submit", function(){
        return confirm("¿Estás seguro de querer borrar el curso? Si borras este elemento también se eliminarán las imágenes relacionadas y no podrán ser recuperadas.");
    });
</script>
@endsection