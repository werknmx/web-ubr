@extends('back.layouts.app')

@section('stylesheets')
<style type="text/css">
    .btn-edit-image{
        position: absolute;
        top: -20px;
        left: 50%;

        transform: translateX(-50%);
        background-color: rgba(38,47,84);
        color: #fff;
        padding: 6px 10px;
        border-radius: 15px;
        text-transform: uppercase;
        font-size: .7em;
        border:none;
        cursor: pointer;
    }

    center{
        position: relative;
    }
</style>
@endsection

@section('content')
<div class="row page-titles">
    <div class="col-md-5 align-self-center">
        <h3 class="text-themecolor">Perfil de Usuario</h3>
    </div>
    <div class="col-md-7 align-self-center">
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="javascript:void(0)">Inicio</a></li>
            <li class="breadcrumb-item active">{{ Auth::user()->name }}</li>
        </ol>
    </div>
</div>

<div class="row">
    <div class="col">
        @include('back.layouts.partials._mensajes')
    </div>
</div>

<!-- Modal -->
<div class="modal fade" id="changeImageModal" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Modal title</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <form role="form" method="POST" action="{{ route('admin.image.update', Auth::user()->id) }}" enctype="multipart/form-data">
            {{ csrf_field() }}
            {{ method_field('PUT') }}

                <div class="modal-body">
                    <div class="row">
                        <div class="col-md-6">
                            <h6>Imagen Actual</h6>
                            <hr>

                            @if( Auth::user()->image == NULL)
                                <img class="card-img-top" src="{{ 'https://www.gravatar.com/avatar/' . md5(strtolower(trim( Auth::user()->email))) . '?d=retro&s=300' }}" alt="{{ Auth::user()->name }}" style="width: 100%;">
                            @else
                                <img class="card-img-top" src="{{ asset('img/usuarios/' . Auth::user()->image ) }}" alt="{{ Auth::user()->name }}">
                            @endif
                        </div>
                        <div class="col-md-6">
                            <h6>Subir Nueva Imagen</h6>
                            <hr>

                            <input class="form-control-file" id="user_image" name="user_image" aria-describedby="fileHelp" type="file">
                            <small id="fileHelp" class="form-text text-muted">Esta es la imagen que te define. Que sea algo cool.</small>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
                    <button type="submit" class="btn btn-primary">Guardar Cambios</button>
                </div>
            </form>
        </div>
    </div>
</div>



<div class="row">
    <!-- Column -->
    <div class="col-lg-4 col-xlg-3 col-md-5">
        <div class="card">
            <div class="card-body">
                <center class="m-t-30"> 
                    <button type="button" class="btn-edit-image" data-toggle="modal" data-target="#changeImageModal">
                      Editar Imagen
                    </button>

                	@if( Auth::user()->image == NULL)
                    <img class="img-circle" width="150" src="{{ 'https://www.gravatar.com/avatar/' . md5(strtolower(trim(Auth::user()->email))) . '?d=retro&s=200' }}" alt="{{ Auth::user()->name }}">
                    @else
                    <img  class="img-circle" width="150" src="{{ asset('img/usuarios/' . Auth::user()->image ) }}" alt="{{ Auth::user()->name }}">
                    @endif
                    <h4 class="card-title m-t-20">{{ Auth::user()->name }}</h4>
                </center>
            </div>
            <div>
                <hr> </div>
            <div class="card-body"> <small class="text-muted">Correo Electrónico</small>
                <h6>{{ Auth::user()->email }}</h6> 
            </div>
        </div>
    </div>
    <!-- Column -->
    <!-- Column -->
    <div class="col-lg-8 col-xlg-9 col-md-7">
        <div class="card">
            <!-- Nav tabs -->
            <ul class="nav nav-tabs profile-tab" role="tablist">
                <li class="nav-item"> <a class="nav-link active" data-toggle="tab" href="#profile" role="tab">Perfil</a></li>
                <li class="nav-item"> <a class="nav-link" data-toggle="tab" href="#settings" role="tab">Configuración</a></li>
            </ul>
            <!-- Tab panes -->
            <div class="tab-content">
                <!--second tab-->
                <div class="tab-pane active" id="profile" role="tabpanel">
                    <div class="card-body">
                        <div class="row">
                            <div class="col-md-3 col-xs-6 b-r"> <strong>Nombre Completo</strong>
                                <br>
                                <p class="text-muted">{{ Auth::user()->name }}</p>
                            </div>
                            <div class="col-md-3 col-xs-6 b-r"> <strong>Correo Electrónico</strong>
                                <br>
                                <p class="text-muted">{{ Auth::user()->email }}</p>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="tab-pane" id="settings" role="tabpanel">
                    <div class="card-body">
                        <form role="form" method="POST" class="form-horizontal form-material p-4" action="{{ route('admin.update', Auth::user()->id) }}">
                        {{ csrf_field() }}
                        {{ method_field('PUT') }}
                        	<div class="row">
                        		<div class="form-group col-md-12">
	                                <label>Nombre Completo</label>
	                                <input type="text" placeholder="{{ Auth::user()->name }}" name="name" class="form-control form-control-line" value="{{ Auth::user()->name }}">
	                            </div>
	                            <div class="form-group col-md-12">
	                                <label>Correo Electrónico</label>
	                                <input type="text" name="email" value="{{ Auth::user()->email }}" class="form-control form-control-line">
	                                <span class="help-block"><small>Si requieres cambiar tu correo solicítalo con un encargado de sistemas.</small></span>
	                            </div>
                        	</div>

                            <hr>
			      			<h3>Seguridad</h3>

				      		<div class="row">
				      			<div class="col-md-6">
				      				<div class="form-group">
										<label for="password">Nueva Contraseña</label>
										<input type="password" name="password" class="form-control form-control-line">
									</div>
				      			</div>

				      			<div class="col-md-6">
				      				<div class="form-group">
										<label for="confirm-password">Confirmar Contraseña</label>
										<input type="text" name="confirm-password" id="confirm-password" class="form-control form-control-line">
									</div>
				      			</div>
				      		</div>

				      		<div class="row">
				      			<div class="form-group col-md-12">
	                                <button type="submit" class="btn btn-success">Actualizar Perfil</button>
	                            </div>
				      		</div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Column -->
</div>
@endsection

@section('scripts')

@endsection