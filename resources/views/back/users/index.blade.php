@extends('back.layouts.app')

@section('stylesheets')

@endsection

@section('content')
<div class="row">
    <div class="col-xl-8">
        <div class="card">
            <div class="card-body">
                <h4 class="mt-0 header-title mb-4">Listado de Usuarios</h4>
                
                @if($usuarios->count())
                <div class="table-responsive">
                    <table class="table table-hover">
                        <thead>
                            <tr>
                                <th scope="col">(#) Id</th>
                                <th scope="col">Nombre</th>
                                <th scope="col">Correo</th>
                                <th scope="col">Departamento</th>
                                <th scope="col">Rol</th>
                                <th scope="col">Creado</th>
                                @if($usuarios->count() > 1)
                                    <th scope="col">Acciones</th>
                                @endif
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($usuarios as $user)
                            <tr>
                                <th scope="row">#{{ $user->id }}</th>
                                <td>
                                    <div>
                                        @if( $user->image == NULL)
                                        <img class="thumb-md rounded-circle mr-2" src="{{ 'https://www.gravatar.com/avatar/' . md5(strtolower(trim($user->email))) . '?d=retro&s=200' }}" alt="{{ $user->name }}">
                                        @else
                                        <img  class="thumb-md rounded-circle mr-2" src="{{ asset('img/usuarios/' . $user->image ) }}" alt="{{ $user->name }}">
                                        @endif
                                        {{ $user->name }}
                                    </div>
                                </td>
                                <td>{{ $user->email }}</td>
                                <td>{{ $user->department or 'Sin Definir' }}</td>
                                <td>{{ $user->getRoleNames() }}</td>
                                <td>{{ $user->created_at }}</td>

                                @if($usuarios->count() > 1)
                                <td>
                                    <div class="btn-group" role="group" aria-label="Basic example">
                                        <button type="button" class="btn btn-sm btn-outline-info"><i class="ti-pencil-alt"></i> Editar</button>
                                        <form method="POST" action="{{ route('admin.destroy', $user->id) }}" style="display: inline-block;">
                                            <button type="submit" class="btn btn-sm btn-outline-danger">
                                                <i class="mdi mdi-delete"></i> Borrar
                                            </button>
                                            {{ csrf_field() }}
                                            {{ method_field('DELETE') }}
                                        </form>
                                    </div>
                                </td>
                                @endif
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
                @else
                <div class="text-center my-5">
                    <h4 class="mb-0">¡Todavía no se registra ningún administrador!</h4>
                </div>
                @endif
            </div>
        </div>
    </div>
    
    <div class="col-xl-4">
        <div class="card">
            <div class="card-body">
                <h4 class="mt-0 header-title mb-4">Crear Nuevo Administrador</h4>
                <form method="POST" action="{{ route('admin.register') }}">
                    @csrf
                    <div class="form-group">
                        <label for="name">Nombre Completo</label>
                        <input id="name" type="text" class="form-control" name="name" value="{{ old('name') }}" required autocomplete="name" autofocus>
                    </div>

                    <div class="form-group">
                        <label for="email">Correo Electrónico</label>
                        <input id="email" type="email" class="form-control" name="email" value="{{ old('email') }}" required autocomplete="email">
                    </div>

                    <div class="form-group">
                        <label for="password">Contraseña</label>
                        <input id="password" type="password" class="form-control" name="password" required autocomplete="new-password">
                    </div>

                    <div class="form-group">
                        <label for="password-confirm">Confirmar Contraseña</label>
                        <input id="password-confirm" type="password" class="form-control" name="password_confirmation" required autocomplete="new-password">
                    </div>

                    <h6 class="mt-4">Definir Roles</h6>
                    <hr>

                    <div class="form-group">
                        <select class="form-control" name="rol">
                            <option value="0">Selecciona un Rol de Usuario</option>
                            @foreach($roles as $rol)
                                <option value="{{ $rol->name }}">{{ $rol->name }}</option>
                            @endforeach
                        </select>
                    </div>

                    <button type="submit" class="btn btn-primary btn-block">Crear Nuevo Usuario</button>
                </form> 
            </div>
        </div>
    </div>
</div>
@endsection

@section('scripts')

@endsection