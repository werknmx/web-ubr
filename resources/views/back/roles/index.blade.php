@extends('back.layouts.app')

@section('stylesheets')

@endsection

@section('content')
<div class="row">
	<div class="col-md-8">
        <div class="card">
            <div class="card-body">
            	<h4 class="mt-0 header-title mb-4">Listado de Roles</h4>

                <div class="table-responsive">
                    <table class="table">
                        <thead>
                            <tr>
                                <th>Nombre</th>
                                <th>Guard</th>
                                <th>Fecha Registro</th>
                                <!--<th>Acciones</th>-->
                            </tr>
                        </thead>
                        <tbody>
                        	@foreach($roles as $rol)
                            <tr>
                                <td><a href="#">{{ $rol->name }}</a></td>
                                <td>{{ $rol->guard_name }}</td>

                                <td><span class="text-muted"><i class="fa fa-clock-o"></i> {{ $rol->created_at }}</span> </td>

                                <!--
                                <td>
                                	<div class="btn-group" role="group" aria-label="Acciones">
                                        <a class="btn btn-success" href="{{ route('roles.edit', $rol->id) }}"><i class="mdi mdi-pencil"></i></a>
			                            <form method="POST" action="{{ route('roles.destroy', $rol->id) }}">
			                                <button type="submit" class="btn btn-danger btn-block"><i class="mdi mdi-delete-forever"></i></button>
			                                {{ csrf_field() }}
			                                {{ method_field('DELETE') }}
			                            </form>
			                        </div>
                                </td>
                            	-->
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>

    <div class="col-md-4">
    	<div class="card">
            <div class="card-body">
                <h6 class="text-uppercase"><small>Agregar Nuevo Rol</small></h6>
                <hr>
		        <form method="POST" action="{{ route('roles.store') }}">
		            {{ csrf_field() }}
		            <div class="form-group mt-2">
		                <input type="text" class="form-control" name="name" />
		            </div>


		            <button type="submit" class="btn btn-success btn-block">Guardar Rol</button>
		        </form>
            </div>
        </div>
    </div>
</div>
@endsection

@section('scripts')
<script type="text/javascript">
	$(function () {
		$('[data-toggle="tooltip"]').tooltip()
	});
</script>
@endsection