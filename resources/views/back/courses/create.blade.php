@extends('back.layouts.app')

@section('stylesheets')

<link rel="stylesheet" href="{{ asset('back/assets/plugins/html5-editor/bootstrap-wysihtml5.css') }}" />
<link href="{{ asset('back/assets/plugins/select2/dist/css/select2.min.css') }}" rel="stylesheet" type="text/css" />

<link rel="stylesheet" href="{{ asset('back/assets/plugins/dropify/dist/css/dropify.min.css') }}">
@endsection

@section('content')
<div class="row page-titles">
    <div class="col-md-5 align-self-center">
        <h3 class="text-themecolor">Nueva Curso</h3>
    </div>
    <div class="col-md-7 align-self-center">
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="javascript:void(0)">Inicio</a></li>
            <li class="breadcrumb-item">Curso</li>
            <li class="breadcrumb-item active">Agregar Nuevo Curso</li>
        </ol>
    </div>
</div>

<form method="POST" action="{{ route('cursos.store') }}" enctype="multipart/form-data">
	{{ csrf_field() }}	

	<div class="row">
		<div class="col col-md-8">
			<div class="card">
				<div class="card-body">
					<h3>Curso</h3>
					<hr>

					<div class="row">
						<div class="col col-md-8">
							<div class="form-group">
								<label for="title">Titulo</label>
								<input class="form-control" type="text" name="title" required="">
							</div>
						</div>

						<div class="col-md-4">
							<div class="form-group">
	                            <label>Tipo de Servicio</label>
	                            <select class="form-control" name="program_id" required="">
	                            	@foreach($programs as $pro)
	                                	<option value="{{ $pro->id }}">{{ $pro->name }}</option>
	                                @endforeach
	                            </select>
	                        </div>
						</div>

						<div class="col col-md-12">
							<div class="form-group">
								<label for="description">Descripción</label>
								<textarea class="form-control" row="5" name="description" required=""></textarea>
							</div>
						</div>

						<div class="col col-md-6">
							<div class="form-group">
								<label for="objective">Objetivo</label>
								<textarea class="form-control" row="5" name="objective" required=""></textarea>

								<!--<input class="form-control" type="text" name="objective" required="">-->
							</div>
						</div>

						<div class="col col-md-6">
							<div class="form-group">
								<label for="addressed_to">Dirigido a</label>
								<textarea class="form-control" row="5" name="addressed_to" required=""></textarea>

								<!--<input class="form-control" type="text" name="addressed_to" required="">-->
							</div>
						</div>

						<div class="col col-md-12">
							<div class="form-group">
								<label for="accreditation_type">Tipo de Acreditación</label>
								<input class="form-control" type="text" name="accreditation_type" required="">
							</div>
						</div>

						<div class="col col-md-12">
							<div class="form-group">
								<label for="admission_profile">Perfil de Ingreso</label>
								<textarea id="mymce_admission" name="admission_profile"></textarea>
							</div>
						</div>

						<div class="col col-md-12">
							<div class="form-group">
								<label for="egress_profile">Perfil de Egreso</label>
								<textarea id="mymce_egress" name="egress_profile"></textarea>
							</div>
						</div>


						<div class="col col-md-4">
							<div class="form-group">
								<label for="duration">Duración</label>
								<input class="form-control" type="text" name="duration" required="">
							</div>
						</div>

						<div class="col col-md-4">
							<div class="form-group">
								<label for="modality">Modalidad</label>
								<input class="form-control" type="text" name="modality" required="">
							</div>
						</div>

						<div class="col col-md-4">
							<div class="form-group">
								<label for="timetable">Horarios</label>
								<input class="form-control" type="text" name="timetable" required="">
							</div>
						</div>

						<div class="col col-md-12">
							<div class="form-group">
								<label for="capacity">Capacidad</label>
								<input class="form-control" type="number" name="capacity" required="">
							</div>
						</div>

						<input type="hidden" name="slogans" value="Slogan Predeterminado">
					</div>
				</div>
			</div>

			<hr>
			<div class="col-md-12">
		    	<div class="row justify-content-end">
		            <div class="col">
		                <div class="form-group text-right">
		                    <a href="{{ route('cursos.index') }}" class="btn btn-link">Cancel</a>
		                    <button type="submit" class="btn btn-success btn-lg">Guardar</button>
		                </div>
		            </div>
		        </div>
		    </div>

		</div>

		<div class="col col-md-4">
			<div class="card mb-3">
				<div class="card-body">
					<h3>Información Adicional</h3>
					<hr>

					<div class="row">
						<div class="col-md-6">
							<div class="form-group">
								<label for="normal_price">Precio Normal</label>
								<input class="form-control" type="number" name="normal_price" required="">
							</div>
						</div>
						<div class="col-md-6">
							<div class="form-group">
								<label for="special_price">Precio Descuento</label>
								<input class="form-control" type="number" name="special_price" required="">
							</div>
						</div>

						<div class="col-md-12">
							<div class="form-group">
	                            <label>Activar Descuento</label>
	                            <select class="form-control" name="has_discount">
	                                <option value="0">No</option>
	                                <option value="1">Si</option>
	                            </select>
	                        </div>
						</div>
					</div>
				</div>
			</div>

			<div class="card mb-3">
				<div class="card-body">
					<h3>Opciones de Imágen</h3>
					<hr>

					<div class="form-group">
						<label for="image">Imagen de Portada</label>
						<input type="file" name="image" class="dropify" data-height="250" required="" />
					</div>
				</div>
			</div>

			<div class="card mb-3">
				<div class="card-body">
					<h3>Creación de Evento</h3>
					<hr>
				</div>
			</div>
		</div>
	</div>
</form>
@endsection


@section('scripts')
    <script src="{{ asset('back/assets/plugins/select2/dist/js/select2.full.min.js') }}" type="text/javascript"></script>

    <script src="{{ asset('back/assets/plugins/tinymce/tinymce.min.js') }}"></script>

    <script src="{{ asset('back/assets/plugins/dropify/dist/js/dropify.min.js') }}"></script>
    <script>
    $(document).ready(function() {
        // Basic
        $('.dropify').dropify();
        $('.select2').select2();
    });
    </script>

    <script>
    $(document).ready(function() {

        if ($("#mymce_admission").length > 0) {
            tinymce.init({
                selector: "textarea#mymce_admission",
                theme: "modern",
                language: 'es_MX',
                height: 300,
                plugins: [
                    "advlist autolink link image lists charmap print preview hr anchor pagebreak spellchecker",
                    "searchreplace wordcount visualblocks visualchars code fullscreen insertdatetime media nonbreaking",
                    "save table contextmenu directionality emoticons template paste textcolor"
                ],
                toolbar: "insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | l      ink image | print preview media fullpage | forecolor backcolor emoticons",

            });
        }
    });

    $(document).ready(function() {

        if ($("#mymce_egress").length > 0) {
            tinymce.init({
                selector: "textarea#mymce_egress",
                theme: "modern",
                language: 'es_MX',
                height: 300,
                plugins: [
                    "advlist autolink link image lists charmap print preview hr anchor pagebreak spellchecker",
                    "searchreplace wordcount visualblocks visualchars code fullscreen insertdatetime media nonbreaking",
                    "save table contextmenu directionality emoticons template paste textcolor"
                ],
                toolbar: "insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | l      ink image | print preview media fullpage | forecolor backcolor emoticons",

            });
        }
    });
    </script>
@endsection