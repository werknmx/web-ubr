@extends('back.layouts.app')

@section('stylesheets')

@endsection

@section('content')
<div class="row page-titles">
    <div class="col-md-5 align-self-center">
        <h3 class="text-themecolor">Cursos</h3>
    </div>
    <div class="col-md-7 align-self-center">
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="javascript:void(0)">Inicio</a></li>
            <li class="breadcrumb-item active">Cursos</li>
        </ol>
    </div>
</div>

<div class="row">
    <div class="col">
        @include('back.layouts.partials._mensajes')
    </div>
</div>

<div class="row mb-4">
    <div class="col col-md-12">
        <a href="{{ route('cursos.create') }}" class="btn btn-rounded btn-success"><i class="fa fa-plus-circle m-r-5"></i> Nuevo Curso</a>
    </div>
</div>

<div class="row">
    <div class="col-md-12">
        <div class="card">
            <div class="card-body">
                <h3>Cursos</h3>
                <hr>

                @if($cursos->count() == NULL || $cursos->count() == 0)
                    <p>No hay cursos. ¡Comienza a creando uno!</p>
                @else 
                <div class="table-responsive">
                    <table class="table">
                        <thead>
                            <tr>
                                <th>#</th>
                                <th>Titulo</th>
                                <th>Descripción</th>
                                <th>Creado el</th>
                                <th>Acciones</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($cursos as $cur)
                            <tr>
                                <td>{{ $cur->id }}</td>
                                <td>{{ $cur->title }}</td>
                                <td>{{ $cur->description }}</td>
                                <td>{{ $cur->created_at }}</td>
                               
                                <td class="text-nowrap">
                                    <a href="{{ route('cursos.show', $cur->id) }}" data-toggle="tooltip" data-original-title="Ver"><i class="mdi mdi-eye text-inverse m-r-10"></i> </a>
                                    <a href="{{ route('cursos.edit', $cur->id) }}" data-toggle="tooltip" data-original-title="Editar"><i class="mdi mdi-pencil text-inverse"></i></a>

                                    <form method="POST" class="delete" action="{{ route('cursos.destroy', $cur->id) }}" style="display: inline-block;">
                                        <button type="submit" class="btn btn-sm btn-outline-danger" data-toggle="tooltip" data-original-title="Borrar"><i class="ti-trash"></i></button>
                                        {{ csrf_field() }}
                                        {{ method_field('DELETE') }}
                                    </form>
                                </td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
                @endif
            </div>
        </div>
    </div>
</div>
@endsection

@section('scripts')
<script>
    $(".delete").on("submit", function(){
        return confirm("¿Estás seguro de querer borrar el curso? Si borras este elemento también se eliminarán las imágenes relacionadas y no podrán ser recuperadas.");
    });
</script>
@endsection