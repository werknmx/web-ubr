@extends('back.layouts.app')

@section('stylesheets')

@endsection

@section('content')
<div class="row page-titles">
	<div class="col-md-5 align-self-center">
		<h3 class="text-themecolor">Detalle de Asesor</h3>
	</div>
	<div class="col-md-7 align-self-center">
		<ol class="breadcrumb">
			<li class="breadcrumb-item"><a href="{{ route('index') }}">Inicio</a></li>
			<li class="breadcrumb-item active">Detalle de Asesor</li>
		</ol>
	</div>
</div>

<div class="row">

</div>
@endsection

@section('scripts')
<script type="text/javascript">
	$(function () {
		$('[data-toggle="tooltip"]').tooltip()
	});
</script>
@endsection