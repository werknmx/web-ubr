@extends('back.layouts.app')

@section('stylesheets')

@endsection

@section('content')
<div class="row page-titles">
	<div class="col-md-5 align-self-center">
		<h3 class="text-themecolor">Historial de Actividad</h3>
	</div>
	<div class="col-md-7 align-self-center">
		<ol class="breadcrumb">
			<li class="breadcrumb-item"><a href="{{ route('index') }}">Inicio</a></li>
			<li class="breadcrumb-item active">Historial de Actividad</li>
		</ol>
	</div>
</div>

<div class="row">
	<div class="col-md-3">
		<div class="card">
			<div class="card-body">
				<div class="p-2"><span class="round" style="background-color: {{ $activity->type->hex  }}"><i class="{{ $activity->type->icon_code }}"></i></span></div>
            	<h5>{{ $activity->type->verb }} <a href="{{ route('listados.show', $activity->contact_id) }}">{{ $activity->contact->name . ' ' . $activity->contact->sur_name }}</a></h5>

            	<hr>

            	<h6 class="mb-2 mt-4">Descripción</h6>
				<p>{{ $activity->activity_description }}</p>

				<h6 class="mb-2 mt-4">Intereses del Prospecto</h6>
            	@foreach($activity->interests as $int)
                   <p>{{ $int->title }}</p>
                @endforeach

                <h6 class="mb-2 mt-4">Estado</h6>
            	@if($activity->activity_status == 'Completada')
            	<span class="label label-success">
            	@elseif($activity->activity_status == 'Pendiente')
            	<span class="label label-primary">
            	@elseif($activity->activity_status == 'Importante')
            	<span class="label label-warning">
            	@else
            	<span class="label label-danger">
            	@endif
            		{{ $activity->activity_status }}
            	</span>

            	<h6 class="mb-2 mt-4">Fecha de Creación</h6>
                @php
			        $tiempo = strtotime($activity->date_end . ' ' . $activity->delivery_time);

			        $date_real = date('d/M . h:i A', $tiempo);

			        $creado = date('d/M . h:i A', strtotime($activity->created_at));
			    @endphp

			    <p><span class="text-muted"><i class="fa fa-clock-o"></i> {{ $creado }}</span></p>

			    <h6 class="mb-2 mt-4">Fecha de Enterga Estimada</h6>
                <p><span class="text-muted"><i class="fa fa-clock-o"></i> {{ $date_real }}</span></p>
			</div>
		</div>	
	</div>

	<div class="col-md-9">
		<div class="row">
			<div class="col-md-6">
				<div class="card">
		            <div class="card-body">
		            	<h4 class="mt-0 header-title mb-4">Historial de Intentos</h4>

		                <div class="table-responsive">
							<table class="table">
								<thead>
									<tr>
										<th>Comentario</th>
										<th>Fecha</th>
									</tr>
								</thead>
								<tbody>
									@foreach($activity->tries as $tr)
		    							<tr>
		    								<td>{{ $tr->comment }}</td>
		    								<td class="text-muted"><i class="fa fa-clock-o"></i> {{ $tr->created_at->format('d/M/y H:i') }}</td>
		    							</tr>
									@endforeach
								</tbody>
							</table>
						</div>
		            </div>
		        </div>
			</div>
			<div class="col-md-6">
				<div class="card">
		            <div class="card-body">
		            	<h4 class="mt-0 header-title mb-4">Historial de Usos</h4>

		                <div class="table-responsive">
							<table class="table">
								<thead>
									<tr>
										<th>Mensaje</th>
										<th>Fecha</th>
									</tr>
								</thead>
								<tbody>
									@foreach($activity->logs as $tr)
		    							<tr>
		    								<td>{{ $tr->log }}</td>
		    								<td class="text-muted"><i class="fa fa-clock-o"></i> {{ $tr->created_at->format('d/M/y H:i') }}</td>
		    							</tr>
									@endforeach
								</tbody>
							</table>
						</div>
		            </div>
		        </div>
			</div>
		</div>
        
    </div>
</div>
@endsection

@section('scripts')
<script type="text/javascript">
	$(function () {
		$('[data-toggle="tooltip"]').tooltip()
	});
</script>
@endsection