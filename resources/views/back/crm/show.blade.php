 @extends('back.layouts.app')

@section('stylesheets')
<link href="{{ asset('back/assets/plugins/clockpicker/dist/jquery-clockpicker.min.css') }}" rel="stylesheet">

<link href="{{ asset('back/assets/plugins/select2/dist/css/select2.min.css') }}" rel="stylesheet" type="text/css" />

<style type="text/css">
    .clockpicker-popover{
        z-index: 1999;
    }

    .select2-container{
        width: 100% !important;
        display: block;
    }
</style>

<style type="text/css">
	.more-info-btn{
		display: block;
		width: 100%;
		position: relative;
		top: -20px;
		text-transform: uppercase;
		text-align: center;
	}

    .round{
    	width: 35px !important;
    	height: 35px !important;
    }

    .round i{
    	position: relative !important;
    	line-height: 35px !important;
    	top: -5px;
    }

    .btn-white{
    	position: absolute;
    	top: -15px;
    	left: -15px;
    	border-radius: 100%;
    	background: #ffb22b !important;
    	color: #fff;
    	height: 30px;
    	width: 30px;
    	line-height: 30px;
    	text-align: center;
    	padding: 0px;
    }

    .badge-pill{
    	display: inline-block;
    	margin-bottom: 5px;
    }

    #activity_box{
    	display: none;
    }

    #button_submit{
    	display: none;
    }
</style>
@endsection

@section('content')
<div class="row page-titles">
	<div class="col-md-5 align-self-center">
		<h3 class="text-themecolor">Oportunidad de Venta</h3>
	</div>
	<div class="col-md-7 align-self-center">
		<ol class="breadcrumb">
			<li class="breadcrumb-item"><a href="{{ route('index') }}">Inicio</a></li>
			<li class="breadcrumb-item"><a href="{{ route('listados.index') }}">Prospectos</a></li>
			<li class="breadcrumb-item active">#{{ $contact->id }}</li>
		</ol>
	</div>
</div>

<div class="row">
	<div class="col-md-12">
		<ul class="list-inline pull-right">
			<li class="list-inline-item"><a class="btn btn-info btn-block mb-2" href="javascript:void(0)" data-toggle="modal" data-target="#estado-modal"><i class="mdi mdi-delta"></i> Editar Estado</a></li>

            <div id="estado-modal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
                <div class="modal-dialog">
                    <div class="modal-content">
                    	<form method="POST" action="{{ route('crm.updateStatus', $contact->id) }}">
						{{ csrf_field() }}
						{{ method_field('PUT') }}
                            <div class="modal-header">
                                <h4 class="modal-title">Editar Estado</h4>
                                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                            </div>
                            <div class="modal-body">
                            	
           
                    			<div class="form-group mb-0" >
									<select class="form-control select2" name="status" required="" data-placeholder="Escoge una opción...">
	                                    <option value="Registro">Registro</option>
	                                    <option value="Prospecto">Prospecto</option>
	                                    <option value="Interesado">Interesado</option>
	                                    <option value="Promesa de Venta">Promesa de Venta</option>
	                                    <option value="Cierre Exitoso">Cierre Exitoso</option>
	                                    <option value="Cierre Perdido">Cierre Perdido</option>
	                                </select>
								</div>
        
                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-default waves-effect" data-dismiss="modal">Cerrar</button>
                                <button type="submit" class="btn btn-danger waves-effect waves-light">Guardar Cambios</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
           
			<li class="list-inline-item"><a class="btn btn-warning btn-sm btn-block mb-2" href="{{ route('listados.edit', $contact->id) }}"><i class="mdi mdi-pencil"></i> Editar</a></li>

			<li class="list-inline-item">
				<form method="POST" action="{{ route('listados.destroy', $contact->id ) }}">
					<button type="submit" class="btn btn-danger btn-sm btn-block"><i class="mdi mdi-delete-forever"></i> Borrar</button>
					{{ csrf_field() }}
					{{ method_field('DELETE') }}
				</form>
			</li>
		</ul>
    </div>
</div>

<div class="row">
	<div class="col">
		@include('back.layouts.partials._mensajes')
	</div>
</div>

<div class="row">
	<div class="col-md-12">
		<div class="card card-body">
			<div class="row align-items-center">
				<div class="col-md-3">
					<h5>Asesor Educativo</h5>
					<div class="media align-items-center">
						<img class="d-flex mr-3" src="https://www.gravatar.com/avatar/867a37177d1fb5773a54c71b98825ef5?d=retro&amp;s=60" alt="Cuenta Ventas" width="35">

						<div class="media-body">
							<h5 class="mt-0 mb-1"><a href="#">{{ $contact->sales_agent->name }}</a></h5>
						</div>
					</div>
				</div>

				<div class="col-md-3 text-center">
					<h5>Potencial de Venta</h5>

					<p class="mb-0">${{ number_format( $total_potencia ) }}</p>
				</div>

				<div class="col-md-3 text-center">
					<h5>Origen Global</h5>
					@if($contact->landing_origin_id == NULL)
					<p class="mb-0">{{ $contact->global_origin }}</p>
					@else
					<p class="mb-0">Landing Page</p>
					@endif
				</div>

				<div class="col-md-3 text-center">
					<h5>Campaña (Origen)</h5>
					<p class="mb-0">
						@if($contact->landing_origin_id == NULL)
						Registro Manual
						@else
						{{ $contact->origin->title }} <a href="{{ route('campanas.show', $contact->origin->id) }}" data-toggle="tooltip" data-placement="bottom" title="Ir a la Campaña"><i class="mdi mdi-web"></i></a>
						@endif
					</p>
				</div>
			</div>
		</div>
	</div>

	<!-- ESTADOS DE CONTACTO -->
	<div class="col-md-12">

		<style type="text/css">
			.estados-row .col:last-child .line{
				visibility: hidden;
			}

			.progress{
				overflow: initial;
			}

			.progress-bar{
				position: relative;
			}

			.progress-bar .circle{
				position: absolute;
				right: -10px;
				top: -10px;
				width: 35px;
				height: 35px;
				line-height: 33px;
				border: 1px solid #fff;
				text-align: center;
				color: #fff;
				z-index: 10;
				border-radius: 100%;
				font-size: .8em;
			}

			.estados-row-btns .btn{
				border-radius: 0px;
				padding: 26.5px 0px;
			}

			.estados-row-btns .btn-cotizacion{
				padding: .75rem 1.5rem !important;
			}

			.estados-row-btns .btn small{
			    display: block;
			    font-size: .5em;
			    margin: 0px auto;
			    text-transform: uppercase;
			}
		</style>

		<div class="progress mb-3">
			@php
				if($contact->status == 'Registro'){
	        		$porcentaje = (0*100)/5;
	        		$status_id = 1;
				}elseif($contact->status == 'Prospecto'){
	        		$porcentaje = (2*100)/5;
	        		$status_id = 2;
	        	}elseif($contact->status == 'Interesado'){
	        		$porcentaje = (3*100)/5;
	        		$status_id = 3;
	        	}elseif($contact->status == 'Promesa de Venta'){
	        		$porcentaje = (4*100)/5;
	        		$status_id = 4;
	        	}elseif($contact->status == 'Cierre Exitoso'){
	        		$porcentaje = (5*100)/5;
	        		$status_id = 5;
	        	}elseif($contact->status == 'Cierre Perdido'){
	        		$porcentaje = (5*100)/5;
	        		$status_id = 5;
	        	}else{
	        		$porcentaje = (1*100)/5;
	        		$status_id = 0;
	        	}
	        @endphp

	        @if($contact->status == 'Registro')
		        <div class="progress-bar bg-danger" style="width: {{ ($porcentaje) }}%" aria-valuenow="{{ $status_id }}" role="progressbar">
		        	<div class="circle bg-secondary">{{ $porcentaje }}%</div>
		        </div>
	        @endif

	        @if($contact->status == 'Prospecto')
		        <div class="progress-bar bg-danger" style="width: {{ ($porcentaje) }}%" aria-valuenow="{{ $status_id }}" role="progressbar">
		        	<div class="circle bg-secondary">{{ $porcentaje }}%</div>
		        </div>
	        @endif

	        @if($contact->status == 'Interesado')
		        <div class="progress-bar bg-warning" style="width: {{ ($porcentaje) }}%" aria-valuenow="{{ $status_id }}" role="progressbar">
		        	<div class="circle bg-secondary">{{ $porcentaje }}%</div>
		        </div>
	        @endif

	        @if($contact->status == 'Promesa de Venta')
		        <div class="progress-bar bg-info" style="width: {{ ($porcentaje) }}%" aria-valuenow="{{ $status_id }}" role="progressbar">
		        	<div class="circle bg-secondary">{{ $porcentaje }}%</div>
		        </div>
	        @endif
	        
	        @if($contact->status == 'Cierre Exitoso')
		        <div class="progress-bar bg-success" style="width: {{ $porcentaje }}%" aria-valuenow="{{ $status_id }}" role="progressbar">
		        	<div class="circle bg-secondary">{{ $porcentaje }}%</div>
		        </div>
	        @endif	        
	    </div>

		<div class="row no-gutters estados-row-btns">

			<!-- REGISTRO -->
			@if($contact->status == 'Registro')
				<div class="col">
					<a href="{{ route('crm.completarRegistro', $contact->id) }}" class="btn btn-info btn-lg btn-block mb-5">Completar <i class="mdi mdi-arrow-right"></i><small>Registro</small> </a>
				</div>
			@else
				<div class="col">
					<a href="#" class="btn btn-info btn-lg btn-block mb-5"><i class="mdi mdi-checkbox-marked-circle-outline"></i><small>Registro</small></a>
				</div>
			@endif

			<!-- PROSPECTO -->
			@if($contact->status == 'Prospecto')
				<div class="col">
					<a href="{{ route('crm.completarProspecto', $contact->id) }}" class="btn btn-info btn-lg btn-block mb-5">Completar <i class="mdi mdi-arrow-right"></i><small>Prospecto</small> </a>
				</div>
			@else
				@if($contact->status == 'Interesado' || $contact->status == 'Cierre Exitoso' || $contact->status == 'Promesa de Venta' || $contact->status == 'Cierre Perdido' )
					<div class="col">
						<a href="#" class="btn btn-info btn-lg btn-block mb-5"><i class="mdi mdi-checkbox-marked-circle-outline"></i><small>Prospecto</small></a>
					</div>
				@else
					<div class="col">
						<a href="#" class="btn btn-info btn-lg btn-block mb-5" style="opacity: .2;"><i class="mdi mdi-checkbox-blank-circle-outline"></i><small>Prospecto</small></a>
					</div>
				@endif
			@endif


			<!-- INTERESADO -->
			@if($contact->status == 'Interesado')
				<div class="col">
					<a href="{{ route('crm.completarInteresado', $contact->id) }}" class="btn btn-info btn-lg btn-block mb-5">Completar <i class="mdi mdi-arrow-right"></i><small>Interesado</small></a>
				</div>
			@else
				@if($contact->status == 'Cierre Exitoso' || $contact->status == 'Promesa de Venta' || $contact->status == 'Cierre Perdido' )
					<div class="col">
						<a href="#" class="btn btn-info btn-lg btn-block mb-5"><i class="mdi mdi-checkbox-marked-circle-outline"></i><small>Interesado</small></a>
					</div>
				@else
					<div class="col">
						<a href="#" class="btn btn-info btn-lg btn-block mb-5" style="opacity: .2;"><i class="mdi mdi-checkbox-blank-circle-outline"></i><small>Interesado</small></a>
					</div>
				@endif
			@endif

			<!-- PROMESA DE VENTA -->
			@if($contact->status == 'Promesa de Venta')
				<div class="col">
					<a href="javascript:void(0)" data-toggle="modal" data-target="#cierre-modal" class="btn btn-info btn-lg btn-block mb-5">Completar <i class="mdi mdi-arrow-right"></i><small>Promesa de Venta</small></a>
				</div>

	            <div id="cierre-modal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
	                <div class="modal-dialog">
	                    <div class="modal-content">
	                    	<form method="POST" action="{{ route('crm.updateStatus', $contact->id) }}">
							{{ csrf_field() }}
							{{ method_field('PUT') }}
	                            <div class="modal-header">
	                                <h4 class="modal-title">¿Que tipo de cierre tiene esta oportunidad?</h4>
	                                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
	                            </div>
	                            <div class="modal-body">
	                    			<div class="form-group mb-0" >
										<select class="form-control" name="status" style="width: 100%; height:36px;">
											<option value="0">Selecciona una Opción</option>

											<option value="Cierre Exitoso">Cierre Exitoso</option>
											<option value="Cierre Perdido">Cierre Perdido</option>
										</select>
									</div>

									<h5 class="mt-4">Información de Pago</h5>
									<hr>

									<div class="row">
										<div class="col-md-6">
											<div class="form-group">
												<label>Referencia de Pago</label>
												<input type="text" name="reference" placeholder="0000000">
											</div>
										</div>
										<div class="col-md-6">
											<div class="form-group">
												<label>Recibo de Pago / Evidencia</label>
												<input type="file" name="attachement">
											</div>
										</div>
									</div>
	                            </div>
	                            <div class="modal-footer">
	                                <button type="button" class="btn btn-default waves-effect" style="padding: 7px 12px !important;" data-dismiss="modal">Cerrar</button>
	                                <button type="submit" class="btn btn-danger waves-effect waves-light" style="padding: 7px 12px !important;">Guardar Cambios</button>
	                            </div>
	                        </form>
	                    </div>
	                </div>
	            </div>
			@else
				@if($contact->status == 'Cierre Exitoso' || $contact->status == 'Cierre Perdido')
					<div class="col">
						<a href="#" class="btn btn-info btn-lg btn-block mb-5"><i class="mdi mdi-checkbox-marked-circle-outline"></i><small>Promesa de Venta</small></a>
					</div>
				@else
					<div class="col">
						<a href="#" class="btn btn-info btn-lg btn-block mb-5" style="opacity: .2;"><i class="mdi mdi-checkbox-blank-circle-outline"></i><small>Promesa de Venta</small></a>
					</div>
				@endif
			@endif

			<!-- CIERRE GENERAL DE contact -->
			@if($contact->status == 'Cierre Exitoso')
				<div class="col">
					<a href="#" class="btn btn-info btn-lg btn-block mb-5"><i class="mdi mdi-checkbox-marked-circle-outline"></i><small>Cierre Exitoso</small></a>
				</div>
			@elseif($contact->status == 'Cierre Perdido')
				<div class="col">
					<a href="#" class="btn btn-danger btn-lg btn-block mb-5"><i class="mdi mdi-close-circle-outline"></i><small>Cierre Perdido</small></a>
				</div>
			@else
				<div class="col">
					<a href="#" class="btn btn-info btn-lg btn-block mb-5" style="opacity: .2;"><i class="mdi mdi-checkbox-blank-circle-outline"></i><small>Cierre</small></a>
				</div>
			@endif			
		</div>
	</div>
	<!-- // ESTADOS DE CONTACTO -->

	<div class="col-sm-3">
		<div class="card">
			<h6 class="client-status bg-info text-white text-center p-4 mt-0 mb-0">
				{{ $contact->global_status }}

				<a href="javascript:void(0)" class="btn-white" data-toggle="modal" data-target="#globalStatusModal"><i class="mdi mdi-pencil"></i></a>
			</h6>

			<div id="globalStatusModal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
	            <div class="modal-dialog">
	                <div class="modal-content">
	                	<form method="POST" action="{{ route('crm.updateGlobalStatus', $contact->id) }}">
						{{ csrf_field() }}
						{{ method_field('PUT') }}
	                        <div class="modal-header">
	                            <h4 class="modal-title">Editar Estado</h4>
	                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
	                        </div>
	                        <div class="modal-body">
	                			<div class="form-group mb-0" >
									<select class="form-control select2" name="global_status" required="">
					                    <option value="Activo">Activo</option>
					                    <option value="Baja Temporal">Baja Temporal</option>
					                    <option value="Baja Definitiva">Baja Definitiva</option>
					                    <option value="Egresado">Egresado</option>
					                </select>
								</div>
	                        </div>
	                        <div class="modal-footer">
	                            <button type="button" class="btn btn-default waves-effect" data-dismiss="modal">Cerrar</button>
	                            <button type="submit" class="btn btn-danger waves-effect waves-light">Guardar Cambios</button>
	                        </div>
	                    </form>
	                </div>
	            </div>
	        </div>

			<div class="text-center mt-5 mb-5">
				<img src="https://ssl.gstatic.com/accounts/ui/avatar_2x.png" class="avatar rounded-circle img-thumbnail" alt="avatar">
			</div>

			<div class="card-body">
				<small class="text-muted pt-3 db">Servicio en su Interés
				</small>

				@if($contact->interests->count() == 0)
					<h6>No se le ha asignado un interés a este prospecto. Edítalo para agregar opciones.</h6>
				@else

		    		@foreach($contact->interests as $int)
		                <a class="badge-pill badge-info" href="{{ route('programas.show', $int->id) }}" data-toggle="tooltip" data-placement="right" title="Filtrar por este Interés">{{ $int->title }}</a>
		            @endforeach
				@endif            
			</div>
		</div>

		<div class="card">
			<div class="card-header">Contacto Principal  <i class="ti-user"></i></div>
			<div class="card-body">
				<small class="text-muted pt-3 db">Nombre Completo</small>
	    		<h6>{{ $contact->name . ' ' . $contact->sur_name }}</h6> 

	    		<small class="text-muted pt-3 db">Teléfono Movil</small>
	    		<h6>{{ $contact->phone_cel }}</h6> 

	    		<small class="text-muted pt-3 db">Teléfono Principal</small>
	    		<h6>{{ $contact->phone_main }}</h6> 

	    		<small class="text-muted pt-3 db">Email</small>
	    		<h6>{{ $contact->email }}</h6>

	    		<hr class="my-5">

	    		<small class="text-muted db">Cumpleaños</small>
	    		<h6>{{ $contact->birthday }}</h6> 

	    		<small class="text-muted pt-3 db">Empresa</small>
	    		<h6>{{ $contact->company }}</h6> 

	    		<small class="text-muted pt-3 db">Puesto</small>
	    		<h6>{{ $contact->job_title }}</h6> 

	    		<small class="text-muted pt-3 db">Giro</small>
	    		<h6>{{ $contact->company_type }}</h6> 
			</div>
		</div>

		<div class="card mb-0">
			<div class="card-header">Datos de Ubicación <i class="ti-direction-alt"></i></div>

	        <iframe src="https://www.google.com/maps/embed/v1/place?key=AIzaSyDhjSfxxL1-NdSlgkiDo5KErlb7rXU5Yw4&q={{ $contact->city }},{{ $contact->state }},{{ $contact->country }}" width="100%" height="250" frameborder="0" style="border:0" allowfullscreen></iframe>

	        <div class="card-body">
				<small class="text-muted pt-3 db">País</small>
	    		<h6>{{ $contact->country }}</h6> 

	    		<small class="text-muted pt-3 db">Ciudad</small>
	    		<h6>{{ $contact->city }}</h6> 

	    		<small class="text-muted pt-3 db">Estado</small>
	    		<h6>{{ $contact->state }}</h6> 
			</div>
		</div>	
	</div>

	<!-- CENTRO DE ACTIVIDAD -->
	<div class="col-md-9">
		{{-- 
		@if($activities_completed != NULL)
		<a href="" class="btn btn-primary mb-4">Agregar Nueva Actividad <i class="ti-plus"></i></a>
		@else
		<a href="javascript:void(0)" data-toggle="tooltip" data-placement="top" title="Solo es posible tener una actividad pendiente a la vez. Completa tu actividad actual para agregar una nueva." class="btn btn-primary disabled mb-4">Agregar Nueva Actividad <i class="ti-plus"></i></a>
		<small class="ml-3">Solo es posible tener una actividad pendiente a la vez. Completa tu actividad actual para agregar una nueva.</small>
		@endif
		<!--
		<div class="card">
			<div class="card-body">
				<h6 class="mb-0">Actividades</h3>
				<hr>

				<form method="POST" action="{{ route('actividades.store') }}" enctype="multipart/form-data">
	            	{{ csrf_field() }}
	            	<div class="row">
	            		<div class="col-md-3">
	            			<div class="form-group">
	            				<label>Actividad Principal</label>
	                            <select class="form-control" name="activity_type_id">
	                                <option value="0">Selecciona una actividad...</option>
	                                @foreach($activity_types as $acty)
	                                	<option value="{{ $acty->id }}">{{ $acty->name }}</option>
	                                @endforeach
	                            </select>
	                        </div>
						</div>

						<div class="col-md-3">
	            			<div class="form-group">
	            				<label>Contacto</label>
	                            <select class="form-control" name="contact_id">
	                                <option value="0">Selecciona un contacto...</option>
	                                <option value="{{ $contact->id }}">{{ $contact->name }}</option>
	                                @foreach($contact_sons as $cts)
	                                	<option value="{{ $cts->id }}">{{ $cts->name }}</option>
	                                @endforeach
	                            </select>
	                        </div>
						</div>

						<div class="col-md-3">
	            			<div class="form-group">
	            				<label>Intereses</label>
	                            <select class="form-control select2 select2-multiple" name="interest_id[]" required="" data-placeholder="Escoge una o varias opciones..." multiple="multiple">
	                                @foreach($courses as $int)
	                                	<option value="{{ $int->id }}">{{ $int->title }}</option>
	                                @endforeach
	                            </select>
	                        </div>
						</div>

						<div class="col-md-3">
							<div class="form-group">
	            				<label>Asignado a</label>
	                            <select class="form-control select2" name="handler_id[]">
	                            	@foreach($users as $us)
	                                <option value="{{ $us->id }}">{{ $us->name }}</option>
	                                @endforeach
	                            </select>
	                        </div>
						</div>

						<div class="col-md-6">
							<div class="form-group">
								<label>Comentario</label>
								<input type="text" name="activity_description" class="form-control" placeholder="Comentarios de esta actividad...">
	                        </div>
						</div>
						<div class="col-md-3">
							<div class="form-group">
	                            <label for="date_end">Fecha de Finalización</label>
	                            <input type="date" name="date_end" class="form-control">
	                        </div>
						</div>

						<div class="col col-md-3">
	                        <div class="form-group">
	                            <label for="date_end">Hora</label>
	                            <div class="input-group clockpicker">
	                                <input type="time" class="form-control" value="12:00" name="delivery_time"> <span class="input-group-addon"> <span class="fa fa-clock-o"></span> </span>
	                            </div>
	                        </div>
	                    </div>
	                </div>
					
					<input type="hidden" name="module_name" value="Detalle de Prospecto">
					<input type="hidden" name="activity_status" value="Pendiente">
					
					<input type="hidden" name="contact_id" value="{{ $contact->id }}">
					

	                <button type="submit" class="btn btn-primary pull-right">Guardar Actividad</button>
	            </form>
			</div>
		</div>
		-->
		--}}

		<div class="card card-body">
			@if($activities->count() == NULL)
			<h3 class="text-center mb-3 mt-3">Este contacto no tiene ninguna actividad. Comienza creando una.</h3>
			@else
			<div class="row">
				<div class="col-md-12">
					<div class="row">
						<div class="col">
							<h6 class="mb-0">Listado de Actividades</h3>
						</div>
						<div class="col text-right">
							
						</div>
					</div>
					<hr>
				</div>
			</div>

			<div class="row">
				<div class="col-md-12">
					<table class="table">
			            <thead>
			                <tr>
			                    <th>Tipo de Actividad</th>
			                    <!--<th>Comentario</th>-->
			                    <th>Intentos</th>
			                    <th>Intereses</th>
			                    <th>Estado</th>
			                    <!--<th>Fecha de Creación</th>-->
			                    <th>Fecha de Entrega</th>
			                    <th>Acciones</th>
			                </tr>
			            </thead>
			            <tbody>
			            	@foreach($activities as $at)
							<tr>
			                    <td>
			                    	<div class="p-2"><span class="round" style="background-color: {{ $at->type->hex  }}"><i class="{{ $at->type->icon_code }}"></i></span></div>
			                    	<h5 data-toggle="tooltip" data-original-title="{{ $at->activity_description }}">{{ $at->type->verb }} <a href="{{ route('listados.show', $at->contact_id) }}">{{ $at->contact->name . ' ' . $at->contact->sur_name }}</a></h5>
			                    </td>
			                    <!--<td>{{ $at->activity_description }}</td>-->

			                    <td><a href="{{ route('actividad.intento.mostrar', $at->id) }}" data-toggle="tooltip" data-original-title="Historial" class="text-muted"><span class="badge badge-info">{{ $at->has_tries }}</span></a></td>

			                    <td>
			                    	@foreach($at->interests as $int)
		                               <p>{{ $int->title }}</p>
		                            @endforeach
			                    </td>
			                    <td>
			                    	@if($at->activity_status == 'Completada')
			                    	<span class="label label-success">
			                    	@elseif($at->activity_status == 'Pendiente')
			                    	<span class="label label-primary">
			                    	@elseif($at->activity_status == 'Importante')
			                    	<span class="label label-warning">
			                    	@else
			                    	<span class="label label-danger">
			                    	@endif
			                    		{{ $at->activity_status }}
			                    	</span>
			                    </td>

			                    @php
							        $tiempo = strtotime($at->date_end . ' ' . $at->delivery_time);

							        $date_real = date('d/M . h:i A', $tiempo);

							        $creado = date('d/M . h:i A', strtotime($at->created_at));
							    @endphp

							    <!--<td><span class="text-muted"><i class="fa fa-clock-o"></i> {{ $creado }}</span> </td>-->
			                    <td><span class="text-muted"><i class="fa fa-clock-o"></i> {{ $date_real }}</span> </td>
			                    <td class="text-nowrap text-right">
					                <div class="btn-group" role="group">

					                	@if($at->completed == false || $at->activity_status == 'Incompleto por Cierre')
							            <a class="btn btn-secondary" href="javascript:void(0)" data-toggle="modal" data-target="#modalCompletarTarea_{{ $at->id }}" data-toggle="tooltip" data-original-title="Completar"><i class="mdi mdi-check"></i></a>
							            @endif

							            <div class="dropdown">
							                <button class="btn btn-secondary" type="button" id="extra_options" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" data-original-title="Opciones">
							                    <i class="mdi mdi-dots-horizontal"></i>
							                </button>

							                <div class="dropdown-menu" aria-labelledby="extra_options" x-placement="bottom-start" style="position: absolute; transform: translate3d(0px, 36px, 0px); top: 0px; left: 0px; will-change: transform;">
							                	@if($at->completed == false || $at->activity_status == 'Incompleto por Cierre')
							                	<a class="dropdown-item" href="javascript:void(0)" data-toggle="modal" data-target="#modalIntentoTarea_{{ $at->id }}"><i class="mdi mdi-star"></i> Marcar Intento</a>

							                	<div class="dropdown-divider"></div>

							                    <a class="dropdown-item" href="javascript:void(0)" data-toggle="modal" data-target="#modalCompletarTarea_{{ $at->id }}"><i class="mdi mdi-check"></i> Completar Tarea</a>
							                    <a class="dropdown-item"  href="javascript:void(0)" data-toggle="modal" data-target="#modalEditarTarea_{{ $at->id }}"><i class="mdi mdi-pencil"></i> Editar Tarea</a>

							                    <a class="dropdown-item" href="{{ route('actividades.prioridad', $at->id) }}"><i class="fa fa-exclamation-circle"></i> Dar Prioridad</a>
							                    
							                    <div class="dropdown-divider"></div>
							                    @endif

							                    <a class="dropdown-item" href="{{ route('actividad.intento.mostrar', $at->id) }}">
							                    	<i class="ti-info"></i> Ver historial
							                    </a>

							                    <form method="POST" id="form_delete_{{ $at->id }}" action="{{ route('actividades.destroy', $at->id) }}">
							                        <button id="borrar_elemento_{{ $at->id }}" type="button" class="dropdown-item"><i class="mdi mdi-delete"></i> Borrar</button>
							                        {{ csrf_field() }}
							                        {{ method_field('DELETE') }}
							                    </form>
							                </div>
							            </div>
							        </div>
					    		</td>
			                </tr>

			                <!-- CREADOR DE INTENTO DE TAREA -->
			                <div class="modal fade" id="modalIntentoTarea_{{ $at->id }}" tabindex="-1" role="dialog" aria-hidden="true">
							    <div class="modal-dialog" role="document">
							        <div class="modal-content">
							            <div class="modal-header">
							                <h5 class="modal-title">Comentario de Intento</h5>
							                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
							                    <span aria-hidden="true">&times;</span>
							                </button>
							            </div>
							            <form method="POST" action="{{ route('actividad.intento', $at->id) }}">
							                {{ csrf_field() }} 
							                <div class="modal-body">
							                    <div class="row">
													<div class="col-md-12">
														<div class="form-group">
															<label>Comentario</label>
															<textarea class="form-control" name="comment" rows="4"></textarea>
								                        </div>
													</div>
								                </div>
							                </div>

							                <div class="modal-footer">
							                    <button type="submit" class="btn btn-primary">Actualizar Intento</button>
							                </div>
							            </form>
							        </div>
							    </div>
							</div>

							<!-- COMPLETAR TAREA -->
			                <div class="modal fade" id="modalCompletarTarea_{{ $at->id }}" tabindex="-1" role="dialog" aria-hidden="true">
							    <div class="modal-dialog" role="document">
							        <div class="modal-content">
							            <div class="modal-header">
							                <h5 class="modal-title">Completar Tarea</h5>
							                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
							                    <span aria-hidden="true">&times;</span>
							                </button>
							            </div>

							            <form method="POST" action="{{ route('actividades.completar', $at->id) }}">
							                {{ csrf_field() }} 

						               		<div class="modal-body">
						               			<!--
									            <div class="row">
									            	<div class="col-md-12">
									            		<h2>Felicidades {{ Auth::user()->name }} por completar la tarea.</h2>
									            		<p>Ahora planea el seguimiento para seguir con tu excelente trabajo.</p>
									            		<hr>
									            	</div>
									            </div>
									        	-->
									            <div class="row">
													<div class="col-md-12">
														<div class="form-group">
															<label>Comentario ¿Como te fue con este prospecto? ¿Cuales fueron los acuerdos en esta actividad?</label>
															<textarea class="form-control" name="comment" rows="4"></textarea>
								                        </div>
													</div>
								                </div>

								                <div id="buttons_box" class="row">
								                	<div class="col-md-6">
								                		<a href="javascript:void(0)" id="newTask" class="btn btn-primary btn-block">Nueva Actividad</a>
								                	</div>
								                	<div class="col-md-6">
								                		<a href="{{ route('crm.prospectoPerdido', $contact->id) }}" class="btn btn-danger btn-block">Prospecto Perdido</a>
								                	</div>
								                </div>

								                <!-- NUEVA TAREA -->
								            	<div id="activity_box">
								            		<div class="row mt-4 mb-4">
								            			<div class="col-md-12 text-right">
								            				<a href="javascript:void(0)" id="CancelNewTask" class="btn btn-secondary btn-sm mt-0 mb-2">Cancelar Nueva Actividad</a>
								            			</div>

									            		<div class="col-md-6">
									            			<div class="form-group">
									            				<label>Actividad Principal</label>
									                            <select class="form-control" name="activity_type_id">
									                                <option value="0">Selecciona una actividad...</option>
									                                @foreach($activity_types as $acty)
									                                	<option value="{{ $acty->id }}">{{ $acty->name }}</option>
									                                @endforeach
									                            </select>
									                        </div>
														</div>

														<div class="col-md-6">
									            			<div class="form-group">
									            				<label>Contacto</label>
									                            <select class="form-control" name="contact_id">
									                                <option value="0">Selecciona un contacto...</option>
									                                <option value="{{ $contact->id }}">{{ $contact->name }}</option>
									                                @foreach($contact_sons as $cts)
									                                	<option value="{{ $cts->id }}">{{ $cts->name }}</option>
									                                @endforeach
									                            </select>
									                        </div>
														</div>

														<div class="col-md-12">
									            			<div class="form-group">
									            				<label>Intereses</label>
									                            <select class="form-control select2 select2-multiple" name="interest_id[]" required="" data-placeholder="Escoge una o varias opciones..." multiple="multiple">
									                                @foreach($courses as $int)
									                                	<option value="{{ $int->id }}">{{ $int->title }}</option>
									                                @endforeach
									                            </select>
									                        </div>
														</div>

														<div class="col-md-3" style="display: none;">
															<div class="form-group">
									            				<label>Asignado a</label>
									                            <select class="form-control select2" name="handler_id[]">
									                            	@foreach($users as $us)
									                                <option value="{{ $us->id }}">{{ $us->name }}</option>
									                                @endforeach
									                            </select>
									                        </div>
														</div>

														<div class="col-md-12">
															<div class="form-group">
																<label>Comentario</label>
																<input type="text" name="activity_description" class="form-control" placeholder="Comentarios de esta actividad...">
									                        </div>
														</div>
														<div class="col-md-6">
															<div class="form-group">
									                            <label for="date_end">Fecha de Finalización</label>
									                            <input type="date" name="date_end" class="form-control">
									                        </div>
														</div>

														<div class="col col-md-6">
									                        <div class="form-group">
									                            <label for="date_end">Hora</label>
									                            <div class="input-group clockpicker">
									                                <input type="time" class="form-control" value="12:00" name="delivery_time"> <span class="input-group-addon"> <span class="fa fa-clock-o"></span> </span>
									                            </div>
									                        </div>
									                    </div>
									                </div>

									                <!-- COMPLETAR ETAPA -->
									                <div class="row">
									                	<div class="col text-right">
									                		<div class="form-check">
									                			<input class="form-check-input" type="checkbox" name="update_status" id="defaultCheck1">
									                			<label class="form-check-label" for="defaultCheck1">
									                				COMPLETAR ETAPA
									                			</label>
									                		</div>
									                	</div>
									                </div>
								            	</div>
							                </div>

							                <div class="modal-footer">
												<input type="hidden" name="module_name" value="Detalle de Prospecto">
												<input type="hidden" name="activity_status" value="Pendiente">
												<input type="hidden" name="contact_id" value="{{ $contact->id }}">

							                    <button id="button_submit" type="submit" class="btn btn-primary">Guardar Seguimiento</button>
							                </div>
							            </form>
							        </div>
							    </div>
							</div>

							<!-- EDITAR TAREA -->
			                <div class="modal fade" id="modalEditarTarea_{{ $at->id }}" tabindex="-1" role="dialog" aria-hidden="true">
							    <div class="modal-dialog" role="document">
							        <div class="modal-content">
							            <div class="modal-header">
							                <h5 class="modal-title">Editar Tarea</h5>
							                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
							                    <span aria-hidden="true">&times;</span>
							                </button>
							            </div>
							            <form method="POST" action="{{ route('actividades.update', $at->id) }}">
							                {{ csrf_field() }} 
							                {{ method_field('PUT') }} 
							                <div class="modal-body">
							                    <div class="row">
								            		<div class="col-md-6">
								            			<div class="form-group">
								            				<label>Actividad Principal</label>
								                            <select class="form-control" name="activity_type_id">
								                                <option value="0">Selecciona una actividad...</option>
								                                @foreach($activity_types as $acty)
								                                	<option {{ ($acty->id == $at->activity_type_id) ? 'selected' : '' }} value="{{ $acty->id }}">{{ $acty->name }}</option>
								                                @endforeach
								                            </select>
								                        </div>
													</div>

													<div class="col-md-6">
														<div class="form-group">
								            				<label>Asignado a</label>
								                            <select class="form-control select2" name="handler_id[]">
								                            	@foreach($users as $us)
								                                <option value="{{ $us->id }}">{{ $us->name }}</option>
								                                @endforeach
								                            </select>
								                        </div>
													</div>

													<div class="col-md-12">
														<div class="form-group">
															<label>Comentario</label>
															<input type="text" name="activity_description" class="form-control" value="{{ $at->activity_description }}">
								                        </div>
													</div>
													<div class="col-md-6">
														<div class="form-group">
								                            <label for="date_end">Fecha de Finalización</label>
								                            <input type="date" name="date_end" value="{{ $at->date_end }}" class="form-control">
								                        </div>
													</div>

													<div class="col col-md-6">
								                        <div class="form-group">
								                            <label for="date_end">Hora</label>
								                            <div class="input-group clockpicker">
								                                <input type="time" class="form-control" value="{{ $at->delivery_time }}" name="delivery_time"> <span class="input-group-addon"> <span class="fa fa-clock-o"></span> </span>
								                            </div>
								                        </div>
								                    </div>
								                </div>
							                </div>

							                <input type="hidden" name="activity_status" value="{{ $at->activity_status }}">
							                <div class="modal-footer">
							                    <button type="submit" class="btn btn-primary">Actualizar Tarea</button>
							                </div>
							            </form>
							        </div>
							    </div>
							</div>
			            	@endforeach

			            	@foreach($contact_sons as $cts)
				            	@foreach($cts->activities as $at)
								<tr>
				                    <td>
				                    	<div class="p-2"><span class="round" style="background-color: {{ $at->type->hex  }}"><i class="{{ $at->type->icon_code }}"></i></span></div>
				                    	<h5>{{ $at->type->verb }} <a href="{{ route('listados.show', $at->contact_id) }}">{{ $at->contact->name . ' ' . $at->contact->sur_name }}</a></h5>
				                    </td>
				                    <td>{{ $at->activity_description }}</td>
				                    <td>
				                    	@if($at->activity_status == 'Completada')
				                    	<span class="label label-success">
				                    	@elseif($at->activity_status == 'Pendiente')
				                    	<span class="label label-primary">
				                    	@elseif($at->activity_status == 'Importante')
				                    	<span class="label label-warning">
				                    	@else
				                    	<span class="label label-danger">
				                    	@endif
				                    		{{ $at->activity_status }}
				                    	</span>
				                    </td>

				                    @php
								        $tiempo = strtotime($at->date_end . ' ' . $at->delivery_time);

								        $date_real = date('d/M . h:i A', $tiempo);

								        $creado = date('d/M . h:i A', strtotime($at->created_at));
								    @endphp

								    <td><span class="text-muted"><i class="fa fa-clock-o"></i> {{ $creado }}</span> </td>
				                    <td><span class="text-muted"><i class="fa fa-clock-o"></i> {{ $date_real }}</span> </td>
				                    <td class="text-nowrap text-right">
						                <div class="btn-group" role="group">

						                	@if($at->completed == false)
								            <a class="btn btn-secondary" href="{{ route('actividades.completar', $at->id) }}" data-toggle="tooltip" data-original-title="Completar"><i class="mdi mdi-check"></i></a>
								            @endif


								            <div class="dropdown">
								                <button class="btn btn-secondary" type="button" id="extra_options" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" data-original-title="Opciones">
								                    <i class="mdi mdi-dots-horizontal"></i>
								                </button>

								                <div class="dropdown-menu" aria-labelledby="extra_options" x-placement="bottom-start" style="position: absolute; transform: translate3d(0px, 36px, 0px); top: 0px; left: 0px; will-change: transform;">

								                	@if($at->completed == false)
								                    <a class="dropdown-item" href="{{ route('actividades.completar', $at->id) }}"><i class="mdi mdi-check"></i> Completar Tarea</a>
								                    <a class="dropdown-item"  href="javascript:void(0)" data-toggle="modal" data-target="#modalEditarTarea_{{ $at->id }}"><i class="mdi mdi-pencil"></i> Editar Tarea</a>

								                    <a class="dropdown-item" href="{{ route('actividades.prioridad', $at->id) }}"><i class="fa fa-exclamation-circle"></i> Dar Prioridad</a>
								                    
								                    <div class="dropdown-divider"></div>
								                    @endif
								                    <a class="dropdown-item" href="{{ route('actividad.intento.mostrar', $at->id) }}">
								                    	<i class="ti-info"></i> Ver historial
								                    </a>

								                    <form method="POST" id="form_delete_{{ $at->id }}" action="{{ route('actividades.destroy', $at->id) }}">
								                        <button id="borrar_elemento_{{ $at->id }}" type="button" class="dropdown-item"><i class="mdi mdi-delete"></i> Borrar</button>
								                        {{ csrf_field() }}
								                        {{ method_field('DELETE') }}
								                    </form>
								                </div>
								            </div>
								        </div>
						    		</td>
				                </tr>

				                <div class="modal fade" id="modalEditarTarea_{{ $at->id }}" tabindex="-1" role="dialog" aria-hidden="true">
								    <div class="modal-dialog" role="document">
								        <div class="modal-content">
								            <div class="modal-header">
								                <h5 class="modal-title">Editar Tarea</h5>
								                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
								                    <span aria-hidden="true">&times;</span>
								                </button>
								            </div>
								            <form method="POST" action="{{ route('actividades.update', $at->id) }}">
								                {{ csrf_field() }} 
								                {{ method_field('PUT') }} 
								                <div class="modal-body">
								                    <div class="row">
									            		<div class="col-md-6">
									            			<div class="form-group">
									            				<label>Actividad Principal</label>
									                            <select class="form-control" name="activity_type_id">
									                                <option value="0">Selecciona una actividad...</option>
									                                @foreach($activity_types as $acty)
									                                	<option {{ ($acty->id == $at->activity_type_id) ? 'selected' : '' }} value="{{ $acty->id }}">{{ $acty->name }}</option>
									                                @endforeach
									                            </select>
									                        </div>
														</div>

														<div class="col-md-6">
															<div class="form-group">
									            				<label>Asignado a</label>
									                            <select class="form-control select2" name="handler_id[]">
									                            	@foreach($users as $us)
									                                <option value="{{ $us->id }}">{{ $us->name }}</option>
									                                @endforeach
									                            </select>
									                        </div>
														</div>

														<div class="col-md-12">
															<div class="form-group">
																<label>Comentario</label>
																<input type="text" name="activity_description" class="form-control" value="{{ $at->activity_description }}">
									                        </div>
														</div>
														<div class="col-md-6">
															<div class="form-group">
									                            <label for="date_end">Fecha de Finalización</label>
									                            <input type="date" name="date_end" value="{{ $at->date_end }}" class="form-control">
									                        </div>
														</div>

														<div class="col col-md-6">
									                        <div class="form-group">
									                            <label for="date_end">Hora</label>
									                            <div class="input-group clockpicker">
									                                <input type="time" class="form-control" value="{{ $at->delivery_time }}" name="delivery_time"> <span class="input-group-addon"> <span class="fa fa-clock-o"></span> </span>
									                            </div>
									                        </div>
									                    </div>
									                </div>
								                </div>

								                <input type="hidden" name="activity_status" value="{{ $at->activity_status }}">
								                <div class="modal-footer">
								                    <button type="submit" class="btn btn-primary">Actualizar Tarea</button>
								                </div>
								            </form>
								        </div>
								    </div>
								</div>
				            	@endforeach
			            	@endforeach
			            </tbody>
			        </table>
				</div>
			</div>
		    @endif
		</div>

		<div class="card card-body mb-3">
			<div class="row">
				<div class="col-md-12">
					<div class="row">
						<div class="col">
							<h6 class="mb-0">Contactos Adicionales</h3>
						</div>
						<div class="col text-right">
							<a href="" data-toggle="modal" data-target="#newContactModal" class="btn btn-primary mb-0 btn-sm">Agregar Contacto <i class="ti-plus"></i></a>
						</div>
					</div>
					<hr>
				</div>
			</div>
			
			@if($contact_sons->count() == NULL)
			<h3 class="text-center mb-3 mt-3">Este contacto no tiene adicionales vinculados. Puedes hacerlo desde la pantalla de edición o creando uno nuevo con el botón superior.</h3>
			@else
		        <table class="table">
		            <thead>
		                <tr>
		                    <th>Nombre</th>
		                    <th>Teléfono Móvil</th>
		                    <th>Teléfono Principal</th>
		                    <th>Email</th>
		                    <th>Intereses</th>
		                    <th>Acciones</th>
		                </tr>
		            </thead>
		            <tbody>
		            	@foreach($contact_sons as $ct)
						<tr>
		                    <td>{{ $ct->name }} {{ $ct->sur_name }}</td>
		                    <td>{{ $ct->phone_cel }}</td>
		                    <td>{{ $ct->phone_main }}</td>
		                    <td>{{ $ct->email }}</td>
		                    <td>
		                    	@if($contact->interests->count() == 0)
									No se le ha asignado un interés a este prospecto. Edítalo para agregar opciones.
								@else
						    		@foreach($contact->interests as $int)
						                <a class="badge-pill badge-info" href="{{ route('programas.show', $int->id) }}" data-toggle="tooltip" data-placement="right" title="Filtrar por este Interés">{{ $int->title }}</a>
						            @endforeach
								@endif  
		                    </td>

		                    <td class="text-nowrap text-right">

				    		</td>
		                </tr>
		            	@endforeach
		            </tbody>
		        </table>
		    @endif
		</div>

		<div class="card card-body">
			<div class="row">
				<div class="col-md-12">
					<div class="row">
						<div class="col">
							<h6 class="mb-0">Historial de Prospecto</h3>
						</div>
					</div>
					<hr>
				</div>
			</div>
			
			@if($contact->logs->count() == NULL)
			<h3 class="text-center mb-3 mt-3">No hay ninguna acción para este prospecto.</h3>
			@else
		        <table class="table">
		            <thead>
		                <tr>
		                    <th>Mensaje</th>
		                    <th>Fecha</th>
		                </tr>
		            </thead>
		            <tbody>
		            	@foreach($contact->logs as $log)
						<tr>
		                    <td>{{ $log->log }}</td>
		               
		                    <td class="text-nowrap text-right">
		                    	{{ $log->created_at }}
				    		</td>
		                </tr>
		            	@endforeach
		            </tbody>
		        </table>
		    @endif
		</div>
	</div>
</div>

<!-- Modal -->
<div class="modal fade" id="newContactModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <form method="POST" action="{{ route('listados.store') }}" enctype="multipart/form-data">
            {{ csrf_field() }}
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Registrar nuevo Contacto</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>

                <div class="modal-body">
                    
                    <h5 class="text-uppercase">Datos de Contacto</h5>
                    <hr>
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label>Nombre</label>
                                <input class="form-control" name="name" type="text" required="">
                            </div>
                        </div>

                        <div class="col-md-6">
                            <div class="form-group">
                                <label>Apellido</label>
                                <input class="form-control" name="sur_name" type="text">
                            </div>
                        </div>

                        <div class="col-md-12">
                            <div class="form-group">
                                <label>Email</label>
                                <input class="form-control" name="email" type="text">
                            </div>
                        </div>

                        <div class="col-md-6">
                            <div class="form-group">
                                <label>Celular</label>
                                <input class="form-control" name="phone_main" type="text" required="">
                            </div>
                        </div>

                        <div class="col-md-6">
                            <div class="form-group">
                                <label>Teléfono</label>
                                <input class="form-control" name="phone_cel" type="text">
                            </div>
                        </div>

                        <div class="col-md-4">
                            <div class="form-group">
                                <label>Empresa</label>
                                <input class="form-control" name="company" type="text">
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <label>Puesto</label>
                                <input class="form-control" name="job_title" type="text">
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <label>Giro</label>
                                <input class="form-control" name="company_type" type="text">
                            </div>
                        </div>

                        <div class="col-md-12">
                            <div class="form-group">
                                <label>Cumpleaños</label>
                                <input class="form-control" name="birthday" type="date">
                            </div>
                        </div>
                    </div>

                    <h5 class="text-uppercase">Información de Estado</h5>
                    <hr>

                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group">
                                <label>Servicio/Producto en su Interés</label>

                                <select class="form-control select2 select2-multiple" name="interest_id[]" required="" data-placeholder="Escoge una o varias opciones..." multiple="multiple">
                                    @foreach($courses as $cr)
                                        <option value="{{ $cr->id }}">{{ $cr->title }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>

                        <input type="hidden" name="status" value="Registro">
                        <input type="hidden" name="assigned_to" value="{{ $contact->sales_agent->id }}">

                        <input type="hidden" name="contact_id" value="{{ $contact->id }}">

                        <div class="col-md-12">
                            <div class="form-group">
                                <label>Origen Global</label>
                                <select class="form-control select2" name="global_origin" data-placeholder="Escoge una opción...">
                                    <option value="Facebook">Facebook</option>
                                    <option value="Instagram">Instagram</option>
                                    <option value="WhatsApp">WhatsApp</option>
                                    <option value="Convenio Empresa">Convenio Empresa</option>
                                    <option value="Referido">Referido</option>
                                    <option value="Otro">Otro</option>
                                </select>
                            </div>
                        </div>

                        <!--
                        <div class="col-md-12">
                            <div class="form-group">
                                <label>Vincular con Campaña</label>
                                <select class="form-control select2" name="landing_origin_id" required="">
                                    @foreach($landing_pages as $la)
                                        <option value="{{ $la->id }}">{{ $la->title }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        -->
                    </div>

                    <h5 class="text-uppercase">Datos de Ubicación</h5>
                    <hr>

                    <div class="row">
                        <div class="col-md-4">
                            <div class="form-group">
                                <label>País</label>
                                <input class="form-control" name="country" type="text" required="">
                            </div>
                        </div>

                        <div class="col-md-4">
                            <div class="form-group">
                                <label>Ciudad</label>
                                <input class="form-control" name="city" type="text" required="">
                            </div>
                        </div>

                        <div class="col-md-4">
                            <div class="form-group">
                                <label>Estado</label>
                                <input class="form-control" name="state" type="text" required="">
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancelar</button>
                    <button type="submit" class="btn btn-primary">Guardar Registro</button>

                    <input type="hidden" name="created_by" value="{{ Auth::user()->id }}" readonly="">
                </div>
            </div>
        </form>
    </div>
</div>
@endsection

@section('scripts')
<script src="{{ asset('back/assets/plugins/clockpicker/dist/jquery-clockpicker.min.js') }}"></script>

<script src="{{ asset('back/assets/plugins/select2/dist/js/select2.full.min.js') }}" type="text/javascript"></script>

<script type="text/javascript">
    $('.clockpicker').clockpicker({
        placement: 'bottom',
        align: 'left',
        autoclose: true
    });

    // For select 2
    $(".select2").select2();

    $("#newTask").click(function() {
		$("#activity_box").toggle("slow");
		$("#buttons_box").toggle("slow");
		$("#button_submit").toggle("slow");
	});

	$("#CancelNewTask").click(function() {
		$("#activity_box").toggle("slow");
		$("#buttons_box").toggle("slow");
		$("#button_submit").toggle("slow");
	});

</script>
@endsection