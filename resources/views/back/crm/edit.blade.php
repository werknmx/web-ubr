@extends('back.layouts.app')

@section('stylesheets')
<link href="{{ asset('back/assets/plugins/clockpicker/dist/jquery-clockpicker.min.css') }}" rel="stylesheet">

<link href="{{ asset('back/assets/plugins/select2/dist/css/select2.min.css') }}" rel="stylesheet" type="text/css" />

<style type="text/css">
    .clockpicker-popover{
        z-index: 1999;
    }

    .select2-container{
        width: 100% !important;
        display: block;
    }
</style>

<style type="text/css">
	.more-info-btn{
		display: block;
		width: 100%;
		position: relative;
		top: -20px;
		text-transform: uppercase;
		text-align: center;
	}

    .round{
    	width: 35px !important;
    	height: 35px !important;
    }

    .round i{
    	position: relative !important;
    	line-height: 35px !important;
    	top: -5px;
    }
</style>
@endsection

@section('content')
<div class="row page-titles">
	<div class="col-md-5 align-self-center">
		<h3 class="text-themecolor">Editando Contacto</h3>
	</div>
	<div class="col-md-7 align-self-center">
		<ol class="breadcrumb">
			<li class="breadcrumb-item"><a href="{{ route('index') }}">Inicio</a></li>
			<li class="breadcrumb-item"><a href="{{ route('listados.index') }}">Prospectos</a></li>
			<li class="breadcrumb-item active">#{{ $contact->id }}</li>
		</ol>
	</div>
</div>

<div class="row">
	<div class="col">
		@include('back.layouts.partials._mensajes')
	</div>
</div>

<form method="POST" action="{{ route('listados.update', $contact->id) }}" enctype="multipart/form-data">
	{{ csrf_field() }}
	{{ method_field('PUT') }}
	<div class="row">
		<div class="col-md-12">
			<div class="card card-body">
				<h5 class="text-uppercase">Datos de Contacto</h5>
				<hr>
				<div class="row">
					<div class="col-md-6">
						<div class="form-group">
							<label>Nombre</label>
							<input class="form-control" name="name" type="text" value="{{ $contact->name }}" required="">
						</div>
					</div>

					<div class="col-md-6">
						<div class="form-group">
							<label>Apellido</label>
							<input class="form-control" name="sur_name" type="text" value="{{ $contact->sur_name }}">
						</div>
					</div>

					<div class="col-md-12">
						<div class="form-group">
							<label>Email</label>
							<input class="form-control" name="email" type="text" value="{{ $contact->email }}">
						</div>
					</div>

					<div class="col-md-6">
						<div class="form-group">
							<label>Celular</label>
							<input class="form-control" name="phone_main" type="text" required="" value="{{ $contact->phone_cel }}">
						</div>
					</div>

					<div class="col-md-6">
						<div class="form-group">
							<label>Teléfono</label>
							<input class="form-control" name="phone_cel" type="text" value="{{ $contact->phone_main }}">
						</div>
					</div>

					<div class="col-md-4">
						<div class="form-group">
							<label>Empresa</label>
							<input class="form-control" name="company" type="text" value="{{ $contact->company }}">
						</div>
					</div>
					<div class="col-md-4">
						<div class="form-group">
							<label>Puesto</label>
							<input class="form-control" name="job_title" type="text" value="{{ $contact->job_title }}">
						</div>
					</div>
					<div class="col-md-4">
						<div class="form-group">
							<label>Giro</label>
							<input class="form-control" name="company_type" type="text" value="{{ $contact->company_type }}">
						</div>
					</div>

					<div class="col-md-12">
						<div class="form-group">
							<label>Cumpleaños</label>
							<input class="form-control" name="birthday" type="date" value="{{ $contact->birthday }}">
						</div>
					</div>
				</div>

				<h5 class="text-uppercase">Información de Estado</h5>
				<hr>

				<div class="row">
					<div class="col-md-6">
						<div class="form-group">
							<label>Servicio/Producto en su Interés</label>

							<select class="form-control select2 select2-multiple" name="interest_id[]" data-placeholder="Escoge una o varias opciones..." multiple="multiple">
								@foreach($programs as $pr)
								<option value="{{ $pr->id }}">{{ $pr->name }}</option>
								@endforeach
							</select>

							<small>* IMPORTANTE: Si se deja esto vacio no se hará ningun cambio.</small>
						</div>
					</div>

					<div class="col-md-6">
						<div class="form-group">
							<label>Asignado a</label>
							<select class="form-control select2" name="assigned_to" required="">
								@foreach($sales_agents as $sa)
								<option {{ ($sa->id == $contact->assigned_to) ? 'selected' : '' }} value="{{ $sa->id }}">{{ $sa->name }}</option>
								@endforeach
							</select>
						</div>
					</div>

					<div class="col-md-12">
                        <div class="form-group">
                            <label>Origen Global</label>
                            <select class="form-control select2" name="global_origin" data-placeholder="Escoge una opción...">
                            	@if($contact->global_origin == 'Facebook')
                            		<option selected value="Facebook">Facebook</option>
                            		<option value="Instagram">Instagram</option>
                                    <option value="WhatsApp">WhatsApp</option>
                                    <option value="Convenio Empresa">Convenio Empresa</option>
                                    <option value="Referido">Referido</option>
                                    <option value="Otro">Otro</option>
                            	@endif

                            	@if($contact->global_origin == 'Instagram')
                            		<option selected value="Instagram">Instagram</option>
                            		<option value="Facebook">Facebook</option>
                                    <option value="WhatsApp">WhatsApp</option>
                                    <option value="Convenio Empresa">Convenio Empresa</option>
                                    <option value="Referido">Referido</option>
                                    <option value="Otro">Otro</option>
                            	@endif

                            	@if($contact->global_origin == 'Whatsapp')
                            		<option selected value="WhatsApp">WhatsApp</option>
                            		<option value="Facebook">Facebook</option>
                                    <option value="Instagram">Instagram</option>
                                    <option value="Convenio Empresa">Convenio Empresa</option>
                                    <option value="Referido">Referido</option>
                                    <option value="Otro">Otro</option>
                            	@endif

                            	@if($contact->global_origin == 'Convenio Empresa')
                            		<option selected value="Convenio Empresa">Convenio Empresa</option>
                            		<option value="Facebook">Facebook</option>
                                    <option value="Instagram">Instagram</option>
                                    <option value="WhatsApp">WhatsApp</option>
                                    <option value="Referido">Referido</option>
                                    <option value="Otro">Otro</option>
                            	@endif

                            	@if($contact->global_origin == 'Referido')
                            		<option selected value="Referido">Referido</option>
                            		<option value="Facebook">Facebook</option>
                                    <option value="Instagram">Instagram</option>
                                    <option value="WhatsApp">WhatsApp</option>
                                    <option value="Convenio Empresa">Convenio Empresa</option>
                                    <option value="Otro">Otro</option>
                            	@endif

                            	@if($contact->global_origin == 'Otro')
                            		<option value="Facebook">Facebook</option>
                                    <option value="Instagram">Instagram</option>
                                    <option value="WhatsApp">WhatsApp</option>
                                    <option value="Convenio Empresa">Convenio Empresa</option>
                                    <option value="Referido">Referido</option>
                            		<option selected value="Otro">Otro</option>
                            	@endif

                            	@if($contact->global_origin == NULL)
                            		<option value="Facebook">Facebook</option>
                                    <option value="Instagram">Instagram</option>
                                    <option value="WhatsApp">WhatsApp</option>
                                    <option value="Convenio Empresa">Convenio Empresa</option>
                                    <option value="Referido">Referido</option>
                                    <option value="Otro">Otro</option>
                            	@endif
                            </select>
                        </div>
                    </div>
				</div>

				<h5 class="text-uppercase">Datos de Ubicación</h5>
				<hr>

				<div class="row">
					<div class="col-md-4">
						<div class="form-group">
							<label>País</label>
							<input class="form-control" name="country" type="text" required="" value="{{ $contact->country }}">
						</div>
					</div>

					<div class="col-md-4">
						<div class="form-group">
							<label>Ciudad</label>
							<input class="form-control" name="city" type="text" required="" value="{{ $contact->city }}">
						</div>
					</div>

					<div class="col-md-4">
						<div class="form-group">
							<label>Estado</label>
							<input class="form-control" name="state" type="text" required="" value="{{ $contact->state }}">
						</div>
					</div>
				</div>


				<div class="row">
					<div class="col text-right">
						<a href="{{ route('listados.show', $contact->id) }}" class="btn btn-secondary">Regresar</a>
	                	<button type="submit" class="btn btn-primary btn-lg">Guardar Registro</button>
					</div>
				</div>
			</div>
		</div>
	</div>
</form>
@endsection

@section('scripts')
<script src="{{ asset('back/assets/plugins/clockpicker/dist/jquery-clockpicker.min.js') }}"></script>

<script src="{{ asset('back/assets/plugins/select2/dist/js/select2.full.min.js') }}" type="text/javascript"></script>

<script type="text/javascript">
    $('.clockpicker').clockpicker({
        placement: 'bottom',
        align: 'left',
        autoclose: true
    });

    // For select 2
    $(".select2").select2();
</script>

@endsection