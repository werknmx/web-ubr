@extends('back.layouts.app')

@section('stylesheets')
<!-- chartist CSS -->
<link href="{{ asset('back/assets/plugins/css-chart/css-chart.css') }}" rel="stylesheet">
<link href="{{ asset('back/css/pages/ribbon-page.css') }}" rel="stylesheet">

<style type="text/css">
    sup{
        top: -2px;
        right: 15px;
    }
</style>
@endsection

@section('content')

<!-- ------------------------- -->
<!-- CONTADOR DE DÍAS Y SEMANAS -->
<!-- ------------------------- -->
@php
    use Carbon\Carbon;
    $semana_curso = Carbon::now()->weekOfYear;
@endphp

<div class="row page-titles">
    <div class="col-md-5 align-self-center">
        <h3 class="text-themecolor">Indicadores de Venta</h3>
    </div>
    <div class="col-md-7 align-self-center">
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="javascript:void(0)">Inicio</a></li>
            <li class="breadcrumb-item active">KPI´s</li>
        </ol>
    </div>
</div>

<div class="row">
    <div class="col">
        @include('back.layouts.partials._mensajes')
    </div>
</div>

<div class="row">
    <div class="col text-right mb-4 mt-4">
        <!--
        <a href="" class="btn btn-outline-primary">Vista General de Ventas</a>
        <a href="" class="btn btn-outline-primary">Ir a Campañas</a>
        -->

        <div class="dropdown" style="display: inline-block; position: relative; top: 0px;">
          <button class="btn btn-primary dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
            <i class="mdi mdi-settings"></i> Configuraciones
          </button>
          <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
            <a class="dropdown-item" href="#" data-toggle="modal" data-target="#goalsModal">Configurar Objetivos</a>
            <div class="dropdown-divider"></div>
            <a class="dropdown-item" href="#">Refrescar Página</a>
          </div>
        </div>
    </div>  
</div>

<!-- MODAL OBJETIVOS -->
<div class="modal fade" id="goalsModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <form method="POST" action="{{ route('metas-objetivos.store') }}">
                {{ csrf_field() }}
                {{ method_field('PUT') }}
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">OBJETIVOS</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label>Cierres Mensuales</label>
                                <input class="form-control" name="cierres_mensuales" value="{{ $cierres_mensuales->objective }}" type="text" required="">
                            </div>
                        </div>

                        <div class="col-md-6">
                            <div class="form-group">
                                <label>Venta Total Anual</label>
                                <input class="form-control" name="venta_total" value="{{ $venta_anual->objective }}" type="text">
                            </div>
                        </div>

                        <div class="col-md-6">
                            <div class="form-group">
                                <label>Promesas de Ventas</label>
                                <input class="form-control" name="promesas_venta" value="{{ $promesa_venta->objective }}" type="text">
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
                    <button type="button" class="btn btn-primary">Guardar Información</button>
                </div>
            </form>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-lg-3 col-md-6">
        <div class="card">
            <div class="card-body">
                <h4 class="card-title">Cierres de Hoy</h4>
                <div class="text-right">
                    <h1 class="font-light"><sup><i class="ti-arrow-up text-success"></i></sup>{{ $ventas_hoy->count() }}</h1>
                </div>

                @php
                    if($cierres_mensuales->objective == 0)
                        $porcentaje_1 = 0;
                    else{
                        $porcentaje_1 = round(($ventas_hoy->count()*100)/($cierres_mensuales->objective/30));
                    }
                @endphp

                <span class="text-success">{{ $porcentaje_1 }}%</span>
                <div class="progress">
                    <div class="progress-bar bg-success" role="progressbar" style="width: {{ $porcentaje_1  }}%; height: 6px;" aria-valuenow="{{ $ventas_hoy->count() }}" aria-valuemin="0" aria-valuemax="100"></div>
                </div>
            </div>
        </div>
    </div>

    <div class="col-lg-3 col-md-6">
        <div class="card">
            <div class="card-body">
                <h4 class="card-title">Cierres Semanales</h4>
                <div class="text-right">
                    <h1 class="font-light"><sup><i class="ti-arrow-up text-info"></i></sup>{{ $ventas_semana->count() }}</h1>
                </div>

                @php
                    if($cierres_mensuales->objective == 0)
                        $porcentaje_2 = 0;
                    else{
                        $porcentaje_2 = round(($ventas_semana->count()*100)/($cierres_mensuales->objective/4));
                    }
                @endphp

                <span class="text-info">{{ $porcentaje_2 }}%</span>
                <div class="progress">
                    <div class="progress-bar bg-info" role="progressbar" style="width: {{ $porcentaje_2 }}%; height: 6px;" aria-valuenow="{{ $ventas_semana->count() }}" aria-valuemin="0" aria-valuemax="100"></div>
                </div>
            </div>
        </div>
    </div>

    <div class="col-lg-3 col-md-6">
        <div class="card mb-3">
            <div class="card-body">
                <h4 class="card-title">Cierres Mensuales</h4>
                <div class="text-right">
                    <h1 class="font-light"><sup><i class="ti-arrow-down text-danger"></i></sup>{{ $ventas_mes->count() }}</h1>
                </div>

                @php
                    if($cierres_mensuales->objective == 0)
                        $porcentaje_3 = 0;
                    else{
                        $porcentaje_3 = round(($ventas_mes->count()*100)/$cierres_mensuales->objective);
                    }
                @endphp

                <span class="text-danger">{{ $porcentaje_3 }}%</span>
                <div class="progress">
                    <div class="progress-bar bg-danger" role="progressbar" style="width: {{ $porcentaje_3  }}%; height: 6px;" aria-valuenow="{{ $ventas_mes->count() }}" aria-valuemin="0" aria-valuemax="100"></div>
                </div>
            </div>
        </div>
    </div>

    <div class="col-lg-3 col-md-6">
        <div class="card">
            <div class="card-body">
                <h4 class="card-title">Venta Total Mensual</h4>
                <div class="text-right">
                    <h1 class="font-light"><sup><i class="ti-arrow-up text-inverse"></i></sup> ${{ number_format($ven_mes) }}</h1>
                </div>

                @php
                    if($venta_anual->objective == 0)
                        $porcentaje_4 = 0;
                    else{
                        $porcentaje_4 = round(($ven_mes*100)/($venta_anual->objective/12));
                    }
                @endphp

                <span class="text-inverse">{{ $porcentaje_4 }}%</span>
                <div class="progress">
                    <div class="progress-bar bg-inverse" role="progressbar" style="width: {{ $porcentaje_4 }}%; height: 6px;" aria-valuenow="{{ $ven_mes }}" aria-valuemin="0" aria-valuemax="100"></div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-md-9">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-body">
                        <h4 class="card-title">Inscritos por Vendedor esta Semana</h4>
                        <p class="card-subtitle">Calculando Semana: {{ $semana_curso }}</p>
                        <hr>

                        <div class="row">                    
                            <div class="col-md-4">
                                <div class="table-responsive" style="height: 445px;">
                                    <table class="table table-hover">
                                        <thead>
                                            <tr>
                                                <th>Asesor</th>
                                                <th class="text-right">Inscritos</th>
                                                <!--<th>Tendencia</th>-->
                                            </tr>
                                        </thead>
                                        <tbody>
                                            @foreach($users as $us)
                                            @php
                                                $us_assigned = $us->id;

                                                $week_start = Carbon::now()->startOfWeek();
                                                $week_end = Carbon::now()->endOfWeek();

                                                $cierres_exitosos = App\Contact::where('assigned_to', $us->id)
                                                ->where('closing_date', '<=', $week_end)
                                                ->where('closing_date', '>=', $week_start)
                                                ->where('status', 'Cierre Exitoso')
                                                ->count();
                                            @endphp

                                            <tr>
                                                <td>{{ $us->name }}</td>
                                                <td class="text-right">{{ $cierres_exitosos }}</td>
                                                <!--
                                                <td><span class="text-danger text-semibold"><i class="fa fa-level-down" aria-hidden="true"></i> 28.76%</span> </td>
                                                -->
                                            </tr>
                                            @endforeach
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                            <div class="col-md-8">
                                {!! $chart->container() !!}
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="col-md-12">
                <div class="card">
                    <div class="card-body">
                        <h4 class="card-title">Actividades por Vendedor esta Semana</h4>
                        <p class="card-subtitle">Calculando Semana: {{ $semana_curso }}</p>
                        <hr>

                        <div class="row">                    
                            <div class="col-md-4">
                                <div class="table-responsive" style="height: 445px;">
                                    <table class="table table-hover">
                                        <thead>
                                            <tr>
                                                <th>Asesor</th>
                                                <th class="text-right">Actividades</th>
                                                <!--<th>Tendencia</th>-->
                                            </tr>
                                        </thead>
                                        <tbody>
                                            @foreach($users as $us)
                                            @php
                                            $actividades = '10';
                                            @endphp

                                            @php
                                                $us_assigned = $us->id;

                                                $week_start = Carbon::now()->startOfWeek();
                                                $week_end = Carbon::now()->endOfWeek();

                                                $actividades = App\Activity::where('issuer_id', $us->id)
                                                ->where('completed_at', '<=', $week_end)
                                                ->where('completed_at', '>=', $week_start)
                                                ->where('completed', true)
                                                ->count();
                                            @endphp

                                            <tr>
                                                <td>{{ $us->name }}</td>
                                                <td class="text-right">{{ $actividades }}</td>
                                                <!--
                                                <td><span class="text-danger text-semibold"><i class="fa fa-level-down" aria-hidden="true"></i> 28.76%</span> </td>
                                                -->
                                            </tr>
                                            @endforeach
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                            <div class="col-md-8">
                                {!! $chart2->container() !!}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!--
        <div class="row">
            <div class="col-lg-12">
                <div class="card">
                    <div class="card-body">
                        <h4 class="card-title">Gráfica de Curvas</h4>
                        <ul class="list-inline text-right">
                            <li>
                                <h5><i class="fa fa-circle m-r-5 text-success"></i>Ideal</h5>
                            </li>
                            <li>
                                <h5><i class="fa fa-circle m-r-5 text-info"></i>Inscritos</h5>
                            </li>
                            <li>
                                <h5><i class="fa fa-circle m-r-5 text-inverse"></i>PY</h5>
                            </li>
                        </ul>

                        <div class="ct-sm-line-chart" style="height: 400px;"></div>
                    </div>
                </div>

                <div class="card-group">
                    <div class="card">
                        <div class="card-body">
                            <div class="row">
                                <div class="col-12">
                                    <h3>86%</h3>
                                    <h6 class="card-subtitle">Inscritos Totales</h6></div>
                                <div class="col-12">
                                    <div class="progress">
                                        <div class="progress-bar bg-success" role="progressbar" style="width: 85%; height: 6px;" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100"></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- Column 
                    <!-- Column 
                    <div class="card">
                        <div class="card-body">
                            <div class="row">
                                <div class="col-12">
                                    <h3>40%</h3>
                                    <h6 class="card-subtitle">Prospectos Pendientes</h6></div>
                                <div class="col-12">
                                    <div class="progress">
                                        <div class="progress-bar bg-info" role="progressbar" style="width: 40%; height: 6px;" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100"></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- Column 
                    <!-- Column 
                    <div class="card">
                        <div class="card-body">
                            <div class="row">
                                <div class="col-12">
                                    <h3>56%</h3>
                                    <h6 class="card-subtitle">Cierres Totales</h6></div>
                                <div class="col-12">
                                    <div class="progress">
                                        <div class="progress-bar bg-danger" role="progressbar" style="width: 56%; height: 6px;" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100"></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- Column 
                </div>
            </div>

            <div class="col-lg-12">
                <div class="card">
                    <div class="card-body">
                        <h4 class="card-title">Tabla de Curvas</h4>

                        <div class="table-responsive" style="height: 445px;">
                            <table class="table table-hover">
                                <thead>
                                    <tr>
                                        <th>Semana</th>
                                        <th>Ideal</th>
                                        <th>Inscritos</th>
                                        <th>PY</th>
                                        <th>Tendencia</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td>0</td>
                                        <td>0</td>
                                        <td>1</td>
                                        <td>8</td>
                                        <td><span class="text-danger text-semibold"><i class="fa fa-level-down" aria-hidden="true"></i> 28.76%</span> </td>
                                    </tr>
                                    <tr>
                                        <td>1</td>
                                        <td>5</td>
                                        <td>0</td>
                                        <td>7</td>
                                        <td><span class="text-success text-semibold"><i class="fa fa-level-up" aria-hidden="true"></i> 58.56%</span> </td>
                                    </tr>
                                    <tr>
                                        <td>2</td>
                                        <td>5</td>
                                        <td>1</td>
                                        <td>2</td>
                                        <td><span class="text-warning text-semibold"><i class="fa fa-level-down" aria-hidden="true"></i> 8.55%</span> </td>
                                    </tr>
                                    <tr>
                                        <td>3</td>
                                        <td>5</td>
                                        <td>1</td>
                                        <td>0</td>
                                        <td><span class="text-info text-semibold"><i class="fa fa-level-up" aria-hidden="true"></i> 35.76%</span> </td>
                                    </tr>
                                    <tr>
                                        <td>4</td>
                                        <td>5</td>
                                        <td>0</td>
                                        <td>7</td>
                                        <td><span class="text-success text-semibold"><i class="fa fa-level-up" aria-hidden="true"></i> 58.56%</span> </td>
                                    </tr>
                                    <tr>
                                        <td>5</td>
                                        <td>5</td>
                                        <td>7</td>
                                        <td>10</td>
                                        <td><span class="text-info text-semibold"><i class="fa fa-level-up" aria-hidden="true"></i> 35.76%</span> </td>
                                    </tr>
                                    <tr>
                                        <td>6</td>
                                        <td>9</td>
                                        <td>2</td>
                                        <td>9</td>
                                        <td><span class="text-danger text-semibold"><i class="fa fa-level-down" aria-hidden="true"></i> 28.76%</span> </td>
                                    </tr>
                                    <tr>
                                        <td>7</td>
                                        <td>12</td>
                                        <td>3</td>
                                        <td>14</td>
                                        <td><span class="text-danger text-semibold"><i class="fa fa-level-down" aria-hidden="true"></i> 28.76%</span> </td>
                                    </tr>
                                    <tr>
                                        <td>8</td>
                                        <td>14</td>
                                        <td>2</td>
                                        <td>15</td>
                                        <td><span class="text-info text-semibold"><i class="fa fa-level-up" aria-hidden="true"></i> 35.76%</span> </td>
                                    </tr>
                                    <tr>
                                        <td>9</td>
                                        <td>0</td>
                                        <td>0</td>
                                        <td>11</td>
                                        <td><span class="text-success text-semibold"><i class="fa fa-level-up" aria-hidden="true"></i> 58.56%</span> </td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>

            <div class="col-lg-12">
                <div class="row">
                    <!-- Column
                    <div class="col-lg-4 col-md-4">
                        <div class="card">
                            <div class="card-body">
                                <div class="row p-t-10 p-b-10">
                                    <!-- Column
                                    <div class="col p-r-0">
                                        <h1 class="font-light">248</h1>
                                        <h6 class="text-muted">Campañas Activas</h6></div>
                                    <!-- Column
                                    <div class="col text-right align-self-center">
                                        <div data-label="30%" class="css-bar m-b-0 css-bar-danger css-bar-20"><i class="mdi mdi-briefcase-check"></i></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- Column 
                    <!-- Column 
                    <div class="col-lg-4 col-md-4">
                        <div class="card">
                            <div class="card-body">
                                <div class="row p-t-10 p-b-10">
                                    <!-- Column 
                                    <div class="col p-r-0">
                                        <h1 class="font-light">32</h1>
                                        <h6 class="text-muted">Vendedores</h6></div>
                                    <!-- Column 
                                    <div class="col text-right align-self-center">
                                        <div data-label="40%" class="css-bar m-b-0 css-bar-warning css-bar-40"><i class="mdi mdi-star-circle"></i></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- Column 
                    <!-- Column 
                    <div class="col-lg-4 col-md-4">
                        <div class="card">
                            <div class="card-body">
                                <div class="row p-t-10 p-b-10">
                                    <!-- Column 
                                    <div class="col p-r-0">
                                        <h1 class="font-light">159</h1>
                                        <h6 class="text-muted">Zonas</h6></div>
                                    <!-- Column 
                                    <div class="col text-right align-self-center">
                                        <div data-label="60%" class="css-bar m-b-0 css-bar-info css-bar-60"><i class="mdi mdi-receipt"></i></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- Column 
                </div>
            </div>
            

            <div class="col-lg-12">
                <div class="card">
                    <div class="card-body">
                        <h4 class="card-title">Avance por Ejecutivos</h4>

                        <div class="ct-bar-chart" style="height: 400px;"></div>
                    </div>
                </div>
            </div>
        </div>
        -->
    </div>

    <div class="col-md-3">
        <div class="ribbon-wrapper card">
            <div class="ribbon ribbon-info">Importante</div>
            <div class="card-body">
                <h4 class="card-title">Filtros por Programa</h4>
                <p>Selecciona uno de los siguientes para ver los indicadores.</p>
            </div>

            <div class="list-group">
                @foreach($programs as $pro)
                    <a href="{{ route('ventas.kpi.show', $pro->id) }}" data-toggle="tooltip" data-placement="left" title="Ver el desglose de indicadores de este Programa" class="list-group-item list-group-item-action flex-column align-items-start">
                        <h5 class="mb-1">{{ $pro->name }}</h5>
                        <small class="mb-4">Última Actualización: {{ $pro->updated_at }}</small>
                    </a>
                @endforeach
            </div>
        </div>

        <div class="card earning-widget">
            <div class="card-body">
                <div class="card-title">
                    <h4 class="card-title m-b-0">Ventas Semana</h4>
                    <h2 class="m-t-0 mb-0">${{ number_format($ven_semana) }}</h2>
                </div>
            </div>
            <div class="card-body b-t">
                <table class="table v-middle no-border">
                    <tbody>
                        @foreach($users as $us)
                        <tr>
                            <td style="width:40px"><img src="{{ asset('back/assets/images/users/1.jpg') }}" class="img-circle" alt="logo" width="50"></td>
                            <td><a href="" data-toggle="tooltip" data-placement="top" title="Filtrar por Asesor">{{ $us->name }}</a></td>
                            <td align="right">
                                <span class="label label-light-info">
                                    @php
                                        $week_start = Carbon::now()->startOfWeek();
                                        $week_end = Carbon::now()->endOfWeek();

                                        $us_assigned = $us->id;

                                        $us_total = App\Contact::where('assigned_to', $us->id)
                                        ->where('status', 'Cierre Exitoso')
                                        ->where('closing_date', '<=', $week_end)
                                        ->where('closing_date', '>=', $week_start)
                                        ->get();

                                        $us_sales = 0;

                                        foreach ($us_total as $v_total) {
                                            foreach($v_total->interests as $v)
                                            {
                                               $us_sales += $v->normal_price;
                                            };
                                        };

                                        $us_sales;
                                    @endphp

                                    ${{ number_format($us_sales) }}
                                </span>
                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
@endsection

@section('scripts')
<script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.7.1/Chart.min.js" charset="utf-8"></script>
{!! $chart->script() !!}
{!! $chart2->script() !!}
@endsection