@extends('back.layouts.app')

@section('stylesheets')
<!-- chartist CSS -->
<link href="{{ asset('back/assets/plugins/chartist-js/dist/chartist.min.css') }}" rel="stylesheet">
<link href="{{ asset('back/assets/plugins/chartist-js/dist/chartist-init.css') }}" rel="stylesheet">
<link href="{{ asset('back/assets/plugins/chartist-plugin-tooltip-master/dist/chartist-plugin-tooltip.css') }}" rel="stylesheet">
@endsection

@section('content')

<div class="row page-titles">
    <div class="col-md-5 align-self-center">
        <h3 class="text-themecolor">Indicadores de Venta para: Entrenamiento Guadalajara</h3>
    </div>
    <div class="col-md-7 align-self-center">
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="javascript:void(0)">Inicio</a></li>
            <li class="breadcrumb-item"><a href="javascript:void(0)">KPI'S</a></li>
            <li class="breadcrumb-item active">Entrenamiento Guadalajara</li>
        </ol>
    </div>
</div>

<div class="row">
    <div class="col">
        @include('back.layouts.partials._mensajes')
    </div>
</div>

<!--
<div class="row">
	<div class="col-md-12 text-center">
		<h5 class="text-uppercase">FILTROS POR ORIGEN</h5>
		<ul class="list-inline mb-5 mt-4">
			<li><a href="" class="btn btn-info">Entrenamiento Guadalajara</a></li>
			<li><a href="" class="btn btn-outline-info">Certificado</a></li>
			<li><a href="" class="btn btn-outline-info">Entrenamiento CDMX</a></li>
			<li><a href="" class="btn btn-outline-success"><i class="mdi mdi-search-web"></i> Buscar otro</a></li>
		</ul>
	</div>
</div>
-->

<div class="row">
    <div class="col-lg-6">
    	<div class="card">
    		<div class="card-body">
    			<h4 class="card-title">Tabla de Curvas</h4>

    			<div class="table-responsive" style="height: 445px;">
                    <table class="table table-hover">
                        <thead>
                            <tr>
                                <th>Semana</th>
                                <th>Ideal</th>
                                <th>Inscritos</th>
                                <th>PY</th>
                                <th>Tendencia</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td>0</td>
                                <td>0</td>
                                <td>1</td>
                                <td>8</td>
                                <td><span class="text-danger text-semibold"><i class="fa fa-level-down" aria-hidden="true"></i> 28.76%</span> </td>
                            </tr>
                            <tr>
                                <td>1</td>
                                <td>5</td>
                                <td>0</td>
                                <td>7</td>
                                <td><span class="text-success text-semibold"><i class="fa fa-level-up" aria-hidden="true"></i> 58.56%</span> </td>
                            </tr>
                            <tr>
                                <td>2</td>
                                <td>5</td>
                                <td>1</td>
                                <td>2</td>
                                <td><span class="text-warning text-semibold"><i class="fa fa-level-down" aria-hidden="true"></i> 8.55%</span> </td>
                            </tr>
                            <tr>
                                <td>3</td>
                                <td>5</td>
                                <td>1</td>
                                <td>0</td>
                                <td><span class="text-info text-semibold"><i class="fa fa-level-up" aria-hidden="true"></i> 35.76%</span> </td>
                            </tr>
                            <tr>
                                <td>4</td>
                                <td>5</td>
                                <td>0</td>
                                <td>7</td>
                                <td><span class="text-success text-semibold"><i class="fa fa-level-up" aria-hidden="true"></i> 58.56%</span> </td>
                            </tr>
                            <tr>
                                <td>5</td>
                                <td>5</td>
                                <td>7</td>
                                <td>10</td>
                                <td><span class="text-info text-semibold"><i class="fa fa-level-up" aria-hidden="true"></i> 35.76%</span> </td>
                            </tr>
                            <tr>
                                <td>6</td>
                                <td>9</td>
                                <td>2</td>
                                <td>9</td>
                                <td><span class="text-danger text-semibold"><i class="fa fa-level-down" aria-hidden="true"></i> 28.76%</span> </td>
                            </tr>
                            <tr>
                                <td>7</td>
                                <td>12</td>
                                <td>3</td>
                                <td>14</td>
                                <td><span class="text-danger text-semibold"><i class="fa fa-level-down" aria-hidden="true"></i> 28.76%</span> </td>
                            </tr>
                            <tr>
                                <td>8</td>
                                <td>14</td>
                                <td>2</td>
                                <td>15</td>
                                <td><span class="text-info text-semibold"><i class="fa fa-level-up" aria-hidden="true"></i> 35.76%</span> </td>
                            </tr>
                            <tr>
                                <td>9</td>
                                <td>0</td>
                                <td>0</td>
                                <td>11</td>
                                <td><span class="text-success text-semibold"><i class="fa fa-level-up" aria-hidden="true"></i> 58.56%</span> </td>
                            </tr>
                        </tbody>
                    </table>
                </div>
    		</div>
    	</div>
    </div>

    <div class="col-lg-6">
        <div class="card">
            <div class="card-body">
                <h4 class="card-title">Gráfica de Curvas</h4>
                <ul class="list-inline text-right">
                	<li>
                        <h5><i class="fa fa-circle m-r-5 text-success"></i>Ideal</h5>
                    </li>
                    <li>
                        <h5><i class="fa fa-circle m-r-5 text-info"></i>Inscritos</h5>
                    </li>
                    <li>
                        <h5><i class="fa fa-circle m-r-5 text-inverse"></i>PY</h5>
                    </li>
                </ul>

                <div class="ct-sm-line-chart" style="height: 400px;"></div>
            </div>
        </div>
    </div>

    <div class="col-lg-6">
    	<div class="card">
    		<div class="card-body">
    			<h4 class="card-title">Tabla de Avance</h4>

    			<div class="table-responsive" style="height: 400px;">
                    <table class="table table-hover">
                        <thead>
                            <tr>
                                <th>Ejecutivos de Venta</th>
                                <th>Avance</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td>Alan</td>
                                <td>0</td>
                            </tr>
                            <tr>
                                <td>Edson</td>
                                <td>3</td>
                            </tr>
                            <tr>
                                <td>Andy</td>
                                <td>0</td>
                            </tr>

                            <tr>
                                <td>Edith</td>
                                <td>2</td>
                            </tr>
                            <tr>
                                <td>Vane</td>
                                <td>2</td>
                            </tr>
                            <tr>
                                <td>Carlos</td>
                                <td>5</td>
                            </tr>
                            <tr>
                                <td>Francisco</td>
                                <td>5</td>
                            </tr>
                        </tbody>
                    </table>
                </div>
    		</div>
    	</div>
    </div>

    <div class="col-lg-6">
        <div class="card">
            <div class="card-body">
                <h4 class="card-title">Avance por Ejecutivos</h4>

                <div class="ct-bar-chart" style="height: 400px;"></div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('scripts')
<!-- chartist chart -->
<script src="{{ asset('back/assets/plugins/chartist-js/dist/chartist.min.js') }}"></script>
<script src="{{ asset('back/assets/plugins/chartist-plugin-tooltip-master/dist/chartist-plugin-tooltip.min.js') }}"></script>
<script type="text/javascript">
	//Simple line chart 

new Chartist.Line('.ct-sm-line-chart', {
	labels: ['1', '2', '3', '4', '5', '6', '7', '8'],
	series: [
	    [5, 5, 5, 5, 5, 9, 12, 12],
	    [1, 0, 1, 1, 0, 7, 2, 3],
	    [8, 7, 2, 0, 7, 10, 9, 9]
	  ]
	}, {
	  fullWidth: true,
	  
	  plugins: [
	    Chartist.plugins.tooltip()
	  ],
	  chartPadding: {
	    right: 40
	  }
});

// Bar chart

var data = {
  labels: ['Alan', 'Edson', 'Andy', 'Edith', 'Vane', 'Carlos', 'Francisco'],
  series: [
    [0, 3, 0, 2, 2, 5, 5],
  ]
};

var options = {
  distributedSeries: true,
};

var responsiveOptions = [
  ['screen and (max-width: 640px)', {
    distributedSeries: true,

    axisX: {
      labelInterpolationFnc: function (value) {
        return value[0];
      }
    }
  }]
];

new Chartist.Bar('.ct-bar-chart', data, options, responsiveOptions);

</script>
@endsection