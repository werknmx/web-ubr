@extends('back.layouts.app')

@section('stylesheets')
<link rel="stylesheet" type="text/css" href="{{ asset('css/filter_db/filter.css') }}">

<!-- Footable CSS -->
<link href="{{ asset('back/assets/plugins/footable/css/footable.core.css') }}" rel="stylesheet">
<link href="{{ asset('back/assets/css/filter-table.css') }}" rel="stylesheet">
<link href="{{ asset('back/assets/plugins/select2/dist/css/select2.min.css') }}" rel="stylesheet" type="text/css" />

<style type="text/css">
    .select2-container{
        display: block;
        width: 100% !important;
    }

    .table td, .table th {
        padding: 1.3rem .75rem;
        vertical-align: middle !important;
        font-weight: 300 !important;
    }

    sup{
    	top: -3px;
    	margin-right: 10px;
    }

    .stats .card-title{
    	margin-bottom: 0px;
    }

    .stats a{
    	font-size: .8em;
    }

    .round{
    	width: 35px !important;
    	height: 35px !important;
    }

    .round i{
    	position: relative !important;
    	line-height: 35px !important;
    	top: -5px;
    }

    .btn-activities{
    	background-color: #745af2;
    	color: #fff;
    	position: fixed;
    	bottom: 30px;
    	right: 40px;
    	padding: 15px 30px;
    	border-radius: 20px;
    	z-index: 2;
    	border: none;
    	box-shadow: 0px 0px 30px -5px rgba(0,0,0,0.4);
    	cursor: pointer;

    	transition: .2s ease-in-out all;
    	-webkit-transition: .2s ease-in-out all;
    	-moz-transition: .2s ease-in-out all;
    	-ms-transition: .2s ease-in-out all;
    	-o-transition: .2s ease-in-out all;
    }

    .btn-activities:hover{
    	transform: scale(1.2);
    }

    .close-activities{
    	background-color: #745af2;
    	color: #fff;
    	position: absolute;
    	top: 50%;
    	left: -25px;
    	height: 30px;
    	width: 30px;
    	line-height: 30px;

    	z-index: 2;
    	border: none;
    	box-shadow: 0px 0px 30px -5px rgba(0,0,0,0.4);
    	cursor: pointer;
    }

    .activities-box{
    	position: fixed;
    	top: 125px;
    	right: -60%;
    	width: 60%;
    	background-color: #fff;
    	border-left: 1px solid #000;
    	z-index: 3;
    	height: 87vh;
    	overflow-x: scroll;

    	transition: .5s ease-in-out all;
    	-webkit-transition: .5s ease-in-out all;
    	-moz-transition: .5s ease-in-out all;
    	-ms-transition: .5s ease-in-out all;
    	-o-transition: .5s ease-in-out all;
    }

    .close-activities{

    }

    .left-sidebar{
    	z-index: 21 !important;
    }

    .active-box{
    	right: 0px;
    }

    .overlay-activities{
    	position: fixed;
    	top: 125px;
    	left: -100%;
    	background-color: rgba(0,0,0,0.5);
    	height: 100vh;
    	width: 100%;
    	z-index: 2;
    	opacity: 0;

    	cursor: url('{{ asset('img/close.png') }}'), auto;	

    	transition: .5s ease-in-out all;
    	-webkit-transition: .5s ease-in-out all;
    	-moz-transition: .5s ease-in-out all;
    	-ms-transition: .5s ease-in-out all;
    	-o-transition: .5s ease-in-out all;
    }

    .active-overlay{
    	left: 0px;
    	opacity: 1;
    }
</style>

@endsection

@section('content')
<div class="row page-titles">
    <div class="col-md-5 align-self-center">
        <h3 class="text-themecolor">Gestión de Relaciones con Clientes</h3>
    </div>
    <div class="col-md-7 align-self-center">
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="{{ url('/admin') }}">Inicio</a></li>
            <li class="breadcrumb-item"><a href="{{ route('listados.index') }}">CRM</a></li>
            <li class="breadcrumb-item active">{{ $filter_name }}</li>
        </ol>
    </div>
</div>

<div class="row">
    <div class="col">
        @include('back.layouts.partials._mensajes')
    </div>
</div>

<button id="btn-activities" class="btn-activities"><i class="ti-list"></i> Ver tus actividades</button>

<!--
<div class="row mb-4 mt-3">
	<div class="col">
		<a href="" class="btn btn-outline-info mb-3">Ver Indicadores por campaña</a>
		<a href="" class="btn btn-info mb-3"><i class="mdi mdi-trending-up"></i> Ver todos los KPI's</a>

		<div class="dropdown" style="display: inline-block; position: relative; top: -8px;">
		  <button class="btn btn-primary dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
		    <i class="mdi mdi-settings"></i>
		  </button>
		  <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
		  	<a class="dropdown-item" href="#">Configurar Objetivos</a>
		  	<div class="dropdown-divider"></div>
		    <a class="dropdown-item" href="#">Refrescar Página</a>
		  </div>
		</div>
	</div>	
</div>
-->

<div class="row mb-4 mt-3">
    <div class="col">
        <a href="{{ route('listados.index') }}" class="btn btn-secondary">Regresar al listado General</a>
    </div>  
</div>

<div class="row">
    <div class="col-md-12">
    	<div class="row">
		    <div class="col-md-12">
	    		<div class="card">
		            <div class="card-body">
		                <h3>Listado de Filtrador . Tipo de Filtro : {{ $filter_name }}</h3>
		                <hr>

						<table id="table-pagination" class="table m-b-0 toggle-arrow-tiny" data-page-size="10">
							<thead>
								<tr>
									<th>Fecha</th>
									<th>Prospecto</th>
									<th>Interés</th>
									<th>Asesor</th>
									<th>Campaña</th>
									<th>Estado</th>
								</tr>
							</thead>
							<tbody>
                                @foreach($contacts as $ct)
                                    <tr>
                                        <td><span class="text-muted"><i class="fa fa-clock-o"></i> {{ $ct->created_at->diffForHumans() }}</span></td>
                                        <td>
                                            <a href="{{ route('listados.show', $ct->id) }}" data-toggle="tooltip" data-placement="top" title="Ver Perfil">{{ $ct->name . ' ' . $ct->sur_name }}</a>

                                            <p style="font-size: 80%; margin-bottom: 0px; margin-top: 10px;"><i class="mdi mdi-cellphone-android"></i> {{ $ct->phone_cel }}</p>
                                            <p style="font-size: 80%; margin-bottom: 0px;"><i class="mdi mdi-email"></i> {{ $ct->email }}</p>
                                        </td>
                                        <td>
                                            @foreach($ct->interests as $int)
                                                <a class="badge-pill badge-info" href="{{ route('programas.show', $int->id) }}" data-toggle="tooltip" data-placement="top" title="Filtrar por este Interés">{{ $int->title }}</a>
                                            @endforeach
                                        </td>
                                        <td><a href="" data-toggle="tooltip" data-placement="top" title="Filtrar por Asesor">{{ $ct->sales_agent->name }}</a></td>

                                        <td>
                                            @if($ct->landing_origin_id == NULL)
                                            <span class="text-uppercase" style="font-size: .8em">Registro Manual</span>
                                            @else
                                            {{ $ct->origin->title }} <a href="" data-toggle="tooltip" data-placement="top" title="Ir a la Campaña"><i class="mdi mdi-web"></i></a>
                                            @endif
                                        </td>
                                        <td>
                                            @if($ct->status == 'Registro')
                                                <span class="badge badge-danger">{{ $ct->status }}</span>
                                            @endif

                                            @if($ct->status == 'Prospecto')
                                                <span class="badge badge-danger">{{ $ct->status }}</span>
                                            @endif

                                            @if($ct->status == 'Interesado')
                                                <span class="badge badge-warning">{{ $ct->status }}</span>
                                            @endif

                                            @if($ct->status == 'Promesa de Venta')
                                                <span class="badge badge-info">{{ $ct->status }}</span>
                                            @endif
                                            
                                            @if($ct->status == 'Cierre Exitoso')
                                                <span class="badge badge-success">{{ $ct->status }}</span>
                                            @endif

                                            @if($ct->status == 'Cierre Perdido')
                                                <span class="badge badge-danger">{{ $ct->status }}</span>
                                            @endif
                                            
                                        </td>
                                    </tr>
                                @endforeach
							</tbody>
							<tfoot>
								<tr>
									<td colspan="12">
										<div class="text-right">
											<ul class="pagination pagination-split m-t-30"> </ul>
										</div>
									</td>
								</tr>
							</tfoot>
						</table>
		            </div>
		        </div>
	    	</div>
    	</div>
    </div>
</div>

<style type="text/css">
    .pagination{
        justify-content: center;
        margin-top: 10px;
    }
</style>

<script type="text/javascript">
     (function() {
       if (document.querySelector('.pagination')!==null) {

         // Set paginator ul and li
         var paginator_ul = document.getElementsByClassName('pagination')[0],
             paginator_li = paginator_ul.getElementsByTagName("LI");

         [].forEach.call(paginator_li, function(li) {
             li.className += " page-item";
             li.firstChild.className += " page-link";
         });
       }
    })();
</script>

<!-- GREAT BOX OF ACTIVITIES -->

<div class="overlay-activities" id="close-activities"></div>

<section class="activities-box">
	<!--<button id="close-activities" class="close-activities"><i class="ti-close"></i></button>-->

	<div class="container-fluid">
		<div class="row">
			<div class="col-md-12">
				<div class="card">
			        <div class="card-body pb-0">
			            <h3>Actividades Pendientes</h3>
		                <hr>
			             
                        <!--
		                <div class="row">
		            		<div class="col-md-4">
		            			<div class="form-group">
		            				<label for="date_end">Filtrar Por Tipo</label>
		                            <select class="form-control" name="department_id">
		                                <option value="0">Selecciona un filtro...</option>
		                                <option value="1">Llamada</option>
		                                <option value="2">Whatsapp</option>
		                                <option value="3">Correo Electrónico</option>
		                                <option value="4">Cita Prescencial</option>
		                                <option value="5">Otros</option>
		                            </select>
		                        </div>
							</div>

							<div class="col-md-4">
		            			<div class="form-group">
		            				<label for="date_end">Filtrar Por Campaña</label>
		                            <select class="form-control" name="department_id">
		                                <option value="0">Selecciona una...</option>
		                                <option value="1">Campaña numero 1</option>
		                                <option value="2">Esta es otra campaña</option>
		                            </select>
		                        </div>
							</div>
						</div>
                        -->

				        @if($activities->count() == NULL)
                        <h3 class="text-center mb-3 mt-3">No tienes ninguna actividad programada. No pierdas el contacto con tus clientes.</h3>
                        @else
                        <div class="table-responsive">
                            <table class="table">
                                <thead>
                                    <tr>
                                        <th>Tipo de Actividad</th>
                                        <th>Comentario</th>
                                        <th>Estado</th>
                                        <th>Fecha</th>
                                        <th>Acciones</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach($activities as $at)
                                    <tr>
                                        <td>
                                            <div class="p-2"><span class="round" style="background-color: {{ $at->type->hex  }}"><i class="{{ $at->type->icon_code }}"></i></span></div>
                                            <h5>{{ $at->type->verb }} <a href="{{ route('listados.show', $at->contact_id) }}">{{ $at->contact->name . ' ' . $at->contact->sur_name }}</a></h5>
                                        </td>
                                        <td>{{ $at->activity_description }}</td>
                                        <td>
                                            @if($at->activity_status == 'Completada')
                                            <span class="label label-success">
                                            @elseif($at->activity_status == 'Pendiente')
                                            <span class="label label-primary">
                                            @elseif($at->activity_status == 'Importante')
                                            <span class="label label-warning">
                                            @else
                                            <span class="label label-danger">
                                            @endif
                                                {{ $at->activity_status }}
                                            </span>
                                        </td>

                                        @php
                                            $tiempo = strtotime($at->date_end . ' ' . $at->delivery_time);

                                            $date_real = date('d/M . h:i A', $tiempo);
                                        @endphp

                                        <td><span class="text-muted"><i class="fa fa-clock-o"></i> {{ $date_real }}</span> </td>
                                        <td class="text-nowrap text-right">
                                            <div class="btn-group" role="group">

                                                @if($at->completed == false)
                                                <a class="btn btn-secondary" href="{{ route('actividades.completar', $at->id) }}" data-toggle="tooltip" data-original-title="Completar"><i class="mdi mdi-check"></i></a>
                                                @endif


                                                <div class="dropdown">
                                                    <button class="btn btn-secondary" type="button" id="extra_options" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" data-original-title="Opciones">
                                                        <i class="mdi mdi-dots-horizontal"></i>
                                                    </button>

                                                    <div class="dropdown-menu" aria-labelledby="extra_options" x-placement="bottom-start" style="position: absolute; transform: translate3d(0px, 36px, 0px); top: 0px; left: 0px; will-change: transform;">
                                                        @if($at->completed == false)
                                                            <a class="dropdown-item" href="{{ route('actividades.completar', $at->id) }}"><i class="mdi mdi-check"></i> Completar Tarea</a>
                                                            <a class="dropdown-item"  href="javascript:void(0)" data-toggle="modal" data-target="#modalEditarTarea_{{ $at->id }}"><i class="mdi mdi-pencil"></i> Editar Tarea</a>

                                                            <a class="dropdown-item" href="{{ route('actividades.prioridad', $at->id) }}"><i class="fa fa-exclamation-circle"></i> Dar Prioridad</a>
                                                            
                                                            <div class="dropdown-divider"></div>
                                                            @endif
                                                        <a class="dropdown-item" href="javascript:void(0)" data-toggle="modal" data-target="#historialModal_{{ $at->id }}">
                                                            <i class="ti-info"></i> Ver historial
                                                        </a>

                                                        <form method="POST" id="form_delete_{{ $at->id }}" action="{{ route('actividades.destroy', $at->id) }}">
                                                            <button id="borrar_elemento_{{ $at->id }}" type="button" class="dropdown-item"><i class="mdi mdi-delete"></i> Borrar</button>
                                                            {{ csrf_field() }}
                                                            {{ method_field('DELETE') }}
                                                        </form>
                                                    </div>
                                                </div>
                                            </div>
                                        </td>
                                    </tr>

                                    <div class="modal fade" id="modalEditarTarea_{{ $at->id }}" tabindex="-1" role="dialog" aria-hidden="true">
                                        <div class="modal-dialog" role="document">
                                            <div class="modal-content">
                                                <div class="modal-header">
                                                    <h5 class="modal-title">Editar Tarea</h5>
                                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                        <span aria-hidden="true">&times;</span>
                                                    </button>
                                                </div>
                                                <form method="POST" action="{{ route('actividades.update', $at->id) }}">
                                                    {{ csrf_field() }} 
                                                    {{ method_field('PUT') }} 
                                                    <div class="modal-body">
                                                        <div class="row">
                                                            <div class="col-md-6">
                                                                <div class="form-group">
                                                                    <label>Actividad Principal</label>
                                                                    <select class="form-control" name="activity_type_id">
                                                                        <option value="0">Selecciona una actividad...</option>
                                                                        @foreach($activity_types as $acty)
                                                                            <option {{ ($acty->id == $at->activity_type_id) ? 'selected' : '' }} value="{{ $acty->id }}">{{ $acty->name }}</option>
                                                                        @endforeach
                                                                    </select>
                                                                </div>
                                                            </div>

                                                            <div class="col-md-6">
                                                                <div class="form-group">
                                                                    <label>Asignado a</label>
                                                                    <select class="form-control select2" name="handler_id[]">
                                                                        @foreach($users as $us)
                                                                        <option value="{{ $us->id }}">{{ $us->name }}</option>
                                                                        @endforeach
                                                                    </select>
                                                                </div>
                                                            </div>

                                                            <div class="col-md-12">
                                                                <div class="form-group">
                                                                    <label>Comentario</label>
                                                                    <input type="text" name="activity_description" class="form-control" value="{{ $at->activity_description }}">
                                                                </div>
                                                            </div>
                                                            <div class="col-md-6">
                                                                <div class="form-group">
                                                                    <label for="date_end">Fecha de Finalización</label>
                                                                    <input type="date" name="date_end" value="{{ $at->date_end }}" class="form-control">
                                                                </div>
                                                            </div>

                                                            <div class="col col-md-6">
                                                                <div class="form-group">
                                                                    <label for="date_end">Hora</label>
                                                                    <div class="input-group clockpicker">
                                                                        <input type="time" class="form-control" value="{{ $at->delivery_time }}" name="delivery_time"> <span class="input-group-addon"> <span class="fa fa-clock-o"></span> </span>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>

                                                    <input type="hidden" name="activity_status" value="{{ $at->activity_status }}">
                                                    <div class="modal-footer">
                                                        <button type="submit" class="btn btn-primary">Actualizar Tarea</button>
                                                    </div>
                                                </form>
                                            </div>
                                        </div>
                                    </div>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                        @endif
				    </div>
			    </div>
			</div>
		</div>
	</div>
</section>

@foreach($activities as $at)
<div class="modal fade" id="historialModal_{{ $at->id }}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Historial de Actividad</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="table-responsive">
                    <table class="table">
                        <thead>
                            <tr>
                                <th>#</th>
                                <th>Usuario</th>
                                <th>Acción</th>
                                <th>Fecha</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td>1</td>
                                <td><a href="">Cuenta Ventas</a></td>
                                <td>Editó la actividad</td>
                                <td class="text-muted"><i class="fa fa-clock-o"></i> 05/08/2019 13:40</td>
                            </tr>
                            <tr>
                                <td>2</td>
                                <td><a href="">Cuenta Ventas</a></td>
                                <td>Editó la actividad</td>
                                <td class="text-muted"><i class="fa fa-clock-o"></i> 05/08/2019 17:00</td>
                            </tr>
                            <tr>
                                <td>3</td>
                                <td><a href="">Cuenta Ventas</a></td>
                                <td>Completó la tarea.</td>
                                <td class="text-muted"><i class="fa fa-clock-o"></i> 08/08/2019 13:40</td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
            </div>
        </div>
    </div>
</div>
@endforeach

@endsection

@section('scripts')
<script src="{{ asset('back/assets/plugins/select2/dist/js/select2.full.min.js') }}" type="text/javascript"></script>

<script type="text/javascript">
    $(".select2").select2();
</script>
<!-- Footable -->
<script src="{{ asset('back/assets/plugins/footable/js/footable.all.min.js') }}"></script>

<script type="text/javascript">
    $(window).on('load', function() {
    	// Pagination
    	// -----------------------------------------------------------------
    	$('#table-pagination').footable();
    	$('#entradas-num').change(function (e) {
    		e.preventDefault();
    		var pageSize = $(this).val();
    		$('#table-pagination').data('page-size', pageSize);
    		$('#table-pagination').trigger('footable_initialized');
    	});
    });

    $('#btn-activities').click(function(){
    	if($('.activities-box').hasClass('active-box')){
    		$('.activities-box').removeClass('active-box');
    		$('.overlay-activities').removeClass('active-overlay');
    	}else{
        	$('.activities-box').addClass('active-box');
        	$('.overlay-activities').addClass('active-overlay');
        }
    });

    $('#close-activities').click(function(){
    	$('.activities-box').removeClass('active-box');
    	$('.overlay-activities').removeClass('active-overlay');
    });
</script>

<script src="{{ asset('js/filter_db/filter.js') }}"></script>

<script type="text/javascript">
	$(function () {
	  $('[data-toggle="tooltip"]').tooltip()
	})
</script>
@endsection