@extends('back.layouts.app')

@section('stylesheets')

@endsection

@section('content')
<div class="row page-titles">
    <div class="col-md-5 align-self-center">
        <h3 class="text-themecolor">Vista de Prueba CRM</h3>
    </div>
    <div class="col-md-7 align-self-center">
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="{{ url('/admin') }}">Inicio</a></li>
            <li class="breadcrumb-item"><a href="{{ route('listados.index') }}">CRM</a></li>
        </ol>
    </div>
</div>

<div class="row">
    <div class="col">
        @include('back.layouts.partials._mensajes')
    </div>
</div>

<div class="row mb-4 mt-3">
    <div class="col">
        <a href="{{ route('listados.index') }}" class="btn btn-secondary">Regresar al listado General</a>
    </div>  
</div>

<div class="card card-body">
	<h3>Esto es una pantalla de prueba</h3>
</div>

@endsection

@section('scripts')

@endsection