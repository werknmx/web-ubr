@extends('back.layouts.app')

@section('stylesheets')
<link rel="stylesheet" type="text/css" href="{{ asset('css/filter_db/filter.css') }}">

<!-- Footable CSS -->
<link href="{{ asset('back/assets/plugins/footable/css/footable.core.css') }}" rel="stylesheet">
<link href="{{ asset('back/assets/css/filter-table.css') }}" rel="stylesheet">
<link href="{{ asset('back/assets/plugins/select2/dist/css/select2.min.css') }}" rel="stylesheet" type="text/css" />

<style type="text/css">
    .select2-container{
        display: block;
        width: 100% !important;
    }

    .table td, .table th {
        padding: 1.3rem .75rem;
        vertical-align: middle !important;
        font-weight: 300 !important;
    }

    sup{
    	top: -3px;
    	margin-right: 10px;
    }

    .stats .card-title{
    	margin-bottom: 0px;
    }

    .stats a{
    	font-size: .8em;
    }

    .round{
    	width: 35px !important;
    	height: 35px !important;
    }

    .round i{
    	position: relative !important;
    	line-height: 35px !important;
    	top: -5px;
    }
</style>

@endsection

@section('content')
<div class="row page-titles">
    <div class="col-md-5 align-self-center">
        <h3 class="text-themecolor">Gestión de Relaciones con Clientes</h3>
    </div>
    <div class="col-md-7 align-self-center">
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="{{ url('/admin') }}">Inicio</a></li>
            <li class="breadcrumb-item"><a href="{{ route('listados.index') }}">CRM</a></li>
            <li class="breadcrumb-item active">Resultado de Búsqueda</li>
        </ol>
    </div>
</div>

<div class="row">
    <div class="col">
        @include('back.layouts.partials._mensajes')
    </div>
</div>

<div class="section-title pt-3 pb-3">
    <div class="row align-items-center">
        <div class="col">
            <h6 class="section text-uppercase">Busqueda</h6>
            @if(Request::input('search_query') == NULL)

            @else
                <h2 class="description mb-2">Elementos que contienen: "{{ Request::input('search_query') }}"</h2>
                <h5>Se han encontrado un total de <strong>{{ $contacts->count() }}</strong> resultado(s).</h5>
            @endif
        </div>
    </div>
    <hr>
</div>

<div class="row mb-4 mt-3">
    <div class="col">
        <a href="{{ route('listados.index') }}" class="btn btn-secondary">Regresar al listado General</a>
    </div>  
</div>

<div class="row">
    <div class="col-md-12">
    	<div class="row">
		    <div class="col-md-12">
	    		<div class="card">
		            <div class="card-body">
		                <h3>Listado de Búsqueda</h3>
		                <hr>

						<table id="table-pagination" class="table m-b-0 toggle-arrow-tiny" data-page-size="10">
							<thead>
								<tr>
									<th>Fecha</th>
									<th>Prospecto</th>
									<th>Interés</th>
									<th>Asesor</th>
									<th>Campaña</th>
									<th>Estado</th>
								</tr>
							</thead>
							<tbody>
                                @foreach($contacts as $ct)
                                    <tr>
                                        <td><span class="text-muted"><i class="fa fa-clock-o"></i> {{ $ct->created_at->diffForHumans() }}</span></td>
                                        <td>
                                            <a href="{{ route('listados.show', $ct->id) }}" data-toggle="tooltip" data-placement="top" title="Ver Perfil">{{ $ct->name . ' ' . $ct->sur_name }}</a>

                                            <p style="font-size: 80%; margin-bottom: 0px; margin-top: 10px;"><i class="mdi mdi-cellphone-android"></i> {{ $ct->phone_cel }}</p>
                                            <p style="font-size: 80%; margin-bottom: 0px;"><i class="mdi mdi-email"></i> {{ $ct->email }}</p>
                                        </td>
                                        <td>
                                            @foreach($ct->interests as $int)
                                                <a class="badge-pill badge-info" href="{{ route('programas.show', $int->id) }}" data-toggle="tooltip" data-placement="top" title="Filtrar por este Interés">{{ $int->name }}</a>
                                            @endforeach
                                        </td>
                                        <td><a href="" data-toggle="tooltip" data-placement="top" title="Filtrar por Asesor">{{ $ct->sales_agent->name }}</a></td>

                                        <td>
                                            @if($ct->landing_origin_id == NULL)
                                            <span class="text-uppercase" style="font-size: .8em">Registro Manual</span>
                                            @else
                                            {{ $ct->origin->title }} <a href="" data-toggle="tooltip" data-placement="top" title="Ir a la Campaña"><i class="mdi mdi-web"></i></a>
                                            @endif
                                        </td>
                                        <td>
                                            @if($ct->status == 'Registro')
                                                <span class="badge badge-danger">{{ $ct->status }}</span>
                                            @endif

                                            @if($ct->status == 'Prospecto')
                                                <span class="badge badge-danger">{{ $ct->status }}</span>
                                            @endif

                                            @if($ct->status == 'Interesado')
                                                <span class="badge badge-warning">{{ $ct->status }}</span>
                                            @endif

                                            @if($ct->status == 'Promesa de Venta')
                                                <span class="badge badge-info">{{ $ct->status }}</span>
                                            @endif
                                            
                                            @if($ct->status == 'Cierre Exitoso')
                                                <span class="badge badge-success">{{ $ct->status }}</span>
                                            @endif

                                            @if($ct->status == 'Cierre Perdido')
                                                <span class="badge badge-danger">{{ $ct->status }}</span>
                                            @endif
                                            
                                        </td>
                                    </tr>
                                @endforeach
							</tbody>
							<tfoot>
								<tr>
									<td colspan="12">
										<div class="text-right">
											<ul class="pagination pagination-split m-t-30"> </ul>
										</div>
									</td>
								</tr>
							</tfoot>
						</table>
		            </div>
		        </div>
	    	</div>
    	</div>
    </div>
</div>

<style type="text/css">
    .pagination{
        justify-content: center;
        margin-top: 10px;
    }
</style>

<script type="text/javascript">
     (function() {
       if (document.querySelector('.pagination')!==null) {

         // Set paginator ul and li
         var paginator_ul = document.getElementsByClassName('pagination')[0],
             paginator_li = paginator_ul.getElementsByTagName("LI");

         [].forEach.call(paginator_li, function(li) {
             li.className += " page-item";
             li.firstChild.className += " page-link";
         });
       }
    })();
</script>
@endsection

@section('scripts')
<script src="{{ asset('back/assets/plugins/select2/dist/js/select2.full.min.js') }}" type="text/javascript"></script>

<script type="text/javascript">
    $(".select2").select2();
</script>
<!-- Footable -->
<script src="{{ asset('back/assets/plugins/footable/js/footable.all.min.js') }}"></script>

<script src="{{ asset('js/filter_db/filter.js') }}"></script>

<script type="text/javascript">
	$(function () {
	  $('[data-toggle="tooltip"]').tooltip()
	})
</script>
@endsection