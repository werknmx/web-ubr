@extends('back.layouts.app')

@section('stylesheets')
<link href="{{ asset('back/css/pages/contact-app-page.css') }}" rel="stylesheet">
<link href="{{ asset('back/css/pages/card-page.css') }}" rel="stylesheet">

<style type="text/css">
    .card{
        margin-bottom: 0px !important;
    }
</style>
@endsection

@section('content')
<div class="row page-titles">
    <div class="col-md-5 align-self-center">
        <h3 class="text-themecolor">Panel Administrativo</h3>
    </div>
    <div class="col-md-7 align-self-center">
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="javascript:void(0)">Inicio</a></li>
            <li class="breadcrumb-item active">Vista General</li>
        </ol>
    </div>
</div>


<div class="row">
    <div class="col-lg-4 col-xlg-3 col-md-5">
        <div class="card"> 
            <img class="card-img" src="{{ asset('back/assets/images/background/socialbg.jpg')}}" alt="Card image">
            <div class="card-img-overlay card-inverse social-profile d-flex">
                <div class="align-self-center">
                    @if( Auth::user()->image == NULL)
                    <img src="{{ 'https://www.gravatar.com/avatar/' . md5(strtolower(trim(Auth::user()->email))) . '?d=retro&s=200' }}" alt="{{ Auth::user()->name }}" class="img-circle mb-4" width="100">
                    @else
                    <img src="{{ asset('img/usuarios/' . Auth::user()->image ) }}" alt="{{ Auth::user()->name }}" class="img-circle mb-4" width="100">
                    @endif

                    <h4 class="card-title">Bievenido, {{ Auth::user()->name }}</h4>
                    <h6 class="card-subtitle">{{ Auth::user()->email }}</h6>
                    <p class="text-white">Esta es la vista general de tu perfil, utiliza los elementos del menú para interactuar con las diferentes secciones.</p>

                    <span class="badge badge-info">
                        ROL: 
                        @if(Auth::user()->hasRole('webmaster'))
                            Web Master
                        @else
                        @endif

                        @if(Auth::user()->hasRole('sales_agent') == 'sales_agent')
                            Asesor de Venta
                        @else
                        @endif

                        @if(Auth::user()->hasRole('admin_agent') == 'admin_agent')
                            Agente de Soporte
                        @else
                        @endif

                        @if(Auth::user()->hasRole('admin_support') == 'admin_support')
                            Admin de Soporte
                        @else
                        @endif
                    </span>
                </div>
            </div>
        </div>
    </div>

    <div class="col-lg-8 col-xlg-9 col-md-7">
        @if($activities->count() == 0)
        <div class="card card-inverse card-success mb-4">
            <div class="card-header">
                <span class="m-b-0 text-white">EXCELENTE, VAS MUY BIEN.</span>
            </div>
            <div class="card-body">
                <h4 class="card-title mb-2">No tienes ninguna actividad pendiente para el día de hoy. Excelente trabajo.</h4>
            </div>
        </div>
        @else
        <div class="card card-inverse card-primary mb-4">
            <div class="card-header">
                <span class="m-b-0 text-white">IMPORTANTE</span>
            </div>
            <div class="card-body">
                <h4 class="card-title mb-2">Hay más de {{ $activities->count() }} actividades pendientes en tu cuenta</h4>
                <p class="card-text">Es momento de trabajar en esos pendientes. Accede a la tu sección de ventas y completa esas tareas. ¡Vamos!, tu puedes.</p>
                <a href="{{ route('listados.index') }}" class="btn btn-inverse mt-3">Ir a Ventas</a>
            </div>
        </div>
        @endif

        <br>

        <div class="card">
            <div class="card-header">
                Estadísticas Rápidas
            </div>
            <div class="card-body">
                <div class="row">
                    <!-- Column -->
                    <div class="col-lg-4 col-md-6">
                        <div class="card">
                            <div class="card-body">
                                <div class="d-flex flex-row">
                                    <div class="round align-self-center round-success"><i class="ti-wallet"></i></div>
                                    <div class="m-l-10 align-self-center">
                                        <h3 class="m-b-0">{{ $blog }}</h3>
                                        <h5 class="text-muted m-b-0">Publicaciones en Blog</h5>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- Column -->
                    <!-- Column -->
                    <div class="col-lg-4 col-md-6">
                        <div class="card">
                            <div class="card-body">
                                <div class="d-flex flex-row">
                                    <div class="round align-self-center round-info"><i class="ti-user"></i></div>
                                    <div class="m-l-10 align-self-center">
                                        <h3 class="m-b-0">{{ $prospect_count }}</h3>
                                        <h5 class="text-muted m-b-0">Prospectos Actuales</h5>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- Column -->
                    <!-- Column -->
                    <div class="col-lg-4 col-md-6">
                        <div class="card">
                            <div class="card-body">
                                <div class="d-flex flex-row">
                                    <div class="round align-self-center round-danger"><i class="ti-user"></i></div>
                                    <div class="m-l-10 align-self-center">
                                        <h3 class="m-b-0">{{ $alert_prospect }}</h3>
                                        <h5 class="text-muted m-b-0">Prospectos Detenidos</h5>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- Column -->
                    <!-- Column -->
                    <div class="col-lg-4 col-md-6">
                        <div class="card">
                            <div class="card-body">
                                <div class="d-flex flex-row">
                                    <div class="round align-self-center round-success"><i class="ti-settings"></i></div>
                                    <div class="m-l-10 align-self-center">
                                        <h3 class="m-b-0">{{ $activities_completed }}</h3>
                                        <h5 class="text-muted m-b-0">Actividades Completas</h5>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- Column -->
                </div>
                <hr>

                <div class="row mt-4">
                    <!-- Column -->
                    <div class="col-md-6">
                        <div class="card">
                            <h4 class="card-title">Cierres de Hoy</h4>
                            <div class="text-right">
                                <h1 class="font-light"><sup><i class="ti-arrow-up text-success"></i></sup>{{ $ventas_hoy->count() }}</h1>
                            </div>

                            @php
                                if($cierres_mensuales->objective == 0)
                                    $porcentaje_1 = 0;
                                else{
                                    $porcentaje_1 = round(($ventas_hoy->count()*100)/($cierres_mensuales->objective/30));
                                }
                            @endphp

                            <span class="text-success">{{ $porcentaje_1 }}%</span>
                            <div class="progress">
                                <div class="progress-bar bg-success" role="progressbar" style="width: {{ $porcentaje_1  }}%; height: 6px;" aria-valuenow="{{ $ventas_hoy->count() }}" aria-valuemin="0" aria-valuemax="100"></div>
                            </div>
                        </div>
                    </div>
                    <!-- Column -->
                    <!-- Column -->
                    <div class="col-md-6">
                        <div class="card">
                            <div class="card-body">
                                <h4 class="card-title">Cierres Semanales</h4>
                                <div class="text-right">
                                    <h1 class="font-light"><sup><i class="ti-arrow-up text-info"></i></sup>{{ $ventas_semana->count() }}</h1>
                                </div>

                                @php
                                    if($cierres_mensuales->objective == 0)
                                        $porcentaje_2 = 0;
                                    else{
                                        $porcentaje_2 = round(($ventas_semana->count()*100)/($cierres_mensuales->objective/4));
                                    }
                                @endphp

                                <span class="text-info">{{ $porcentaje_2 }}%</span>
                                <div class="progress">
                                    <div class="progress-bar bg-info" role="progressbar" style="width: {{ $porcentaje_2 }}%; height: 6px;" aria-valuenow="{{ $ventas_semana->count() }}" aria-valuemin="0" aria-valuemax="100"></div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- Column -->
                    <!-- Column -->
                    <div class="col-md-6">
                        <div class="card">
                            <div class="card-body">
                                <h4 class="card-title">Cierres Mensuales</h4>
                                <div class="text-right">
                                    <h1 class="font-light"><sup><i class="ti-arrow-down text-danger"></i></sup>{{ $ventas_mes->count() }}</h1>
                                </div>

                                @php
                                    if($cierres_mensuales->objective == 0)
                                        $porcentaje_3 = 0;
                                    else{
                                        $porcentaje_3 = round(($ventas_mes->count()*100)/$cierres_mensuales->objective);
                                    }
                                @endphp

                                <span class="text-danger">{{ $porcentaje_3 }}%</span>
                                <div class="progress">
                                    <div class="progress-bar bg-danger" role="progressbar" style="width: {{ $porcentaje_3  }}%; height: 6px;" aria-valuenow="{{ $ventas_mes->count() }}" aria-valuemin="0" aria-valuemax="100"></div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- Column -->
                    <!-- Column -->
                    <div class="col-md-6">
                        <div class="card">
                            <div class="card-body">
                                <h4 class="card-title">Venta Total Mensual</h4>
                                <div class="text-right">
                                    <h1 class="font-light"><sup><i class="ti-arrow-up text-inverse"></i></sup> ${{ number_format($ven_mes) }}</h1>
                                </div>

                                @php
                                    if($venta_anual->objective == 0)
                                        $porcentaje_4 = 0;
                                    else{
                                        $porcentaje_4 = round(($ven_mes*100)/($venta_anual->objective/12));
                                    }
                                @endphp

                                <span class="text-inverse">{{ $porcentaje_4 }}%</span>
                                <div class="progress">
                                    <div class="progress-bar bg-inverse" role="progressbar" style="width: {{ $porcentaje_4 }}%; height: 6px;" aria-valuenow="{{ $ven_mes }}" aria-valuemin="0" aria-valuemax="100"></div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- Column -->
                </div>
            </div>
        </div>
    </div>
</div>
<!--
<div class="row">
    <div class="col-lg-3">
        <div class="card">
            <div class="card-body">
                <div class="d-flex">
                    <div class="m-r-20 align-self-center"><img src="{{ asset('back/assets/images/icon/income.png') }}" alt="Income" /></div>
                    <div class="align-self-center">
                        <h6 class="text-muted m-t-10 m-b-0">Total Income</h6>
                        <h2 class="m-t-0">953,000</h2></div>
                </div>
            </div>
        </div>
    </div>
    <div class="col-lg-3">
        <div class="card">
            <div class="card-body">
                <div class="d-flex">
                    <div class="m-r-20 align-self-center"><img src="{{ asset('back/assets/images/icon/expense.png') }}" alt="Income" /></div>
                    <div class="align-self-center">
                        <h6 class="text-muted m-t-10 m-b-0">Total Expense</h6>
                        <h2 class="m-t-0">236,000</h2></div>
                </div>
            </div>
        </div>
    </div>
    <div class="col-lg-3">
        <div class="card">
            <div class="card-body">
                <div class="d-flex">
                    <div class="m-r-20 align-self-center"><img src="{{ asset('back/assets/images/icon/assets.png') }}" alt="Income" /></div>
                    <div class="align-self-center">
                        <h6 class="text-muted m-t-10 m-b-0">Total Assets</h6>
                        <h2 class="m-t-0">987,563</h2></div>
                </div>
            </div>
        </div>
    </div>
    <div class="col-lg-3">
        <div class="card">
            <div class="card-body">
                <div class="d-flex">
                    <div class="m-r-20 align-self-center"><img src="{{ asset('back/assets/images/icon/staff.png') }}" alt="Income" /></div>
                    <div class="align-self-center">
                        <h6 class="text-muted m-t-10 m-b-0">Total Staff</h6>
                        <h2 class="m-t-0">987,563</h2></div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-lg-6 col-md-12">
        <div class="card">
            <div class="card-body">
                <div class="d-flex">
                    <div>
                        <h3 class="card-title m-b-5"><span class="lstick"></span>Sales Overview </h3>
                    </div>
                    <div class="ml-auto">
                        <select class="custom-select b-0">
                            <option selected="">January 2017</option>
                            <option value="1">February 2017</option>
                            <option value="2">March 2017</option>
                            <option value="3">April 2017</option>
                        </select>
                    </div>
                </div>
                <div id="sales-overview2" class="p-relative" style="height:360px;"></div>
                <div class="stats-bar">
                    <div class="row text-center">
                        <div class="col-lg-4 col-md-4">
                            <div class="p-20">
                                <h6 class="m-b-0">Total Sales</h6>
                                <h3 class="m-b-0">$10,345</h3>
                            </div>
                        </div>
                        <div class="col-lg-4 col-md-4">
                            <div class="p-20">
                                <h6 class="m-b-0">This Month</h6>
                                <h3 class="m-b-0">$7,589</h3>
                            </div>
                        </div>
                        <div class="col-lg-4 col-md-4">
                            <div class="p-20">
                                <h6 class="m-b-0">This Week</h6>
                                <h3 class="m-b-0">$1,476</h3>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="col-lg-6 col-md-12">
        <div class="card">
            <div class="card-body">
                <div class="d-flex">
                    <div>
                        <h3 class="card-title m-b-5"><span class="lstick"></span>Sales Overview </h3>
                    </div>
                    <div class="ml-auto">
                        <select class="custom-select b-0">
                            <option selected="">January 2017</option>
                            <option value="1">February 2017</option>
                            <option value="2">March 2017</option>
                            <option value="3">April 2017</option>
                        </select>
                    </div>
                </div>
                <div id="ct-sales3-chart" class="p-relative" style="height:360px;"></div>
                <div class="stats-bar">
                    <div class="row text-center">
                        <div class="col-lg-4 col-md-4">
                            <div class="p-20">
                                <h6 class="m-b-0">Total Sales</h6>
                                <h3 class="m-b-0">$10,345</h3>
                            </div>
                        </div>
                        <div class="col-lg-4 col-md-4">
                            <div class="p-20">
                                <h6 class="m-b-0">This Month</h6>
                                <h3 class="m-b-0">$7,589</h3>
                            </div>
                        </div>
                        <div class="col-lg-4 col-md-4">
                            <div class="p-20">
                                <h6 class="m-b-0">This Week</h6>
                                <h3 class="m-b-0">$1,476</h3>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-lg-8">
        <div class="card">
            <div class="card-body">
                <div class="d-flex">
                    <div>
                        <h3 class="card-title m-b-5"><span class="lstick"></span>Total Visits </h3>
                    </div>
                    <div class="ml-auto">
                        <select class="custom-select b-0">
                            <option selected="">January 2017</option>
                            <option value="1">February 2017</option>
                            <option value="2">March 2017</option>
                            <option value="3">April 2017</option>
                        </select>
                    </div>
                </div>
                <div id="visitfromworld" style="width:100%!important; height:415px"></div>
            </div>
        </div>
    </div>
    <div class="col-lg-4">
        <div class="card">
            <div class="card-body">
                <h4 class="card-title"><span class="lstick"></span>Browser Stats</h4>
                <table class="table browser m-t-30 no-border">
                    <tbody>
                        <tr>
                            <td style="width:40px"><img src="{{ asset('back/assets/images/browser/chrome-logo.png') }}" alt=logo /></td>
                            <td>Google Chrome</td>
                            <td class="text-right">23%</td>
                        </tr>
                        <tr>
                            <td><img src="{{ asset('back/assets/images/browser/firefox-logo.png') }}" alt=logo /></td>
                            <td>Mozila Firefox</td>
                            <td class="text-right">15%</td>
                        </tr>
                        <tr>
                            <td><img src="{{ asset('back/assets/images/browser/safari-logo.png') }}" alt=logo /></td>
                            <td>Apple Safari</td>
                            <td class="text-right">07%</td>
                        </tr>
                        <tr>
                            <td><img src="{{ asset('back/assets/images/browser/internet-logo.png') }}" alt=logo /></td>
                            <td>Internet Explorer</td>
                            <td class="text-right">09%</td>
                        </tr>
                        <tr>
                            <td><img src="{{ asset('back/assets/images/browser/opera-logo.png') }}" alt=logo /></td>
                            <td>Opera mini</td>
                            <td class="text-right">23%</td>
                        </tr>
                        <tr>
                            <td><img src="{{ asset('back/assets/images/browser/edge-logo.png') }}" alt=logo /></td>
                            <td>Microsoft edge</td>
                            <td class="text-right">09%</td>
                        </tr>
                        <tr>
                            <td><img src="{{ asset('back/assets/images/browser/netscape-logo.png') }}" alt=logo /></td>
                            <td>Netscape Navigator</td>
                            <td class="text-right">04%</td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
-->

<!--
<div class="row">
    <div class="col-lg-6 col-md-12">
        <div class="card">
            <div class="card-body">
                <div class="d-flex">
                    <div>
                        <h4 class="card-title"><span class="lstick"></span>Projects of the Month</h4></div>
                    <div class="ml-auto">
                        <select class="custom-select b-0">
                            <option selected="">January 2017</option>
                            <option value="1">February 2017</option>
                            <option value="2">March 2017</option>
                            <option value="3">April 2017</option>
                        </select>
                    </div>
                </div>
                <div class="table-responsive m-t-20">
                    <table class="table vm no-th-brd pro-of-month no-wrap">
                        <thead>
                            <tr>
                                <th colspan="2">Assigned</th>
                                <th>Name</th>
                                <th>Priority</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td style="width:50px;"><span class="round"><img src="{{ asset('back/assets/images/users/1.jpg') }}" alt="user" width="50"></span></td>
                                <td>
                                    <h6>Sunil Joshi</h6><small class="text-muted">Web Designer</small></td>
                                <td>Elite Admin</td>
                                <td><span class="label label-success label-rounded">Low</span></td>
                            </tr>
                            <tr class="active">
                                <td><span class="round"><img src="{{ asset('back/assets/images/users/2.jpg') }}" alt="user" width="50"></span></td>
                                <td>
                                    <h6>Andrew</h6><small class="text-muted">Project Manager</small></td>
                                <td>Real Homes</td>
                                <td><span class="label label-info label-rounded">Medium</span></td>
                            </tr>
                            <tr>
                                <td><span class="round round-success"><img src="{{ asset('back/assets/images/users/3.jpg') }}" alt="user" width="50"></span></td>
                                <td>
                                    <h6>Bhavesh patel</h6><small class="text-muted">Developer</small></td>
                                <td>MedicalPro Theme</td>
                                <td><span class="label label-primary label-rounded">High</span></td>
                            </tr>
                            <tr>
                                <td><span class="round round-primary"><img src="{{ asset('back/assets/images/users/4.jpg') }}" alt="user" width="50"></span></td>
                                <td>
                                    <h6>Nirav Joshi</h6><small class="text-muted">Frontend Eng</small></td>
                                <td>Elite Admin</td>
                                <td><span class="label label-danger label-rounded">Low</span></td>
                            </tr>
                            <tr>
                                <td><span class="round round-warning"><img src="{{ asset('back/assets/images/users/5.jpg') }}" alt="user" width="50"></span></td>
                                <td>
                                    <h6>Micheal Doe</h6><small class="text-muted">Content Writer</small></td>
                                <td>Helping Hands</td>
                                <td><span class="label label-success label-rounded">High</span></td>
                            </tr>
                            <tr>
                                <td><span class="round round-danger"><img src="{{ asset('back/assets/images/users/6.jpg') }}" alt="user" width="50"></span></td>
                                <td>
                                    <h6>Johnathan</h6><small class="text-muted">Graphic</small></td>
                                <td>Digital Agency</td>
                                <td><span class="label label-info label-rounded">High</span></td>
                            </tr>
                            <tr>
                                <td><span class="round round-primary"><img src="{{ asset('back/assets/images/users/4.jpg') }}" alt="user" width="50"></span></td>
                                <td>
                                    <h6>Nirav Joshi</h6><small class="text-muted">Frontend Eng</small></td>
                                <td>Elite Admin</td>
                                <td><span class="label label-danger label-rounded">Low</span></td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
-->
@endsection

@section('scripts')

@endsection