<aside class="left-sidebar">
    <!-- Sidebar scroll-->
    <div class="scroll-sidebar">
        <!-- Sidebar navigation-->
        <nav class="sidebar-nav">
            <ul id="sidebarnav">
                <li class="nav-devider"></li>
                <li class="nav-small-cap">PERSONAL</li>
                <li><a class="has-arrow waves-effect waves-dark" href="{{ route('home') }}" aria-expanded="false"><i class="mdi mdi-gauge"></i><span class="hide-menu">Vista Rápida</span></a></li>

                @if(auth()->user()->can('all_access') || auth()->user()->can('sales_access')) 
                <li><a class="has-arrow waves-effect waves-dark" href="#" aria-expanded="false"><i class="mdi mdi-sale"></i><span class="hide-menu">Ventas</span></a>
                    <ul aria-expanded="false" class="collapse">
                        <li><a href="{{ route('listados.index') }}"><i class="mdi mdi-view-dashboard"></i> Vista General</a></li>
                        <li><a href="{{ route('ventas.kpi.index') }}"><i class="mdi mdi-trending-up"></i> KPI's</a></li>
                    </ul>
                </li>
                @endif

                {{--
                @if(auth()->user()->can('all_access') || auth()->user()->can('payment_access')) 
                <li class="two-column"><a class="has-arrow waves-effect waves-dark" href="#" aria-expanded="false"><i class="mdi mdi-wallet"></i><span class="hide-menu">Pagos</span></a>
                    <ul aria-expanded="false" class="collapse">
                        <li><a href="app-chat.html"><i class="mdi mdi-view-dashboard"></i> Vista General</a></li>
                        <li><a href="app-chat.html"><i class="mdi mdi-credit-card"></i> Pagos Manuales</a></li>
                        <li><a href="app-chat.html"><i class="mdi mdi-trending-up"></i> KPI's</a></li>
                        
                    </ul>
                </li>
                @endif
                --}}

                <!--
                <li><a class="has-arrow waves-effect waves-dark" href="#" aria-expanded="false"><i class="mdi mdi-book-open-variant"></i><span class="hide-menu">Noticias / Blog</span></a>
                    <ul aria-expanded="false" class="collapse">
                        <li>
                            <a class="has-arrow" href="#" aria-expanded="false">Publicaciones</a>
                            <ul aria-expanded="false" class="collapse">
                                <li><a href="{{ route('blog.create') }}">Crear Nueva</a></li>
                                <li><a href="{{ route('blog.index') }}">Ver Todas</a></li>
                            </ul>
                        </li>
                        <li><a href="{{ route('categorias.index') }}">Categorías</a></li>
                        <li><a href="{{ route('etiquetas.index') }}">Etiquetas</a></li>
                    </ul>
                </li>
                -->  

                @if(auth()->user()->can('all_access') || auth()->user()->can('admin_access_services')) 
                <li class="two-column"><a class="has-arrow waves-effect waves-dark" href="#" aria-expanded="false"><i class="mdi mdi-bullhorn"></i><span class="hide-menu">Marketing </span></a>
                    <ul aria-expanded="false" class="collapse">
                        <li><a href="{{ route('campanas.index') }}"><i class="mdi mdi-trophy-award"></i> Campañas</a></li>
                        <li><a href="{{ route('galerias.index') }}"><i class="mdi mdi-image-filter"></i> Galerías</a></li>
                        <li><a href="{{ route('cursos.index') }}"><i class="mdi mdi-theater"></i> Cursos</a></li>
                        <li><a href="{{ route('programas.index') }}"><i class="mdi mdi-timetable"></i> Servicios / Programas</a></li>

                        <li><a href="#" class="has-arrow"><i class="mdi mdi-pencil"></i> Blog / Noticias</a>
                            <ul aria-expanded="false" class="collapse">
                                <li><a href="{{ route('blog.index') }}">Listado General</a></li>
                                <li><a href="{{ route('categorias.index') }}">Categorías</a></li>
                                <li><a href="{{ route('etiquetas.index') }}">Etiquetas</a></li>
                            </ul>
                        </li>

                        <li><a href="{{ route('eventos.index') }}"><i class="mdi mdi-timetable"></i> Eventos</a></li>
                        <!--<li><a href="javascript:void(0)"><i class="mdi mdi-flask-outline"></i> Becas</a></li>-->
                    </ul>
                </li>
                @endif

                @if(auth()->user()->can('all_access') || auth()->user()->can('admin_access')) 
                <li><a class="has-arrow waves-effect waves-dark" href="#" aria-expanded="false"><i class="mdi mdi-television-guide"></i><span class="hide-menu">Administrativo</span></a>
                    <ul aria-expanded="false" class="collapse">
                        <li><a href="{{ route('reloj-checador.index') }}"><i class="mdi mdi-view-dashboard"></i> 
                            Entradas de Hoy</a>
                        </li>
                        <li><a href="{{ route('reloj-checador.history') }}"><i class="mdi mdi-view-dashboard"></i> Historial Reloj Checador</a></li>
                    </ul>
                </li>
                @endif

                @if(auth()->user()->can('all_access') || auth()->user()->can('admin_access')) 
                <li><a class="has-arrow waves-effect waves-dark" href="#" aria-expanded="false"><i class="mdi mdi-account-multiple"></i><span class="hide-menu">Usuarios / Roles</span></a>
                    <ul aria-expanded="false" class="collapse">
                        <li><a href="{{ route('admin.index') }}"><i class="mdi mdi-view-dashboard"></i> Listado General</a></li>
                        <li><a href="{{ route('roles.index') }}"><i class="mdi mdi-account-settings"></i> Roles</a></li>

                        <li><a href="{{ route('register') }}"><i class="mdi mdi-account-plus"></i> Registrar Nuevo Usuario</a></li>
                    </ul>
                </li>
                @endif
            </ul>
        </nav>
        <!-- End Sidebar navigation -->
    </div>
    <!-- End Sidebar scroll-->
</aside>