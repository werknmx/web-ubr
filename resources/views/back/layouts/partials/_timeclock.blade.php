<style type="text/css">
	.time-clock{
		position: fixed;
		bottom: 30px;
		left: 30px;
		z-index: 99;
	}

	.time-clock-btn{
		padding: 12px 30px;
		display: block;
		background-color: #000;
		color: #fff;
		border-radius: 25px;
		text-align: center;
		border:none;
		cursor: pointer;
	}

	.check-in-box,
	.check-out-box{
		display: none;

		position: relative;
		
		background-color: #fff;
		padding: 15px 20px;
		border: 1px solid rgba(0,0,0,0.4);
		border-radius: 13px;
		z-index: 999;
		margin-bottom: 10px;
		width: 250px;
	}

	.check-in-box h6{
		text-transform: uppercase;
		font-size: .8em;
		font-weight: bold;
		margin-bottom: 0px;
	}

	.days-marker{
		display: none;
	}

	.check-out-box ul span{
		display: inline-block;
		margin-right: 5px;
		width: 20px;
		text-align: right;
		font-weight: bold;
	}
</style>

<!-- Funciones PHP de Reloj -->
@php
	use Carbon\Carbon;

	$relojes = App\TimeClock::where('user_id', Auth::user()->id)->where('check_out', NULL)->whereDate('check_in', Carbon::now())->get();
	$reloj_completo = App\TimeClock::where('user_id', Auth::user()->id)->whereDate('check_out', Carbon::now())->get();
@endphp

<!-- Esqueleto de Reloj Checador-->
<div class="time-clock">
	@if($relojes->count() == 0)
		<div class="check-in-box">
			@if($reloj_completo->count() == 0)
			<form method="POST" action="{{ route('reloj-checador.store') }}" enctype="multipart/form-data">
	        {{ csrf_field() }}
				<h4 class="text-center mb-3">Check-In</h4>
				<textarea class="form-control" name="comment" placeholder="Tu Comentario de entrada..." required=""></textarea>
				<input type="hidden" name="user_id" value="{{ Auth::user()->id }}">
				<button type="submit" class="mt-3 btn-block btn btn-primary"><i class="mdi mdi-alarm-check"></i> Check-In</a>
			</form>
			@else
				@foreach($reloj_completo as $rl)
				@php
					$check_in = Carbon::create($rl->check_in);
					$check_out = Carbon::create($rl->check_out);
				@endphp
				<h4 class="text-center mb-3">Tus Datos de Hoy</h4>

				<h6>Hora que entraste hoy:</h6>
				<p>{{ $check_in->format('h:m:s a') }}</p>

				<h6>Hora que saliste hoy:</h6>
				<p>{{ $check_out->format('h:m:s a') }}</p>

				<h6>Comentario de entrada:</h6>
				<p>{{ $rl->comment }}</p>

				<hr>

				<p>¿Algún problema con la información? <a href="">Contacta a soporte</a></p>
				@endforeach
			@endif
		</div>

		<button id="checkIn" class="time-clock-btn">
			@if($reloj_completo->count() == 0)
			<i class="mdi mdi-alarm-plus"></i> Realiza tu Check-In
			@else
			<i class="mdi mdi-history"></i> Reloj Checador
			@endif
		</button>

		<script type="text/javascript">
			$("#checkIn").click(function() {
				$(".check-in-box").toggle("slow");
			});
		</script>
	@else
		<div class="check-out-box">
			<h4 class="text-center mb-3">Horas para Check-Out <i class="mdi mdi-information" data-toggle="tooltip" data-placement="right" title="Esto es para términos estadísticos, no afecta tu rendimiento ni acceso a la plataforma."></i></h4>
			<ul class="list-unstyled">
	            <li class="days-marker"><span id="days"></span>días</li>
	            <li><span id="hours"></span>horas</li>
	            <li><span id="minutes"></span>minutos</li>
	            <li><span id="seconds"></span>segundos</li>
	        </ul>

	        @foreach($relojes as $reloj)
	        <form method="POST" action="{{ route('reloj-checador.update', $reloj->id) }}" enctype="multipart/form-data">
	        	{{ csrf_field() }}
	        	{{ method_field('PUT') }}
				<button type="submit" class="mt-3 btn-block btn btn-primary"><i class="mdi mdi-alarm-check"></i> Check-Out Ahora</a>
			</form>
			@endforeach
		</div>
		<button id="checkOut" class="time-clock-btn"><i class="mdi mdi-alarm-plus"></i> Marcar Check-Out</button>

		<!-- Contador de Horas -->
		@php
			$day_shift = Carbon::today()->format('M d, Y');
		@endphp

		<script type="text/javascript">
			$("#checkOut").click(function() {
				$(".check-out-box").toggle("slow");
			});
		</script>

		<script>
		    const second = 1000,
		    minute = second * 60,
		    hour = minute * 60,
		    day = hour * 24;

		    let countDown = new Date('{{ $day_shift }} 17:00:00').getTime(),
		    x = setInterval(function() {

		        let now = new Date().getTime(),
		        distance = countDown - now;

		        document.getElementById('days').innerText = Math.floor(distance / (day)),
		        document.getElementById('hours').innerText = Math.floor((distance % (day)) / (hour)),
		        document.getElementById('minutes').innerText = Math.floor((distance % (hour)) / (minute)),
		        document.getElementById('seconds').innerText = Math.floor((distance % (minute)) / second);

		//do something later when date is reached
		//if (distance < 0) {
		//  clearInterval(x);
		//  'IT'S MY BIRTHDAY!;
		//}

		}, second)
		</script>
	@endif
</div>
