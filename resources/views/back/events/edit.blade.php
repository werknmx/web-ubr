@extends('back.layouts.app')

@section('stylesheets')
<link href="{{ asset('back/assets/plugins/bootstrap-datepicker/bootstrap-datepicker.min.css') }}" rel="stylesheet" type="text/css" />
@endsection

@section('content')
<div class="row page-titles">
    <div class="col-md-5 align-self-center">
        <h3 class="text-themecolor">Editar Evento</h3>
    </div>
    <div class="col-md-7 align-self-center">
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="javascript:void(0)">Inicio</a></li>
            <li class="breadcrumb-item">Marketing</li>
            <li class="breadcrumb-item">Eventos</li>
            <li class="breadcrumb-item active">{{ $evento->title }}</li>
        </ol>
    </div>
</div>

<form method="POST" action="{{ route('eventos.update', $evento->id) }}" enctype="multipart/form-data">
	{{ csrf_field() }}
	{{ method_field('PUT') }}

	<div class="row">
		<div class="col col-md-12">
			<div class="card">
				<div class="card-body">
					<h3>Evento</h3>
					<hr>

					<div class="row">
                        <div class="col-md-12 form-group">
                            <label class="control-label">Titulo de Evento</label>
                            <input class="form-control form-white" placeholder="El nombre de tu evento" type="text" name="name" value="{{ $evento->name }}" />
                        </div>

                        <div class="col-md-12 form-group">
                            <label class="control-label">Descripción</label>
                            <textarea class="form-control form-white" name="description" rows="3">{{ $evento->description }}</textarea>
                        </div>


                        <div class="col-md-12 form-group">
                            <label class="control-label">Fechas del Evento</label>
                            <div class="input-daterange input-group" id="date-range">
                                <input type="text" class="form-control" name="start_date" value="{{ $evento->start_date }}" />
                                <span class="input-group-addon bg-info b-0 text-white">a</span>
                                <input type="text" class="form-control" name="end_date" value="{{ $evento->end_date }}" />
                            </div>
                        </div> 

                        <!--
                        <div class="col-md-6 form-group">
                        	<label class="control-label">Fecha de Inicio</label>
                            <input class="form-control form-white" type="datetime-local" name="start_date" />
                        </div>

                        <div class="col-md-6 form-group">
                        	<label class="control-label">Fecha de Finalización</label>
                            <input class="form-control form-white" type="datetime-local" name="end_date" />
                        </div>
                        -->

                        <div class="col-md-12">
                        	<h4>Información Adicional (OPCIONAL)</h4>
                        	<hr>
                        </div>

                        <div class="col-md-12 form-group">
                        	<label class="control-label">Lugar</label>
                            <input class="form-control form-white" placeholder="Lugar del Evento" type="text" name="location" value="{{ $evento->location }}" />
                        </div>

                        <div class="col-md-6 form-group">
                        	<label class="control-label">Teléfono Contacto</label>
                            <input class="form-control form-white" placeholder="Contacto Principal" type="text" name="contact_info_phone" value="{{ $evento->contact_info_phone }}" />
                        </div>

                        <div class="col-md-6 form-group">
                        	<label class="control-label">Correo Contacto</label>
                            <input class="form-control form-white" placeholder="Contacto Principal" type="text" name="contact_info_mail" value="{{ $evento->contact_info_mail }}" />
                        </div>
                    </div>
				</div>
			</div>

			<hr>
			<div class="col-md-12">
		    	<div class="row justify-content-end">
		            <div class="col">
		                <div class="form-group text-right">
		                    <a href="{{ route('eventos.index') }}" class="btn btn-link">Cancelar</a>
		                    <button type="submit" class="btn btn-success btn-lg">Guardar</button>
		                </div>
		            </div>
		        </div>
		    </div>
		</div>
	</div>
</form>
@endsection

@section('scripts')
<!-- Date range Plugin JavaScript -->
<script src="{{ asset('back/assets/plugins/bootstrap-datepicker/bootstrap-datepicker.min.js') }}"></script>

<script type="text/javascript">
    jQuery('#date-range').datepicker({
        toggleActive: true,
        language: 'es_MX',
        format: 'yyyy/mm/dd',
        todayHighlight: true
    });

    jQuery('#datepicker-inline').datepicker({
        todayHighlight: true
    });
</script>
@endsection