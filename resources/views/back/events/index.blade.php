@extends('back.layouts.app')

@section('stylesheets')
<!-- Calendar CSS -->
<link href="{{ asset('back/assets/plugins/calendar/dist/fullcalendar.css') }}" rel="stylesheet" />

<link href="{{ asset('back/assets/plugins/bootstrap-datepicker/bootstrap-datepicker.min.css') }}" rel="stylesheet" type="text/css" />
@endsection

@section('content')
<div class="row page-titles">
    <div class="col-md-5 align-self-center">
        <h3 class="text-themecolor">Eventos</h3>
    </div>
    <div class="col-md-7 align-self-center">
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="javascript:void(0)">Inicio</a></li>
            <li class="breadcrumb-item active">Eventos</li>
        </ol>
    </div>
</div>

<div class="row">
    <div class="col">
        @include('back.layouts.partials._mensajes')
    </div>
</div>

<div class="row">
    <div class="col-md-12">
        <div class="card">
            <div class="">
                <div class="row">
                    <div class="col-lg-3">
                        <div class="card-body">
                            <h4 class="card-title m-t-10">Calendario de Eventos</h4>
                            <div class="row">
                                <div class="col-md-12 mb-5">
                                    <a href="#" data-toggle="modal" data-target="#add-new-event" class="btn m-t-10 btn-info btn-block waves-effect waves-light">
                                        <i class="ti-plus"></i> Agregar Nuevo Evento
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-9">
                        <div class="card-body b-l calender-sidebar">
                            <div id="calendar"></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div id="calendarModal" class="modal none-border fade">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h4 id="modalTitle" class="modal-title"></h4>
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span> <span class="sr-only">close</span></button>
            </div>
            <div class="modal-body">
                <h3 id="description"></h3>

                <p class="mb-0"><i class="mdi mdi-map-marker"></i> <span id="infoOne"></span></p>
                <p class="mb-0"><i class="mdi mdi-phone-in-talk"></i> <span id="infoTwo"></span></p>
                <p class="mb-0"><i class="mdi mdi-email"></i> <span id="infoThree"></span></p>

            </div>
            <div class="modal-footer">
                <!--
                <form method="POST" class="delete" action="">
                    <button type="submit" id="deleteUrl" class="btn btn-outline-danger"><i class="mdi mdi-delete"></i></button>
                    {{ csrf_field() }}
                    {{ method_field('DELETE') }}
                </form>
                -->

                <a href="" id="deleteUrl" class="btn btn-outline-danger"><i class="mdi mdi-delete"></i></a>
                <a href="" id="eventUrl" class="btn btn-primary"><i class="mdi mdi-pencil"></i> Editar Evento</a>
                <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
            </div>
        </div>
    </div>
</div>

<div class="modal fade none-border" id="add-new-event">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title"><strong>Nuevo</strong> Evento</h4>
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
            </div>
            <form method="POST" action="{{ route('eventos.store') }}">
                {{ csrf_field() }}
            	<div class="modal-body">
                    <div class="row">
                        <div class="col-md-12 form-group">
                            <label class="control-label">Titulo de Evento</label>
                            <input class="form-control form-white" placeholder="El nombre de tu evento" type="text" name="name" required="" />
                        </div>

                        <div class="col-md-12 form-group">
                            <label class="control-label">Descripción</label>
                            <textarea class="form-control form-white" name="description" rows="3" required=""></textarea>
                        </div>


                        <div class="col-md-12 form-group">
                            <label class="control-label">Fechas del Evento</label>
                            <div class="input-daterange input-group" id="date-range">
                                <input type="text" class="form-control" name="start_date" required="" />
                                <span class="input-group-addon bg-info b-0 text-white">a</span>
                                <input type="text" class="form-control" name="end_date" required="" />
                            </div>
                        </div> 

                        <!--
                        <div class="col-md-6 form-group">
                        	<label class="control-label">Fecha de Inicio</label>
                            <input class="form-control form-white" type="datetime-local" name="start_date" />
                        </div>

                        <div class="col-md-6 form-group">
                        	<label class="control-label">Fecha de Finalización</label>
                            <input class="form-control form-white" type="datetime-local" name="end_date" />
                        </div>
                        -->

                        <div class="col-md-12">
                        	<h4>Información Adicional (OPCIONAL)</h4>
                        	<hr>
                        </div>

                        <div class="col-md-12 form-group">
                        	<label class="control-label">Lugar</label>
                            <input class="form-control form-white" placeholder="Lugar del Evento" type="text" name="location" required=""/>
                        </div>

                        <div class="col-md-6 form-group">
                        	<label class="control-label">Teléfono Contacto</label>
                            <input class="form-control form-white" placeholder="Contacto Principal" type="text" name="contact_info_phone" required=""/>
                        </div>

                        <div class="col-md-6 form-group">
                        	<label class="control-label">Correo Contacto</label>
                            <input class="form-control form-white" placeholder="Contacto Principal" type="text" name="contact_info_mail" />
                        </div>
                    </div>
	            </div>
	            <div class="modal-footer">
	                <button type="submit" class="btn btn-danger waves-effect waves-light">Guardar</button>
	                <button type="button" class="btn btn-secondary waves-effect" data-dismiss="modal">Cerrar</button>
	            </div>
            </form>
        </div>
    </div>
</div>
<!-- END MODAL -->
@endsection

@section('scripts')
<!-- Calendar JavaScript -->
<script src="{{ asset('back/assets/plugins/calendar/jquery-ui.min.js') }}"></script>
<script src="{{ asset('back/assets/plugins/moment/moment.js') }}"></script>
<script src="{{ asset('back/assets/plugins/calendar/dist/fullcalendar.min.js') }}"></script>
<script src="{{ asset('back/assets/plugins/calendar/dist/locale/es.js') }}"></script>

<script type="text/javascript">
	$('#calendar').fullCalendar({
        events : [
            @foreach($calendar_events as $event)
                @php
                    $time = $event->end_date;
                    $new_end = date('Y-m-d', strtotime($time. ' + 1 days'));
                @endphp
            {
                title : '{{ $event->name }}',
                description : '{{ $event->description }}',
                start : '{{ $event->start_date }}',
                end : '{{ $new_end }}',
                url : '{{ route('eventos.edit', $event->id) }}',
                deleteUrl : '{{ route('eventos.hard.destroy', $event->id) }}',
                className: '{{ $event->type }}',
                infoOne: '{{ $event->location }}',
                infoTwo: '{{ $event->contact_info_phone }}',
                infoThree: '{{ $event->contact_info_mail }}',
            },
            @endforeach
        ],

        allDay: false,
        eventLimit: false,
        defaultView: 'month',  
        handleWindowResize: true,  
        locale: 'es',

        eventClick:  function(event, jsEvent, view) {
            $('#modalTitle').html(event.title);
            $('#description').html(event.description);
            $('#infoOne').html(event.infoOne);
            $('#infoTwo').html(event.infoTwo);
            $('#infoThree').html(event.infoThree);
            $('#eventUrl').attr('href',event.url);
            $('#deleteUrl').attr('href',event.deleteUrl);
            $('#calendarModal').modal();
            if (event.url) {
                return false;
            }
        },

        header: {
            left: 'prev,next today',
            center: 'title',
            right: 'month,basicWeek,basicDay'
        }
    });
</script>

<!-- Date range Plugin JavaScript -->
<script src="{{ asset('back/assets/plugins/bootstrap-datepicker/bootstrap-datepicker.min.js') }}"></script>

<script type="text/javascript">
    jQuery('#date-range').datepicker({
        toggleActive: true,
        language: 'es_MX',
        format: 'yyyy/mm/dd',
        todayHighlight: true
    });

    jQuery('#datepicker-inline').datepicker({
        todayHighlight: true
    });
</script>
@endsection