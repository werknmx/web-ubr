@extends('back.layouts.app')

@section('stylesheets')
<link href="{{ asset('back/assets/plugins/chartist-js/dist/chartist.min.css') }}" rel="stylesheet">
<link href="{{ asset('back/assets/plugins/chartist-js/dist/chartist-init.css') }}" rel="stylesheet">
<link href="{{ asset('back/assets/plugins/chartist-plugin-tooltip-master/dist/chartist-plugin-tooltip.css') }}" rel="stylesheet">
<link href="{{ asset('back/assets/plugins/css-chart/css-chart.css') }}" rel="stylesheet">

<link href="{{ asset('back/assets/plugins/bootstrap-datepicker/bootstrap-datepicker.min.css') }}" rel="stylesheet" type="text/css" />
@endsection

@section('content')
<div class="row page-titles">
    <div class="col-md-5 align-self-center">
        <h3 class="text-themecolor">Campaña</h3>
    </div>
    <div class="col-md-7 align-self-center">
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="javascript:void(0)">Inicio</a></li>
            <li class="breadcrumb-item"><a href="javascript:void(0)">Campaña</a></li>
            <li class="breadcrumb-item active">{{ $campaign->title }}</li>
        </ol>
    </div>
</div>

<div class="row">
    <div class="col">
        @include('back.layouts.partials._mensajes')
    </div>
</div>

<div class="row mb-4">
    <div class="col">
        <a href="{{ route('campanas.index') }}" class="btn btn-rounded btn-success">Regresar</a>
    </div>
    <div class="col text-right">
        <a href="" class="btn btn-sm btn-info"><i class="fa fa-file-excel-o"></i> Exportar Estadísticas (.xlsx)</a>
        <a href="" class="btn btn-sm btn-outline-info">Editar Campaña</a>
        <a href="" class="btn btn-sm btn-outline-danger">Borrar Campaña</a>
    </div>
</div>

<div class="row">
    <div class="col-md-9">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-body">
                        <h3>Información de campaña</h3>
                        <hr>

                        <div class="row">
                            <div class="col-md-1">
                                <small class="text-muted pt-3 db">ID</small>
                                <h6>{{ $campaign->id }}</h6> 
                            </div>
                            <div class="col-md-3">
                                <small class="text-muted pt-3 db">Título/Nombre</small>
                                <h6>{{ $campaign->title }}</h6> 
                            </div>
                            <div class="col-md-4">
                                <small class="text-muted pt-3 db">Descripción</small>
                                <h6>{{ $campaign->description }}</h6> 
                            </div>
                            <div class="col-md-2">
                                <small class="text-muted pt-3 db">Creado</small>
                                <h6>{{ $campaign->created_at }}</h6> 
                            </div>
                            <div class="col-md-2">
                                <small class="text-muted pt-3 db">Actualizado</small>
                                <h6>{{ $campaign->updated_at }}</h6> 
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="col-md-12">
                <div class="card">
                    <div class="card-body">
                        <div class="row">
                            <div class="col">
                                <h3>Landing Pages</h3>
                            </div>
                            <div class="col text-right">
                                <a href="#" data-toggle="modal" data-target="#landingModal" class="btn btn-rounded btn-success"><i class="fa fa-plus-circle m-r-5"></i> Crear Nueva Landing</a>
                            </div>
                        </div>
                        <hr>

                        @if($landings->count() == NULL || $landings->count() == 0)
                            <p>No hay Landing Pages activas. ¡Comienza a creando una!</p>
                        @else 
                            <div class="card-columns">
                                @foreach($landings as $lp)
                                <div class="card" style="box-shadow: 0px 0px 20px -10px rgba(0,0,0,.3);">
                                    <h5 class="btn-block bg-info text-white text-center py-2" style="font-size: .8em; opacity: .6;">CÓDIGO ÚNICO: {{ $lp->id }}</h5>
                                    <div class="card-body">
                                        <h3 class="card-title">{{ $lp->title }}</h3>
                                        <p class="card-text mb-0">{{ $lp->description }}</p>

                                        <small><a target="_blank" href="{{ route('landing.detalle', $lp->slug) }}">{{ route('landing.detalle', $lp->slug) }}</a></small>
                                    </div>

                                    <div class="card-body">
                                        <h6 class="text-uppercase">REGISTROS DE PROSPECTO</h6>
                                        <div class="d-flex flex-row">
                                            <div class="p-10 p-l-0 b-r">
                                                <h6 class="font-light">Totales</h6><b>{{ number_format($lp->contactos->count()) }}</b></div>
                                            <div class="p-10 b-r">
                                                <h6 class="font-light">Semana</h6><b>{{ number_format($lp->contactos->count()) }}</b>
                                            </div>
                                            <div class="p-10">
                                                <h6 class="font-light">Hoy</h6><b>{{ number_format($lp->contactos->count()) }}</b>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="card-body">
                                        <h6 class="text-uppercase">ESTADÍSTICAS DE LANDING</h6>
                                        <div class="d-flex flex-row">
                                            <div class="p-10 p-l-0 b-r">
                                                <h6 class="font-light">Visitas</h6><b>{{ number_format($lp->view_count) }}</b></div>
                                            <div class="p-10 b-r">
                                                <h6 class="font-light">Registros</h6><b>{{ number_format($lp->register_count) }}</b>
                                            </div>
                                            <div class="p-10">
                                                @php
                                                    if($lp->view_count == 0){
                                                        $conversion_rate = 0;
                                                    }elseif($lp->register_count == 0){
                                                        $conversion_rate = 0;
                                                    }else{
                                                        $conversion_rate = ($lp->register_count / $lp->view_count); 
                                                    }
                                                @endphp

                                                <h6 class="font-light">Conversión</h6><b>{{ number_format("$conversion_rate", 2) }}%</b>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="card-body">
                                        <div class="row">
                                            <div class="col text-center">
                                                <a href="{{ route('landing_pages.edit', $lp->id) }}" class="btn btn-sm btn-outline-primary"><i class="mdi mdi-pencil"></i> Editar</a>

                                                <form method="POST" class="delete" action="{{ route('landing_pages.destroy', $lp->id) }}" style="display: inline-block;">
                                                    <button type="submit" class="btn btn-sm btn-outline-danger"><i class="ti-trash"></i>Eliminar</button>
                                                    {{ csrf_field() }}
                                                    {{ method_field('DELETE') }}
                                                </form>
                                            </div>
                                        </div>
                                    </div>

                                    <a href="javascript:void(0)" data-toggle="modal" data-target="#prospectoModal_{{ $lp->id }}" class="btn btn-block btn-info mb-0"><i class="fa fa-address-book-o"></i> Ver Prospectos</a>
                                    <a href="{{ route('contactos.export', $lp->id) }}" class="btn btn-block btn-success mt-0"><i class="fa fa-file-excel-o"></i> Exportar Prospectos a Excel</a>
                                </div>

                                <div class="modal fade" id="prospectoModal_{{ $lp->id }}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                  <div class="modal-dialog modal-lg" role="document">
                                    <div class="modal-content">
                                      <div class="modal-header">
                                        <h5 class="modal-title" id="exampleModalLabel">Prospectos Registrados</h5>
                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                          <span aria-hidden="true">&times;</span>
                                        </button>
                                      </div>
                                      <div class="modal-body">

                                        @if($lp->contactos->count() == NULL)
                                        <div class="text-center">
                                            <h3>No hay ningún prospecto registrado en esta campaña. Regresa más adelante.</h3>
                                        </div>
                                        @else
                                        <div class="table-responsive">
                                            <table class="table">
                                                <thead>
                                                    <tr>
                                                        <th>#</th>
                                                        <th>Nombre</th>
                                                        <th>Email</th>
                                                        <th>Teléfono</th>
                                                        <th>Estado</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    @foreach($lp->contactos as $ct)
                                                    <tr>
                                                        <td>{{ $ct->id }}</td>
                                                        <td>{{ $ct->name }}</td>
                                                        <td>{{ $ct->email }}</td>
                                                        <td>{{ $ct->phone_cel }}</td>
                                                        <td><span class="label label-success">{{ $ct->status }}</span> </td>
                                                    </tr>
                                                    @endforeach
                                                </tbody>
                                            </table>
                                        </div>
                                        @endif
                                      </div>
                                      <div class="modal-footer">
                                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
                                        <a href="{{ route('contactos.export', $lp->id) }}" class="btn btn-success"><i class="fa fa-file-excel-o"></i> Exportar Prospectos a Excel</a>
                                      </div>
                                    </div>
                                  </div>
                                </div>
                                @endforeach
                            </div>
                        @endif
                    </div>
                </div>
            </div>
        </div>
        
    </div>

    <div class="col-md-3">
        <div class="card">
            <div class="card-body">
                <h3>Estadísticas de Campaña</h3>
                @php

                    $total_views = 0;
                    $total_registries = 0;

                    /* CONTEO DE VISTAS */
                    foreach($landings as $vclp){
                        $total_views += $vclp->view_count;
                    }

                    /* CONTEO DE REGISTROS */
                    foreach($landings as $rclp){
                        $total_registries += $rclp->register_count;
                    }

                    $total_views;
                    $total_registries;

                    if($total_views == 0){
                        $total_conversion = 0;
                    }elseif($total_registries == 0){
                        $total_conversion = 0;
                    }else{
                        $total_conversion = $total_registries / $total_views;
                    }
                    
                @endphp
            </div>
        </div>

        <div class="card">
            <div class="card-body">
                <!-- Row -->
                <div class="row">
                    <div class="col-8"><span class="display-6">{{ number_format($total_views) }} <i class="ti-angle-up font-14 text-success"></i></span>
                        <h6>Visitas Totales</h6></div>
                        <div class="col-4 align-self-center text-right p-l-0">
                            <div id="sparklinedash"></div>
                        </div>
                </div>
            </div>
        </div>

        <div class="card">
            <div class="card-body">
                <!-- Row -->
                <div class="row">
                    <div class="col-8"><span class="display-6">{{ number_format($total_registries) }} <i class="ti-angle-up font-14 text-success"></i></span>
                        <h6>Registros Totales</h6></div>
                        <div class="col-4 align-self-center text-right p-l-0">
                            <div id="sparklinedash2"></div>
                        </div>
                </div>
            </div>
        </div>

        <div class="card">
            <div class="card-body">
                <!-- Row -->
                <div class="row">
                    <div class="col-8"><span class="display-6">{{ number_format("$total_conversion", 2) }}%<i class="ti-angle-up font-14 text-success"></i></span>
                        <h6>Conversión</h6></div>
                        <div class="col-4 align-self-center text-right p-l-0">
                            <div id="sparklinedash4"></div>
                        </div>
                </div>
            </div>
        </div>       
    </div>
</div>

<div class="modal fade" id="landingModal" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <form method="POST" action="{{ route('landing_pages.store') }}" enctype="multipart/form-data">
                {{ csrf_field() }}
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Crear Nueva Landing Page</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="form-group col-md-12">
                            <label>Nombre de Landing Page</label>
                            <input class="form-control" name="title" type="text" placeholder="Título" required="">
                        </div>
                        
                        <div class="form-group col-md-12">
                            <label>Descripción / Detalles</label>
                            <textarea class="form-control" rows="3" name="description" placeholder="" required=""></textarea>
                        </div>

                        <div class="col-md-12 form-group">
                            <label class="control-label">Fechas del Evento</label>
                            <div class="input-daterange input-group" id="date-range">
                                <input type="text" class="form-control" name="start_date" />
                                <span class="input-group-addon bg-info b-0 text-white">a</span>
                                <input type="text" class="form-control" name="end_date" />
                            </div>
                        </div>

                        <div class="col-md-12 form-group">
                            <label class="control-label">Lugar del Evento</label>
                            <input class="form-control" name="place" type="text" placeholder="Ciudad, Estado" required="">
                        </div> 

                        <div class="form-group col-md-6">
                            <label>Plantilla (Estilo)</label>
                            <select class="form-control" name="template_id">
                                <option value="1">Estilo 1</option>
                                <option value="2">Estilo 2</option>
                                <option value="3">Estilo 2 Variación</option>
                                <option value="4">Estilo 3</option>
                            </select>
                        </div>

                        <div class="form-group col-md-6">
                            <label>Curso Asociado:</label>
                            <select class="form-control" name="courses_id">
                                @foreach($courses as $c)
                                    <option value="{{ $c->id }}">{{ $c->title }}</option>
                                @endforeach
                            </select>
                        </div>

                        <!--
                        <div class="form-group col-md-6">
                            <label>Formulario:</label>
                            <input class="form-control" name="has_form" type="text" required="">
                        </div>
                        -->

                        <div class="form-group col-md-12">
                            <label>Nombre de Origen</label>
                            <input class="form-control" name="origin_name" type="text" required="">
                            <small>Esta información es importante para el equipo de ventas y análisis.</small>
                        </div>

                        <div class="col-md-12">                
                            <h3>Opciones de Vínculos</h3>
                            <hr>
                        </div>

                        <div class="form-group col-md-12">
                            <label>Link para Botón de "Más Información"</label>
                            <input class="form-control" name="btn_link" type="text">
                        </div>

                        <div class="col-md-12">                
                            <h3>Opciones SEO</h3>
                            <hr>
                        </div>

                        <div class="form-group col-md-6">
                            <label>Palabras Clave</label>
                            <input class="form-control" name="seo_keywords" type="text" required="">
                            <small>Separa cada palabra con una coma. Opcional</small>
                        </div>

                        <div class="form-group col-md-6">
                            <label>Permisos de Robots</label>
                            <input class="form-control" name="seo_robots" type="text" required="" value="ALL">
                            <small>Opcional</small>
                        </div>

                        <div class="form-group col-md-6">
                            <label>Código de Google Analytics</label>
                            <textarea class="form-control" name="google_analytics_code" required="" rows="4">NN</textarea>
                            <small>IMPORTANTE: Solo copiar y pegar, no realizar ningún formato adicional</small>
                        </div>

                        <div class="form-group col-md-6">
                            <label>Código de Google Ads</label>
                            <textarea class="form-control" name="google_ads_conversion_code" required="" rows="4">NN</textarea>
                            <small>IMPORTANTE: Solo copiar y pegar, no realizar ningún formato adicional</small>
                        </div>

                        <div class="form-group col-md-12">
                            <label>Código de Facebook Pixel</label>
                            <textarea class="form-control" name="facebook_pixel_code" required="" rows="4">NN</textarea>
                            <small>IMPORTANTE: Solo copiar y pegar, no realizar ningún formato adicional</small>
                        </div>

                        <input type="hidden" name="campaign_id" value="{{ $campaign->id }}">
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-link" data-dismiss="modal">Cancelar</button>
                    <button type="submit" class="btn btn-primary">Guardar</button>
                </div>
            </form>
        </div>
    </div>
</div>
@endsection

@section('scripts')
<!-- Date range Plugin JavaScript -->
<script src="{{ asset('back/assets/plugins/bootstrap-datepicker/bootstrap-datepicker.min.js') }}"></script>

<script type="text/javascript">
    jQuery('#date-range').datepicker({
        toggleActive: true,
        language: 'es_MX',
        format: 'yyyy/mm/dd',
        todayHighlight: true
    });

    jQuery('#datepicker-inline').datepicker({
        todayHighlight: true
    });
</script>
@endsection