@extends('back.layouts.app')

@section('stylesheets')

@endsection

@section('content')
<div class="row page-titles">
    <div class="col-md-5 align-self-center">
        <h3 class="text-themecolor">Editar Campaña</h3>
    </div>
    <div class="col-md-7 align-self-center">
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="javascript:void(0)">Inicio</a></li>
            <li class="breadcrumb-item"><a href="javascript:void(0)">Editar Campaña</a></li>
            <li class="breadcrumb-item active">{{ $campaign->title }}</li>
        </ol>
    </div>
</div>

<div class="row">
    <div class="col">
        @include('back.layouts.partials._mensajes')
    </div>
</div>

<div class="row mb-4">
    <div class="col col-md-12">
        <a href="{{ route('campanas.index') }}" class="btn btn-rounded btn-success">Regresar</a>
    </div>
</div>

<div class="row">
    <div class="col-md-12">
        <div class="card">
            <div class="card-body">
                <td>{{ $campaign->id }}</td>
                <td>{{ $campaign->title }}</td>
                <td>{{ $campaign->description }}</td>
                <td>{{ $campaign->created_at }}</td>
            </div>
        </div>
    </div>
</div>
@endsection

@section('scripts')

@endsection