@extends('back.layouts.app')

@section('stylesheets')

@endsection

@section('content')
<div class="row page-titles">
    <div class="col-md-5 align-self-center">
        <h3 class="text-themecolor">Campañas</h3>
    </div>
    <div class="col-md-7 align-self-center">
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="javascript:void(0)">Inicio</a></li>
            <li class="breadcrumb-item active">Campañas</li>
        </ol>
    </div>
</div>

<div class="row">
    <div class="col">
        @include('back.layouts.partials._mensajes')
    </div>
</div>

<div class="row mb-4">
    <div class="col col-md-6">
        <a href="#" data-toggle="modal" data-target="#campañaModal" class="btn btn-rounded btn-success"><i class="fa fa-plus-circle m-r-5"></i> Nueva Campaña</a>
    </div>
    <div class="col col-md-6 text-right">
        <a href="{{ route('contactos.full.export') }}" class="btn btn-rounded btn-outline-info"><i class="fa fa-file-excel-o m-r-5"></i> Exportar todos los Prospectos</a>
    </div>
</div>

<div class="row">
    <div class="col-md-12">
        <div class="card">
            <div class="card-body">
                <h3 class="mb-0">Listado de Campañas</h3>
            </div>
        </div>

        @if($campañas->count() == NULL || $campañas->count() == 0)
            <p>No hay campañas activas. ¡Comienza a creando una!</p>
        @else 

        <div class="card-columns">
        	@foreach($campañas as $cp)
    		<div class="card">
                <div class="card-body">
                    <h3 class="card-title">{{ $cp->title }}</h3>
                    <p class="card-text mb-5">{{ $cp->description }}</p>

                    <a href="{{ route('campanas.show', $cp->id) }}" class="btn btn-primary"><i class="mdi mdi-eye"></i> Ver</a>
                    <a href="{{ route('campanas.edit', $cp->id) }}" class="btn btn-outline-primary"><i class="mdi mdi-pencil"></i> Editar</a>

                    <form method="POST" class="delete" action="{{ route('campanas.destroy', $cp->id) }}" style="display: inline-block;">
                        <button type="submit" class="btn btn-outline-danger"><i class="ti-trash"></i>Eliminar</button>
                        {{ csrf_field() }}
                        {{ method_field('DELETE') }}
                    </form>
                </div>
            </div>
        	@endforeach
        </div>
        @endif
    </div>
</div>

<div class="modal fade" id="campañaModal" tabindex="-1" role="dialog" aria-hidden="true">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<form method="POST" action="{{ route('campanas.store') }}" enctype="multipart/form-data">
	            {{ csrf_field() }}
				<div class="modal-header">
					<h5 class="modal-title" id="exampleModalLabel">Crear Nueva Campaña</h5>
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
				</div>
				<div class="modal-body">
					<div class="row">
						<div class="col-md-12">
							<div class="form-group">
								<label>Nombre de Campaña</label>
								<input class="form-control" name="title" type="text" placeholder="Título de Campaña" required="">
							</div>
							
							<div class="form-group">
								<label>Descripción / Detalles</label>
								<textarea class="form-control" rows="3" name="description" placeholder="" required=""></textarea>
							</div>
						</div>
					</div>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-link" data-dismiss="modal">Cancelar</button>
					<button type="submit" class="btn btn-primary">Guardar</button>
				</div>
			</form>
		</div>
	</div>
</div>

@endsection

@section('scripts')
<script>
    $(".delete").on("submit", function(){
        return confirm("¿Estás seguro de querer borrar la campaña? Si borras este elemento también se eliminarán las páginas y landings relacionadas y no podrán ser recuperadas.");
    });
</script>
@endsection