<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    @if($landing->seo_robots)
    <meta name="robots" content="{{ $landing->seo_robots }}">
    @else
    <meta name="robots" content="INDEX,FOLLOW,NOARCHIVE">
    @endif

    <meta name="description" content="{{ $landing->description }}">
    <meta name="author" content="UBR">

    <title>UBR | {{ $landing->curso->title }}</title>

    <link rel="apple-touch-icon" sizes="180x180" href="/apple-touch-icon.png">
    <link rel="icon" type="image/png" sizes="32x32" href="/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="16x16" href="/favicon-16x16.png">
    <link rel="manifest" href="/site.webmanifest">
    <link rel="mask-icon" href="/safari-pinned-tab.svg" color="#313d66">
    <meta name="msapplication-TileColor" content="#313d66">
    <meta name="theme-color" content="#313d66">

    <meta property="og:type" content="website">
    <meta property="og:image:height" content="266">
    <meta property="og:image:width" content="508">
    <meta property="og:title" content="Universidad de Bienes Raíces">
    <meta property="og:description" content="{{ $landing->description }}">
    <meta property="og:url" content="aprendizaje.universidaddebienesraices.com">
    <meta property="og:image" content="aprendizaje.universidaddebienesraices.com/tile-wide.png">

    <link rel="canonical" href="http://aprendizaje.universidaddebienesraices.com">

    <!-- Bootstrap core CSS -->
    <link href="{{ asset('landing_pages/vendor/bootstrap/css/bootstrap.min.css') }}" rel="stylesheet">


    <link href="https://fonts.googleapis.com/css?family=Libre+Baskerville:400,400i|Roboto:400,700" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="{{ asset('landing_pages/css/custom.css') }}">

    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/ionicons/4.4.8/css/ionicons.min.css" />

    <!-- VIDEO JS -->
    <link href="https://vjs.zencdn.net/7.3.0/video-js.css" rel="stylesheet">
</head>

<body>

<!--
<div class="nav">
<div class="container">
<div class="row">
<div class="col-md-4">

</div>
<div class="col-md-4">
<a href="http://www.universidaddebienesraices.com"><h1 class="logo hide-text">Universidad de Bienes Raices</h1></a>
</div>
<div class="col-md-4">

</div>
</div>
</div>
</div>
-->

<section class="jumbotron">
    <video class="video_play" playsinline="playsinline" autoplay="autoplay" muted="muted" loop="loop">
        <source src="{{ asset('landing_pages/img/video.mp4') }}" type="video/mp4">
        </video>
        <div class="overlay"></div>

        <div class="container">
            <div class="row align-items-center">
                <div class="col-md-7 pr-5">
                    <a href="http://www.universidaddebienesraices.com"><h1 class="logo hide-text mb-3">Universidad de Bienes Raices</h1></a>
                    <h2>Conviértete en un exitoso Empresario e Inversionista en Bienes Raíces.</h2>
                    <p>Aprende a generar dinero en Bienes Raíces con nuestro programa en {{ $landing->curso->title }}.</p>
                </div>
                <div class="col-md-5 pr-5">
                    <div class="card mr-5">
                        <div class="arrow-icon"><i class="ionicons ion-ios-arrow-down"></i></div>
                        <form id="form" action="{{ route('contact.store.landing', $landing->slug) }}" class="mt-4" role="form">
                        	{{ csrf_field() }}
                            <div class="row">
                                <div class="col-md-12">
                                    <h4 class="mb-4" style="margin-top: -25px;">Regístrate a nuestro {{ $landing->curso->title }}</h4>
                                </div>
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <input type="text" name="name" id="name" class="form-control" placeholder="Tu Nombre Completo" autocomplete="name" required autocomplete="name">
                                        <div class="help-block with-errors"></div>
                                    </div>
                                </div>

                                <div class="col-md-12">
                                    <div class="form-group">
                                        <input type="email" name="email" id="email" class="form-control" placeholder="Tu Correo Electrónico" autocomplete="email" required autocomplete="email">
                                        <div class="help-block with-errors"></div>
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <input type="phone" name="phone_cel" id="phone" class="form-control" placeholder="Teléfono" autocomplete="tel" required autocomplete="tel-national">
                                        <div class="help-block with-errors"></div>
                                    </div>
                                </div>

                                <div class="col-md-12">
                                    <hr>
                                    <div class="form-group">
                                        <label>¿Qué buscas conseguir al asistir a este entrenamiento?</label>
                                        <select name="motive" id="motive" class="custom-select form-control">
                                            <option selected>Selecciona una opción</option>
                                            <option value="Me causó curiosidad la temática del entrenamiento">Me causó curiosidad la temática del entrenamiento</option>
                                            <option value="Continuamente asisto a entrenamiento de este tipo">Continuamente asisto a entrenamiento de este tipo</option>
                                            <option value="Ya tengo un negocio y busco mejorar">Ya tengo un negocio y busco mejorar</option>
                                            <option value="Quiero emprender un negocio en los próximos meses">Quiero emprender un negocio en los próximos meses</option>
                                        </select>
                                        <div class="help-block with-errors"></div>
                                    </div>
                                </div>
                            </div>

                            <div class="form-group text-center">
                                <button name="submit" type="submit" class="btn btn-primary">
                                    Enviar tu información
                                </button>
                            </div>
                        </form>

                        <small class="text-center"><a href="" data-toggle="modal" data-target="#privacyModal" class="mb-0">Consulta nuestro aviso de privacidad</a></small>
                    </div>
                </div>
            </div>
        </div>
    </section>


    <div class="date-separator text-center">
        <div class="container">
            <div class="row">
                <i class="ionicons ion-android-calendar"></i>
                <div class="col-md-12">
                    @php
                        $meses = array("Ene","Feb","Mar","Abr","May","Jun","Jul","Ago","Sep","Oct","Nov","Dic");

                        $start_time = strtotime($landing->start_date);
                        $start_day = date('d', $start_time);
                        $start_month = $meses[date('n', $start_time)-1];

                        $end_time = strtotime($landing->end_date);
                        $end_day = date('d', $end_time);
                        $end_month = $meses[date('n', $end_time)-1];

                        $end_year = date('Y', $end_time);
                    @endphp

                    <h3>{{ $start_day . ' ' . $start_month }} al {{ $end_day  . ' ' . $end_month }} del {{ $end_year }}</h3>
                    <p>{{ $landing->place }}</p>
                </div>
                <i class="ionicons ion-location"></i>
            </div>
        </div>
    </div>

    <!-- Main Info -->
    <div class="container">
        <div class="row">
            <div class="col-md-8 offset-md-2">
                <video id="spot-ubr" class="video-js" controls preload="auto" width="100%" height="650"
                  poster="{{ asset('landing_pages/video/cover-spot.png') }}" data-setup="{}">
                    <source src="{{ asset('landing_pages/video/spot.mp4') }}" type='video/mp4'>
                    <p class="vjs-no-js">
                      Para ver este video es necesario tener activado las características Javascript de tu navegador.
                      <a href="https://videojs.com/html5-video-support/" target="_blank">Con soporte para video en HTML5</a>
                    </p>
                </video>
            </div>
            <div class="col-lg-12 text-center">
                <h3 class="pb-5">Un entrenamiento diseñado para que puedas crear tu primer capital o multiplicarlo mediante más de 15 técnicas de inversión en Bienes Raíces</h3>

                <p style="font-size: 1.1em; padding: 7px 0px;">"Aprende más de 15 técnicas comprobadas para ganar y multiplicar tu dinero en Bienes Raíces."</p>
                <p style="font-size: 1.1em; padding: 7px 0px;">"Cerrarás negocios durante el entrenamiento, crearás un equipo de poder y descubrirás nuevas oportunidades"</p>
                <p style="font-size: 1.1em; padding: 7px 0px;">"Recibirás modelos de contratos sin costo extra para ti, que necesitarás para iniciar tu negocio en Bienes Raíces"</p>

                <hr>
                <h4 class="pt-5 pb-5">Aprenderás dónde y cómo invertir tu dinero en Bienes Raíces, para multiplicar tus ganancias</h4>
            </div>
        </div>
    </div>

    <!-- Articulos -->
    <div class="articles-separator">
        <div class="container">
            <div class="row">
                <div class="col-md-8 offset-md-2 text-center">
                    <img src="{{ asset('landing_pages/img/forbes_logo.png') }}" class="mb-4" alt="Logo Forbes">
                    <p class="text-center">"Cuando hablamos de tener tres empresas, una constructora, una promotora de inversión y una empresa de entrenamiento en bienes raíces, nos es difícil pensar en alguien menor de 30 años, por eso Juan Carlos Zamora de 28 años sorprendió con su historia de vida, trayectoria y determinación, para impulsarse a sí mismo y ahora a nuevos emprendedores con la Universidad de Bienes Raíces".</p>

                     <a class="btn-link btn text-right" href="https://www.forbes.com.mx/aprendizajes-de-poder-de-juan-carlos-zamora-premio-hermex-2016/" target="_blank">Lee el articulo aqui</a>
                </div>
            </div>
        </div>
    </div>

    <!-- Parallax -->
    <div class="parallax-separator parallax-1">
        <div class="overlay"></div>
        <div class="container">
            <div class="row">
                <div class="col-md-8 offset-md-2">
                    <h3>En el último entrenamiento, nuestros alumnos cerraron múltiples negocios; generando una cifra de más de <br> <strong>1 Millón de dólares</strong></h3>

                    <a href="{{ $landing->btn_link }}" target="_blank" class="btn btn-secondary">Más Información</a>
                </div>
            </div>
        </div>
    </div>

    <!-- Razones -->
    <div class="reasons-separator">
        <div class="container">
            <div class="row">
                <div class="col-md-8 offset-md-2 text-center">
                    <h3>Tres razones por las que este entrenamiento de Bienes Raíces es único en su tipo</h3>
                </div>
                <div class="col-md-4">
                    <div class="box text-center">
                        <div class="icon"><img src="{{ asset('landing_pages/img/circle-1.png') }}"></div>
                        <div class="box-content">
                            <p>Aprenderás cómo iniciar en el negocio sin capital inicial. <strong>Conocerás más de 15 técnicas probadas para capitalizarte o multiplicar tus ingresos más allá de la simple “compra-venta” o ser el “agente inmobiliario”.</strong></p>
                        </div>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="box text-center">
                        <div class="icon"><img src="{{ asset('landing_pages/img/circle-2.png') }}"></div>
                        <div class="box-content">
                            <p> Obtendrás todo lo que necesitas para empezar tu negocio. <strong>Te daremos modelos de contrato con un valor comercial de $5,000 USD para negocios de préstamos con garantía hipotecaria, asociación en participación, entre otros.</strong></p>
                        </div>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="box text-center">
                        <div class="icon"><img src="{{ asset('landing_pages/img/circle-3.png') }}"></div>
                        <div class="box-content">
                            <p><strong>Te ahorrarás al menos 10 años de carrera profesional. Desarrollamos 3 pilares para que aceleres tus resultados financieros: facilitadores con experiencia como inversionistas, herramientas para iniciar tu negocio y un espacio de Networking.</strong></p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- Galería -->
    <div class="masonry-gallery mb-5">
        <div class="container">
            <div class="row">
                <div class="card-columns">
                    <div class="card">
                        <img class="card-img-top img-fluid" src="{{ asset('landing_pages/img/galeria/1.jpg') }}" alt="Card image cap">
                    </div>
                    <div class="card">
                         <img class="card-img-top img-fluid" src="{{ asset('landing_pages/img/galeria/2.jpg') }}" alt="Card image cap">
                    </div>
                    <div class="card">
                         <img class="card-img-top img-fluid" src="{{ asset('landing_pages/img/galeria/3.jpg') }}" alt="Card image cap">
                    </div>
                    <div class="card">
                         <img class="card-img-top img-fluid" src="{{ asset('landing_pages/img/galeria/4.jpg') }}" alt="Card image cap">
                    </div>
                    <div class="card">
                         <img class="card-img-top img-fluid" src="{{ asset('landing_pages/img/galeria/5.jpg') }}" alt="Card image cap">
                    </div>
                    <div class="card">
                         <img class="card-img-top img-fluid" src="{{ asset('landing_pages/img/galeria/6.jpg') }}" alt="Card image cap">
                    </div>
                    <div class="card">
                         <img class="card-img-top img-fluid" src="{{ asset('landing_pages/img/galeria/7.jpg') }}" alt="Card image cap">
                    </div>
                    <div class="card">
                         <img class="card-img-top img-fluid" src="{{ asset('landing_pages/img/galeria/8.jpg') }}" alt="Card image cap">
                    </div>
                    <div class="card">
                         <img class="card-img-top img-fluid" src="{{ asset('landing_pages/img/galeria/9.jpg') }}" alt="Card image cap">
                    </div>
                    <div class="card">
                         <img class="card-img-top img-fluid" src="{{ asset('landing_pages/img/galeria/10.jpg') }}" alt="Card image cap">
                    </div>
                    <div class="card">
                         <img class="card-img-top img-fluid" src="{{ asset('landing_pages/img/galeria/11.jpg') }}" alt="Card image cap">
                    </div>
                    <div class="card">
                         <img class="card-img-top img-fluid" src="{{ asset('landing_pages/img/galeria/12.jpg') }}" alt="Card image cap">
                    </div>
                    <div class="card">
                         <img class="card-img-top img-fluid" src="{{ asset('landing_pages/img/galeria/13.jpg') }}" alt="Card image cap">
                    </div>
                    <div class="card">
                         <img class="card-img-top img-fluid" src="{{ asset('landing_pages/img/galeria/14.jpg') }}" alt="Card image cap">
                    </div>
                    <div class="card">
                         <img class="card-img-top img-fluid" src="{{ asset('landing_pages/img/galeria/15.jpg') }}" alt="Card image cap">
                    </div>
                    <div class="card">
                         <img class="card-img-top img-fluid" src="{{ asset('landing_pages/img/galeria/16.jpg') }}" alt="Card image cap">
                    </div>
                    <div class="card">
                         <img class="card-img-top img-fluid" src="{{ asset('landing_pages/img/galeria/17.jpg') }}" alt="Card image cap">
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- Separador -->
    <div class="jcz-separator">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="image-jcz">
                        <img src="{{ asset('landing_pages/img/jcz.jpg') }}" alt="Juan Carlos Zamora">
                    </div>

                    <h3 style="margin-top: 100px;">¿Por qué Juan Carlos Zamora es el instructor perfecto para este entrenamiento?</h3>
                </div>

                <div class="col-md-12">
                    <ol>
                        <li>Juan Carlos Zamora es un emprendedor que creció desde cero y sin capital inicial. Creó su propio negocio de Bienes Raíces, hasta facturar actualmente más de 7 cifras en menos de 6 años. </li>
                        <li>No proviene de una familia de empresarios. En sus inicios trabajó como mesero, cajero y encuestador hasta convertirse en agente inmobiliario; y posteriormente en un inversionista en Bienes Raíces</li>
                        <li>Fue nombrado como el Joven Emprendedor del Año, ganador del Premio Hermex 2016 y reconocido por la revista FORBES.</li>
                        <li>Actualmente es dueño de más de 5 empresas en Bienes Raíces; entre ellas una Constructora, una Desarrolladora Inmobiliaria, una Franquicia de Bienes Raíces, un Fondo de Inversión para Bienes Raíces y también es dueño y fundador de la primera Universidad de Bienes Raíces, con la que ha generado casos de éxito a nivel internacional. Estas empresas han crecido exponencialmente gracias a las técnicas que aprenderás en este entrenamiento.</li>
                        <li class="text-center">Es autor bestseller de los libros: "Invierte sin o poco dinero en Bienes Raíces" y "Mentalidad de Empresario".</li>
                    </ol>
                </div>

                <!-- Libros -->
                <div class="col-md-12">
                    <div class="row">
                        <div class="col-md-2 text-center">
                            <a href="https://www.amazon.es/Invierte-poco-Dinero-Bienes-Raices-ebook/dp/B011W01LJU/ref=sr_1_3?s=books&ie=UTF8&qid=1532459028&sr=1-3" target="_blank"><img class="libro" src="{{ asset('landing_pages/img/libro2.jpg') }}" alt="Libro 2"></a>
                        </div>

                        <div class="col-md-2 offset-md-4 text-center">
                            <a href="https://www.amazon.es/Mentalidad-empresario-Desarrolla-habilidades-aseguren-ebook/dp/B07F6DW1WZ/ref=sr_1_6?s=books&ie=UTF8&qid=1532459028&sr=1-6" target="_blank"><img class="libro" src="{{ asset('landing_pages/img/libro1.jpg') }}" alt="Libro 1"></a>
                        </div>
                    
                        <div class="col-md-12">
                            <h3 class="pt-5 pb-5">Te compartirá toda su experiencia y te enseñará cómo cualquier persona con deseos puede replicar sus pasos</h3>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- Separador -->
    <div class="parallax-separator parallax-2">
        <div class="overlay"></div>
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <h3 class="mb-5">¿Quiénes se pueden beneficiar?</h3>
                </div>
                <div class="col-md-4">
                    <p>Empresarios que deseen multiplicar sus fuentes de ingreso con activos de la más baja volatilidad y riesgo en el mercado de Bienes Raíces.</p>
                </div>
                <div class="col-md-4">
                    <p>Empleados y emprendedores que deseen iniciar un negocio en Bienes Raíces con capital inicial.</p>
                </div>
                <div class="col-md-4">
                    <p>Empresarios que deseen crear relaciones duraderas conociendo a otros  empresarios e inversionistas en el entrenamiento</p>
                </div>

                <div class="col-md-12">
                    <div class="text-center">
                        <a href="#form" class="btn btn-secondary">Más Información</a>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- Pie de Página -->
    <footer>
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <a class="mb-0" href="http://www.universidaddebienesraices.com"><h1 class="logo hide-text mb-0">Universidad de Bienes Raíces</h1></a>
                </div>
            </div>
        </div>
    </footer>

    <!-- Modal -->
    <div class="modal fade" id="privacyModal" tabindex="-1" role="dialog" aria-labelledby="privacyModalLabel" aria-hidden="true">
      <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
          <div class="modal-body">
            <div id="inyectaraviso"></div>
          </div>
        </div>
      </div>
    </div>

    <!-- Bootstrap core JavaScript -->
    <script src="{{ asset('landing_pages/vendor/jquery/jquery.min.js') }}"></script>
    <script src="{{ asset('landing_pages/vendor/bootstrap/js/bootstrap.bundle.min.js') }}"></script>

    <script type="text/javascript">
        $(function() {
          $('a[href*="#"]:not([href="#"])').click(function() {
            if (location.pathname.replace(/^\//,'') == this.pathname.replace(/^\//,'') && location.hostname == this.hostname) {
              var target = $(this.hash);
              target = target.length ? target : $('[name=' + this.hash.slice(1) +']');
              if (target.length) {
                $('html, body').animate({
                  scrollTop: target.offset().top
                }, 600);
                return false;
              }
            }
          });
        });
    </script>

    <script> 
    $(function(){
      $("#inyectaraviso").load("{{ asset('landing_pages/aviso.html') }}"); 
    });
    </script> 

    {!! $landing->google_analytics_code !!}
    {!! $landing->google_ads_conversion_code !!}
    {!! $landing->facebook_pixel_code !!}

    <!-- VIDEO JS -->
    <script src="https://vjs.zencdn.net/7.3.0/video.js"></script>
</body>
</html>
