@extends('layouts.front')

@section('seo')
<!-- SEO -->
<meta name="description" content="Te enseñaremos más de 15 técnicas comprobadas para generar riqueza y multiplicarla en los bienes raices, incluso sin tener capital inicial o algún tipo de experiencia.">
<meta name="keywords" content="universidad, bienes, raices, inmobiliaria, tratos inmobiliarios, cursos, capacitaciones, tallers, conferencias, aprende a ganar, dinero, ganar dinero">
<meta name="robots" content="INDEX,FOLLOW,NOARCHIVE">

<!-- OG SEO -->
<meta property="og:image:width" content="1293">
<meta property="og:image:height" content="677">
<meta property="og:title" content="UBR - Universidad de Bienes Raíces">
<meta property="og:description" content="Te enseñaremos más de 15 técnicas comprobadas para generar riqueza y multiplicarla en los bienes raices, incluso sin tener capital inicial o algún tipo de experiencia.">
<meta property="og:url" content="http://www.universidaddebienesraices.com/">
<meta property="og:image" content="http://www.universidaddebienesraices.com/img/logo.jpg">
@endsection

@section('stylesheets')

@endsection

@section('content')
<section class="jumbotron jumbotron-intro mb-0">
	<div class="overlay"></div>
	<div class="container">
		<div class="row align-items-center">
			<div class="col-md-8 offset-md-2">
				<div class="text-center">
					<p class="uppercase-title wow fadeInUp">Entérate de lo último en Bienes Raíces y la Universidad.</p>
					<h3 class="wow fadeInUp">Blog y Noticias</h3>
				</div>
			</div>
		</div>
	</div>
</section>

<section class="blog">
	<div class="container">
		<div class="row">
			@foreach($publicaciones as $publicacion)
			<div class="col-md-4">
				<a href="{{ route('blog.detalle', $publicacion->slug) }}" class="card-blog wow fadeInUp">
					<div class="image-box">
						<img src="{{ asset('img/blog/covers/' . $publicacion->image) }}">

						<div class="image-hover">
							<div class="btn btn-primary">Leer Artículo</div>
						</div>
					</div>

					<div class="info-box">
						<h6><span>Publicado en:</span> {{ $publicacion->categoria->name }}</h6>
						<h2>{{ $publicacion->title }}</h2>

						<p>{{ $publicacion->summary }}</p>

						<div class="media autor-blog">
							@if( $publicacion->autor->image == NULL)
	                        <img class="mr-3 rounded-circle" src="{{ 'https://www.gravatar.com/avatar/' . md5(strtolower(trim($publicacion->autor->email))) . '?d=retro&s=50' }}" alt="{{ $publicacion->autor->name }}">
	                        @else
	                        <img width="50px" class="mr-3 rounded-circle" src="{{ asset('img/usuarios/' . $publicacion->autor->image ) }}" alt="{{ $publicacion->autor->name }}">
	                        @endif                        
                                
							<div class="media-body">
								<p class="mt-0">
									<span>Escrito por:</span><br>
									{{ $publicacion->autor->name }}
								</p>
							</div>
						</div>
					</div>

				</a>
			</div>
			@endforeach		
		</div>

		<div class="row">
			<div class="col-md-12 text-center">
				{{ $publicaciones->links() }}
			</div>
		</div>
	</div>
</section>
@endsection

@section('scripts')

@endsection