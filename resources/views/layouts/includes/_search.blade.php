<div id="search">
    <button type="button" class="close">×</button>
    <form role="search" action="{{ route('search.query') }}">
        <input name="query" type="search" value="" placeholder="Buscar cursos, noticias, eventos o galerías..." />
        <button type="submit" class="btn btn-primary">Buscar</button>
    </form>
</div>

<script type="text/javascript">
$(function () {
    $('a[href="#search"]').on('click', function(event) {
        event.preventDefault();
        $('#search').addClass('open');
        $('#search > form > input[type="search"]').focus();
    });
    
    $('#search, #search button.close').on('click keyup', function(event) {
        if (event.target == this || event.target.className == 'close' || event.keyCode == 27) {
            $(this).removeClass('open');
        }
    });
});
</script>