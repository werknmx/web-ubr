<!-- RESPONSIVE NAVIGATION -->
<div class="sidebar-overlay"></div>
<div id="sidebar-wrapper">
    <div class="brand-header">
        <a href="#" class="close-btn">x</a>
    </div>

    <div class="sidebar-nav">
    	<div class="sidebar-content">
    		<!-- sidebar-header  -->
    		<div class="sidebar-search">
    			<div>
    				<div class="input-group">
    					<input type="text" class="form-control search-menu" placeholder="Estoy buscando...">
    					<div class="input-group-append">
    						<span class="input-group-text">
    							<i class="fa fa-search" aria-hidden="true"></i>
    						</span>
    					</div>
    				</div>
    			</div>
    		</div>
    		<!-- sidebar-search  -->
    		<div class="sidebar-menu">
    			<ul>

    				<li class="sidebar-dropdown">
    					<a href="#">
    						<i class="fa fa-tachometer-alt"></i>
    						<span>Contáctanos</span>
    					</a>
    					<div class="sidebar-submenu">
    						<ul>
    							<li>
    								<a href="#">contacto@universidaddebienesraices.com</a>
    							</li>
    							<li>
    								<a href="#">+52 55 683 30 778</a>
    							</li>

    							<li>
    								<a href="#">Facebook</a>
    							</li>
    							<li>
    								<a href="#">Instagram</a>
    							</li>
    						</ul>
    					</div>
    				</li>

    				<li class="special-button">
    					<a href="{{ url('/plataforma/login') }}">
    						<i class="fa fa-folder"></i>
    						<span>Acceso a Plataforma</span>
    					</a>
    				</li>

    				<li>
    					<a href="{{ route('index') }}">
    						<i class="fa fa-book"></i>
    						<span>Inicio</span>
    					</a>
    				</li>

                    <li>
                        <a href="{{ route('cursos') }}">
                            <i class="fa fa-book"></i>
                            <span>Oferta Educativa <span class="badge badge-pill badge-success">Nuevos</span></span>
                        </a>
                    </li>

    				<li class="sidebar-dropdown">
    					<a href="#">
    						<i class="fa fa-tachometer-alt"></i>
    						<span>Conoce UBR</span>
    						<span class="badge badge-pill badge-warning">Nuevo</span>
    					</a>
    					<div class="sidebar-submenu">
    						<ul>
    							<li>
    								<a href="{{ route('nosotros') }}">Nosotros</a>
    							</li>

    							<li>
    								<a href="{{ route('blog') }}">Blog y Noticias
    									<span class="badge badge-pill badge-success">Nuevos</span>
    								</a>
    							</li>

                                <!--
    							<li>
    								<a href="admisiones">Admisiones y Horarios</a>
    							</li>
    							<li>
    								<a href="maestros">Maestros</a>
    							</li>
                                -->
    							<li>
    								<a href="{{ route('mensaje_director') }}">Mensaje del Director</a>
    							</li>
    						</ul>
    					</div>
    				</li>

    				<li>
    					<a href="{{ route('eventos') }}">
    						<i class="fa fa-book"></i>
    						<span>Eventos</span>
    						<span class="badge badge-pill badge-danger">3</span>
    					</a>
    				</li>

    				<li>
    					<a href="{{ route('testimonios') }}">
    						<i class="fa fa-calendar"></i>
    						<span>Testimonios</span>
    					</a>
    				</li>  

    				<li>
    					<a href="{{ route('galerias') }}">
    						<i class="fa fa-folder"></i>
    						<span>Galerías</span>
    					</a>
    				</li>

                    <!--
    				<li>
    					<a href="libro">
    						<i class="fa fa-folder"></i>
    						<span>Libro</span>
    					</a>
    				</li>-->
    			</ul>
    		</div>
    	</div>
    	<!--
        <ul class="menu-elements list-unstyled">
            <li><a class="close-sidebar" href="">Inicio</a></li>
            <li><a class="close-sidebar" href="">Oferta Educativa</a></li>
            <li><a class="close-sidebar" href="">Características</a></li>
            <li><a class="close-sidebar" href="">Conócenos</a></li>
            <li><a class="close-sidebar" href="javascript:void(0)" data-toggle="modal" data-target="#contactModal">Contáctanos</a></li>
        </ul>
    	-->
    </div>
</div>
<!-- / RESPONSIVE NAVIGATION -->

<!-- ACTIONS HEADER -->
<nav class="responsive-nav">
    <h1 class="logo hide-text">UBR</h1>

    <a href="javascript:void(0)" onclick="toggleMenu()" class="btn-menu">
    <svg version="1.1"
         xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" xmlns:a="http://ns.adobe.com/AdobeSVGViewerExtensions/3.0/"
         x="0px" y="0px" width="65px" height="41px" viewBox="0 0 65 41" style="enable-background:new 0 0 65 41;" xml:space="preserve">
    <defs>
    </defs>
    <path style="fill:#fff;" d="M62.629,9H2.371C1.062,9,0,7.938,0,6.629V2.371C0,1.062,1.062,0,2.371,0l60.257,0
        C63.938,0,65,1.062,65,2.371v4.257C65,7.938,63.938,9,62.629,9z"/>
    <path style="fill:#fff;" d="M62.629,25H2.371C1.062,25,0,23.938,0,22.629v-4.257C0,17.062,1.062,16,2.371,16h60.257
        C63.938,16,65,17.062,65,18.371v4.257C65,23.938,63.938,25,62.629,25z"/>
    <path style="fill:#fff;" d="M62.629,41H2.371C1.062,41,0,39.938,0,38.629v-4.257C0,33.062,1.062,32,2.371,32h60.257
        C63.938,32,65,33.062,65,34.371v4.257C65,39.938,63.938,41,62.629,41z"/>
    </svg>
    </a>
</nav>
<!-- / ACTIONS HEADER -->

<!-- MAIN NAVIGATION -->
<nav class="nav-wrap">
	<div class="pre-nav">
		<div class="container">
			<div class="row align-items-center">
				<div class="col-md-6">
					<ul class="nav mb-0">
						<li class="nav-item"><a class="nav-link" href="{{ route('testimonios') }}">Testimonios</a></li>
						<li class="nav-item"><a class="nav-link" href="{{ route('galerias') }}">Galerías</a></li>
						<!--<li class="nav-item"><a class="nav-link" href="libro">Libro</a></li>-->
						<li class="nav-item"><a class="nav-link" href="{{ route('nosotros') }}">Nosotros</a></li>
					</ul>
				</div>
				<div class="col-md-6 text-right">
					<ul class="nav mb-0">
						<li class="nav-item"><a class="nav-link" href="mailto:contacto@universidaddebienesraices.com"><i class="fas fa-envelope"></i> contacto@universidaddebienesraices.com</a></li>
						<li class="nav-item"><a class="nav-link" href="tel:+525568330778"><i class="fas fa-phone"></i> +52 55 68330778</a></li>

						<li class="nav-item"><a class="nav-link pr-2" href="https://www.facebook.com/universidaddebienesraices" target="_blank"><i class="fab fa-facebook-square"></i></a></li>
						<li class="nav-item"><a class="nav-link pl-2 pr-0" href="https://www.instagram.com/universidaddebienesraices/" target="_blank"><i class="fab fa-instagram"></i></a></li>
					</ul>
				</div>
			</div>
		</div>
	</div>
	<div class="main-nav">
		<div class="container">
			<div class="row align-items-center">
				<div class="col-md-3">
					<a href="{{ route('index') }}"><h1 class="logo hide-text">UBR</h1></a>
				</div>
				<div class="col-md-9 text-right">
					<ul class="nav mb-0">
                        <!--
						<li class="nav-item dropdown">
							<a class="nav-link" href="javascript:void(0)" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Oferta Educativa <i class="fas fa-caret-down caret-nav"></i></a>

							<div class="dropdown-menu" aria-labelledby="dropdownMenuLink">
							    <a class="dropdown-item" href="programas"><i class="fas fa-dot-circle"></i> Programas</a>
							    <a class="dropdown-item" href="{{ route('cursos') }}"><i class="fas fa-dot-circle"></i> Cursos</a>
							</div>
						</li>
                        -->

                        <li class="nav-item"><a class="nav-link" href="{{ route('cursos') }}">Oferta Educativa</a></li>

						<li class="nav-item dropdown">
							<a class="nav-link" href="javascript:void(0)" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"> Conoce UBR <i class="fas fa-caret-down caret-nav"></i></a>

							<div class="dropdown-menu" aria-labelledby="dropdownMenuLink">
							    <a class="dropdown-item" href="{{ route('nosotros') }}"><i class="fas fa-dot-circle"></i> Nosotros</a>
							    <a class="dropdown-item" href="{{ route('blog') }}"><i class="fas fa-dot-circle"></i> Blog y Noticias</a>
							    <div class="dropdown-divider"></div>
							    <!--<a class="dropdown-item" href="admisiones"><i class="fas fa-dot-circle"></i> Admisiones y Horarios</a>-->
							    <!--<a class="dropdown-item" href="maestros"><i class="fas fa-dot-circle"></i> Maestros</a>-->
							    <a class="dropdown-item" href="{{ route('mensaje_director') }}"><i class="fas fa-dot-circle"></i> Mensaje del Director</a>
							</div>
						</li>

						<li class="nav-item">
                            <a class="nav-link" href="{{ route('eventos') }}">Eventos
                                @php
                                    $counter_events = App\Event::where('start_date', '<=', Carbon\Carbon::now()->month(12)->endOfMonth())->where('start_date', '>=', Carbon\Carbon::now()->month(1)->startOfMonth())->count();
                                @endphp
                                <span class="badge badge-danger counter-events">{{ $counter_events }}</span>
                            </a>
                        </li>

                        <li class="nav-item"><a class="nav-link" href="{{ route('contacto') }}">Contacto</a></li>

						<li class="nav-item"><a class="nav-link btn btn-primary" href="{{ url('/plataforma/login') }}">Acceso a Plataforma <i class="fas fa-door-open"></i></a></li>

                        <li class="nav-item"><a class="search-btn custom-icon" href="#search"><i class="fas fa-search"></i></a></li>
					</ul>
				</div>
			</div>
		</div>
	</div>
</nav>
<!-- /MAIN NAVIGATION -->
