<section class="medios">
	<div class="container">
		<div class="col-md-12">
			<h2 class="big-title wow fadeInUp">Opiniones de los medios</h2>
		</div>

		<div class="col-md-12 text-center">
			<ul class="list-inline mb-0">
				<li class="list-inline-item wow fadeInUp"><a href="https://www.forbes.com.mx/aprendizajes-de-poder-de-juan-carlos-zamora-premio-hermex-2016/#gs.2ILvoJs" target="_blank"><img class="img-fluid" src="{{ asset('img/forbes.png') }}"></a></li>
				<li class="list-inline-item wow fadeInUp"><a href="https://www.youtube.com/watch?v=WYG9JlJAX8s" target="_blank"><img class="img-fluid" src="{{ asset('img/canaco.png') }}"></a></li>
			</ul>
		</div>
	</div>
</section>

<footer>
	<div class="container">
		<div class="row">
			<div class="col-md-4">
				<h1 class="logo hide-text">UBR</h1>
			</div>
			<div class="col-md-8">
				<div class="row">
					<div class="col-md-4">
						<h5>Explora</h5>

						<ul class="list-unstyled">
							<li><a href="{{ route('index') }}">Inicio</a></li>
				            <li><a href="{{ route('cursos') }}">Cursos</a></li>
				            <li><a href="{{ route('eventos') }}">Eventos</a></li>
				            <li><a href="{{ route('blog') }}">Blog</a></li>
						</ul>
					</div>
					<div class="col-md-4">
						<h5>UBR a Fondo</h5>

						<ul class="list-unstyled">
							<li><a href="{{ route('testimonios') }}">Testimonios</a></li>
							<li><a href="{{ route('galerias') }}">Galerias</a></li>
							<!--<li><a href="libro">Libro</a></li>-->
							<li><a href="{{ route('nosotros') }}">Nosotros</a></li>
						</ul>
					</div>
					<div class="col-md-4">
						<h5>Los últimos tres</h5>

						<ul class="list-unstyled">
							<li><a href="{{ route('index') }}">Inicio</a></li>
							<!--<li><a href="programas">Programas</a></li>-->
							<li><a href="{{ route('cursos') }}">Cursos</a></li>
							<li><a href="{{ route('eventos') }}">Eventos</a></li>
							<li><a href="{{ route('blog') }}">Blog</a></li>
						</ul>
					</div>
				</div>
			</div>
		</div>
	</div>
</footer>

<div class="post-footer">
	<div class="container">
		<div class="row">
			<div class="col-12 col-md-6">
				<p class="mb-0">Universidad de Bienes Raices 2018 Todos los Derechos Reservados <i class="fas fa-info-circle" data-toggle="tooltip" title="Página cargada en {{ (microtime(true) - LARAVEL_START) }} segundos."></i></p>
			</div>
			<div class="col-12 col-md-6 text-right">
				<ul class="mb-0 list-inline">
					<li class="list-inline-item"><a href="{{ route('privacidad') }}">Aviso de Privacidad</a></li>
					<li class="list-inline-item"><a href="{{ route('terminos') }}">Términos y Condiciones</a></li>
				</ul>
			</div>
		</div>
	</div>
</div>

<!-- /////////////// BOTÓN DE WHATSAPP ///////////// -->
<a data-toggle="tooltip" title="¡Envíanos un Mensaje por Whatsapp!" href="https://api.whatsapp.com/send?phone=+5215528923763&amp;text=
							¡Buen día!%20%20%20
							Quiero información sobre su servicio." target="_blank" class="btn-float btn-whatsapp"><i class="fab fa-whatsapp"></i></a>
<!-- /////////////////////////////////////////////// -->


<!-- Modal -->
<div class="modal fade" id="privacyModal" tabindex="-1" role="dialog" aria-labelledby="privacyModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <div id="inyectaraviso"></div>
      </div>
    </div>
  </div>
</div>