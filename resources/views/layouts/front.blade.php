<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
	<head>
		<meta charset="utf-8">
		<title>UBR | Universidad de Bienes Raíces</title>

		<meta name="description" content="">
	    <meta name="viewport" content="width=device-width, initial-scale=1">

		<link rel="apple-touch-icon" sizes="180x180" href="/apple-touch-icon.png">
		<link rel="icon" type="image/png" sizes="32x32" href="/favicon-32x32.png">
		<link rel="icon" type="image/png" sizes="16x16" href="/favicon-16x16.png">
		<link rel="manifest" href="/site.webmanifest">
		<link rel="mask-icon" href="/safari-pinned-tab.svg" color="#1d2b55">
		<meta name="msapplication-TileColor" content="#1d2b55">
		<meta name="theme-color" content="#1d2b55">

		@yield('seo')
	    
		<!-- BOOTSTRAP 4 -->
		<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">

		<!-- ANIMATE CCS -->
	    <link rel="stylesheet" type="text/css" href="{{ asset('css/libs/animate.css') }}">

	    <!-- FONT AWESOME Library -->
		<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.2/css/all.css" integrity="sha384-fnmOCqbTlWIlj8LyTjo7mOUStjsKC4pOpQbqyi7RrhN7udi9RwhKkMHpvLbHG9Sr" crossorigin="anonymous">

		<!-- Swiper Touch CSS -->
	    <link rel="stylesheet" type="text/css" href="{{ asset('css/libs/swiper.min.css') }}">

	    @yield('stylesheets')

		<!-- CUSTOM -->
		<link rel="stylesheet" type="text/css" href="{{ asset('css/custom.css') }}">
		<link rel="stylesheet" type="text/css" href="{{ asset('css/sidebar.css') }}">
	</head>
	<body>
		@include('layouts.nav')

		@yield('content')

		@include('layouts.footer')

		<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
		<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
		<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>

		<!-- SIDEBAR -->
		<script type="text/javascript" src="{{ asset('js/sidebar.js') }}"></script>

		<script type="text/javascript">
		    // Hide Header on on scroll down
		    var didScroll;
		    var lastScrollTop = 0;
		    var delta = 5;
		    var navbarHeight = $('nav').outerHeight();

		    $(window).scroll(function(event){
		        didScroll = true;
		    });

		    setInterval(function() {
		        if (didScroll) {
		            hasScrolled();
		            didScroll = false;
		        }
		    }, 50);

		    function hasScrolled() {
		        var st = $(this).scrollTop();
		        
		        // Make sure they scroll more than delta
		        if(Math.abs(lastScrollTop - st) <= delta)
		            return;
		        
		        // If they scrolled down and are past the navbar, add class .nav-up.
		        // This is necessary so you never see what is "behind" the navbar.
		        if (st > lastScrollTop && st > navbarHeight){
		            // Scroll Down
		            $('nav').removeClass('nav-down').addClass('nav-up');
		        } else {
		            // Scroll Up
		            if(st + $(window).height() < $(document).height()) {
		                $('nav').removeClass('nav-up').addClass('nav-down');
		            }
		        }
		        
		        lastScrollTop = st;
		    }

		    $(window).scroll(function() {    
		        var scroll = $(window).scrollTop();

		        if (scroll >= 20) {
		            $("nav").addClass("darkback");
		        }else{
		            $("nav").removeClass("darkback");
		        }
		    });

		    $(document).ready(function(){
		      $('[data-toggle="tooltip"]').tooltip();
		    });
		</script>

		<!-- Swiper JS -->
		<script src="{{ asset('js/libs/swiper.min.js') }}"></script>

		<!-- Initialize Swiper -->
		<script>
		var swiper = new Swiper('.swiper-container', {
		  slidesPerView: '1',
		  spaceBetween: 30,
		  pagination: {
		    el: '.swiper-pagination',
		    clickable: true,
		  },

		  breakpoints: {
		    // when window width is <= 320px
		    320: {
		      slidesPerView: 1,
		      spaceBetween: 10
		    },
		    // when window width is <= 480px
		    480: {
		      slidesPerView: 1,
		      spaceBetween: 20
		    },
		    // when window width is <= 640px
		    640: {
		      slidesPerView: 2,
		      spaceBetween: 30
		    }
		  }
		});
		</script>

		<script type="text/javascript" src="{{ asset('contact-form/mail.js') }}"></script>

		<!-- WOW JS -->
		<script type="text/javascript" src="{{ asset('js/libs/wow.min.js') }}"></script>
		<script type="text/javascript">
			new WOW().init();
		</script>

		@yield('scripts')

		@include('layouts.includes._search')
	</body>
</html>