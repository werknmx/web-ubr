@extends('layouts.front')

@section('seo')
<!-- SEO -->
<meta name="description" content="Te enseñaremos más de 15 técnicas comprobadas para generar riqueza y multiplicarla en los bienes raices, incluso sin tener capital inicial o algún tipo de experiencia.">
<meta name="keywords" content="universidad, bienes, raices, inmobiliaria, tratos inmobiliarios, cursos, capacitaciones, tallers, conferencias, aprende a ganar, dinero, ganar dinero">
<meta name="robots" content="INDEX,FOLLOW,NOARCHIVE">

<!-- OG SEO -->
<meta property="og:image:width" content="1293">
<meta property="og:image:height" content="677">
<meta property="og:title" content="UBR - Universidad de Bienes Raíces">
<meta property="og:description" content="Te enseñaremos más de 15 técnicas comprobadas para generar riqueza y multiplicarla en los bienes raices, incluso sin tener capital inicial o algún tipo de experiencia.">
<meta property="og:url" content="http://www.universidaddebienesraices.com/">
<meta property="og:image" content="http://www.universidaddebienesraices.com/img/logo.jpg">
@endsection

@section('stylesheets')
<!-- VIDEO JS -->
<link href="https://vjs.zencdn.net/7.3.0/video-js.css" rel="stylesheet">

<!-- Popup CSS -->
<link href="{{ asset('back/assets/plugins/Magnific-Popup-master/dist/magnific-popup.css') }}" rel="stylesheet">
@endsection

@section('content')
<section class="jumbotron jumbotron-main">
	<div class="overlay hidden-sm"></div>
	<div class="container hidden-sm">
		<div class="row align-items-center">
			<div class="col-md-8">
				<div class="jumbotron-padding">
					<p class="uppercase-title wow fadeInLeft">Formamos Empresarios e inversionistas en bienes raíces</p>
					<h3 class="wow fadeInLeft">Estudia con nosotros y adquiere conocimientos a detalle del mundo de los Bienes Raíces.</h3>
					<div class="line-divider wow fadeInLeft"></div>

					<p class="content wow fadeInLeft">Nuestros programas apuestan por tu profesionalización ya que te brindamos los conocimientos legales, fiscales y de marketing que te permitirán salir con las herramientas listas para aplicar.</p>
				</div>
			</div>
			<div class="col-md-4">
				<a href="javascript:void(0)" data-toggle="modal" data-target="#modalVideo"  class="text-center video-padding wow fadeInRight">
					<!--<h6>Dale Play</h6>-->
					<i class="far fa-play-circle"></i>
					<p>Conoce la universidad</p>
				</a>
			</div>
		</div>
	</div>
</section>

<!-- Modal -->
<div class="modal fade" id="modalVideo" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>

            <div class="modal-body p-0 mb-0">
                <video id="spot-ubr" class="video-js" controls preload="auto" poster="./video/cover-spot.png" data-setup="{}">
                    <source src="./video/bienvenida.mp4" type='video/mp4'>
                    <p class="vjs-no-js">
                      Para ver este video es necesario tener activado las características Javascript de tu navegador.
                      <a href="https://videojs.com/html5-video-support/" target="_blank">Con soporte para video en HTML5</a>
                    </p>
                </video>
            </div>
        </div>
    </div>
</div>

<div class="good-links">
	<div class="container">
		<div class="row">
            <div class="col-md-12 hidden-md">
                <div class="ubr-box">
                    <h3 class="ubr-box-top">Diplomados y Cursos</h3>
                    <div class="ubr-box-content">
                        <a href="{{ route('cursos') }}" class="btn btn-primary">Prescencial</a>
                        <a href="{{ route('cursos') }}" class="btn btn-primary">En Línea</a>
                    </div>
                </div>
            </div>

            <div class="col-md-12 hidden-md">
                <h3 class="title-blogs wow fadeInUp">Noticias / Blog</h3>
            </div>

			<div class="col-md-6">
				<div class="row">
					@foreach($publicaciones as $publicacion)
					<div class="col-md-6">
						<div class="blog-box wow fadeInUp">
							<div class="overlay"></div>

							<img class="img-position" src="{{ asset('img/blog/covers/' . $publicacion->image) }}">

							<div class="blog-box-icons">
								<span class="icon"><i class="fas fa-pencil-alt"></i></span>

								<span class="badge">Desde el Blog</span>
							</div>
							<div class="blog-box-content">
								<h4>{{ substr($publicacion->title, 0, 100)}} {{ strlen($publicacion->title) > 100 ? "[ ... ]" : "" }}</h4>
								<a href="{{ route('blog.detalle', $publicacion->slug) }}">Leer Artículo >></a>
							</div>
						</div>
					</div>
					@endforeach
				</div>
			</div>
			<div class="col-md-6 hidden-sm">
				<div class="ubr-box wow fadeInUp">
					<h3 class="ubr-box-top">Conoce nuestros cursos disponibles</h3>
					<div class="ubr-box-content">
						<h4>Se parte de la Universidad de Bienes Raíces</h4>
						<a href="{{ route('contacto') }}" class="btn btn-primary">Contáctanos</a>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

<section class="nosotros">
	<div class="container">
		<div class="row">
			<div class="col-md-4">
				<p class="special-text wow fadeInUp">¡Transforma las oportunidades en tu mejor fuente de ingresos!</p>
				<p class="special-text wow fadeInUp">Conviértete en un profesional en el negocio de los bienes raíces</p>
			</div>
			<div class="col-md-4 pr-5">
				<h4 class="wow fadeInUp">Tu mejor inversión</h4>
				<p class="pr-3 wow fadeInUp">Bienvenid@ a la Universidad de Bienes Raíces, donde formamos empresarios e inversionistas en el campo inmobiliario.</p>

				<p class="pr-3 wow fadeInUp">Nuestra especialidad es enseñarte de voz de los expertos las claves y estrategias del mundo de los negocios en bienes raíces.</p>

				<p class="pr-3 wow fadeInUp">Contribuimos a forjar emprendedores visionarios y líderes que se atrevan a hacer realidad hoy todos sus sueños.</p>
			</div>
			<div class="col-md-4">
				<h4 class="wow fadeInUp">Categorías de Interés</h4>
				<ul class="list-unstyled">
					<li class="wow fadeInUp"><i class="far fa-check-circle"></i> Educación</li>
					<li class="wow fadeInUp"><i class="far fa-check-circle"></i> Bienes Ráices</li>
					<li class="wow fadeInUp"><i class="far fa-check-circle"></i> Negocio</li>
					<li class="wow fadeInUp"><i class="far fa-check-circle"></i> Desarrollos Inmobiliarios</li>
					<li class="wow fadeInUp"><i class="far fa-check-circle"></i> Finanzas</li>
					<li class="wow fadeInUp"><i class="far fa-check-circle"></i> Emprendedores</li>
					<li class="wow fadeInUp"><i class="far fa-check-circle"></i> Libertad Financiera</li>
				</ul>
			</div>
		</div>
	</div>

	<a href="{{ route('cursos') }}" class="btn btn-primary">Conoce nuestra oferta Educativa</a>
</section>

<section class="gallery">
	<div class="row no-gutters">
		<div class="col-12 wow fadeInUp">
            <div class="swiper-container swiper-gallery">
                <div class="swiper-wrapper">
                    @foreach($galerias as $galeria)
                    <div class="swiper-slide">
                        <div class="card card-galeria card-galeria-full">
                            <a class="image-popup-vertical-fit" href="{{ asset('img/galerias/covers/' . $galeria->cover_image) }}"> <img src="{{ asset('img/galerias/covers/' . $galeria->cover_image) }}" alt="{{ $galeria->title }}" /> 
                            </a>

                            @if($galeria->imagenes->count())
                                <a href="{{ route('galeria.detalle', $galeria->slug) }}" class="more-images">Ver todas las imágenes</a>
                            @endif

                            <div class="card-galeria-content">
                                <h4>{{ $galeria->title }}</h4>
                                <p>{{ $galeria->body }}</p>
                            </div>
                        </div>
                        
                    </div>
                    @endforeach
                </div>
            </div>
		</div>
	</div>
</section>

<section class="estadisticas">
	<div class="overlay"></div>
	<div class="container-fluid">
		<div class="row text-center">
			<div class="col-md-6">
				<h4>+ 5,000</h4>
				<h6>Alumnos</h6>
			</div>
			<div class="col-md-6">
				<h4>12 Países</h4>
				<h6>Presencia Internacional</h6>
			</div>
		</div>
	</div>
</section>

<section class="testimonios">
	<div class="container">
		<div class="col-md-12">
			<h2 class="big-title wow fadeInUp">Testimonios de nuestro alumnado</h2>
		</div>

        <div class="row">
            <div class="col-md-6">
                <div class="card wow fadeInUp">
                    <iframe width="100%" height="315" src="https://www.youtube.com/embed/yMe7qknB27U" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

                    <div class="card-body">
                        <h3>Alicia de León</h3>
                        <p>Entrenamiento Intensivo CDMX</p>
                    </div>
                </div>
            </div>
            <div class="col-md-6">
                <div class="card wow fadeInUp">
                    <iframe width="100%" height="315" src="https://www.youtube.com/embed/JlVHUcEQkBU" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

                    <div class="card-body">
                        <h3>Ricardo Gómez</h3>
                        <p>Entrenamiento Intensivo CDMX</p>
                    </div>
                </div>
            </div>

            <div class="col-md-6">
                <div class="card wow fadeInUp">
                    <iframe width="100%" height="315" src="https://www.youtube.com/embed/PcVinHTns3M" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

                    <div class="card-body">
                        <h3>Comentario</h3>
                        <p>Entrenamiento Intensivo CDMX</p>
                    </div>
                </div>
            </div>
            <div class="col-md-6">
                <div class="card wow fadeInUp">
                    <iframe width="100%" height="315" src="https://www.youtube.com/embed/9zhmPYNXj9k" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

                    <div class="card-body">
                        <h3>Comentario</h3>
                        <p>Entrenamiento Intensivo CDMX</p>
                    </div>
                </div>
            </div>
        </div>

        <!--
		<div class="swiper-container wow fadeInUp">
            <div class="swiper-wrapper">
                <div class="swiper-slide">
                    <div class="testimonial-box">
                        <div class="testimonial-text">
                            <h5>David</h5>
                            <p>Juan nos dio mucha motivación, es perder el miedo a quitarse el no puedo, de ir siempre hacia adelante. Mi negocio lo hice sin capital, apliqué la técnica de compra-venta y hoy tengo un negocio millonario con más de 400% de utilidad.</p>
                        </div>

                        <div class="testimonial-image">
                            <img src="{{ asset('img/testimonial-image-1.png') }}">
                        </div>

                        <span class="icon-custom icon-quote-start"></span>
                        <span class="icon-custom icon-quote-end"></span>
                    </div>
                </div>

                <div class="swiper-slide">
                    <div class="testimonial-box">
                        <div class="testimonial-image">
                            <img src="{{ asset('img/testimonial-image-2.png') }}">
                        </div>

                        <div class="testimonial-text">
                            <h5>Joselyn</h5>
                            <p>Tengo 5 años en Bienes Raíces, pero Juan me ayudó a aprender muchas técnicas que antes no aplicaba. Estoy muy agradecida porque me llevo un gran conocimiento y conocí mucha gente con Universidad de Bienes Raíces</p>
                        </div>

                        <span class="icon-custom icon-quote-start"></span>
                        <span class="icon-custom icon-quote-end"></span>
                    </div>
                </div>

                <div class="swiper-slide">
                    <div class="testimonial-box">
                        <div class="testimonial-image">
                            <img src="{{ asset('img/testimonial-image-3.png') }}">
                        </div>

                        <div class="testimonial-text">
                            <h5>Jose Luís Lumbreas</h5>
                            <p>Poniendo en práctica las técnicas aprendidas logre, mejorar mis ventas sin dinero, una de ellas conseguir una propiedad en obra negra sin dinero.</p>
                        </div>

                        <span class="icon-custom icon-quote-start"></span>
                        <span class="icon-custom icon-quote-end"></span>
                    </div>
                </div>

                <div class="swiper-slide">
                    <div class="testimonial-box">
                        <div class="testimonial-text">
                            <h5>Juan Martín</h5>
                            <p>Aprendí distintas técnicas para optimizar mi dinero y asi generar más ingreso a largo plazo. Recomiendo ampliamente este curso  por las técnicas exclusivas que manejan aquí, saldrán con mucho conocimiento para poder aplicarlo.</p>
                        </div>

                        <div class="testimonial-image">
                            <img src="{{ asset('img/testimonial-image-3.png') }}">
                        </div>

                        <span class="icon-custom icon-quote-start"></span>
                        <span class="icon-custom icon-quote-end"></span>
                    </div>
                </div>

                <div class="swiper-slide">
                    <div class="testimonial-box">
                        <div class="testimonial-image">
                            <img src="{{ asset('img/testimonial-image-3.png') }}">
                        </div>

                        <div class="testimonial-text">
                            <h5>Andrés Zarazuza</h5>
                            <p>Asistí al seminario de inversiones de Bienes Raíces por la inquietud de cómo funcionaban, una vez que tome el curso me di cuenta que hay muchas herramientas para generar dinero. Lo recomiendo ampliamente porque si te cambia mucho tu percepción y tu manera de ver las cosas.</p>
                        </div>

                        <span class="icon-custom icon-quote-start"></span>
                        <span class="icon-custom icon-quote-end"></span>
                    </div>
                </div>

                <div class="swiper-slide">
                    <div class="testimonial-box">
                        <div class="testimonial-text">
                            <h5>Maricarmen Peredo</h5>
                            <p>Juan Carlos es un gran maestro. El entrenamiento te lo recomiendo a ti que eres un padre de familia, para que empieces a preparar a tu hijo ya que el mundo de afuera necesita gente triunfadora, gente con éxito, y Juan Carlos es la persona idónea para enseñarte el conocimiento y poder de cambiar tu vida no solo económicamente, sino en todas las demás áreas de tu vida.</p>
                        </div>

                        <div class="testimonial-image">
                            <img src="{{ asset('img/testimonial-image-1.png') }}">
                        </div>

                        <span class="icon-custom icon-quote-start"></span>
                        <span class="icon-custom icon-quote-end"></span>
                    </div>
                </div>

                <div class="swiper-slide">
                    <div class="testimonial-box">
                        <div class="testimonial-image">
                            <img src="{{ asset('img/testimonial-image-2.png') }}">
                        </div>

                        <div class="testimonial-text">
                            <h5>Mario Bueno</h5>
                            <p>Definitivamente es un curso bastante idóneo para todas aquellas personas que quieran incursionar en Bienes Raíces. Actualmente  la manera más efectiva para hacer negocios es educándonos, y en esta universidad maximizar tus conocimientos.</p>
                        </div>

                        <span class="icon-custom icon-quote-start"></span>
                        <span class="icon-custom icon-quote-end"></span>
                    </div>
                </div>

                <div class="swiper-slide">
                    <div class="testimonial-box">
                        <div class="testimonial-image">
                            <img src="{{ asset('img/testimonial-image-3.png') }}">
                        </div>

                        <div class="testimonial-text">
                            <h5>Nury, Bogotá</h5>
                            <p>Estaba un poco cerrada a venir, porque yo decía que conocía de todo de Bienes Raíces, y no me imagine que me pudiera aportar tanto como me ha aportado en este momento. He tenido muchas sorpresas, he encontrado que aquí se desarrollan muchísimas oportunidades que nunca vemos, porque no tenemos captación de oportunidades y capital racional. No sabía nada de esto, y es sumamente importante para mis negocios. Los felicito por esta Universidad. Los invito a invertir en este entrenamiento, y en ustedes mismo.</p>
                        </div>

                        <span class="icon-custom icon-quote-start"></span>
                        <span class="icon-custom icon-quote-end"></span>
                    </div>
                </div>
            </div>

            <div class="swiper-pagination"></div>
        </div>
        -->
	</div>
</section>
@endsection

@section('scripts')

<!-- VIDEO JS -->
<script src="https://vjs.zencdn.net/7.3.0/video.js"></script>

<script src="{{ asset('back/assets/plugins/Magnific-Popup-master/dist/jquery.magnific-popup.min.js') }}"></script>
<script src="{{ asset('back/assets/plugins/Magnific-Popup-master/dist/jquery.magnific-popup-init.js') }}"></script>

<!-- Swiper JS -->
<script src="{{ asset('js/libs/swiper.min.js') }}"></script>

<!-- Initialize Swiper -->
<script>
var swiper = new Swiper('.swiper-gallery', {
  slidesPerView: '3',
  spaceBetween: 0,
  loop: true,
  pagination: {
    el: '.swiper-pagination',
    clickable: true,
  },

  breakpoints: {
    // when window width is <= 320px
    320: {
      slidesPerView: 1,
      spaceBetween: 10
    },
    // when window width is <= 480px
    480: {
      slidesPerView: 1,
      spaceBetween: 20
    },
    // when window width is <= 640px
    640: {
      slidesPerView: 2,
      spaceBetween: 30
    }
  }
});
</script>
@endsection