<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

   	@if(!empty($landing))
	    @if($landing->seo_robots)
	    <meta name="robots" content="{{ $landing->seo_robots }}">
	    @else
	    <meta name="robots" content="INDEX,FOLLOW,NOARCHIVE">
	    @endif

	    <meta name="description" content="{{ $landing->description }}">
	    <meta name="author" content="UBR">

	    <title>UBR | {{ $landing->curso->title }}</title>
    @else
    	<meta name="description" content="Gracias por tu contacto. Hablaremos pronto.">
	    <meta name="author" content="UBR">

    	<title>UBR | Gracias</title>
    @endif

    <link rel="apple-touch-icon" sizes="180x180" href="/apple-touch-icon.png">
    <link rel="icon" type="image/png" sizes="32x32" href="/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="16x16" href="/favicon-16x16.png">
    <link rel="manifest" href="/site.webmanifest">
    <link rel="mask-icon" href="/safari-pinned-tab.svg" color="#313d66">
    <meta name="msapplication-TileColor" content="#313d66">
    <meta name="theme-color" content="#313d66">

    <meta property="og:type" content="website">
    <meta property="og:image:height" content="266">
    <meta property="og:image:width" content="508">
    <meta property="og:title" content="Universidad de Bienes Raíces">
    @if(!empty($landing))
    <meta property="og:description" content="{{ $landing->description }}">
    @else
    <meta property="og:description" content="Gracias por tu contacto. Hablaremos pronto.">
    @endif
    <meta property="og:url" content="universidaddebienesraices.com">

    <meta property="og:image" content="universidaddebienesraices.com/tile-wide.png">

    <!-- Bootstrap core CSS -->
    <link href="{{ asset('landing_pages/vendor/bootstrap/css/bootstrap.min.css') }}" rel="stylesheet">


    <link href="https://fonts.googleapis.com/css?family=Libre+Baskerville:400,400i|Roboto:400,700" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="{{ asset('landing_pages/css/custom.css') }}">
</head>

<body>
	<style type="text/css">
		.jumbotron{
			height: 100vh;
			overflow-x: scroll;
		}

		.jumbotron p{
			font-size: 1em;
			margin-bottom: 50px;
		}

		.btn-primary{
			background: #fff;
			color: #313d66;
			border:1px solid transparent;

			padding: 10px 30px;
			font-size: .8em;
			text-transform: uppercase;
			margin-bottom: 30px;
		}

		.btn-outline-primary{
			color: #fff;
			border-color: #fff;
			padding: 10px 30px;
			font-size: .8em;
			text-transform: uppercase;
			margin-bottom: 30px;

		}

		.btn-primary:hover,
		.btn-outline-primary:hover{
			background: #313d66;
			color: #fff;
			border: 1px solid transparent;
		}
	</style>

	<section class="jumbotron">
	    <div class="overlay"></div>

	    <div class="container">
	        <div class="row align-items-center">
	            <div class="col-md-6 offset-md-3 text-center">
	                <a href="http://www.universidaddebienesraices.com"><h1 class="logo hide-text mb-3">Universidad de Bienes Raices</h1></a>
	                <h2>Registro Exitoso</h2>
	                @if(!empty($landing))
	                <p>Gracias por registrarte a nuestro programa en {{ $landing->curso->title }}. Nuestro equipo de ventas contactará contigo lo más pronto posible.</p>
	                <a href="{{ route('landing.detalle', $landing->slug) }}" class="btn btn-primary">Regresar</a>
	                <a href="{{ route('index') }}" class="btn btn-outline-primary">Visita el Sitio de UBR</a>
	                @else
	                <p>Gracias por dejar tus datos. Nuestro equipo de ventas contactará contigo lo más pronto posible.</p>
	                <a href="{{ route('index') }}" class="btn btn-outline-primary">Regresar al Sitio de UBR</a>
	                @endif
	                
	            </div>
	        </div>
	    </div>
	</section>

    <!-- Bootstrap core JavaScript -->
    <script src="{{ asset('landing_pages/vendor/jquery/jquery.min.js') }}"></script>
    <script src="{{ asset('landing_pages/vendor/bootstrap/js/bootstrap.bundle.min.js') }}"></script>
</body>
</html>
