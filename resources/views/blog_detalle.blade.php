@extends('layouts.front')

@section('seo')
<!-- SEO -->
<meta name="description" content="{{ $publicacion->summary }}">
<meta name="keywords" content="universidad, bienes, raices, inmobiliaria, tratos inmobiliarios, cursos, capacitaciones, tallers, conferencias, aprende a ganar, dinero, ganar dinero">
<meta name="robots" content="INDEX,FOLLOW,NOARCHIVE">

<!-- OG SEO -->
<meta property="og:type" content="website">
<meta property="og:image" content="{{ asset('img/blog/covers/' . $publicacion->image) }}">
<meta property="og:image:width" content="1280">
<meta property="og:image:height" content="720">
<meta property="og:title" content="{{ $publicacion->title }}">
<meta property="og:description" content="{{ $publicacion->summary }}">
<meta property="og:url" content="{{ route('blog.detalle', $publicacion->slug) }}">
@endsection

@section('stylesheets')
<style type="text/css">
	.jumbotron-blog .overlay{
		background: rgba(38,47,84,.7);
	}

	.jumbotron-blog{
		padding-top: 250px;
	    padding-bottom: 170px !important;

	    background: url('{{ asset('img/blog/covers/' . $publicacion->image) }}') !important;
		background-size: cover !important;
		background-attachment: fixed !important;
		background-position: center center !important;
		background-repeat: no-repeat !important;
	}
</style>
@endsection

@section('content')
<section class="jumbotron jumbotron-blog mb-0">
	<div class="overlay"></div>
	<div class="container">
		<div class="row align-items-center">
			<div class="col-md-8 offset-md-2">
				<div class="text-center">
					<h3 class="wow fadeInUp mb-3">{{ $publicacion->title }}</h3>
					<p class="wow fadeInUp mb-5">{{ $publicacion->summary }}</p>

					<div class="autor-center-wrap">
						<div class="media autor-blog wow fadeInUp">
							@if( $publicacion->autor->image == NULL)
	                        <img class="mr-3 rounded-circle" src="{{ 'https://www.gravatar.com/avatar/' . md5(strtolower(trim($publicacion->autor->email))) . '?d=retro&s=50' }}" alt="{{ $publicacion->autor->name }}">
	                        @else
	                        <img width="50px" class="mr-3 rounded-circle" src="{{ asset('img/usuarios/' . $publicacion->autor->image ) }}" alt="{{ $publicacion->autor->name }}">
	                        @endif                        
                                
							<div class="media-body">
								<p class="mt-0">
									<span>Escrito por:</span><br>
									<span style="color: #f7f1e3;">{{ $publicacion->autor->name }}</a>
									<!--
									<a style="color: #f7f1e3;" href="{{ route('autor.blog.list', $publicacion->autor_id) }}">{{ $publicacion->autor->name }}</a>
									-->
								</p>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>

<section class="blog-detalle">
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<div class="card-blog-detalle wow fadeInUp">
					<div class="info-blog-detalle">
						<ul class="list-inline mb-0">
							<li class="list-inline-item mr-4"><span>Publicado en:</span> <a href="">{{ $publicacion->categoria->name }}</a></li>
							<li class="list-inline-item">
								<span>Etiquetas: </span>
								@foreach($publicacion->etiquetas as $tg)
									<a href="" class="badge-blog">{{ $tg->name }}</a>
								@endforeach
							</li>
						</ul>
					</div>

					<div class="card-blog-detalle-body">
						{!! $publicacion->body !!}
					</div>
				</div>
			</div>
		</div>
	</div>

	<div class="container blog-recommended">
		<div class="row">
			<div class="col-md-12">
				<div class="big-title big-title-big px-5">		
					<p class="uppercase-title wow fadeInUp">Continua con tu travesía de aprendizaje.</p>
					<h3 class="wow fadeInUp">Publicaciones Recomendadas</h3>
					<div class="line-divider wow fadeInUp"></div>
				</div>
			</div>

			@foreach($publicaciones as $publicacion)
			<div class="col-md-4">
				<a href="{{ route('blog.detalle', $publicacion->slug) }}" class="card-blog wow fadeInUp">
					<div class="image-box">
						<img src="{{ asset('img/blog/covers/' . $publicacion->image) }}">

						<div class="image-hover">
							<div class="btn btn-primary">Leer Artículo</div>
						</div>
					</div>

					<div class="info-box">
						<h6><span>Publicado en:</span> {{ $publicacion->categoria->name }}</h6>
						<h2>{{ $publicacion->title }}</h2>

						<p>{{ $publicacion->summary }}</p>

						<div class="media autor-blog">
							@if( $publicacion->autor->image == NULL)
	                        <img class="mr-3 rounded-circle" src="{{ 'https://www.gravatar.com/avatar/' . md5(strtolower(trim($publicacion->autor->email))) . '?d=retro&s=50' }}" alt="{{ $publicacion->autor->name }}">
	                        @else
	                        <img width="50px" class="mr-3 rounded-circle" src="{{ asset('img/usuarios/' . $publicacion->autor->image ) }}" alt="{{ $publicacion->autor->name }}">
	                        @endif                        
                                
							<div class="media-body">
								<p class="mt-0">
									<span>Escrito por:</span><br>
									{{ $publicacion->autor->name }}
								</p>
							</div>
						</div>
					</div>

				</a>
			</div>
			@endforeach		
		</div>

	</div>
</section>
@endsection

@section('scripts')

@endsection