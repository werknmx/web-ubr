@extends('layouts.front')

@section('seo')
<!-- SEO -->
<meta name="description" content="Te enseñaremos más de 15 técnicas comprobadas para generar riqueza y multiplicarla en los bienes raices, incluso sin tener capital inicial o algún tipo de experiencia.">
<meta name="keywords" content="universidad, bienes, raices, inmobiliaria, tratos inmobiliarios, cursos, capacitaciones, tallers, conferencias, aprende a ganar, dinero, ganar dinero">
<meta name="robots" content="INDEX,FOLLOW,NOARCHIVE">

<!-- OG SEO -->
<meta property="og:image:width" content="1293">
<meta property="og:image:height" content="677">
<meta property="og:title" content="UBR - Universidad de Bienes Raíces">
<meta property="og:description" content="Te enseñaremos más de 15 técnicas comprobadas para generar riqueza y multiplicarla en los bienes raices, incluso sin tener capital inicial o algún tipo de experiencia.">
<meta property="og:url" content="http://www.universidaddebienesraices.com/">
<meta property="og:image" content="http://www.universidaddebienesraices.com/img/logo.jpg">
@endsection

@section('stylesheets')
<!-- Popup CSS -->
<link href="{{ asset('back/assets/plugins/Magnific-Popup-master/dist/magnific-popup.css') }}" rel="stylesheet">

<style type="text/css">
	.card-columns{
		columns: 3 !important;
	}
</style>
@endsection

@section('content')

<div class="container-fluid">
	<div class="row">
		<div class="col-md-4 offset-md-4 text-center">
			<h1 class="galeria-title">{{ $galeria->title }}</h1>
			<p class="galeria-body">{{ $galeria->body }}</p>
		</div>
	</div>
</div>

<section class="gallery-showcase">
	<div class="card-columns">
		<div class="card card-galeria">
            <a class="image-popup-vertical-fit" href="{{ asset('img/galerias/covers/' . $galeria->cover_image) }}"> <img src="{{ asset('img/galerias/covers/' . $galeria->cover_image) }}" alt="{{ $galeria->title }}" /> 
            </a>
            <div class="card-galeria-content">
            	<p>{{ $galeria->body }}</p>
            </div>
        </div>

		@foreach($galeria->imagenes as $galeria)
		<div class="card card-galeria">
            <a class="image-popup-vertical-fit" href="{{ asset('img/galerias/extras/' . $galeria->image) }}"> <img src="{{ asset('img/galerias/extras/' . $galeria->image) }}" alt="{{ $galeria->title }}" /> 
            </a>

            <div class="card-galeria-content">
            	<p>{{ $galeria->description }}</p>
            </div>
        </div>
		@endforeach
	</div>
</section>


<div class="row">
	<div class="col-md-12">
		<div class="big-title big-title-big px-5">		
			<p class="uppercase-title wow fadeInUp">Descubre más momentos de la Universidad</p>
			<h3 class="wow fadeInUp" >Galerías Recomendadas</h3>
			<div class="line-divider wow fadeInUp"></div>
		</div>
	</div>
</div>

<section class="gallery mt-4">
	<div class="card-columns">
		@foreach($galerias as $galeria)
		<div class="card card-galeria">
            <a class="image-popup-vertical-fit" href="{{ asset('img/galerias/covers/' . $galeria->cover_image) }}"> <img src="{{ asset('img/galerias/covers/' . $galeria->cover_image) }}" alt="{{ $galeria->title }}" /> 
            </a>

            @if($galeria->imagenes->count())
            	<a href="{{ route('galeria.detalle', $galeria->slug) }}" class="more-images">Ver todas las imágenes</a>
            @endif

            <div class="card-galeria-content">
            	<h4>{{ $galeria->title }}</h4>
            	<p>{{ $galeria->body }}</p>
            </div>
        </div>
		@endforeach
	</div>
</section>

@endsection

@section('scripts')
<script type="text/javascript">
	$( document ).ready(function() {
	    $('nav').addClass('darkback_int');
	});
</script>

<script src="{{ asset('back/assets/plugins/Magnific-Popup-master/dist/jquery.magnific-popup.min.js') }}"></script>
<script src="{{ asset('back/assets/plugins/Magnific-Popup-master/dist/jquery.magnific-popup-init.js') }}"></script>
@endsection