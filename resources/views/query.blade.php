@extends('layouts.front')

@section('seo')
<!-- SEO -->
<meta name="description" content="Te enseñaremos más de 15 técnicas comprobadas para generar riqueza y multiplicarla en los bienes raices, incluso sin tener capital inicial o algún tipo de experiencia.">
<meta name="keywords" content="universidad, bienes, raices, inmobiliaria, tratos inmobiliarios, cursos, capacitaciones, tallers, conferencias, aprende a ganar, dinero, ganar dinero">
<meta name="robots" content="INDEX,FOLLOW,NOARCHIVE">

<!-- OG SEO -->
<meta property="og:image:width" content="1293">
<meta property="og:image:height" content="677">
<meta property="og:title" content="UBR - Universidad de Bienes Raíces">
<meta property="og:description" content="Te enseñaremos más de 15 técnicas comprobadas para generar riqueza y multiplicarla en los bienes raices, incluso sin tener capital inicial o algún tipo de experiencia.">
<meta property="og:url" content="http://www.universidaddebienesraices.com/">
<meta property="og:image" content="http://www.universidaddebienesraices.com/img/logo.jpg">
@endsection

@section('content')

<section class="jumbotron jumbotron-intro">
	<div class="overlay"></div>
	<div class="container">
		<div class="row align-items-center">
			<div class="col-md-8">
				<div class="jumbotron-padding">
					<h3 class="wow fadeInLeft">Búsqueda General</h3>
					<div class="line-divider wow fadeInLeft"></div>

					@if(Request::input('query') == NULL)
					@else
						<p class="content wow fadeInLeft">Elementos que contienen: "{{ Request::input('query') }}"</p>
					@endif
				</div>
			</div>
		</div>
	</div>
</section>

<section class="contact-boxes">
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<div class="card">
					<div class="card-body">
						@if(Request::input('query') == NULL)
			
						@else
						<div class="alert alert-dismissible alert-info mb-5">
						  <button type="button" class="close" data-dismiss="alert">&times;</button>
						   Encontramos un total de: <strong>{{ $results }}</strong> resultado(s).
						</div>
						@endif

						@if(Request::input('query') == NULL)
							<div class="col">
								<h3 class="text-center"><i class="ionicons ion-sad-outline"></i> No hay resultados.</h3>
							</div>
						@else
						<div class="row">
							<div class="col-md-12">
								<h5 class="text-uppercase"><small><i class="ionicons ion-images"></i> Eventos</small> <span class="badge badge-primary">{{ $events->count() }}</span></h5>
								<hr>

								<div class="card-columns">
									@foreach($events as $ev)
									<div class="card card-evento">
										<div class="row no-gutters">
											<div class="date-box col-md-3">
												@php
													$time = strtotime($ev->start_date);

													$day = date('d', $time);
													$month = date('M', $time);
												@endphp
												<h4>{{ $day }}</h4>
												<h6>{{ $month }}</h6>
											</div>
											<div class="evento-info col-md-9">
												<h5>{{ $ev->type }}</h5>
												<h2>{{ $ev->name }}</h2>
												<p class="description">{{ $ev->description }}</p>

												@if($ev->location == NULL)
												@else
												<p><small><i class="fas fa-location-arrow"></i> {{ $ev->location }}</small></p>
												@endif

												@if($ev->contact_info_phone == NULL)
												@else
												<p><small><i class="fas fa-phone"></i> {{ $ev->contact_info_phone }}</small></p>
												@endif

												@if($ev->contact_info_mail == NULL)
												@else
												<p><small><i class="fas fa-envelope"></i> {{ $ev->contact_info_email }}</small></p>
												@endif

												<ul class="list-inline">
													<li class="list-inline-item"><i class="far fa-clock"></i> {{ $ev->start_date }}</li>
													<li class="list-inline-item"><i class="far fa-clock"></i> {{ $ev->end_date }}</li>
												</ul>
											</div>
										</div>
									</div>
									@endforeach
								</div>
							</div>

							<div class="col-md-12">
								<h5 class="text-uppercase"><small><i class="ionicons ion-images"></i> Blog</small> <span class="badge badge-primary">{{ $blogs->count() }}</span></h5>
								<hr>

								<div class="row">
									@foreach($blogs as $publicacion)
									<div class="col-md-4">
										<a href="{{ route('blog.detalle', $publicacion->slug) }}" class="card-blog wow fadeInUp">
											<div class="image-box">
												<img src="{{ asset('img/blog/covers/' . $publicacion->image) }}">

												<div class="image-hover">
													<div class="btn btn-primary">Leer Artículo</div>
												</div>
											</div>

											<div class="info-box">
												<h6><span>Publicado en:</span> {{ $publicacion->categoria->name }}</h6>
												<h2>{{ $publicacion->title }}</h2>

												<p>{{ $publicacion->summary }}</p>

												<div class="media autor-blog">
													@if( $publicacion->autor->image == NULL)
							                        <img class="mr-3 rounded-circle" src="{{ 'https://www.gravatar.com/avatar/' . md5(strtolower(trim($publicacion->autor->email))) . '?d=retro&s=50' }}" alt="{{ $publicacion->autor->name }}">
							                        @else
							                        <img width="50px" class="mr-3 rounded-circle" src="{{ asset('img/usuarios/' . $publicacion->autor->image ) }}" alt="{{ $publicacion->autor->name }}">
							                        @endif                        
						                                
													<div class="media-body">
														<p class="mt-0">
															<span>Escrito por:</span><br>
															{{ $publicacion->autor->name }}
														</p>
													</div>
												</div>
											</div>

										</a>
									</div>
									@endforeach
								</div>
							</div>

							<div class="col-md-12">
								<h5 class="text-uppercase"><small><i class="ionicons ion-images"></i> Cursos</small> <span class="badge badge-primary">{{ $courses->count() }}</span></h5>
								<hr>

								<div class="row">
									@foreach($courses as $curso)
									<div class="col-md-6">
										<a href="{{ route('curso.detalle', $curso->slug) }}" class="card-cursos">
											<div class="image-box">
												<img class="img-fluid" src="{{ asset('img/cursos/covers/' . $curso->image) }}">

												<div class="image-hover">
													<div class="btn btn-primary">Regístrate a este curso</div>
												</div>
											</div>
											<div class="info-box" style="min-height: 350px;">
												<!--<h6>{{ $curso->modality }}</h6>-->
												<h2>{{ $curso->title }}</h2>

												<ul class="list-unstyled">
													<li><span>Modalidad:</span> {{ $curso->modality }}</li>
													<li class="mt-2"><span>Horario:</span> {{ $curso->timetable }} </li>

													<li class="mt-2"><span>Acreditaciones:</span> {{ $curso->accreditation_type }}</li>
												</ul>
												<!--
												<div class="media align-items-center">
													<div class="media-left">
														<img src="http://placehold.it/50x50" class="media-object" style="width:30px">
													</div>
													<div class="media-body">
														<h4 class="media-heading mb-0">Nombre de Profesor</h4>
														<p class="mt-0">Area de Especialidad</p>
													</div>
												</div>
												-->
												<div class="position-bottom">						
													<hr>
													<ul class="list-inline">
														<li class="list-inline-item"><i class="fas fa-book"></i> {{ $curso->duration }}</li>
														<li class="list-inline-item"><i class="fas fa-users"></i> Capacidad: {{ $curso->capacity }} Alumn@s</li>
													</ul>
												</div>
											</div>
										</a>
									</div>
									@endforeach
								</div>
							</div>

							<div class="col-md-12">
								<h5 class="text-uppercase"><small><i class="ionicons ion-images"></i> Galerías</small> <span class="badge badge-primary">{{ $galleries->count() }}</span></h5>
								<hr>

								<div class="card-columns">
									@foreach($galleries as $galeria)
									<div class="card card-galeria">
							            <a class="image-popup-vertical-fit" href="{{ asset('img/galerias/covers/' . $galeria->cover_image) }}"> <img src="{{ asset('img/galerias/covers/' . $galeria->cover_image) }}" alt="{{ $galeria->title }}" /> 
							            </a>

							            @if($galeria->imagenes->count())
							            	<a href="{{ route('galeria.detalle', $galeria->slug) }}" class="more-images">Ver todas las imágenes</a>
							            @endif

							            <div class="card-galeria-content">
							            	<h4>{{ $galeria->title }}</h4>
							            	<p>{{ $galeria->body }}</p>
							            </div>
							        </div>
									@endforeach
								</div>
							</div>
						</div>
						@endif
					</div>
				</div>
			</div>
		</div>
	</div>
</section>
@endsection
