@extends('layouts.front')

@section('seo')
<!-- SEO -->
<meta name="description" content="Te enseñaremos más de 15 técnicas comprobadas para generar riqueza y multiplicarla en los bienes raices, incluso sin tener capital inicial o algún tipo de experiencia.">
<meta name="keywords" content="universidad, bienes, raices, inmobiliaria, tratos inmobiliarios, cursos, capacitaciones, tallers, conferencias, aprende a ganar, dinero, ganar dinero">
<meta name="robots" content="INDEX,FOLLOW,NOARCHIVE">

<!-- OG SEO -->
<meta property="og:image:width" content="1293">
<meta property="og:image:height" content="677">
<meta property="og:title" content="UBR - Universidad de Bienes Raíces">
<meta property="og:description" content="Te enseñaremos más de 15 técnicas comprobadas para generar riqueza y multiplicarla en los bienes raices, incluso sin tener capital inicial o algún tipo de experiencia.">
<meta property="og:url" content="http://www.universidaddebienesraices.com/">
<meta property="og:image" content="http://www.universidaddebienesraices.com/img/logo.jpg">
@endsection

@section('stylesheets')

@endsection

@section('content')
<section class="jumbotron jumbotron-intro mb-0">
	<div class="overlay"></div>
	<div class="container">
		<div class="row align-items-center">
			<div class="col-md-8 offset-md-2">
				<div class="text-center">
					<p class="uppercase-title wow fadeInUp">Queremos ayudarte a construir tus sueños</p>
					<h3 class="wow fadeInUp">Cursos</h3>
					<div class="line-divider wow fadeInUp"></div>

					<a href="{{ asset('contacto') }}" class="btn btn-primary wow fadeInUp">Contacta con nosotros</a>
				</div>
			</div>
		</div>
	</div>
</section>

<section class="cursos">
	<div class="container">
		@foreach($cursos->chunk(2) as $cursos)
		<div class="row">
			@foreach($cursos as $curso)
			<div class="col-md-6">
				<a href="{{ route('curso.detalle', $curso->slug) }}" class="card-cursos">
					<div class="image-box">
						<img class="img-fluid" src="{{ asset('img/cursos/covers/' . $curso->image) }}">

						<div class="image-hover">
							<div class="btn btn-primary">Regístrate a este curso</div>
						</div>
					</div>
					<div class="info-box" style="min-height: 350px;">
						<!--<h6>{{ $curso->modality }}</h6>-->
						<h2>{{ $curso->title }}</h2>

						<ul class="list-unstyled">
							<li><span>Modalidad:</span> {{ $curso->modality }}</li>
							<li class="mt-2"><span>Horario:</span> {{ $curso->timetable }} </li>

							<li class="mt-2"><span>Acreditaciones:</span> {{ $curso->accreditation_type }}</li>
						</ul>
						<!--
						<div class="media align-items-center">
							<div class="media-left">
								<img src="http://placehold.it/50x50" class="media-object" style="width:30px">
							</div>
							<div class="media-body">
								<h4 class="media-heading mb-0">Nombre de Profesor</h4>
								<p class="mt-0">Area de Especialidad</p>
							</div>
						</div>
						-->
						<div class="position-bottom">						
							<hr>
							<ul class="list-inline">
								<li class="list-inline-item"><i class="fas fa-book"></i> {{ $curso->duration }}</li>
								<li class="list-inline-item"><i class="fas fa-users"></i> Capacidad: {{ $curso->capacity }} Alumn@s</li>
							</ul>
						</div>
					</div>
				</a>
			</div>
			@endforeach		
		</div>
		@endforeach
	</div>
</section>
@endsection

@section('scripts')

@endsection