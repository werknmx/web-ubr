@extends('layouts.front')

@section('seo')
<!-- SEO -->
<meta name="description" content="Te enseñaremos más de 15 técnicas comprobadas para generar riqueza y multiplicarla en los bienes raices, incluso sin tener capital inicial o algún tipo de experiencia.">
<meta name="keywords" content="universidad, bienes, raices, inmobiliaria, tratos inmobiliarios, cursos, capacitaciones, tallers, conferencias, aprende a ganar, dinero, ganar dinero">
<meta name="robots" content="INDEX,FOLLOW,NOARCHIVE">

<!-- OG SEO -->
<meta property="og:image:width" content="1293">
<meta property="og:image:height" content="677">
<meta property="og:title" content="UBR - Universidad de Bienes Raíces">
<meta property="og:description" content="Te enseñaremos más de 15 técnicas comprobadas para generar riqueza y multiplicarla en los bienes raices, incluso sin tener capital inicial o algún tipo de experiencia.">
<meta property="og:url" content="http://www.universidaddebienesraices.com/">
<meta property="og:image" content="http://www.universidaddebienesraices.com/img/logo.jpg">
@endsection

@section('stylesheets')
<!-- Popup CSS -->
<link href="{{ asset('back/assets/plugins/Magnific-Popup-master/dist/magnific-popup.css') }}" rel="stylesheet">
@endsection

@section('content')
<section class="jumbotron jumbotron-intro">
	<div class="overlay"></div>
	<div class="container">
		<div class="row align-items-center">
			<div class="col-md-8">
				<div class="jumbotron-padding">
					<h3 class="wow fadeInLeft">Conoce de nosotros</h3>
					<div class="line-divider wow fadeInLeft"></div>

					<p class="content wow fadeInLeft">Nos hemos especializado en el sector educativo, tomando como  prioridad, enseñar  y poner en las manos de nuestro mercado la oportunidad de cambiar su futuro con visión y liderazgo.</p>
				</div>
			</div>
		</div>
	</div>
</section>

<section class="nosotros-content">
	<div class="container">
		<div class="row align-items-center pt-5">
			<div class="col-md-6">
				<div class="big-title pr-5 text-left">		
					<h3 class="wow fadeInLeft">Bienvenido a la Universidad</h3>
					<div class="line-divider wow fadeInLeft"></div>
				</div>

				<p class="pr-5">La primera Universidad de Bienes Raíces en México fue creada  en el año 2015  por Juan Carlos Zamora. Empresario mexicano que ganó el reconocimiento como Joven emprendedor en el año 2016,  otorgado por  la Cámara Nacional del Comercio, por  poseer  una empresa reconocida como caso de éxito en la innovación, servicio y desarrollo económico</p>

				<p class="pr-5">La Universidad de Bienes Raíces como instituto, se ha especializado en el sector educativo, tomando como  prioridad, enseñar  y poner en las manos de nuestro mercado la oportunidad de cambiar su futuro con visión y liderazgo.</p>

				<p class="pr-5">Nuestro compromiso como institución educativa es compartir nuestros conocimientos para cambiar vidas, ayudar a más personas a generar mayores  ingresos y a crear su propia empresa.</p>
			</div>
			<div class="col-md-6">
				<img class="img-fluid wow fadeInRight" src="{{ asset('img/nosotros-1.jpg') }}">
			</div>
		</div>

		<div class="row info-text-box">
			<div class="col-md-4">
				<div class="text-box wow fadeInUp">
					<!--<h6 class="big-num">01</h6>-->
					<h4>Nuestra Misión</h4>

					<p>Formar empresarios e inversionistas en bienes raíces que generen riqueza, ayudando y sirviendo de manera positiva en la economía de sus familias y su entorno social.</p>

					<p>A través de nuestros programas educativos y de investigación y el desarrollo de nuevas tecnologías.</p>
				</div>
			</div>
			<div class="col-md-4">
				<div class="text-box wow fadeInUp">
					<!--<h6 class="big-num">02</h6>-->
					<h4>Nuestra Visión</h4>

					<p>Ser reconocidos como la institución líder en América Latina que forma empresarios e inversionistas en bienes raíces.</p>

					<p>Teniendo presencia en los 3 países con mayor crecimiento económico en este sector habiendo formado a más de 4,000 alumnos.</p>
				</div>
			</div>
			<div class="col-md-4">
				<div class="text-box wow fadeInUp">
					<!--<h6 class="big-num">03</h6>-->
					<h4>Nuestros Valores</h4>

					<ul class="">
						<li>Disfrutar siempre lo que hacemos.</li>
						<li>Servicio excepcional: ofrecer una experiencia WOW de Servicio al Cliente.</li>
						<li>Respeto en nuestro entorno y hacia los demás.</li>
						<li>Mejora continua en la vida en el negocio.</li>
						<li>Trabajar en equipo hacia la visión 2024.</li>
					</ul>
				</div>
			</div>
		</div>
	</div>
</section>

<section class="gallery">
	<div class="row no-gutters">
		<div class="col-12 wow fadeInUp">
            <div class="swiper-container swiper-gallery">
                <div class="swiper-wrapper">
                    @foreach($galerias as $galeria)
                    <div class="swiper-slide">
                        <div class="card card-galeria card-galeria-full">
                            <a class="image-popup-vertical-fit" href="{{ asset('img/galerias/covers/' . $galeria->cover_image) }}"> <img src="{{ asset('img/galerias/covers/' . $galeria->cover_image) }}" alt="{{ $galeria->title }}" /> 
                            </a>

                            @if($galeria->imagenes->count())
                                <a href="{{ route('galeria.detalle', $galeria->slug) }}" class="more-images">Ver todas las imágenes</a>
                            @endif

                            <div class="card-galeria-content">
                                <h4>{{ $galeria->title }}</h4>
                                <p>{{ $galeria->body }}</p>
                            </div>
                        </div>
                        
                    </div>
                    @endforeach
                </div>
            </div>
		</div>
	</div>
</section>

<section class="estadisticas">
	<div class="overlay"></div>
	<div class="container-fluid">
		<div class="row text-center">
			<div class="col-md-6">
				<h4>+ 5,000</h4>
				<h6>Alumnos</h6>
			</div>
			<div class="col-md-6">
				<h4>12 Países</h4>
				<h6>Presencia Internacional</h6>
			</div>
		</div>
	</div>
</section>

<section class="nosotros-content py-5">
	<div class="container">
		<div class="row align-items-center pt-5">
			<div class="col-md-6">
				<img class="img-fluid wow fadeInRight" src="{{ asset('img/nosotros-2.jpg') }}">
			</div>

			<div class="col-md-6">
				<p class="pl-5 pr-4">Somos una Universidad que tiene como principio  ayudar a cada vez más personas a cumplir sus sueños, generar riquezas y alcanzar su libertad financiera a través del gran negocio del sector inmobiliario, el motor secreto que le da poder a la Universidad de Bienes Raíces, son aquellas personas que están decididas y apasionadas por cumplir sus metas en base al esfuerzo y dedicación.</p>

				<p class="pl-5 pr-4">Con alcance nacional e internacional, se han entrenado a más de 5,000  personas de manera presencial  y online, quienes buscan invertir y generar ingresos en  Bienes Raíces.</p>

				<p class="pl-5 pr-4">Desde sus inicios, la universidad de Bienes Raíces contribuye a la creación de fuentes de empleos y apoya causas altruistas y de responsabilidad social.</p>
			</div>
		</div>
	</div>
</section>

<section class="nosotros-content">
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<div class="big-title big-title-big px-5 text-center">		
					<p class="uppercase-title wow fadeInUp">Formamos Empresarios e inversionistas inmobiliarios</p>
					<h3 class="wow fadeInUp">Impacto</h3>
					<div class="line-divider wow fadeInUp"></div>

					<p class="content wow fadeInUp">Empresarios, inversionistas y mercado en general de México, Colombia, Guatemala, Argentina, Perú, China, Italia, Gran Bretaña, Polonia, España, Canadá y Estados Unidos han puesto su confianza en nuestras manos para ingresarlos al mundo Inmobiliario.</p>
				</div>
			</div>

			<div class="col-md-12">
				<div class="map">
					<img class="img-fluid" src="{{ asset('img/map_nosotros.svg') }}">
				</div>
			</div>
		</div>
	</div>
</section>
@endsection

@section('scripts')
<script src="{{ asset('back/assets/plugins/Magnific-Popup-master/dist/jquery.magnific-popup.min.js') }}"></script>
<script src="{{ asset('back/assets/plugins/Magnific-Popup-master/dist/jquery.magnific-popup-init.js') }}"></script>

<!-- Initialize Swiper -->
<script>
var swiper = new Swiper('.swiper-gallery', {
  slidesPerView: '3',
  spaceBetween: 0,
  loop: true,
  pagination: {
    el: '.swiper-pagination',
    clickable: true,
  },

  breakpoints: {
    // when window width is <= 320px
    320: {
      slidesPerView: 1,
      spaceBetween: 10
    },
    // when window width is <= 480px
    480: {
      slidesPerView: 1,
      spaceBetween: 20
    },
    // when window width is <= 640px
    640: {
      slidesPerView: 2,
      spaceBetween: 30
    }
  }
});
</script>
@endsection