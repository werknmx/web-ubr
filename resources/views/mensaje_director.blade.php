@extends('layouts.front')

@section('seo')
<!-- SEO -->
<meta name="description" content="Te enseñaremos más de 15 técnicas comprobadas para generar riqueza y multiplicarla en los bienes raices, incluso sin tener capital inicial o algún tipo de experiencia.">
<meta name="keywords" content="universidad, bienes, raices, inmobiliaria, tratos inmobiliarios, cursos, capacitaciones, tallers, conferencias, aprende a ganar, dinero, ganar dinero">
<meta name="robots" content="INDEX,FOLLOW,NOARCHIVE">

<!-- OG SEO -->
<meta property="og:image:width" content="1293">
<meta property="og:image:height" content="677">
<meta property="og:title" content="UBR - Universidad de Bienes Raíces">
<meta property="og:description" content="Te enseñaremos más de 15 técnicas comprobadas para generar riqueza y multiplicarla en los bienes raices, incluso sin tener capital inicial o algún tipo de experiencia.">
<meta property="og:url" content="http://www.universidaddebienesraices.com/">
<meta property="og:image" content="http://www.universidaddebienesraices.com/img/logo.jpg">
@endsection

@section('stylesheets')
<!-- VIDEO JS -->
<link href="https://vjs.zencdn.net/7.3.0/video-js.css" rel="stylesheet">
@endsection

@section('content')
<section class="mensaje-director" style="margin-top: 150px;">
	<div class="container-fluid">
		<div class="row align-items-center">
			<div class="col-md-6 p-0">
				<div class="director-btn-wrap">
					<a href="javascript:void(0)" data-toggle="modal" data-target="#modalVideo" class="text-center video-padding wow fadeIn">
						<!--<h6>Dale Play</h6>-->
						<i class="far fa-play-circle"></i>
						<p>Conoce la universidad</p>
					</a>
				</div>
			</div>
			<div class="col-md-6 p-0">
				<div class="director-box">
					<h3>Mensaje del Director</h3>
					<div class="line-divider"></div>

					<p>En espera de que este espacio pueda brindarle la oportunidad de adquirir conocimientos y experiencias que lo introduzcan en el mundo de los negocios en bienes raíces, le doy la bienvenida a UNIVERSIDAD DE BIENES RAíCES.</p>

					<p>Quienes formamos parte de esta universidad, portamos el estandarte de hacer partícipes a todos de nuestras experiencias, y así acortar el camino al éxito. Nuestra filosofía es ofrecer un servicio excepcional a nuestros alumnos, de los cuales hemos tenido en nuestras manos la responsabilidad de acompañar en su desarrollo, más de 4000 alumnos han confiado su educación en nosotros.</p>

					<p>Los invito a conocer nuestros programas educativos y formar parte de ellos, nos dará mucho gusto verte pronto.</p>

					<h5>¡Nuevamente bienvenidos!</h5>

					<div class="firma">
						<h4> Juan Carlos Zamora</h4>
						<h4><small>Director de Universidad de Bienes Raíces</small></h4>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>

<!-- Modal -->
<div class="modal fade" id="modalVideo" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>

            <div class="modal-body p-0 mb-0">
                <video id="spot-ubr" class="video-js" controls preload="auto" poster="./video/cover-spot.png" data-setup="{}">
                    <source src="./video/bienvenida.mp4" type='video/mp4'>
                    <p class="vjs-no-js">
                      Para ver este video es necesario tener activado las características Javascript de tu navegador.
                      <a href="https://videojs.com/html5-video-support/" target="_blank">Con soporte para video en HTML5</a>
                    </p>
                </video>
            </div>
        </div>
    </div>
</div>

@endsection

@section('scripts')
<!-- VIDEO JS -->
<script src="https://vjs.zencdn.net/7.3.0/video.js"></script>

<script type="text/javascript">
	$( document ).ready(function() {
	    $('nav').addClass('darkback_int');
	});
</script>
@endsection