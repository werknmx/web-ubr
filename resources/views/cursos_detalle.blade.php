@extends('layouts.front')

@section('seo')
<!-- SEO -->
<meta name="description" content="{{ $curso->description }}">
<meta name="keywords" content="universidad, bienes, raices, inmobiliaria, tratos inmobiliarios, cursos, capacitaciones, tallers, conferencias, aprende a ganar, dinero, ganar dinero">
<meta name="robots" content="INDEX,FOLLOW,NOARCHIVE">

<!-- OG SEO -->
<meta property="og:image:width" content="1293">
<meta property="og:image:height" content="677">
<meta property="og:title" content="{{ $curso->title }}">
<meta property="og:description" content="{{ $curso->description }}">
<meta property="og:url" content="{{ route('curso.detalle', $curso->slug) }}">
<meta property="og:image" content="{{ asset('img/cursos/covers/' . $curso->image) }}">
@endsection

@section('stylesheets')

<style type="text/css">
	.jumbotron-curso .overlay{
		background: rgba(38,47,84,.7);
	}

	.jumbotron-curso{
		padding-left: 0px !important;
		padding-right: 0px !important;
		padding-bottom: 0px !important;
		/*padding-top: 250px;
	    padding-bottom: 350px;
	    min-height: calc(60% - 100px);*/

	    background: url('{{ asset('img/cursos/covers/' . $curso->image) }}');
		background-size: cover;
		background-attachment: fixed;
		background-position: center center;
		background-repeat: no-repeat;
	}
</style>
@endsection

@section('content')
<section class="jumbotron jumbotron-curso mb-0">
	<div class="overlay"></div>

	<section class="cursos-detalle-top">
		<div class="cursos-detalle-box-hidden">
			<h3>{{ $curso->title }}</h3>
		</div>

		<div class="cursos-detalle-box-line"></div>

		<div class="container">
			<div class="row no-gutters">
				<div class="col-md-4">
					<div class="cursos-detalle-box">
						<h6>Formamos Empresarios e Inversionistas en Bienes Raíces</h6>
						<div class="content-box">
							<h3>{{ $curso->title }}</h3>
							<div class="line-divider"></div>
							<p>{{ $curso->description }}</p>
						</div>

						<div class="list-buttons">
							<div class="row no-gutters">
								<div class="col">
									<a href="javascript:void(0)" class="btn btn-dark-blue" data-toggle="modal" data-target="#modalPhone"><i class="fas fa-phone"></i> Pedir Informes</a>
								</div>
								<div class="col">
									<a class="btn btn-light-blue" href="javascript:void(0)" data-toggle="modal" data-target="#modalContact"><i class="far fa-envelope"></i> Contacto</a>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="col-md-8">
					<div class="row align-items-center no-gutters">

						<div class="col-md-4">
							<div class="media">
								<div class="media-left">
									<i class="far fa-clock"></i>
								</div>
								<div class="media-body">
									<h4 class="media-heading">Duración</h4>
									<p>{{ $curso->duration }}</p>
								</div>
							</div>
						</div>
						<div class="col-md-4">
							<div class="media">
								<div class="media-left">
									<i class="fas fa-graduation-cap"></i>
								</div>
								<div class="media-body">
									<h4 class="media-heading">Modalidad</h4>
									<p>{{ $curso->modality }}</p>
								</div>
							</div>
						</div>

						<div class="col-md-4">
							<div class="media">
								<div class="media-left">
									<i class="far fa-calendar-alt"></i>
								</div>
								<div class="media-body">
									<h4 class="media-heading">Horario</h4>
									<p>{{ $curso->timetable }}</p>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>
</section>



@if($curso->accreditation_type == NULL)
@else
<section class="acreditacion">
	<div class="container">
		<div class="row">
			<div class="col">
				<h3><span>Acreditación:</span> {{ $curso->accreditation_type }}</h3>
			</div>
		</div>
	</div>
</section>
@endif

<section class="nosotros">
	<div class="container">
		@if($curso->admission_profile == NULL && $curso->egress_profile == NULL)
		@else
		<div class="row pb-4">
			@if($curso->admission_profile == NULL)
			@else
			<div class="col-md-6 pr-5">
				<h4 class="wow fadeInUp"><i class="far fa-id-badge"></i> Perfil de Ingreso</h4>

				<div class="row">
					<div class="col-md-12 wow fadeInUp">
						{!! $curso->admission_profile !!}
					</div>
				</div>
			</div>
			@endif

			@if($curso->egress_profile == NULL)
			@else
			<div class="col-md-6 pr-5">
				<h4 class="wow fadeInUp"><i class="fas fa-id-badge"></i> Perfil de Egreso</h4>

				<div class="row">
					<div class="col-md-12 wow fadeInUp">
						{!! $curso->egress_profile !!}
					</div>
				</div>
			</div>
			@endif
		</div>
		@endif

		<div class="row mt-5">
			<div class="col-md-4">
				<p class="special-text wow fadeInUp">¡Transforma las oportunidades en tu mejor fuente de ingresos!</p>
				<p class="special-text wow fadeInUp">Conviértete en un profesional en el negocio de los bienes raíces</p>
			</div>

			<div class="col-md-4 pr-5">
				<h4 class="wow fadeInUp">Objetivo del Entrenamiento</h4>
				<p class="pr-3 wow fadeInUp">{{ $curso->objective }}</p>
			</div>
			<div class="col-md-4">
				<div class="side-cursos wow fadeInUp">
					<h4>Dirigido a</h4>

					<ul class="list-group">
						<li class="list-group-item d-flex justify-content-between align-items-center">
							<span>
								<i class="fas fa-graduation-cap"></i> {{ $curso->addressed_to }}
							</span>
						</li>
					</ul>

					<a href="javascript:void(0)" data-toggle="modal" data-target="#modalPhone" class="btn btn-dark-blue btn-block mb-0 mt-3"><i class="fas fa-phone"></i> Pedir Informes</a>
					<a href="javascript:void(0)" data-toggle="modal" data-target="#modalContact" class="btn btn-light-blue btn-block mt-0"><i class="far fa-envelope"></i> Contactar</a>
				</div>
			</div>
		</div>
	</div>
</section>

<!-- Modal Contacto -->
<div class="modal fade" id="modalContact" tabindex="-1" role="dialog" aria-hidden="true">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title" id="exampleModalLabel">Contacta con Nosotros</h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div class="modal-body">
				<form action="" class="mt-4" role="form" id="form">
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <input type="text" name="name" id="name" class="form-control" placeholder="Nombre Completo" autocomplete="name" required autocomplete="name">
                                <div class="help-block with-errors"></div>
                            </div>
                        </div>

                        <div class="col-md-6">
                            <div class="form-group">
                                <input type="email" name="email" id="email" class="form-control" placeholder="Correo Electrónico" autocomplete="email" required autocomplete="email">
                                <div class="help-block with-errors"></div>
                            </div>
                        </div>

                        <div class="col-md-6">
                            <div class="form-group">
                                <input type="text" name="subject" id="subject" class="form-control" placeholder="Asunto" required>
                                <div class="help-block with-errors"></div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <input type="phone" name="phone" id="phone" class="form-control" placeholder="Teléfono" autocomplete="tel" required autocomplete="tel-national">
                                <div class="help-block with-errors"></div>
                            </div>
                        </div>

                        <div class="col-md-12">
                            <div class="form-group">
                                <textarea class="form-control" placeholder="Mensaje" autocomplete="tel" required></textarea>
                                <div class="help-block with-errors"></div>
                            </div>
                        </div>
                    </div>
               
                    <button name="submit" id="submit" class="btn btn-contact" style="border-radius: 25px;width: 70%;">
                        <i class="fas fa-envelope"></i> Enviar tu información
                    </button>


                    <label id="info" class="info"></label>
                </form> 
			</div>
		</div>
	</div>
</div>

<!-- Modal Teléfono -->
<div class="modal fade" id="modalPhone" tabindex="-1" role="dialog" aria-hidden="true">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title" id="exampleModalLabel">Llama Directamente a las Oficinas</h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div class="modal-body text-center">
				<h6 class="mb-1" style="opacity: .7; text-transform: uppercase;
				font-size: 1em;">Linea directa:</h6>
				<h3 class="mb-5"><i class="fa fa-phone"></i> (+52) 556 833 07 78</h3>

				<h6 class="mb-1" style="opacity: .7; text-transform: uppercase;
				font-size: 1em;">Whatsapp:</h6>
				<h3 class="mb-3"><i class="fab fa-whatsapp"></i> +(52) 553 677 15 94</h3>
			</div>
		</div>
	</div>
</div>
@endsection

@section('scripts')

@endsection