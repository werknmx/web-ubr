<?php

use Illuminate\Database\Seeder;

use App\User;

class AdminSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $user = User::create([
            'name' => 'Admin UBR',
            'email' => 'admin@ubr.com',
            'password' => bcrypt('ubr12345'),
        ]);
        $user->assignRole('webmaster');

        $user->save();

        $user = new \App\User([
            'name' => "Ventas Juancho",
            'email' => "ventas1@ubr.mx",
            'password' => bcrypt('ubr12345'),
        ]);
        $user->assignRole('sales_agent');

        $user->save();

        $user = new \App\User([
            'name' => "Ventas Ramirez",
            'email' => "ventas2@ubr.mx",
            'password' => bcrypt('ubr12345'),
        ]);
        $user->assignRole('sales_agent');

        $user->save();

        $user = new \App\User([
            'name' => "Ventas Martínez",
            'email' => "ventas3@ubr.mx",
            'password' => bcrypt('ubr12345'),
        ]);
        $user->assignRole('sales_agent');

        $user->save();

        $user = new \App\User([
            'name' => "Gerente",
            'email' => "gerente@ubr.mx",
            'password' => bcrypt('ubr12345'),
        ]);
        $user->assignRole('admin_agent');

        $user->save();

        $user = new \App\User([
            'name' => "Asistente Administrativo",
            'email' => "support@ubr.mx",
            'password' => bcrypt('ubr12345'),
        ]);
        $user->assignRole('admin_support');

        $user->save();
    }
}
