<?php

use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Permission;

class PermissionsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Permission::create([
            'name' => 'all_access',
        ]);

        Permission::create([
            'name' => 'sales_access',
        ]);

        Permission::create([
            'name' => 'sales_access_prospects',
        ]);

        Permission::create([
            'name' => 'sales_access_kpis',
        ]);

        Permission::create([
            'name' => 'clients_access',
        ]);

        Permission::create([
            'name' => 'payments_access',
        ]);

        Permission::create([
            'name' => 'admin_access',
        ]);

        Permission::create([
            'name' => 'admin_access_services',
        ]);

        Permission::create([
            'name' => 'admin_access_users',
        ]);
    }
}
