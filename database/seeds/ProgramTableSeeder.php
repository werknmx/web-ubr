<?php

use Illuminate\Database\Seeder;

use App\Program;

class ProgramTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $program = Program::create([
            'name' => 'CAT',
            'slug' => 'cat',
            'description' => 'Programa 1',
        ]);

        $program = Program::create([
            'name' => 'Entrenamiento Prescencial',
            'slug' => 'entrenamiento_prescencial',
            'description' => 'Programa 2',
        ]);

        $program = Program::create([
            'name' => 'Certificado',
            'slug' => 'certificado',
            'description' => 'Programa 3',
        ]);

        $program = Program::create([
            'name' => 'Master',
            'slug' => 'master',
            'description' => 'Programa 4',
        ]);

        $program = Program::create([
            'name' => 'Coaching',
            'slug' => 'coaching',
            'description' => 'Programa 5',
        ]);

        $program = Program::create([
            'name' => 'Asesorías',
            'slug' => 'asesorias',
            'description' => 'Programa 6',
        ]);
    }
}
