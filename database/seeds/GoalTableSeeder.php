<?php

use Illuminate\Database\Seeder;

use App\Goals;

class GoalTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $goal = Goals::create([
            'type' => 'Closing',
            'name' => 'Cierres de Hoy',
            'objective' => '1',
            'temporality' => 'Daily',
            'date_start' => NULL,
            'date_end' => NULL,
        ]);

        $goal = Goals::create([
            'type' => 'Closing',
            'name' => 'Cierres Semanales',
            'objective' => '10',
            'temporality' => 'Weekly',
            'date_start' => NULL,
            'date_end' => NULL,
        ]);

        $goal = Goals::create([
            'type' => 'Closing',
            'name' => 'Cierres Mensuales',
            'objective' => '50',
            'temporality' => 'Monthly',
            'date_start' => NULL,
            'date_end' => NULL,
        ]);

        $goal = Goals::create([
            'type' => 'Sales',
            'name' => 'Venta Total Anual',
            'objective' => '100000',
            'temporality' => 'Monthly',
            'date_start' => NULL,
            'date_end' => NULL,
        ]);

        $goal = Goals::create([
            'type' => 'Prospects',
            'name' => 'Promesa de Venta',
            'objective' => '10',
            'temporality' => 'Weekly',
            'date_start' => NULL,
            'date_end' => NULL,
        ]);
    }
}
