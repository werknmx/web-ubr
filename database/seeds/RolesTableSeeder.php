<?php

use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Role;

class RolesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $role = Role::create([
        	'name' => 'webmaster',
        ]);

        $role->givePermissionTo('all_access');
        
        $role = Role::create([
        	'name' => 'sales_agent',
        ]);

        $role->givePermissionTo('sales_access');
        $role->givePermissionTo('sales_access_prospects');
        $role->givePermissionTo('sales_access_kpis');
        $role->givePermissionTo('clients_access');
        $role->givePermissionTo('payments_access');

        $role = Role::create([
        	'name' => 'admin_agent',
        ]);

        $role->givePermissionTo('sales_access');
        $role->givePermissionTo('clients_access');
        $role->givePermissionTo('payments_access');
        $role->givePermissionTo('admin_access');

        $role = Role::create([
        	'name' => 'admin_support',
        ]);

        $role->givePermissionTo('payments_access');
        $role->givePermissionTo('admin_access');
        $role->givePermissionTo('clients_access');
    }
}
