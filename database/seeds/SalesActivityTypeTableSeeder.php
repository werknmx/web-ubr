<?php

use Illuminate\Database\Seeder;

use App\ActivityType;

class SalesActivityTypeTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $type = ActivityType::create([
            'name' => 'Llamada',
            'verb' => 'Llamar a',
            'slug' => 'llamada',
            'icon_code' => 'icon-phone',
            'hex' => '#398bf7',
        ]);

        $type = ActivityType::create([
            'name' => 'Whatsapp',
            'verb' => 'Contactar por Whatsapp a',
            'slug' => 'whatsapp',
            'icon_code' => 'icon-screen-smartphone',
            'hex' => '#06d79c',
        ]);

        $type = ActivityType::create([
            'name' => 'Correo Electrónico',
            'verb' => 'Mandar un correo a',
            'slug' => 'correo_electronico',
            'icon_code' => 'icon-envelope-open',
            'hex' => '#745af2',
        ]);

        $type = ActivityType::create([
            'name' => 'Cita Prescencial',
            'verb' => 'Tener cita prescencial con',
            'slug' => 'cita_prescencial',
            'icon_code' => 'icon-people',
            'hex' => '#ffb22b',
        ]);

        $type = ActivityType::create([
            'name' => 'Otro',
            'verb' => 'Otra actividad con',
            'slug' => 'otro',
            'icon_code' => 'icon-rocket',
            'hex' => '#2f3d4a',
        ]);
    }
}
