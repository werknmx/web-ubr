<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddCountersToLandingPagesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('landing_pages', function (Blueprint $table) {
            $table->integer('view_count')->default(0)->nullable()->after('city');
            $table->integer('register_count')->default(0)->nullable()->after('city');;
            $table->string('conversion_rate')->nullable()->after('city');;
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('landing_pages', function (Blueprint $table) {
            $table->dropColumn('view_count');
            $table->dropColumn('register_count');
            $table->dropColumn('conversion_rate');
        });
    }
}
