<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddProgramAndCostToCoursesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('courses', function (Blueprint $table) {
            $table->integer('program_id')->nullable()->after('description');
            $table->boolean('has_discount')->nullable()->after('image');
            $table->string('special_price')->nullable()->after('image');
            $table->string('normal_price')->nullable()->after('image');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('courses', function (Blueprint $table) {
            $table->dropColumn('program_id');
            $table->dropColumn('special_price');
            $table->dropColumn('normal_price');
            $table->dropColumn('has_discount');
        });
    }
}
