<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddConfigDataToLandingPages extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('landing_pages', function (Blueprint $table) {
            $table->text('facebook_pixel_code')->nullable()->after('google_analytics_code');
            $table->string('place')->nullable()->after('description');
            $table->date('end_date')->nullable()->after('description');
            $table->date('start_date')->nullable()->after('description');
            $table->string('btn_link')->nullable()->after('origin_name');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('landing_pages', function (Blueprint $table) {
            $table->dropColumn('facebook_pixel_code');
            $table->dropColumn('start_date');
            $table->dropColumn('end_date');
            $table->dropColumn('place');
            $table->dropColumn('btn_link');
        });
    }
}
