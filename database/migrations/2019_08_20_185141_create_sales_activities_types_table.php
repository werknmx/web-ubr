<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSalesActivitiesTypesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sales_activities_types', function (Blueprint $table) {
            $table->bigIncrements('id');

            $table->string('name');
            $table->string('verb')->nullable();
            $table->string('slug')->nullable();
            $table->string('icon_code')->nullable();
            $table->string('hex')->nullable();
            
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('sales_activities_types');
    }
}
