<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLandingPagesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('landing_pages', function (Blueprint $table) {
            $table->bigIncrements('id');

            $table->integer('campaign_id')->nullable();
            $table->string('title');
            $table->string('description')->nullable();
            $table->string('slug')->nullable();
            $table->integer('template_id')->nullable();
            $table->integer('courses_id')->nullable();

            $table->boolean('has_form')->default(true)->nullable();
            $table->string('origin_name')->nullable();

            /* SEO */
            $table->string('seo_title')->nullable();
            $table->text('seo_description')->nullable();
            $table->string('seo_keywords')->nullable();
            $table->string('seo_robots')->nullable();

            /* Analytics */
            $table->string('google_analytics_code')->nullable();

            /* Info */
            $table->string('date_string')->nullable();
            $table->date('date')->nullable();
            $table->string('country')->nullable();
            $table->string('state')->nullable();
            $table->string('city')->nullable();


            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('landing_pages');
    }
}
