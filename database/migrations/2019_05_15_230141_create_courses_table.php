<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCoursesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('courses', function (Blueprint $table) {
            $table->bigIncrements('id');

            $table->string('title')->unique();
            $table->text('description');
            $table->text('objective')->nullable();
            $table->text('addressed_to')->nullable();

            $table->text('slogans')->nullable();

            $table->text('accreditation_type');

            $table->text('admission_profile')->nullable();
            $table->text('egress_profile')->nullable();

            $table->string('duration');
            $table->string('modality');
            $table->string('timetable');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('courses');
    }
}
