<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSalesActivitiesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sales_activities', function (Blueprint $table) {
            $table->bigIncrements('id');

            $table->integer('issuer_id')->unsigned();

            $table->string('module_name');

            $table->integer('activity_type_id')->nullable();
            $table->text('activity_description')->nullable();

            $table->string('activity_status');

            $table->date('date_start')->nullable();
            $table->date('date_end');
            $table->time('delivery_time')->nullable();
            
            $table->integer('contact_id')->unsigned()->nullable();

            $table->boolean('completed')->default(false);
            $table->boolean('is_urgent')->default(false)->nullable();
            
            $table->integer('completed_by_id')->unsigned()->nullable();
            $table->date('completed_at')->nullable();
            $table->time('completed_time')->nullable();

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('sales_activities');
    }
}
