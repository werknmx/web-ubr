<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddInfoToContactsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('contacts', function (Blueprint $table) {
            $table->string('global_status')->after('phone_cel')->default('Activo');

            $table->string('company_type')->after('phone_cel')->nullable();
            $table->string('job_title')->after('phone_cel')->nullable();
            $table->string('company')->after('phone_cel')->nullable();

            $table->string('birthday')->after('sur_name')->nullable();
            $table->string('global_origin')->after('assigned_to')->nullable();

            $table->string('city')->after('state')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('contacts', function (Blueprint $table) {
            $table->dropColumn('global_status');
            $table->dropColumn('company_type');
            $table->dropColumn('job_title');
            $table->dropColumn('company');
            $table->dropColumn('birthday');
            $table->dropColumn('city');
            $table->dropColumn('global_origin');
        });
    }
}
