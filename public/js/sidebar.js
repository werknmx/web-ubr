$('#menu-mobile').on('click', function () {
    toggleMenu();
});

$('.sidebar-overlay').on('click', function () {
    $('body').removeClass('toggled');
    $('.clonado').remove();
});

$('.close-btn').on('click', function () {
    $('body').removeClass('toggled');
    $('.clonado').remove();
});

$('.close-sidebar').on('click', function () {
    $('body').removeClass('toggled');
    $('.clonado').remove();
});

$('.sidebar-dropdown > a').on('click', function() {
	$('.sidebar-submenu').slideUp(100);

	if ($(this).parent().hasClass('active')) {
		$('.sidebar-dropdown').removeClass('active');
		$(this).parent().removeClass('active');
	} else {
		$('.sidebar-dropdown').removeClass('active');
		$(this).next('.sidebar-submenu').slideDown(100);
		$(this).parent().addClass('active');
	}
});

function toggleMenu() {
    $('body').addClass('toggled');
};
