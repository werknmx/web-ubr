<?php

function academia_import_files() {
    return array(
        array(
            'import_file_name'             => esc_html__('Academia Demo', 'academia'),
            'local_import_file'            => trailingslashit( get_template_directory() ) . 'lib/demo-data/academia-content-demo.xml',
            'local_import_widget_file'     => trailingslashit( get_template_directory() ) . 'lib/demo-data/widget-data.json',
            'import_preview_image_url'     => 'http://academiawp.demo.themexpert.com/wp-content/uploads/2017/01/academia-home.jpg',
            'import_notice'                => __( 'After you import this demo, you will have to setup the slider separately.', 'academia' ),
        ),
    );
}
add_filter( 'pt-ocdi/import_files', 'academia_import_files' );


function academia_after_import_setup() {
    // Assign menus to their locations.
    $main_menu = get_term_by( 'name', 'All Pages', 'nav_menu' );

    set_theme_mod( 'nav_menu_locations', array(
            'primary' => $main_menu->term_id,
        )
    );

    // Assign front page and posts page (blog page).
    $front_page_id = get_page_by_title( 'Home' );
    $blog_page_id  = get_page_by_title( 'Blog' );

    update_option( 'show_on_front', 'page' );
    update_option( 'page_on_front', $front_page_id->ID );
    update_option( 'page_for_posts', $blog_page_id->ID );

}
add_action( 'pt-ocdi/after_import', 'academia_after_import_setup' );


function academia_demo_page_setup( $default_settings ) {
    $default_settings['parent_slug'] = 'themes.php';
    $default_settings['page_title']  = esc_html__( 'One Click Demo Import' , 'academia' );
    $default_settings['menu_title']  = esc_html__( 'Import Demo Data' , 'academia' );
    $default_settings['capability']  = 'import';
    $default_settings['menu_slug']   = 'ac-one-click-demo-import';

    return $default_settings;
}
add_filter( 'pt-ocdi/plugin_page_setup', 'academia_demo_page_setup' );
