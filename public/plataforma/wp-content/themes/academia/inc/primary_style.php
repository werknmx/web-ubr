<?php


function color_luminance( $hex, $percent ) {
    /**
     * Lightens/darkens a given colour (hex format), returning the altered colour in hex format.7
     * @param str $hex Colour as hexadecimal (with or without hash);
     * @percent float $percent Decimal ( 0.2 = lighten by 20%(), -0.4 = darken by 40%() )
     * @return str Lightened/Darkend colour as hexadecimal (with hash);
     */

    // validate hex string

    $hex = preg_replace( '/[^0-9a-f]/i', '', $hex );
    $new_hex = '#';

    if ( strlen( $hex ) < 6 ) {
        $hex = $hex[0] + $hex[0] + $hex[1] + $hex[1] + $hex[2] + $hex[2];
    }

    // convert to decimal and change luminosity
    for ($i = 0; $i < 3; $i++) {
        $dec = hexdec( substr( $hex, $i*2, 2 ) );
        $dec = min( max( 0, $dec + $dec * $percent ), 255 );
        $new_hex .= str_pad( dechex( $dec ) , 2, 0, STR_PAD_LEFT );
    }

    return $new_hex;
}


function hex2rgb($hex) {
    /**
     * Convert hexcolor to RGB color.
     * @param str $hex Colour as hexadecimal (with or without hash);
     * @return str RGB color value;
     */

    $hex = str_replace("#", "", $hex);

    if(strlen($hex) == 3) {
        $r = hexdec($hex[0].$hex[0]);
        $g = hexdec($hex[1].$hex[1]);
        $b = hexdec($hex[2].$hex[2]);
    } else {
        $r = hexdec($hex[0].$hex[1]);
        $g = hexdec($hex[2].$hex[3]);
        $b = hexdec($hex[4].$hex[5]);
    }

    $rgb = array($r, $g, $b);
    return implode(",", $rgb);
}

function tx_academia_primary_style()
{
    $theme_primary_color = cs_get_option('tx_theme_primary_color');
    if ($theme_primary_color) { ?>

        <style>
            a,
            .home-v5 .nav .current-menu-item > a,
            .home-v5 .nav .current-menu-ancestor > a,
            .home-v5 .nav .current-menu-item > a:hover,
            .home-v5 .nav .current-menu-ancestor > a:hover,
            .home-v5 .nav .current-menu-item > a:focus,
            .home-v5 .nav .current-menu-ancestor > a:focus,
            .home-v5 .nav .current-menu-item > a,
            .home-v5 .nav .current-menu-item > a:hover,
            .home-v5 .nav .current-menu-item > a:focus,
            .home-v5 .nav .current-menu-item > a.open,
            .woocommerce button.button.alt:hover,
            .btn-special:hover,
            input[type="submit"]:hover,
            .woocommerce #respond input#submit:hover,
            .woocommerce a.button:hover,
            .woocommerce button.button:hover,
            .woocommerce input.button:hover,
            .woocommerce a.button.alt:hover,
            .woocommerce input.button.alt:hover,
            #tribe-bar-form .tribe-bar-submit input[type=submit]:hover,
            .button.purchase-button:hover,
            .complete-lesson-button:hover,
            .button-start-quiz:hover,
            .button-finish-quiz:hover,
            .tribe-events-widget-link a:hover,
            .tribe-events-back a:hover,
            .btn-bordered,
            .pagination a,
            .nav-links .nav-previous:hover a,
            .tribe-events-sub-nav .nav-previous:hover a,
            .nav-links .tribe-events-nav-previous:hover a,
            .tribe-events-sub-nav .tribe-events-nav-previous:hover a,
            .nav-links .nav-previous:hover::before,
            .tribe-events-sub-nav .nav-previous:hover::before,
            .nav-links .tribe-events-nav-previous:hover::before,
            .tribe-events-sub-nav .tribe-events-nav-previous:hover::before,
            .nav-links .nav-next:hover a,
            .tribe-events-sub-nav .nav-next:hover a,
            .nav-links .tribe-events-nav-next:hover a,
            .tribe-events-sub-nav .tribe-events-nav-next:hover a,
            .nav-links .tribe-events-nav-right:hover a,
            .tribe-events-sub-nav .tribe-events-nav-right:hover a,
            .nav-links .nav-next:hover::after,
            .tribe-events-sub-nav .nav-next:hover::after,
            .nav-links .tribe-events-nav-next:hover::after,
            .tribe-events-sub-nav .tribe-events-nav-next:hover::after,
            .nav-links .tribe-events-nav-right:hover::after,
            .tribe-events-sub-nav .tribe-events-nav-right:hover::after,
            .post.format-link .article-intro a:hover,
            .course-title:hover,
            .course-title a:hover,
            .woocommerce span.onsale,
            .woocommerce .quantity .qty,
            .owl-nav .btn,
            #back-to-top .btn,
            .ac-feature:hover h4,
            .ac-double-border-btn,
            .titlecontent .download-details .theme-features ul li i,
            .site-footer .tagcloud a:hover,
            .site-footer .btn.btn-default,
            .site-footer ul li a:hover,
            .text-primary,
            .ac-radius-md-btn:hover {
                color: <?php echo $theme_primary_color; ?>;
            }

            .vc_tta-tab.vc_active > a,
            .vc_tta-panel.vc_active .vc_tta-panel-heading > a,
            {
                color: <?php echo $theme_primary_color; ?> !important;
            }



            .bg-primary,
            .home-v1 .nav ul.dropdown-menu,
            .home-v2 .nav ul.dropdown-menu,
            .home-v4 .nav ul.dropdown-menu,
            .home-v3 .nav ul.dropdown-menu,
            #tx-trigger-effects button,
            .home-v4 .ac-header,
            .btn,
            button,
            input[type="submit"],
            .woocommerce #respond input#submit,
            .woocommerce a.button,
            .woocommerce button.button,
            .woocommerce input.button,
            .woocommerce a.button.alt,
            .woocommerce input.button.alt,
            #tribe-bar-form .tribe-bar-submit input[type=submit],
            .button.purchase-button,
            .complete-lesson-button,
            .button-start-quiz,
            .button-finish-quiz,
            input[type="button"],
            .tribe-events-widget-link a,
            .tribe-events-back a,
            .single_add_to_cart_button,
            .woocommerce button.button.alt,
            .btn-bordered:hover,
            .pagination .active a,
            .pagination .active a:hover,
            .nav-links .nav-previous,
            .tribe-events-sub-nav .nav-previous,
            .nav-links .tribe-events-nav-previous,
            .tribe-events-sub-nav .tribe-events-nav-previous,
            .nav-links .nav-next,
            .tribe-events-sub-nav .nav-next,
            .nav-links .tribe-events-nav-next,
            .tribe-events-sub-nav .tribe-events-nav-next,
            .nav-links .tribe-events-nav-right,
            .tribe-events-sub-nav .tribe-events-nav-right,
            .post.format-link .article-intro a,
            section.widget_calendar div.calendar_wrap table thead tr th,
            .course-header,
            .custom-header,
            .simple-testimonial #academia-testimonial.testimonial-style-2 .carousel-indicators li.active,
            #tribe-events .tribe-events-calendar thead th,
            #tribe-events .tribe-events-button,
            #tribe-events .tribe-events-button:hover,
            #tribe_events_filters_wrapper input[type=submit],
            .tribe-events-button,
            .tribe-events-button.tribe-active:hover,
            .tribe-events-button.tribe-inactive,
            .tribe-events-button:hover,
            .tribe-events-calendar td.tribe-events-present div[id*=tribe-events-daynum-],
            .tribe-events-calendar td.tribe-events-present div[id*=tribe-events-daynum-] > a,
            .home-v2 .ac-navbar .navbar-toggle,
            .home-v5 .nav .current-menu-item > a::before,
            .home-v5 .nav .current-menu-ancestor > a::before,
            .tx-menu,
            .woocommerce .woocommerce-MyAccount-navigation li,
            .ac-default-btn,
            .ac-large-btn,
            .ac-medium-btn,
            .ac-small-btn,
            .titlecontent .download-details .download-button .ac-highlight-btn,
            #tribe-events #tribe-events-content .tribe-events-schedule .tribe-events-cost,
            .learnpress-course-item .learnpress-course-thumbnail .course-price,
            #popup_title,
            .preset-on #mc_embed_signup input#mc-embedded-subscribe,
            .preset-on #mc_embed_signup input#mc-embedded-subscribe:focus {
                background-color: <?php echo $theme_primary_color; ?>;
            }

            .vc_tta-tab,
            .vc_tta-panel-heading,
            .vc_tta-panel-heading:hover,
            .vc_tta-panel.vc_active .vc_tta-panel-heading,
            .preset-on .rev_slider .tp-bgimg,
            .preset-on .rev_slider .rev-btn,
            .preset-on .vc_btn3-style-custom {
                background-color: <?php echo $theme_primary_color; ?> !important;
            }


            .pagination .active a,
            .pagination .active a:hover,
            .simple-testimonial #academia-testimonial.testimonial-style-2 .carousel-indicators li.active,
            .btn-bordered,
            #loginform input,
            .woocommerce form .form-row input.input-text,
            .woocommerce form .form-row textarea,
            .form-group input,
            .form-group textarea,
            input[type="text"],
            input[type="email"],
            textarea,
            .post.sticky,
            .post.format-link .article-intro a,
            section.widget_calendar div.calendar_wrap table thead tr th,
            section.widget_calendar div.calendar_wrap table tbody tr td,
            .woocommerce .quantity .qty,
            select,
            .owl-nav .btn,
            #back-to-top .btn-primary,
            .woocommerce .woocommerce-ordering select,
            select.form-control,
            #lang_sel a,
            .ac-radius-btn:hover,
            .ac-radius-md-btn:hover,
            .ac-double-border-btn,
            .ac-double-border-btn:before,
            .site-footer .form-control,
            .site-footer .btn.btn-default {
                border-color: <?php echo $theme_primary_color; ?>;
            }

            #lang_sel ul ul {
                border-top-color: <?php echo $theme_primary_color; ?> !important;
            }

            #tribe-events .tribe-events-calendar thead th {
                border-left-color: <?php echo $theme_primary_color; ?>;
            }
            #tribe-events .tribe-events-calendar thead th,
            #course-curriculum-popup #popup-main #popup-header .popup-menu {
                border-right-color: <?php echo $theme_primary_color; ?>;
            }

            #tribe-bar-form input[type=text], #tribe-bar-form input[type=text]:focus,
            #course-curriculum-popup #popup-main #popup-header,
            .learn-press-tabs .learn-press-nav-tabs .learn-press-nav-tab.active,
            .learn-press-user-profile .learn-press-tabs > li.current {
                border-bottom-color: <?php echo $theme_primary_color; ?>;
            }

            .home .home-v4 .ac-header {
                background-color: rgba(<?php echo hex2rgb($theme_primary_color) ?>, 0.8);
            }

            .ac-teachers-team .ateam .team-body {
                background: rgba(<?php echo hex2rgb($theme_primary_color) ?>, 0.9);
            }

            .tribe-events-calendar div[id*=tribe-events-daynum-] {
                background-color: rgba(<?php echo hex2rgb($theme_primary_color) ?>, 0.75);
            }
            .tribe-events-calendar td.tribe-events-past div[id*=tribe-events-daynum-] {
                background-color: rgba(<?php echo hex2rgb($theme_primary_color) ?>, 0.6);
            }
            #tribe-events-content .tribe-events-calendar td {
                border: 1px solid rgba(<?php echo hex2rgb($theme_primary_color) ?>, 0.5);
            }

            .home-v1 .nav > li > a::before,
            .home-v2 .nav > li > a::before,
            .home-v3 .nav > li::before,
            .home-v3 .nav > .dropdown::before {
                background-color: <?php echo color_luminance($theme_primary_color, 0.10) ?>;
            }

            .home-v1 .nav ul.dropdown-menu li a:hover,
            .home-v2 .nav ul.dropdown-menu li a:hover,
            .home-v4 .nav ul.dropdown-menu li a:hover,
            .home-v5 .nav ul.dropdown-menu li a:hover,
            .home-v1 .nav ul.dropdown-menu li a:focus,
            .home-v2 .nav ul.dropdown-menu li a:focus,
            .home-v4 .nav ul.dropdown-menu li a:focus,
            .home-v5 .nav ul.dropdown-menu li a:focus,
            .home-v1 .nav ul.dropdown-menu li a:active,
            .home-v2 .nav ul.dropdown-menu li a:active,
            .home-v4 .nav ul.dropdown-menu li a:active,
            .home-v5 .nav ul.dropdown-menu li a:active,
            .home-v3 .nav ul.dropdown-menu li a:hover,
            .home-v3 .nav ul.dropdown-menu li a:focus,
            .home-v3 .nav ul.dropdown-menu li a:active,
            .home-v3 .nav .tx_megamenu:hover,
            .home-v3 .nav .tx_megamenu:focus,
            .home-v3 .nav .tx_megamenu.open {
                background-color: <?php echo color_luminance($theme_primary_color, 0.15) ?>;
            }
            section.widget_calendar div.calendar_wrap table tbody tr td#today {
                background: <?php echo color_luminance($theme_primary_color, 0.10) ?>;
            }
            a:hover, a:focus {
                color: <?php echo color_luminance($theme_primary_color, -0.15) ?>;
            }
            a.bg-primary:hover,
            a.bg-primary:focus {
                background-color: <?php echo color_luminance($theme_primary_color, -0.10) ?>;
            }


        </style>

    <?php }
}

add_action('wp_head', 'tx_academia_primary_style');
