<?php

/**
 * Enqueue parent and child styles
 */
function academia_child_enqueue_styles() {
	wp_enqueue_style( 'academia-parent', get_template_directory_uri() . '/style.css' );
	wp_enqueue_style( 'academia-child', get_stylesheet_directory_uri() . '/style.css', array( 'academia-parent' ) );
}

add_action( 'wp_enqueue_scripts', 'academia_child_enqueue_styles' );