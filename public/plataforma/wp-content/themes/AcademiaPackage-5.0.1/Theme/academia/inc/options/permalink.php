<?php

function permalink_framework_options( $options ) {

    $options[]    = array(
        'name'      => 'tx_academia_permalink',
        'title'     => esc_html__('Permalink', 'academia'),
        'icon'      => 'fa fa-link',
        'fields'    => array(

            /**
             * Custom Permalink
             */

            array(
                'id'           => 'tx_academia_permalink_enable',
                'type'         => 'switcher',
                'title'        => __('Custom Permalink', 'academia'),
                'desc'         => __('Enable custom permalink for Custom Post Type and Taxonomy.', 'academia'),
            ),

            /**
             * Post Type Slug
             */

            array(
                'type'    => 'heading',
                'content' => __('Post Type', 'academia'),
                'dependency'   => array( 'tx_academia_permalink_enable', '==', 'true' ),
            ),

            array(
                'id'      => 'tx_academia_permalink_course',
                'type'    => 'text',
                'title'   => __('Course Slug', 'academia'),
                'desc'    => __('Type your desired slug for Course Post Type.', 'academia'),
                'dependency'   => array( 'tx_academia_permalink_enable', '==', 'true' ),
            ),

            array(
                'id'      => 'tx_academia_permalink_notice',
                'type'    => 'text',
                'title'   => __('Notice Board Slug', 'academia'),
                'desc'    => __('Type your desired slug for Notice Board Post Type.', 'academia'),
                'dependency'   => array( 'tx_academia_permalink_enable', '==', 'true' ),
            ),

            array(
                'id'      => 'tx_academia_permalink_teacher',
                'type'    => 'text',
                'title'   => __('Teachers Slug', 'academia'),
                'desc'    => __('Type your desired slug for Teachers Post Type.', 'academia'),
                'dependency'   => array( 'tx_academia_permalink_enable', '==', 'true' ),
            ),

            array(
                'id'      => 'tx_academia_permalink_testimonial',
                'type'    => 'text',
                'title'   => __('Testimonial Slug', 'academia'),
                'desc'    => __('Type your desired slug for Testimonial Post Type.', 'academia'),
                'dependency'   => array( 'tx_academia_permalink_enable', '==', 'true' ),
            ),

            array(
                'type'    => 'heading',
                'content' => __('Taxonomy', 'academia'),
                'dependency'   => array( 'tx_academia_permalink_enable', '==', 'true' ),
            ),

            array(
                'id'      => 'tx_academia_permalink_course_category',
                'type'    => 'text',
                'title'   => __('Course Category Slug', 'academia'),
                'desc'    => __('Type your desired slug for Course Category Taxonomy.', 'academia'),
                'dependency'   => array( 'tx_academia_permalink_enable', '==', 'true' ),
            ),

            array(
                'id'      => 'tx_academia_permalink_notice_category',
                'type'    => 'text',
                'title'   => __('Notice Board Category Slug', 'academia'),
                'desc'    => __('Type your desired slug for Notice Board Category Taxonomy.', 'academia'),
                'dependency'   => array( 'tx_academia_permalink_enable', '==', 'true' ),
            ),

            array(
                'id'      => 'tx_academia_permalink_teacher_course_name',
                'type'    => 'text',
                'title'   => __('Course Name Slug', 'academia'),
                'desc'    => __('Type your desired slug for Course Name Taxonomy from Teachers Post Type.', 'academia'),
                'dependency'   => array( 'tx_academia_permalink_enable', '==', 'true' ),
            ),



        )
    );

    return $options;

}
add_filter( 'cs_framework_options', 'permalink_framework_options' );
