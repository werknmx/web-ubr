<?php

function academia_change_post_types_slug($args, $post_type) {

    $permalink_enable = cs_get_option('tx_academia_permalink_enable');


    if (('academia_course' === $post_type ) && $permalink_enable && cs_get_option('tx_academia_permalink_course')) {
        $args['rewrite']['slug'] = cs_get_option('tx_academia_permalink_course');
    }


    if (('academia_notice' === $post_type ) && $permalink_enable && cs_get_option('tx_academia_permalink_notice')) {
        $args['rewrite']['slug'] = cs_get_option('tx_academia_permalink_notice');
    }

    if (('academia_teacher' === $post_type ) && $permalink_enable && cs_get_option('tx_academia_permalink_teacher')) {
        $args['rewrite']['slug'] = cs_get_option('tx_academia_permalink_teacher');
    }

    if (('academia_testimonial' === $post_type ) && $permalink_enable && cs_get_option('tx_academia_permalink_testimonial')) {
        $args['rewrite']['slug'] = cs_get_option('tx_academia_permalink_testimonial');
    }

    return $args;
}

add_filter('register_post_type_args', 'academia_change_post_types_slug', 10, 2);



function academia_change_taxonomies_slug( $args, $taxonomy ) {

    $permalink_enable = cs_get_option('tx_academia_permalink_enable');


    if (('academia_course_category' === $taxonomy ) && $permalink_enable && cs_get_option('tx_academia_permalink_course_category')) {
        $args['rewrite']['slug'] = cs_get_option('tx_academia_permalink_course_category');
    }

    if (('academia_notice_category' === $taxonomy ) && $permalink_enable && cs_get_option('tx_academia_permalink_notice_category')) {
        $args['rewrite']['slug'] = cs_get_option('tx_academia_permalink_notice_category');
    }

    if (('course_name' === $taxonomy ) && $permalink_enable && cs_get_option('tx_academia_permalink_teacher_course_name')) {
        $args['rewrite']['slug'] = cs_get_option('tx_academia_permalink_teacher_course_name');
    }


    return $args;
}
add_filter( 'register_taxonomy_args', 'academia_change_taxonomies_slug', 10, 2 );

