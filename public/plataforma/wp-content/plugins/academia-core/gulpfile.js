var gulp = require('gulp');
var zip = require('gulp-zip');
var wpPot = require('gulp-wp-pot');
var sort = require('gulp-sort');


gulp.task('package', function () {
    gulp.src([
        './*',
        './*/**',
        '!./bower_components/**',
        '!./node_modules/**',
        '!./bower_components',
        '!./node_modules',
        '!./assets'

    ])
        .pipe(zip('academia-core.zip'))
        .pipe(gulp.dest('.'));
});

gulp.task('generatePot', function () {
    return gulp.src([
        './*.php',
        './*/**.php',
        './*/*/**.php',
        '!./bower_components/**',
        '!./node_modules/**',
        '!./assets/**',
        '!./bower_components',
        '!./node_modules',
        '!./assets' ])
        .pipe(sort())
        .pipe(wpPot( {
            domain: 'academia',
            destFile:'academia.pot',
            package: 'academia',
            bugReport: 'https://www.themexpert.com',
            lastTranslator: 'ThemeXpert <info@themexpert.com>',
            team: 'ThemeXpert <info@themexpert.com>'
        } ))
        .pipe(gulp.dest('languages'));
});
