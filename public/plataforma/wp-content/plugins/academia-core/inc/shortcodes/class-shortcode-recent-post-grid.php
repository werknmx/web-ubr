<?php

/**
 *
 * Recent Post Shortcode
 *
 */
class Academia_Shortcode_Recent_Post_Grid
{


    /**
     *
     * Shortcode Name
     *
     * @var string
     */

    private $name = 'academia-recent-post-grid';

    private $post_type = 'post';

    /**
     * Instance of class
     */
    private static $instance;

    /**
     * Initialization
     */
    public static function init()
    {
        if (null === self::$instance) {
            self::$instance = new self;
        }

        return self::$instance;
    }


    private function __construct()
    {

        add_shortcode($this->name, array($this, 'create_recent_post_grid_shortcode'));
    }


    /**
     * Shortcode Function
     *
     * @param $atts
     *
     * @return string
     */

    public function create_recent_post_grid_shortcode($atts)
    {

        ob_start();

        $option = array(
            'item' => 2,
            'order' => 'DESC',
            'orderby' => 'date',
            'column' => 2,
        );

        $s = shortcode_atts($option, $atts);

        $args = array(
            'post_type' => $this->post_type,
            'order' => $s['order'],
            'orderby' => $s['orderby'],
            'posts_per_page' => $s['item'],
            'post__not_in' => get_option('sticky_posts'),
        );

        switch ($s['column']) {
            case 2:
                $column = 'col-md-6';
                break;
            case 3:
                $column = 'col-md-4';
                break;
            case 4:
                $column = 'col-md-3';
                break;
            default:
                $column = 'col-md-4';
        }

        $q = new WP_Query($args);

        if ($q->have_posts()): ?>

            <div class="row">

                <?php while ($q->have_posts()): $q->the_post(); ?>

                    <div class="col-sm-6 <?php echo $column; ?> ac-post-grid-item">
                        <div class="shadow-box">
                            <?php if (has_post_thumbnail()): ?>
                                <div class="thumbnail-wrapper">
                                    <a href="<?php the_permalink(); ?>"><?php the_post_thumbnail('large', array('class' => 'img-responsive')); ?></a>
                                </div>
                            <?php endif; ?>
                            <div class="post-grid-meta">
                                <p><?php the_time('d F, Y'); ?></p>
                                <h4><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h4>
                            </div>
                        </div>
                    </div>


                <?php endwhile;
                wp_reset_postdata(); ?>
            </div>


        <?php else: ?>

            <p> <?php esc_html_e('Sorry no post found.', 'academia'); ?> </p>

        <?php endif;

        $output = ob_get_clean();

        return $output;

    }


}
