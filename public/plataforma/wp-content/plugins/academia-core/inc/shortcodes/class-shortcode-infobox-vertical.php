<?php
/**
 *
 * Quick Info Shortcode
 *
 */


class Academia_Shortcode_InfoBox_Vertical{


    /**
     *
     * Shortcode Name
     * @var string
     */

    private $name = 'info-box-vertical';

    /**
     * Instance of class
     */
    private static $instance;

    /**
     * Initialization
     */
    public static function init() {
        if ( null === self::$instance ) {
            self::$instance = new self;
        }

        return self::$instance;
    }


    private function __construct() {
        add_shortcode( $this->name, array( $this, 'create_info_box_vertical_shortcode' ) );
    }


    /**
     * Shortcode Function
     *
     * @param $atts
     * @return string
     */

    public function create_info_box_vertical_shortcode( $atts , $content) {

        ob_start();

        $default = array(
            'image' => '',
            'image-size' => '',

            'section-background' => '',
            'content-align' => '',

            'show-content' => '',

            'title-text' => '',
            'title-text-color' => '',
            'title-text-size' => '',
            'title-margin' => '',
            'title-padding' => '',

            'subtitle-text' => '',
            'subtitle-text-color' => '',
            'subtitle-text-size' => '',
            'subtitle-font-weight' => '',
            'subtitle-letter-spacing' => '',
            'subtitle-margin' => '',
            'subtitle-padding' => '',

            'button_show'         => '',// button true or false
            'button_text'         => '',// button text
            'button_link'         => '',// button link
            'button-type'         => '',
            'button_link_target'  => '',// button target

            'content-color'   => '',
            'content-text-size' => '',
            'content-line-height' => '',
            'content-margin' => '',
            'content-padding' => '',

            'css'   => '',
            'class'   => '',

        );
        $info = shortcode_atts( $default, $atts );
        $css_class = apply_filters( VC_SHORTCODE_CUSTOM_CSS_FILTER_TAG, vc_shortcode_custom_css_class( $info ['css'], ' ' ), $this->name, $info );
        ?>

        <div class="infobox infobox-vertical <?php echo $css_class; ?> <?php echo $info['class']; ?> ">
            <div
                class="infobox-wrapper"
                style="text-align: <?php echo $info['content-align']; ?>"
            >
                <div class="row">

                    <div class="col-md-12 infobox-item" style='
                        background:<?php echo $info['section-background']; ?>;
                        '>


                        <?php if($info['image']): ?>
                            <?php
                            $img_size = $info['image-size'];
                            $pos = strpos($img_size, 'x');
                            if ($pos === false) {
                                $image = wp_get_attachment_image($info['image'], $img_size);
                            } else {
                                $img_x = explode('x', $img_size);
                                $width = (int) $img_x[0];
                                $height = (int) $img_x[1];
                                $image = wp_get_attachment_image($info['image'], array($width, $height));
                            }

                            ?>
                            <div class="infobox-image">
                                <div class="image-wrapper">
                                    <?php echo $image; ?>
                                </div>
                            </div>
                        <?php endif; ?>
                        <!--                        end of /.infobox-icon -->


                        <?php if($info['show-content']): ?>
                            <div class="infobox-content">

                                <div class="title-wrapper" style='
                                    margin:<?php echo $info['title-margin']; ?>;
                                    padding:<?php echo $info['title-padding']; ?>;
                                    '>
                                    <h3 style='
                                        color:<?php echo $info['title-text-color']; ?>;
                                        font-size:<?php echo $info['title-text-size']; ?>;
                                        '>
                                        <?php echo $info['title-text']; ?>
                                    </h3>
                                </div>

                                <div class="subtitle-wrapper" style='
                                    margin:<?php echo $info['subtitle-margin']; ?>;
                                    padding:<?php echo $info['subtitle-padding']; ?>;
                                    '>
                                    <h4 style='
                                        color:<?php echo $info['subtitle-text-color']; ?>;
                                        font-size:<?php echo $info['subtitle-text-size']; ?>;
                                        font-weight:<?php echo $info['subtitle-font-weight']; ?>;
                                        letter-spacing:<?php echo $info['subtitle-letter-spacing']; ?>;
                                        '>
                                        <?php echo $info['subtitle-text']; ?>
                                    </h4>
                                </div>

                                <div class="content-wrapper">
                                    <div class="content-section" style='
                                        color:<?php echo $info['content-color']; ?>;
                                        margin:<?php echo $info['content-margin']; ?>;
                                        padding:<?php echo $info['content-padding']; ?>;
                                        font-size:<?php echo $info['content-text-size']; ?>;
                                        line-height:<?php echo $info['content-line-height']; ?>;
                                        '>
                                        <?php echo $content; ?>
                                    </div>
                                </div>

                                <?php if($info['button_show']): ?>
                                    <div class="call-to-action">
                                        <?php if($info['button_text']): ?>
                                            <a
                                                class="<?php echo $info['button-type'] ? $info['button-type'] : 'ac-grd-btn';  ?>"
                                                href="<?php echo esc_url($info['button_link']); ?>"
                                                btn-text="<?php echo esc_html($info['button_text']); ?>"
                                                target="<?php echo esc_attr($info['button_link_target']); ?>"
                                            >
                                                <?php echo esc_html($info['button_text']); ?>
                                            </a>
                                        <?php endif; ?>
                                    </div>
                                <?php endif; ?>

                            </div>
                            <!--                        end of /.infobox-content-->
                        <?php endif; ?>

                    </div>
                    <!--                    end of /.row-->
                </div>
                <!--                end of /.infobox-v2-->
            </div>
            <!--            end of /.infobox-wrapper-->
        </div>
        <!--        end of /.infobox-->

        <?php
        $output = ob_get_clean();

        return $output;

    }
//    end of the function

}