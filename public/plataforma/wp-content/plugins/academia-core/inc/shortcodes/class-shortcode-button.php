<?php
/*
* Woocommerce button v1
*/
class Academia_Shortcode_Button{
    /**
     * Name of the shortcode
     * @var string
     */
    private $name = 'ac-button';
    private static $instance;


    public static function init(){
        if(null == self::$instance){
            self::$instance = new self;
        }
        return self::$instance;
    }

    /**
     * Shortcode construct
     */
    private function __construct(){
        add_shortcode($this->name, array($this, 'create_academia_button'));
    }


    public function create_academia_button($atts){
        ob_start();
        $option = array(
            'button-text'       => '',
            'ac-button-link'       => '',
            'button-align'       => '',
            'button-font-weight'       => '',
            'button-type'      => '',
            'button-link-target'      => '',
        );
        $ab = shortcode_atts( $option, $atts );


        ?>

        <div
            class="ac-button"
            style="text-align: <?php echo $ab['button-align']; ?>;"
        >

            <?php if($ab['button-text']): ?>
                <a
                    href="<?php echo esc_url($ab['ac-button-link']); ?>"
                    btn-text="<?php echo $ab['button-text']; ?>"
                    target="<?php echo esc_attr($ab['button-link-target'])?>"
                    class="<?php echo $ab['button-type'] ? $ab['button-type'] : 'ac-default-btn';  ?>"
                    style="font-weight: <?php echo $ab['button-font-weight']; ?>;"
                >
                    <?php echo $ab['button-text']; ?>
                </a>
            <?php endif; ?>



        </div>




        <?php
        $output = ob_get_clean();
        return $output;
    }

}