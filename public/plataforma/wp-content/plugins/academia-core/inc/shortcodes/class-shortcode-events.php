<?php

/**
 *
 * Recent Post Shortcode
 *
 */
class Academia_Shortcode_Events {


    /**
     *
     * Shortcode Name
     * @var string
     */

    private $name = 'academia-upcoming-events';

    private $post_type = 'tribe_events';

    /**
     * Instance of class
     */
    private static $instance;

    /**
     * Initialization
     */
    public static function init() {
        if (null === self::$instance) {
            self::$instance = new self;
        }

        return self::$instance;
    }


    private function __construct() {

        add_shortcode($this->name, array($this, 'create_upcoming_events_shortcode'));
    }


    /**
     * Shortcode Function
     *
     * @param $atts
     * @return string
     */

    public function create_upcoming_events_shortcode($atts) {

        ob_start();

        if ( class_exists( 'Tribe__Events__Main' ) ):

        $option = array(
            'item' => 3,
        );

        $s = shortcode_atts($option, $atts);

        $args = array(
            'post_type' => $this->post_type,
            'posts_per_page' => $s['item'],
        );

        $q = new WP_Query($args);

        if ($q->have_posts()): ?>


            <?php while ($q->have_posts()): $q->the_post(); ?>

                <div class="ac-upcoming-events">
                    <div class="media">
                        <div class="media-left">

                            <?php echo tribe_event_featured_image(null, 'medium') ?>

                        </div>
                        <div class="media-body">
                            <h4 class="media-heading"><a href="<?php echo esc_url(tribe_get_event_link()); ?>"
                                                         title="<?php the_title_attribute() ?>"> <?php the_title(); ?></a>
                            </h4>
                            <div class="content-desc">
                                <p> <?php echo tribe_events_event_schedule_details() ?> </p>
                            </div>
                        </div>
                    </div>
                </div>


            <?php endwhile;
            wp_reset_postdata(); ?>


        <?php else: ?>

            <p> <?php esc_html_e('Sorry no post found.', 'academia'); ?> </p>

        <?php endif;
        else:
            _e('Please Activate The Events Calendar Plugin.', 'academia');
        endif;

        $output = ob_get_clean();

        return $output;

    }


}