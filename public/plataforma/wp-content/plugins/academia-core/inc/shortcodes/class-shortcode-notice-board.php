<?php

/**
 *
 * Notice Board Shortcode
 *
 */
class Academia_Shortcode_Notice_Board {


    /**
     *
     * Shortcode Name
     * @var string
     */

    private $name = 'academia-notice-board';

    private $post_type = 'academia_notice';

    /**
     * Instance of class
     */
    private static $instance;

    /**
     * Initialization
     */
    public static function init() {
        if (null === self::$instance) {
            self::$instance = new self;
        }

        return self::$instance;
    }


    private function __construct() {

        add_shortcode($this->name, array($this, 'create_notice_board_shortcode'));
    }


    /**
     * Shortcode Function
     *
     * @param $atts
     * @return string
     */

    public function create_notice_board_shortcode($atts) {

        ob_start();

        $option = array(
            'item' => 4,
            'order' => 'DESC',
            'orderby' => 'date',

        );

        $s = shortcode_atts($option, $atts);

        $args = array(
            'post_type' => $this->post_type,
            'order' => $s['order'],
            'orderby' => $s['orderby'],
            'posts_per_page' => $s['item'],
        );

        $q = new WP_Query($args);

        if ($q->have_posts()): ?>


            <div class="row">

                <?php while ($q->have_posts()): $q->the_post(); ?>

                    <div class="col-sm-6 col-md-3 ac-notice-item">
                        <div class="shadow-box">
                            <?php if (has_post_thumbnail()): ?>
                                <div class="thumbnail-wrapper">
                                    <a href="<?php the_permalink(); ?>"><?php the_post_thumbnail('large', array('class' => 'img-responsive')); ?></a>
                                </div>
                            <?php endif; ?>
                            <div class="notice-meta">
                                <p><?php the_time('d F, Y'); ?></p>
                                <h4><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h4>
                            </div>
                        </div>
                    </div>


                <?php endwhile;
                wp_reset_postdata(); ?>

            </div>

        <?php else: ?>

            <p> <?php esc_html_e('Sorry no post found.', 'academia'); ?> </p>

        <?php endif;

        $output = ob_get_clean();

        return $output;

    }


}