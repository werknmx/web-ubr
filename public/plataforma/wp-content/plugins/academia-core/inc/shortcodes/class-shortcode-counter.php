<?php

/**
 *
 * Counter Shortcode
 *
 */
class Academia_Shortcode_Counter {


    /**
     *
     * Shortcode Name
     * @var string
     */

    private $name = 'academia-counter';


    /**
     * Instance of class
     */
    private static $instance;

    /**
     * Initialization
     */
    public static function init() {
        if ( null === self::$instance ) {
            self::$instance = new self;
        }

        return self::$instance;
    }


    private function __construct() {

        add_shortcode( $this->name, array( $this, 'create_counter' ) );
    }


    /**
     * Shortcode Function
     *
     * @param $atts
     *
     * @return string
     */

    public function create_counter( $atts, $content ) {
        $default      = array(
            'id'                =>  'tcountx',
            'css'               => '',
            'start_form'        =>  '0',
            'end_to'            =>  '100',
            'animation_speed'   =>  '1000',
            'unit'              =>  '+',
            'color'             =>'#6AB42F',
            'class'				=>'',
            'font'				=>'36',
        );
        $counter_info = shortcode_atts( $default, $atts );

        //print_r( $short_info );
        ob_start();
        $css_class = apply_filters( VC_SHORTCODE_CUSTOM_CSS_FILTER_TAG, vc_shortcode_custom_css_class( $counter_info ['css'], ' ' ), $this->name, $counter_info );
        ?>

        <div class="tx-counter <?php echo esc_attr( $counter_info ['class'] ); ?> <?php echo esc_attr( $css_class ); ?>" id="<?php echo $counter_info['id'];?>">
            <span class="tx-counter-item" style="color:<?php  echo $counter_info['color'];?>; font-size: <?php echo $counter_info ['font']; ?>px;"><b class="timer" data-from="<?php  echo $counter_info['start_form'];?>" data-to="<?php  echo $counter_info['end_to'];?>" data-speed="<?php  echo $counter_info['animation_speed'];?>"></b><?php  echo esc_html($counter_info['unit']);?></span>
        <?php echo wpb_js_remove_wpautop( $content, true ); ?>

        </div>


        <script type="text/javascript">

            (function ($) {
                var id = <?php  echo $counter_info['id'];?>;



                $(window).on('load scroll', function () {

                    $this = $(this)

                    if (($this.scrollTop() + $this.height()) > $(id).offset().top) {

                        //if happened once
                        if ($(id).hasClass('visible') == false) {
                            //your event
                            $(id).find('.timer').countTo();
                            $(id).addClass('visible');
                        }


                    }


                })
            })(jQuery)


        </script>


        <?php $output = ob_get_clean();

        return $output;

    }


}

