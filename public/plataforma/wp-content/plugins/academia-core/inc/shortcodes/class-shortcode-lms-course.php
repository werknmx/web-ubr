<?php

/**
 *
 * Recent Post Shortcode
 *
 */
class Academia_Shortcode_LMS_Course {


    /**
     *
     * Shortcode Name
     * @var string
     */

    private $name = 'academia-lms-course';

    private $post_type = 'lp_course';

    /**
     * Instance of class
     */
    private static $instance;

    /**
     * Initialization
     */
    public static function init() {
        if (null === self::$instance) {
            self::$instance = new self;
        }

        return self::$instance;
    }


    private function __construct() {

        add_shortcode($this->name, array($this, 'create_lms_course_shortcode'));
    }


    /**
     * Shortcode Function
     *
     * @param $atts
     * @return string
     */

    public function create_lms_course_shortcode($atts) {

        ob_start();

        if ( class_exists( 'LearnPress' ) ):

        $option = array(
            'item' => 8,
            'order' => 'DESC',
            'orderby' => 'date',
            'style' => 'carousel'

        );

        $s = shortcode_atts($option, $atts);

        $args = array(
            'post_type' => $this->post_type,
            'order' => $s['order'],
            'orderby' => $s['orderby'],
            'posts_per_page' => $s['item'],
        );

        $q = new WP_Query($args);

        if ($q->have_posts()): ?>

            <?php if ($s['style'] == 'carousel'): ?>

                <div id="lms-course-carousel" class="owl-carousel">

                    <?php while ($q->have_posts()): $q->the_post(); ?>

                        <div class="owl-item course-item-carousel wow zoomIn">

                            <article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>

                                <div class="learnpress-course-item shadow-box">
                                    <?php do_action('learn_press_before_course_header'); ?>
                                    <div class="learnpress-course-thumbnail">
                                        <a href="<?php echo esc_url(get_permalink()); ?>"> <?php the_post_thumbnail('large'); ?> </a>
                                        <?php learn_press_course_price(); ?>
                                    </div>
                                    <div class="learnpress-course-content">
                                        <?php
                                        do_action('learn_press_before_the_title');
                                        the_title(sprintf('<h3 class="course-title"><a href="%s" rel="bookmark">', esc_url(get_permalink())), '</a></h3>');
                                        //do_action( 'learn_press_after_the_title' );
                                        ?>
                                        <!-- .entry-header -->
                                        <?php do_action('learn_press_before_course_content'); ?>
                                        <div class="learnpress-course-meta">
                                            <?php learn_press_course_instructor(); ?>
                                            <i class="fa fa-group"></i>
                                            <?php learn_press_course_students(); ?>
                                        </div>
                                        <div class="learnpress-course-description">
                                            <?php
                                            do_action('learn_press_before_the_content');
                                            the_excerpt();
                                            do_action('learn_press_after_the_content');
                                            ?>
                                        </div>
                                        <div class="learnpress-course-readmore">
                                            <a href="<?php echo esc_url(get_permalink()); ?>"
                                               class="btn btn-special"><?php esc_html_e('Read More', 'academia'); ?></a>
                                        </div>

                                    </div>

                                </div>

                            </article>

                        </div>


                    <?php endwhile;
                    wp_reset_postdata(); ?>
                </div>
            <?php endif; ?>


            <?php if ($s['style'] == 'grid'): ?>

                <?php $course_counter = 0; ?>

                <div class="row">

                    <?php while ($q->have_posts()): $q->the_post(); ?>

                        <?php ++$course_counter; ?>


                        <div class="col-sm-6 col-md-4">
                            <article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>

                                <div class="learnpress-course-item shadow-box">
                                    <?php do_action( 'learn_press_before_course_header' ); ?>
                                    <div class="learnpress-course-thumbnail">
                                        <a href="<?php echo esc_url( get_permalink() ); ?>"> <?php the_post_thumbnail('large'); ?> </a>
                                        <?php learn_press_course_price(); ?>
                                    </div>
                                    <div class="learnpress-course-content">
                                        <?php
                                        do_action( 'learn_press_before_the_title' );
                                        the_title( sprintf( '<h3 class="course-title"><a href="%s" rel="bookmark">', esc_url( get_permalink() ) ), '</a></h3>' );
                                        //do_action( 'learn_press_after_the_title' );
                                        ?>
                                        <!-- .entry-header -->
                                        <?php do_action( 'learn_press_before_course_content' ); ?>
                                        <div class="learnpress-course-meta">
                                            <?php learn_press_course_instructor(); ?>
                                            <i class="fa fa-group"></i>
                                            <?php learn_press_course_students(); ?>
                                        </div>
                                        <div class="learnpress-course-description">
                                            <?php
                                            do_action( 'learn_press_before_the_content' );
                                            the_excerpt();
                                            do_action( 'learn_press_after_the_content' );
                                            ?>
                                        </div>
                                        <div class="learnpress-course-readmore">
                                            <a href="<?php echo esc_url( get_permalink() ); ?>" class="btn btn-special"><?php esc_html_e( 'Read More', 'academia' ); ?></a>
                                        </div>

                                    </div>

                                </div>

                            </article>
                        </div>


                        <?php
                        if ($course_counter % 3 == 0) {
                            echo '</div><div class="row">';
                        }
                    endwhile;
                    wp_reset_postdata(); ?>
                </div>
            <?php endif; ?>


        <?php else: ?>

            <p> <?php _e('Sorry no post found.', 'academia'); ?> </p>

        <?php endif;
        else:
            _e('Please Activate LearPress Plugin.', 'academia');
        endif;

        $output = ob_get_clean();

        return $output;

    }


}