<?php



$academia_counter_mapper = array(
    "name" => __("Counter", 'academia'),
    "description" => __("Counter.", 'academia'),
    "base" => "academia-counter",
    "class" => "",
    "controls" => "full",
    "icon" => $icon,
    "category" => $category,
    "params" => array(

        array(
            "type" => "textfield",
            "class" => "",
            "heading" => __( "ID", 'academia' ),
            "param_name" => "id",
            "value" => uniqid('vc_counter'), //Default Red color
            "description" => __( "Please Change ID if you have copied the extension", 'academia' )
        ),
        array(
            "type" => "textfield",
            "class" => "",
            "heading" => __( "Class", 'academia' ),
            "param_name" => "class",
            "value" => '', //Default Red color
            "description" => __( "Implement your css through extra class", 'academia' )
        ),
        array(
            'type' => 'css_editor',
            'heading' => __( 'CSS', 'academia' ),
            'param_name' => 'css',
            'group' => __( 'Design options', 'academia' ),
        ),
        array(
            "type" => "colorpicker",
            "class" => "",
            "heading" => __( "Number Color", 'academia' ),
            "param_name" => "color",
            "value" => '#6AB42F', //Default Red color
            "description" => __( "Choose Number color", 'academia' )
        ),

        array(
            "type" => "textfield",
            "class" => "",
            "heading" => __( "Font Size", 'academia' ),
            "param_name" => "font",
            "value" => '36', //Default Red color
            "description" => __( "Font Size in px. (do not include px)", 'academia' ),
        ),

        array(
            'type' => 'textarea_html',
            'holder' => 'div',
            'class' => '',
            'heading' => __( 'Text', 'academia' ),
            'param_name' => 'content',
            'value' => __( "Lorem ipsum dolor sit ameted, consectetur adipisicing elit.  ", 'academia' ),
            'description' => __( 'Enter your content.', 'academia' ),
        ),

        array(
            "type" => "textfield",
            "class" => "",
            "heading" => __( "Unit", 'academia' ),
            "param_name" => "unit",
            "value" => '+', //Default Red color
            "description" => __( "Unit Ex:(Kg, Miles, +)", 'academia' ),
        ),

        array(
            "type" => "textfield",
            "class" => "",
            "heading" => __( "Counter Start Form", 'academia' ),
            "param_name" => "start_form",
            "value" => '0', //Default Red color
            "description" => __( "Must Be a number", 'academia' ),
        ),
        array(
            "type" => "textfield",
            "class" => "",
            "heading" => __( "Counter End To", 'academia' ),
            "param_name" => "end_to",
            "value" => '100', //Default Red color
            "description" => __( "Must Be a number", 'academia' ),

        ),
        array(
            "type" => "textfield",
            "class" => "",
            "heading" => __( "Animation Speed", 'academia' ),
            "param_name" => "animation_speed",
            "value" => '1000', //Default Red color
            "description" => __( "Must Be a number", 'academia' ),

        ),


    )
);

