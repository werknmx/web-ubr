<?php


$academia_button_mapper = array(
    'name' => __('Button', 'academia'),
    'description' => __('Button collection.', 'academia'),
    'base' => 'ac-button',
    'class' => '',
    'controls' => 'full',
    'icon' => $icon,
    'category' => $category,
    'params' => array(

        /**
         * Button text
         */
        array(
            'type' => 'textfield',
            'holder' => 'div',
            'class' => '',
            'heading' => __('Button Text', 'academia'),
            'param_name' => 'button-text',
            'value' => __('', 'academia'),
            'save_always' => true,
            'description' => __('Write button text', 'academia')
        ),
        /**
         * Button Link
         */
        array(
            'type' => "textfield",
            'holder' => "div",
            'class' => "",
            'heading' => __("Button link", "mebel"),
            'param_name' => "ac-button-link",
            'value' => __("", "mebel"),
            'description' => __("Write your button link", "mebel")
        ),
        /**
         * Button link target
         */
        array(
            'type' => 'dropdown',
            'holder' => 'div',
            'class' => '',
            'heading' => __('Button Target', 'academia'),
            'param_name' => 'button-link-target',
            'value' => array(
                'Same Window' => __('', 'academia'),
                'New Window' => __('_blank', 'academia'),
            ),
            'save_always' => true,
            'description' => __('Your button positon', 'academia')
        ),
        /**
         * Button align
         */
        array(
            'type' => 'dropdown',
            'holder' => 'div',
            'class' => '',
            'heading' => __('Button Position', 'academia'),
            'param_name' => 'button-align',
            'value' => array(
                'Left' => __('left', 'academia'),
                'Center' => __('center', 'academia'),
                'Right' => __('right', 'academia'),
            ),
            'save_always' => true,
            'description' => __('Your button positon', 'academia')
        ),

        /**
         * Fullscreen section or not
         */
        array(
            'type' => 'dropdown',
            'holder' => 'div',
            'class' => '',
            'heading' => __('Button type', 'academia'),
            'param_name' => 'button-type',
            'value' => array(
                __('Default', 'academia') => 'ac-default-btn',
                __('Gradient', 'academia') => 'ac-grd-btn',
                __('Large', 'academia') => 'ac-large-btn',
                __('Medium', 'academia') => 'ac-medium-btn',
                __('Border Radius Medium', 'academia') => 'ac-radius-md-btn',
                __('Gradient Medium', 'academia') => 'ac-grd-md-btn',
                __('Small', 'academia') => 'ac-small-btn',
                __('Simple', 'academia') => 'ac-simple-btn',
                __('Double Bordered', 'academia') => 'ac-double-border-btn',
                __('Disable', 'academia') => 'ac-disable-btn',
            ),
            'save_always' => true,
            'description' => __('Choose button styling', 'academia')
        ),
        array(
            'type' => 'dropdown',
            'holder' => 'div',
            'class' => '',
            'heading' => __('Font Weight', 'academia'),
            'param_name' => 'button-font-weight',
            'value' => array(
                'Normal' => __('normal', 'academia'),
                'Light' => __('300', 'academia'),
                'Bold' => __('bold', 'academia'),
            ),
            'save_always' => true,
            'description' => __('Choose button text font weight', 'academia')
        ),


    )
);




