<?php


$academia_events_mapper = array(
    "name" => __("Upcoming Events", 'academia'),
    "description" => __("Display upcoming events.", 'academia'),
    "base" => "academia-upcoming-events",
    "class" => "",
    "controls" => "full",
    "icon" => $icon,
    "category" => $category,
    "params" => array(
        array(
            "type" => "textfield",
            "holder" => "div",
            "class" => "",
            "heading" => __("Event Count", 'academia'),
            "param_name" => "item",
            "value" => 3,
            "description" => __("Total events to display. For displaying all input -1. Default is 3.", 'academia'),

        )
    )
);
