<?php

$academia_infobox_vertical_mapper = array(
    "name"				=> __("Info Box Vertical", 'academia'),
    'description'		=> __('Vertical - Image, Title, Sub-title, Content', 'academia'),
    "base"				=> "info-box-vertical",
    "class"				=> "",
    "icon"              => $icon,
    "category"          => $category,
    "params"	=> array(
        /**
         * Section Background
         */
        array(
            'type'			=> "colorpicker",
            'holder'		=> "div",
            'class'			=> "",
            'heading'		=> __("Section  Background", 'academia'),
            'param_name'	=> "section-background",
            'value'			=> __("", 'academia'),
            'description'	=> __("Choose your section background color", 'academia')
        ),

        /**
         * Image uploader
         */
        array(
            'type'			=> "attach_image",
            'holder'		=> "div",
            'class'			=> "",
            'heading'		=> __("Upload your image", 'academia'),
            'param_name'	=> "image",
            'value'			=> __("", 'academia'),
            'description'	=> __("Choose your preferred image", 'academia')
        ),
        /**
         * Attach image size
         */
        array(
            'type'			=> "textfield",
            'holder'		=> "div",
            'class'			=> "",
            'heading'		=> __("Image size", 'academia'),
            'param_name'	=> "image-size",
            'value'			=> __("", 'academia'),
            'description'	=> __("Choose your image size ex. full, large, medium, thumbnail or you can use widthxheight with lower 'x' like this '100x80'.", 'academia')
        ),
        /**
         * align full content
         */
        array(
            'type'		=> 'dropdown',
            'holder'	=> 'div',
            'class'		=> '',
            'heading'		=> __('content alignment', 'academia'),
            'param_name'	=> 'content-align',
            'value'			=> array(
                'Left'          => __('left', 'academia'),
                'Right'         => __('right', 'academia'),
                'Center'        => __('center', 'academia'),
            ),
            'save_always'   => true,
            'description'	=> __('Choose content alignment', 'academia')
        ),


        /**
         * Enable or disable content
         * return true or false
         */
        array(
            'type'		    => 'checkbox',
            'holder'	    => 'div',
            'class'		    => '',
            'heading'		=> __('Show content', 'academia'),
            'param_name'	=> 'show-content',
            'value'	        => false,
            'save_always'   => true,
            'description'	=> __('Want to show Content', 'academia')
        ),


        /**
         * Title
         */
        array(
            'type'			=> "textfield",
            'holder'		=> "div",
            'class'			=> "",
            'heading'		=> __("Title Text", 'academia'),
            'param_name'	=> "title-text",
            'value'			=> __("", 'academia'),
            'group' => __( 'Title', 'academia' ),
            'dependency'    => array(
                'element'   => 'show-content',
                'not_empty' => true
            ),
            'description'	=> __("Write your title text", 'academia')
        ),
        /**
         * Title color
         */
        array(
            'type'			=> "colorpicker",
            'holder'		=> "div",
            'class'			=> "",
            'heading'		=> __("Text color", 'academia'),
            'param_name'	=> "title-text-color",
            'value'			=> __("", 'academia'),
            'group' => __( 'Title', 'academia' ),
            'dependency'    => array(
                'element'   => 'show-content',
                'not_empty' => true
            ),
            'description'	=> __("Choose your title text color", 'academia')
        ),
        /**
         * Title font size
         */
        array(
            'type'			=> "textfield",
            'holder'		=> "div",
            'class'			=> "",
            'heading'		=> __("Title Text Size", 'academia'),
            'param_name'	=> "title-text-size",
            'value'			=> __("", 'academia'),
            'group' => __( 'Title', 'academia' ),
            'dependency'    => array(
                'element'   => 'show-content',
                'not_empty' => true
            ),
            'description'	=> __("Choose your title font size in px ex. - 14px", 'academia')
        ),
        /**
         * Title margin
         */
        array(
            'type'			=> "textfield",
            'holder'		=> "div",
            'class'			=> "",
            'heading'		=> __("Title Margin", 'academia'),
            'param_name'	=> "title-margin",
            'value'			=> __("", 'academia'),
            'group' => __( 'Title', 'academia' ),
            'dependency'    => array(
                'element'   => 'show-content',
                'not_empty' => true
            ),
            'description'	=> __("ex 10px 20px -- top-bottom & left-right, not use comma", 'academia'),
        ),
        /**
         * Title padding
         */
        array(
            'type'			=> "textfield",
            'holder'		=> "div",
            'class'			=> "",
            'heading'		=> __("Title Padding", 'academia'),
            'param_name'	=> "title-padding",
            'value'			=> __("", 'academia'),
            'group' => __( 'Title', 'academia' ),
            'dependency'    => array(
                'element'   => 'show-content',
                'not_empty' => true
            ),
            'description'	=> __("ex 10px 20px -- top-bottom & left-right, not use comma", 'academia'),

        ),


        /**
         * Subtitle
         */
        array(
            'type'			=> "textfield",
            'holder'		=> "div",
            'class'			=> "",
            'heading'		=> __("Subtitle Text", 'academia'),
            'param_name'	=> "subtitle-text",
            'value'			=> __("", 'academia'),
            'group' => __( 'Subtitle', 'academia' ),
            'dependency'    => array(
                'element'   => 'show-content',
                'not_empty' => true
            ),
            'description'	=> __("Write your subtitle text", 'academia')
        ),
        /**
         * Subtitle color
         */
        array(
            'type'			=> "colorpicker",
            'holder'		=> "div",
            'class'			=> "",
            'heading'		=> __("Subtitle color", 'academia'),
            'param_name'	=> "subtitle-text-color",
            'value'			=> __("", 'academia'),
            'group' => __( 'Subtitle', 'academia' ),
            'dependency'    => array(
                'element'   => 'show-content',
                'not_empty' => true
            ),
            'description'	=> __("Choose your subtitle text color", 'academia')
        ),
        /**
         * Subtitle font size
         */
        array(
            'type'			=> "textfield",
            'holder'		=> "div",
            'class'			=> "",
            'heading'		=> __("Subtitle font size", 'academia'),
            'param_name'	=> "subtitle-text-size",
            'value'			=> __("", 'academia'),
            'group' => __( 'Subtitle', 'academia' ),
            'dependency'    => array(
                'element'   => 'show-content',
                'not_empty' => true
            ),
            'description'	=> __("Choose your subtitle font size", 'academia')
        ),
        /**
         * Subtitle font weight
         */
        array(
            'type'			=> "textfield",
            'holder'		=> "div",
            'class'			=> "",
            'heading'		=> __("Subtitle font weight", 'academia'),
            'param_name'	=> "subtitle-font-weight",
            'value'			=> __("", 'academia'),
            'group' => __( 'Subtitle', 'academia' ),
            'dependency'    => array(
                'element'   => 'show-content',
                'not_empty' => true
            ),
            'description'	=> __("Write your subtitle font weight ex 300, 400, 700", 'academia')
        ),
        /**
         * Subtitle letter spacing
         */
        array(
            'type'			=> "textfield",
            'holder'		=> "div",
            'class'			=> "",
            'heading'		=> __("Subtitle letter spacing", 'academia'),
            'param_name'	=> "subtitle-letter-spacing",
            'value'			=> __("", 'academia'),
            'group' => __( 'Subtitle', 'academia' ),
            'dependency'    => array(
                'element'   => 'show-content',
                'not_empty' => true
            ),
            'description'	=> __("Write your subtitle letter spacing", 'academia')
        ),

        /**
         * SubTitle margin
         */
        array(
            'type'			=> "textfield",
            'holder'		=> "div",
            'class'			=> "",
            'heading'		=> __("Sub Title Margin", 'academia'),
            'param_name'	=> "subtitle-margin",
            'value'			=> __("", 'academia'),
            'group' => __( 'Subtitle', 'academia' ),
            'dependency'    => array(
                'element'   => 'show-content',
                'not_empty' => true
            ),'description'	=> __("ex 10px 20px -- top-bottom & left-right, not use comma", 'academia'),
        ),
        /**
         * SubTitle padding
         */
        array(
            'type'			=> "textfield",
            'holder'		=> "div",
            'class'			=> "",
            'heading'		=> __("Sub Title Padding", 'academia'),
            'param_name'	=> "subtitle-padding",
            'value'			=> __("", 'academia'),
            'group' => __( 'Subtitle', 'academia' ),
            'dependency'    => array(
                'element'   => 'show-content',
                'not_empty' => true
            ),'description'	=> __("ex 10px 20px -- top-bottom & left-right, not use comma", 'academia'),
        ),

        /**
         * Content
         */
        array(
            'type'			=> "textarea",
            'holder'		=> "div",
            'class'			=> "",
            'heading'		=> __("Content", 'academia'),
            'param_name'	=> "content",
            'value'			=> __("", 'academia'),
            'group' => __( 'Content', 'academia' ),
            'dependency'    => array(
                'element'   => 'show-content',
                'not_empty' => true
            ),
            'description'	=> __("Write your content text", 'academia')
        ),
        /**
         * Content text color
         */
        array(
            'type'			=> "colorpicker",
            'holder'		=> "div",
            'class'			=> "",
            'heading'		=> __("Content text color", 'academia'),
            'param_name'	=> "content-color",
            'value'			=> __("", 'academia'),
            'group' => __( 'Content', 'academia' ),
            'dependency'    => array(
                'element'   => 'show-content',
                'not_empty' => true
            ),
            'description'	=> __("Choose your content text color", 'academia')
        ),
        /**
         * Content font size
         */
        array(
            'type'			=> "textfield",
            'holder'		=> "div",
            'class'			=> "",
            'heading'		=> __("Content font size", 'academia'),
            'param_name'	=> "content-text-size",
            'value'			=> __("", 'academia'),
            'group' => __( 'Content', 'academia' ),
            'dependency'    => array(
                'element'   => 'show-content',
                'not_empty' => true
            ),
            'description'	=> __("Choose your Content font size", 'academia')
        ),
        /**
         * Content line-height
         */
        array(
            'type'			=> "textfield",
            'holder'		=> "div",
            'class'			=> "",
            'heading'		=> __("Content line height", 'academia'),
            'param_name'	=> "content-line-height",
            'value'			=> __("", 'academia'),
            'group' => __( 'Content', 'academia' ),
            'dependency'    => array(
                'element'   => 'show-content',
                'not_empty' => true
            ),
            'description'	=> __("Choose your Content line height", 'academia')
        ),
        /**
         * content margin
         */
        array(
            'type'			=> "textfield",
            'holder'		=> "div",
            'class'			=> "",
            'heading'		=> __("Content Margin", 'academia'),
            'param_name'	=> "content-margin",
            'value'			=> __("", 'academia'),
            'group' => __( 'Content', 'academia' ),
            'dependency'    => array(
                'element'   => 'show-content',
                'not_empty' => true
            ),'description'	=> __("ex 10px 20px -- top-bottom & left-right, not use comma", 'academia'),
        ),
        /**
         * content padding
         */
        array(
            'type'			=> "textfield",
            'holder'		=> "div",
            'class'			=> "",
            'heading'		=> __("Content Padding", 'academia'),
            'param_name'	=> "content-padding",
            'value'			=> __("", 'academia'),
            'group' => __( 'Content', 'academia' ),
            'dependency'    => array(
                'element'   => 'show-content',
                'not_empty' => true
            ),'description'	=> __("ex 10px 20px -- top-bottom & left-right, not use comma", 'academia'),
        ),

        /**
         * Checkbox
         * Want to show button
         * true or false
         */
        array(
            'type'		    => 'checkbox',
            'holder'	    => 'div',
            'class'		    => '',
            'heading'		=> __('Show button', 'academia'),
            'param_name'	=> 'button_show',
            'value'	        => false,
            'group' => __( 'Button', 'academia' ),
            'save_always'   => true,
            'description'	=> __('Want to show button or not', 'academia')
        ),
        /**
         * Button text
         */
        array(
            'type'		    => 'textfield',
            'holder'	    => 'div',
            'class'		    => '',
            'heading'		=> __('Button Text', 'academia'),
            'param_name'	=> 'button_text',
            'value'	        => __('', 'academia'),
            'group' => __( 'Button', 'academia' ),
            'save_always'   => true,
            'dependency'    => array(
                'element'   => 'button_show',
                'not_empty' => true
            ),
            'description'	=> __('Choose your button text', 'academia')
        ),
        /**
         * Button type of button class
         */
        array(
            'type'		=> 'dropdown',
            'holder'	=> 'div',
            'class'		=> '',
            'heading'		=> __('Button type', 'academia'),
            'param_name'	=> 'button-type',
            'value'			=> array(
                __('Default', 'academia') => 'ac-default-btn',
                __('Gradient', 'academia') => 'ac-grd-btn',
                __('Large', 'academia') => 'ac-large-btn',
                __('Medium', 'academia') => 'ac-medium-btn',
                __('Border Radius Medium', 'academia') => 'ac-radius-md-btn',
                __('Gradient Medium', 'academia') => 'ac-grd-md-btn',
                __('Small', 'academia') => 'ac-small-btn',
                __('Simple', 'academia') => 'ac-simple-btn',
                __('Disable', 'academia') => 'ac-disable-btn',
            ),
            'group' => __( 'Button', 'academia' ),
            'dependency'    => array(
                'element'   => 'button_show',
                'not_empty' => true
            ),
            'save_always'   => true,
            'description'	=> __('Choose button styling', 'academia')
        ),

        /**
         * Button link
         */
        array(
            'type'		    => 'textfield',
            'holder'	    => 'div',
            'class'		    => '',
            'heading'		=> __('Button Link', 'academia'),
            'param_name'	=> 'button_link',
            'value'	        => __('', 'academia'),
            'group' => __( 'Button', 'academia' ),
            'save_always'   => true,
            'dependency'    => array(
                'element'   => 'button_show',
                'not_empty' => true
            ),
            'description'	=> __('Write your button link', 'academia')
        ),
        /**
         * Button link target
         */
        array(
            'type'		=> 'dropdown',
            'holder'	=> 'div',
            'class'		=> '',
            'heading'		=> __('Button Target', 'academia'),
            'param_name'	=> 'button_link_target',
            'value'			=> array(
                'Same Window'    => __('', 'academia'),
                'New Window'    => __('_blank', 'academia'),
            ),
            'group' => __( 'Button', 'academia' ),
            'save_always'   => true,
            'dependency'    => array(
                'element'   => 'button_show',
                'not_empty' => true
            ),
            'description'	=> __('Your button position', 'academia')
        ),


        /**
         * extra class
         */
        array(
            'type'			=> "textfield",
            'holder'		=> "div",
            'class'			=> "",
            'heading'		=> __("Extra Class", 'academia'),
            'param_name'	=> "class",
            'value'			=> __("", 'academia'),
            'description'	=> __("Write your content text", 'academia')
        ),

        /**
         * Css styleer
         */
        array(
            'type' => 'css_editor',
            'heading' => __( 'CSS', 'academia' ),
            'param_name' => 'css',
            'group' => __( 'Design options', 'academia' ),
        ),

    )
);


