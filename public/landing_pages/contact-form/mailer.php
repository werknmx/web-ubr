<?php

if($_POST) {
  $to = "ubr@werkn.mx"; // your mail here
  $email = filter_var($_POST["email"], FILTER_SANITIZE_EMAIL);
  $name = filter_var($_POST["name"], FILTER_SANITIZE_STRING);
  $phone = filter_var($_POST["phone"], FILTER_SANITIZE_STRING);
  $motive = filter_var($_POST["motive"], FILTER_SANITIZE_STRING);
  $place_event = filter_var($_POST["place_event"], FILTER_SANITIZE_STRING);
  $subject = "Nuevo Petición de Contacto";
  $body = "Nombre: $name\n Teléfono: $phone\n E-mail: $email \n\n\n Motivo: $motive\n\n\n Evento: $place_event";
  
  if(@mail($to, $subject, $body)) {
    $output = json_encode(array('success' => true));
    die($output);
  } else {
    $output = json_encode(array('success' => false));
    die($output);
  }
};

?>