/* FORMULARIO DE CONTACTO */
$(document).ready(function() {

  var form = $('#form'),
      email = $('#email'),
      name = $('#name'),
      phone = $('#phone'),
      motive = $('#motive'),
      place_event = $('#place_event'),
      info = $('#info'),
      submit = $("#submit");
  
  form.on('input', '#email, #name, #phone, #motive, #place_event', function() {
    $(this).css('border-color', '');
    info.html('').slideUp();
  });
  
  submit.on('click', function(e) {
    e.preventDefault();
    if(validate()) {
      $.ajax({
        type: "POST",
        url: "contact-form/mailer.php",
        data: form.serialize(),
        dataType: "json"
      }).done(function(data) {
        if(data.success) {
          email.val('');
          name.val('');
          phone.val('');
          motive.val('');
          info.html('¡Mensaje Enviado! Nos comunicaremos pronto.').css('color', 'green').slideDown();
        } else {
          info.html('¡No se pudo enviar el correo! ¡Intenta de nuevo!').css('color', 'blue').slideDown();
        }
      });
    }
  });
  
  function validate() {
    var valid = true;
    var regex = /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/;
    
    if(!regex.test(email.val())) {
      email.css('border-color', 'red');
      valid = false;
    }
    if($.trim(name.val()) === "") {
      name.css('border-color', 'red');
      valid = false;
    }
    if($.trim(phone.val()) === "") {
      phone.css('border-color', 'red');
      valid = false;
    }
    if($.trim(motive.val()) === "") {
      motive.css('border-color', 'red');
      valid = false;
    }
    
    return valid;
  }

});
