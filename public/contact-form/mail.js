/* FORMULARIO DE CONTACTO */
$(document).ready(function() {

  var form = $('#form'),
      email = $('#email'),
      name = $('#name'),
      phone = $('#phone'),
      info = $('#info'),
      subject = $('#subject'),
      submit = $("#submit");
  
  form.on('input', '#email, #name, #phone', function() {
    $(this).css('border-color', '');
    info.html('').slideUp();
  });
  
  submit.on('click', function(e) {
    e.preventDefault();
    if(validate()) {
      $.ajax({
        type: "POST",
        url: "contact-form/mailer.php",
        data: form.serialize(),
        dataType: "json"
      }).done(function(data) {
        if(data.success) {
          email.val('');
          name.val('');
          phone.val('');
          info.html('¡Mensaje Enviado! Nos comunicaremos pronto.').css('color', 'green').slideDown();
        } else {
          info.html('¡No se pudo enviar el correo! ¡Intenta de nuevo!').css('color', 'green').slideDown();
        }
      });
    }
  });
  
  function validate() {
    var valid = true;
    var regex = /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/;
    
    if(!regex.test(email.val())) {
      email.css('border-color', 'red');
      valid = false;
    }
    if($.trim(name.val()) === "") {
      name.css('border-color', 'red');
      valid = false;
    }
    if($.trim(phone.val()) === "") {
      phone.css('border-color', 'red');
      valid = false;
    }
    
    return valid;
  }

});
